package br.com.gestorflex.pergunta;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Resposta;
import br.com.gestorflex.model.RespostaPergunta;

public class PerguntaOption implements PerguntaInterface {

    private List<RadioButton> opcoes;
    private TextView descricaoDaPergunta;

    @Override
    public View criarPerguntaView(Activity activity, Pergunta pergunta, List<Resposta> respostas, String respostaUsuario, Context context) {
        View view = activity.getLayoutInflater().inflate(R.layout.objetiva_layout, null);
        descricaoDaPergunta = (TextView) view.findViewById(R.id.pergunta_objetiva_descricao);
        descricaoDaPergunta.setText(pergunta.getPegunta());
        descricaoDaPergunta.setTag(String.valueOf(pergunta.getId()));
        RadioGroup radioGroup = view.findViewById(R.id.pergunta_objetiva_radio_group);

        radioGroup.removeAllViews();


        int dp = getPixelValueFromDp(context, 4);
        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, dp, 0, dp);

        opcoes = new ArrayList<>();
        for (Resposta resposta : respostas) {
            RadioButton opcao = new RadioButton(activity);

            opcao.setTag(String.valueOf(resposta.getId()));
            opcao.setText(resposta.getResposta());
            opcao.setBackgroundResource(R.drawable.radio_flat_selector);
            opcao.setLayoutParams(params);

            opcao.setPadding(0, dp, dp, dp);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                opcao.setElevation(4);
            }
            //opcao.setButtonDrawable(R.color.colorTransparente);

            opcoes.add(opcao);
            radioGroup.addView(opcao);
        }

        if (respostaUsuario == null) {
            opcoes.get(0).setChecked(true);
        } else {
            for (RadioButton radioButton : opcoes) {
                if (radioButton.getText().equals(respostaUsuario) && respostaUsuario != null)
                    radioButton.setChecked(true);
            }
        }
        return view;
    }


    public static int getPixelValueFromDp(Context context, int valorDp) {
        float densidade = context.getResources().getDisplayMetrics().density;
        return (int) (valorDp * densidade);
    }

    @Override
    public RespostaPergunta getRespostaDoUsuario() {
        RespostaPergunta respostaUsuario = new RespostaPergunta();
        for (RadioButton opcao : opcoes) {
            if (opcao.isChecked()) {
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String dataAtual = formato.format(Calendar.getInstance().getTime());
                respostaUsuario.setId(Integer.parseInt(opcao.getTag().toString()));
                respostaUsuario.setIdPergunta(Integer.parseInt(descricaoDaPergunta.getTag().toString()));
                respostaUsuario.setDtAtual(dataAtual);
                respostaUsuario.setDescricaoResposta(opcao.getText().toString());
                break;
            }
        }
        return respostaUsuario;
    }

    @Override
    public int pegaNumDeVendas() {
        return 1;
    }

}
