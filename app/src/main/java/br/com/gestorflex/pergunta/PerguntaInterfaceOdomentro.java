package br.com.gestorflex.pergunta;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import java.util.List;

import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Resposta;
import br.com.gestorflex.model.RespostaPerguntaOdomentro;

public interface PerguntaInterfaceOdomentro {
    public View criarPerguntaView(Activity activity, Pergunta pergunta, List<Resposta> respostas, String respostaUsuario, Context context);
    public RespostaPerguntaOdomentro getRespostaDoUsuario() throws Exception;
    public int pegaNumDeVendas() throws Exception;

}
