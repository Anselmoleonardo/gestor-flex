package br.com.gestorflex.pergunta;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Resposta;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.ui.activity.QuestionarioPesquisaActivity;
import br.com.gestorflex.ui.activity.QuestionarioPesquisaOdomentroActivity;
import br.com.gestorflex.util.CPF;

public class PerguntaSubjetiva implements PerguntaInterface {

    private static final int PERGUNTA_SUBJETIVA_NUMERICA = 3;
    private static final int PERGUNTA_SUBJETIVA_NUMERICA4 = 4;
    private static final int PERGUNTA_SUBJETIVA_TEXTUAL = 2;
    private static final int NUMERO_MINIMO_DE_DIGITOS = 1;
    private static final int NUEMERO_MINIMO_DE_CARACTERES = 3;
    private EditText editText;
    private int tipoPergunta;
    private TextView descricaoDaPergunta;
    private Activity act = null;
    String textoResposta = "";
    String validaNtc = "";
    int validacao = 0;
    private Context context;

    @Override
    public View criarPerguntaView(Activity activity, Pergunta pergunta, List<Resposta> respostas, String respostaUsuario, Context context) {
        View view = activity.getLayoutInflater().inflate(R.layout.subjetiva_layout, null);
        descricaoDaPergunta = view.findViewById(R.id.pergunta_subjetiva_descricao);
        descricaoDaPergunta.setTag(String.valueOf(pergunta.getId()));
        descricaoDaPergunta.setText(pergunta.getPegunta());
        this.act = activity;
        this.context = context;
        editText = view.findViewById(R.id.pergunta_subjetiva_edit_text);
        tipoPergunta = pergunta.getTipo();
        if (tipoPergunta == PERGUNTA_SUBJETIVA_NUMERICA || tipoPergunta == PERGUNTA_SUBJETIVA_NUMERICA4) {
            editText.setInputType(android.text.InputType.TYPE_CLASS_NUMBER);
        }
        editText.setTag(respostas.get(0).getId());
        if (respostaUsuario != null) {
            editText.setText(respostaUsuario);
        }
        return view;
    }

    @Override
    public RespostaPergunta getRespostaDoUsuario() throws Exception {
        RespostaPergunta respostaUsuario = new RespostaPergunta();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dataAtual = formato.format(Calendar.getInstance().getTime());
        respostaUsuario.setDtAtual(dataAtual);
        textoResposta = "";

        if (tipoPergunta == PERGUNTA_SUBJETIVA_NUMERICA4) {
            textoResposta = editText.getText().toString();
        } else if (tipoPergunta == PERGUNTA_SUBJETIVA_NUMERICA) {
            textoResposta = editText.getText().toString();
            if (Integer.parseInt(textoResposta) > 0) {
                List<TextInputLayout> listText = act instanceof QuestionarioPesquisaActivity ? ((QuestionarioPesquisaActivity) act).dialagAdapater.getList() : ((QuestionarioPesquisaOdomentroActivity) act).dialagAdapater.getList();
                if (act instanceof QuestionarioPesquisaActivity) {
                    validacao = ((QuestionarioPesquisaActivity) act).dialagAdapater.getValidacao();
                }
                Log.e("validacao ", validacao + "");
                for (int i = 0; i < listText.size(); i++) {
                    if (validacao == 1) {
                        if (listText.get(i).getEditText().getText().toString().trim().length() == 11) {
                            if (CPF.isValid(listText.get(i).getEditText().getText().toString().trim())) {
                                if (listText.size() - 1 == i) {
                                    textoResposta = textoResposta + listText.get(i).getEditText().getText();
                                } else {
                                    textoResposta = textoResposta + listText.get(i).getEditText().getText() + "|";
                                }
                            } else {
                                listText.clear();
                                throw new Exception("FAVOR, Informar um CPF válido!");
                            }
                        } else {
                            listText.clear();
                            throw new Exception("FAVOR, Informar corretamente o CPF com 11 dígitos");
                        }
                    } else if (validacao == 2) {
                        if (listText.get(i).getEditText().getText().toString().trim().length() > 5) {
                            validaNtc = (listText.get(i).getEditText().getText().toString().trim().substring(2, 3));

                            if (validaNtc.equals("9")) {
                                if (listText.size() - 1 == i) {
                                    textoResposta = textoResposta + listText.get(i).getEditText().getText();

                                } else {
                                    textoResposta = textoResposta + listText.get(i).getEditText().getText() + "|";
                                }
                            } else {
                                listText.clear();
                                throw new Exception("FAVOR, Informar o NTC corretamente!");
                            }
                        } else {
                            listText.clear();
                            throw new Exception("FAVOR, Informar o NTC incluindo DDD!");
                        }
                    } else {
                        if (listText.size() - 1 == i) {
                            textoResposta = textoResposta + listText.get(i).getEditText().getText();

                        } else {
                            textoResposta = textoResposta + listText.get(i).getEditText().getText() + "|";
                        }
                    }
                }

            }
        }
        respostaUsuario.setDescricaoResposta(textoResposta);
        String idResposta = editText.getTag().toString();
        respostaUsuario.setId(Integer.parseInt(idResposta));
        respostaUsuario.setIdPergunta(Integer.parseInt(descricaoDaPergunta.getTag().toString()));

        if (textoResposta.length() == 0)
            throw new Exception("Você não pode deixar este campo vazio!");
        return respostaUsuario;
    }

    @Override
    public int pegaNumDeVendas() throws Exception {
        String textoResposta = editText.getText().toString();
        if (textoResposta.length() == 0)
            throw new Exception("Você não pode deixar este campo vazio!");
        return Integer.parseInt(textoResposta);
    }


}

