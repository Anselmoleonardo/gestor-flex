package br.com.gestorflex.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.net.ConnectivityManager;
import androidx.appcompat.app.AlertDialog;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Aparelho;
import br.com.gestorflex.model.Usuario;

public class ErrorUtil {

    // VARIÁVEIS
    private static final int STACK_TRACE_LEVELS_UP = 3;
    // MENSAGENS
    private static String caminhoErro;

    public static void enviarErroPorEmail(Context context, Exception ex) {
        try {
            String msg = "DADOS DO APARELHO:\n\n"
                    + "COMPRIMENTO: " + Aparelho.getComprimentoTela(context) + "\n"
                    + "ALTURA: " + Aparelho.getAlturaTela(context) + "\n"
                    + "MODELO: " + Aparelho.getModelo() + "\n"
                    + "API: " + Aparelho.getApi() + "\n"
                    + "ICCID: " + Aparelho.getIccid(context) + "\n"
                    + "IMEI: " + Aparelho.getImei(context) + "\n"
                    + "INTERNET: " + identificarInternet(context)
                    + "\n\n================\n\n"
                    + "CAMINHO DO ERRO:\n\n" + caminhoErro
                    + "\n\n================"
                    + "\n\nMENSAGEM DO ERRO:\n\n"
                    + montarMsgErro(ex);
            EmailUtil.sendExceptionEmail(context, criarAssunto(context), msg);
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso)
                    .setMessage(R.string.message_alert_erro_enviado)
                    .setPositiveButton(R.string.label_alert_button_ok, null)
                    .show();
        } catch (Exception e) {
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso)
                    .setMessage("CAMINHO DO ERRO:\n" + caminhoErro
                            + "\n\nMENSAGEM DO ERRO:\n"
                            + e.getMessage())
                    .setPositiveButton(R.string.label_alert_button_ok, null)
                    .show();
        }
    }

    public static String montarCaminhoErro() {
        caminhoErro = "";
        String classe = Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getFileName();
        caminhoErro += " -> " + classe.substring(0, classe.length() - 5);
        caminhoErro += " (" + Thread.currentThread().getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName() + ")";
        return caminhoErro;
    }

    private static String montarMsgErro(Exception ex) {
        // SE FOR ERRO
        String msgGlobal;
        ex.printStackTrace();
        String classeErro = "CLASSE DO ERRO:\n\n" + ex.getStackTrace()[STACK_TRACE_LEVELS_UP].getClassName();
        String metodoErro = "MÉTODO DO ERRO:\n\n" + ex.getStackTrace()[STACK_TRACE_LEVELS_UP].getMethodName();
        String linhaErro = "LINHA DO ERRO:\n\n" + ex.getStackTrace()[STACK_TRACE_LEVELS_UP].getLineNumber();
        msgGlobal = ex.getMessage() + "\n\n" + classeErro + "\n\n" + metodoErro + "\n\n" + linhaErro;
        msgGlobal += "\n\n";
        for (int i = 0; i < (ex.getStackTrace().length >= 20 ? 20 : ex.getStackTrace().length); i++) {
            if (ex.getStackTrace()[i].getClassName().contains("br.com.")) {
                msgGlobal += ex.getStackTrace()[i] + "\n\n";

            }
        }
        return msgGlobal;
    }

    private static String criarAssunto(Context context) {
        Usuario usuario = UsuarioCTRL.getUsuario(context);
        String email = usuario.getEmail();
        if (email == null || email.isEmpty()) {
            Account[] accounts = AccountManager.get(context).getAccounts();
            email = (accounts != null && accounts.length > 0) ? accounts[0].name : "";
        }
        String data = DateUtil.getCurrentDate();
        String assunto = "[" + context.getApplicationInfo().loadLabel(context.getPackageManager()) + " v" + Aparelho.getVersao() + "] "
                + usuario.getNomeRegional() + " Matrícula Usuário: " + usuario.getMatricula() + " Email: " + email + " Data: " + data;
        return assunto;
    }

    private static String identificarInternet(Context context) {
        String internet = "";
        final ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            internet = "WIFI";
        } else if (mobile.isConnectedOrConnecting()) {
            internet = "REDE DE DADOS";
        }
        return internet;
    }
}
