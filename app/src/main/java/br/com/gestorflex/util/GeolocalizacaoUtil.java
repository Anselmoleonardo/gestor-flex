package br.com.gestorflex.util;

/**
 * Created by Frann on 01/04/2019.
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Locale;

import br.com.gestorflex.model.Geolocalizacao;

public final class GeolocalizacaoUtil implements LocationListener {
    // CLASSES
    private Location location; // location
    // Declaração do Location Manager
    protected LocationManager locationManager;

    // VARIÁVEIS
    private final Context context;
    // flag para o status do GPS
    private boolean isGPSEnabled = false;
    // flag para o status da rede (Network)
    private boolean isNetworkEnabled = false;
    // flag para saber se é possível obter a localizacao
    private boolean canGetLocation = false;
    // A distância mínima para mudar Atualizações em metros
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // O tempo mínimo de entre as atualizações em milissegundos
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10 * 1; // 10 seconds

    private static GeolocalizacaoUtil INSTANCE;

    // construtor para uso na activity
    public GeolocalizacaoUtil(Context context) {
        this.context = context;
    }

    public static synchronized GeolocalizacaoUtil getInstance(Context context) {
        if (INSTANCE == null)
            INSTANCE = new GeolocalizacaoUtil(context.getApplicationContext());
        return INSTANCE;
    }

    public static boolean verificarGeolocalizacao(Context context) {
        GeolocalizacaoUtil geolocalizacaoUtil = new GeolocalizacaoUtil(context);
        if (!geolocalizacaoUtil.locationEnable()) {
            showSettingsAlert(context);
            return false;
        } else
            geolocalizacaoUtil.getGeolocalizacao();
        if (Geolocalizacao.getLatitude() == 0)
            return false;
        return true;
    }

    public boolean locationEnable() {
        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        // Obtendo o status do GPS
        locationManager.getProviders(true);

        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        canGetLocation = !(isGPSEnabled == false || isNetworkEnabled == false);

        return canGetLocation;
    }

    public Location getProviderLocation(String provider) {
        if (locationManager != null) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location == null)
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if (location == null)
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
        }
        return location;
    }

    public static String getModoDePrecisaoGps(Context context) {
        ErrorUtil.montarCaminhoErro();
        try {
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            String retornoMode = "";
            if (mode == 0) {
                retornoMode = "Precisão : DESLIGADO";
            } else if (mode == 1) {
                // LOCATION_MODE_SENSORS_ONLY
                retornoMode = "Precisão : BAIXA";
            } else if (mode == 2) {
                // LOCATION_MODE_BATTERY_SAVING
                retornoMode = "Precisão : MÉDIA";
            } else if (mode == 3) {
                retornoMode = "Precisão : ALTA";
            }
            return retornoMode;

        } catch (Exception ex) {
            ErrorUtil.enviarErroPorEmail(context, ex);
            return "Modo de precisão não encontrado";
        }
    }


    public String getBestProvider() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        criteria.setAltitudeRequired(false);
        criteria.setSpeedRequired(true);
        criteria.setCostAllowed(true);
        criteria.setBearingRequired(false);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        String provider = locationManager.getBestProvider(criteria, false);
        return provider;
    }

    public void getGeolocalizacao() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
            } else {
                localizar();
            }
        } else {
            localizar();
        }
    }

    public static boolean verificarInternet(Context context, boolean isExibirMsg) throws Exception {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        if (isExibirMsg) {
            new androidx.appcompat.app.AlertDialog.Builder(context)
                    .setTitle("Aviso")
                    .setMessage("Ative sua conexão com a internet!")
                    .setPositiveButton("OK", null)
                    .show();
        }
        return false;
    }

    public static void pararAtualizacoes(Context context) {
        getInstance(context).stopUsingGPS();
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app
     */


    public static Boolean verificarGPSHabilitado(Context context) {
        GeolocalizacaoUtil geolocalizacaoUtil = new GeolocalizacaoUtil(context);
        if (!geolocalizacaoUtil.locationEnable()) {
            geolocalizacaoUtil.showSettingsAlert(context);
            return false;
        } else {
            geolocalizacaoUtil.getGeolocalizacao();
            return true;
        }
    }


    private void localizar() {
        ErrorUtil.montarCaminhoErro();
        try {
            locationManager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
            if (locationEnable()) {
                if (locationManager.isProviderEnabled(getBestProvider())) {
                    location = getProviderLocation(getBestProvider());
                    if (location != null) {
                        Geolocalizacao.setLatitude(location.getLatitude());
                        Geolocalizacao.setLongitude(location.getLongitude());
                    } else {
                        Geolocalizacao.setLatitude(0);
                        Geolocalizacao.setLongitude(0);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(context, ex);
        } finally {
            locationManager.removeUpdates(GeolocalizacaoUtil.this);
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
        }
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GeolocalizacaoUtil.this);
        }
    }

    /**
     * Function to show settings alert dialog On pressing Settings button will
     * lauch Settings Options
     */
    public static void showSettingsAlert(final Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle("AVISO");
        // Setting Dialog Message
        alertDialog
                .setMessage("Sua GEOLOCALIZAÇÃO está desativada!");
        // On pressing Settings button
        alertDialog.setPositiveButton("Configurar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    }
                });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public static String coletarEndereco(Context context, LatLng latLng) {
        String endereco = "";
        try {
//            if (!HttpManager.verificarInternet(context, false))
//                return "Endereço não encontrado";

            Geocoder geo = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.isEmpty()) {
                endereco = "Endereço não encontrado";
            } else {
                if (addresses.size() > 0) {
                    String end = addresses.get(0).getAddressLine(0);
                    String[] parts = end.split(",");
                    endereco = parts[0] + "\n" + parts[1];
                }
            }
        } catch (Exception e) {
            endereco = "Endereço não encontrado";
        }

        return endereco;
    }

    public static boolean isLocalizacaoFicticia(Context context) {
//        if (Build.VERSION.SDK_INT >= 23) {
//            int    adb  = Settings.Secure.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED , 0);
//            String mock = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION);
//            return (adb == 1 || mock.equals("1")) ? true : false;
//        }else{
//            String mock = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ALLOW_MOCK_LOCATION);
//            return (mock.equals("1")) ? true : false;
//        }
        return false;

    }
}