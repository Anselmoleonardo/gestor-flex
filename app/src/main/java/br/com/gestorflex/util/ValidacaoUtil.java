package br.com.gestorflex.util;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.material.textfield.TextInputLayout;

import br.com.gestorflex.R;

public class ValidacaoUtil {

    public static boolean validarCamposVazios(Context context, TextInputLayout _tilText) {
        if (_tilText == null) return false;
        if (_tilText.getEditText().getText().toString().trim().isEmpty()) {
            criarErroCampos(_tilText, String.format(context.getString(R.string.message_error_campo_vazio), _tilText.getHint()));
            return false;
        }
        return true;
    }

    public static boolean validarCamposDataInvalida(Context context, TextInputLayout _tilText) {
        if (_tilText == null) return false;
        if (!DateUtil.validateDate(_tilText.getEditText().getText().toString())) {
            criarErroCampos(_tilText, context.getString(R.string.message_error_data_invalida));
            return false;
        }
        return true;
    }

    public static boolean validarCamposQtdCaracteres(Context context, TextInputLayout _tilText, int quantidade) {
        if (_tilText == null) return false;
        if (_tilText.getEditText().getText().toString().trim().length() < quantidade) {
            criarErroCampos(_tilText, String.format(context.getString(R.string.erro_message_quantiade_caracteres), _tilText.getHint()));
            return false;
        }
        return true;
    }

    private static void criarErroCampos(TextInputLayout _tilText, String mensagem) {
        _tilText.setError(mensagem);
        _tilText.getParent().requestChildFocus(_tilText, _tilText);
        if (_tilText.getEndIconMode() != TextInputLayout.END_ICON_NONE)
            _tilText.setErrorIconDrawable(null);
    }

    public static void removerErroTextInputLayout(TextInputLayout _tilText) {
        if (_tilText == null) return;
        if (_tilText.getError() != null) {
            _tilText.setError(null);
            _tilText.setErrorEnabled(false);
        }
    }

    public static boolean validarSpinners(Context context, AppCompatSpinner _spSpinner, String msgErro) {
        if (_spSpinner == null) return false;
        if (_spSpinner.getSelectedItemPosition() == 0) {
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso).setMessage(msgErro)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        return true;
    }

}
