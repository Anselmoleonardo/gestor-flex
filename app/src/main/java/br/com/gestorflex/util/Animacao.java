package br.com.gestorflex.util;

import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

public class Animacao {

    public static void animarView(final View v, final String property,
                                  final float from, final float to) {

        v.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        v.getViewTreeObserver().removeGlobalOnLayoutListener(
                                this);

                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(
                                v, property, from, to);
                        transAnimation.setDuration(750);
                        transAnimation.start();
                    }
                });
    }

    public static void animarView(final View v, final String property,
                                  final float from, final float to, final int duration) {

        v.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        v.getViewTreeObserver().removeGlobalOnLayoutListener(
                                this);

                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(
                                v, property, from, to);
                        transAnimation.setDuration(duration);
                        transAnimation.start();
                    }
                });
    }

    public static void piscarView(final View v, int durationIn, int durationOut) throws Exception {

        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(durationIn);

        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(durationOut);

        v.startAnimation(in);

        in.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {
                v.startAnimation(out);

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {

            }

            @Override
            public void onAnimationStart(Animation animation) {
            }
        });

        out.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {
                v.startAnimation(in);

            }

            @Override
            public void onAnimationRepeat(Animation arg0) {

            }

            @Override
            public void onAnimationStart(Animation animation) {

            }
        });
    }
}
