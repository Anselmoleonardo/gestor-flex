package br.com.gestorflex.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;

import br.com.gestorflex.BuildConfig;

public class FotoUtil {

    public static String CAMINHO_FOTO;

    public static Uri criarFotoURI_Odometro(Context context) throws IOException {
        File image = null;
        File storageDir = null;
        Uri photoURI = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(BuildConfig.VERSION_NAME
                    , "_foto.jpg", storageDir);
            CAMINHO_FOTO = image.getAbsolutePath();
            photoURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".android.fileprovider", image);
        } else {
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            CAMINHO_FOTO = storageDir.getAbsolutePath() + "/" + BuildConfig.VERSION_NAME + "_foto.jpg";
            image = new File(CAMINHO_FOTO);
            photoURI = Uri.fromFile(image);
        }
        return photoURI;
    }


    public static void exibirFotoOdometro(Context context, ImageView imageView, Bitmap bitmap, CardView cardView) {
        try {
            bitmap = ImageUtil.carregaBitmap(FotoUtil.CAMINHO_FOTO, context);
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
                customizaImagem(imageView, cardView);
            } else {
                FotoUtil.CAMINHO_FOTO = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Não foi possível carregar a foto! \n Tente novamente",
                    Toast.LENGTH_LONG).show();
        }
    }

    private static void customizaImagem(ImageView imageView, CardView cardView) {
        imageView.setAdjustViewBounds(true);
        imageView.setPadding(8, 8, 8, 8);
        cardView.setVisibility(View.VISIBLE);
        cardView.setCardElevation(5);
        cardView.setRadius(8);
        cardView.setUseCompatPadding(true);
    }

    public static int getRotation(File file) throws Exception {
        int rotate = 0;
        ExifInterface exif = new ExifInterface(file.getAbsolutePath());
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
        }
        return rotate;
    }

}

