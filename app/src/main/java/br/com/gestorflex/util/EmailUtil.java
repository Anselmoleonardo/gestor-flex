package br.com.gestorflex.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Authenticator;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import br.com.gestorflex.R;

/**
 * Created by Patrick on 16/01/2017.
 */

public class EmailUtil {

    public static final String SENHA_EMAIL_SUPORTE = "senhaContaSuporte";

    public static void sendExceptionEmail(final Context context, final String assunto, final String erro) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SENHA_EMAIL_SUPORTE, context.MODE_PRIVATE);
        String senhaEmail = sharedPreferences.getString("senhaEmail", "pronto2019");
        final String fromEmail = context.getString(R.string.exception_email_from); //requires valid gmail id
        final String password = senhaEmail; // correct password for gmail id
        final String toEmail = context.getString(R.string.exception_email_to); // can be any email id
        System.out.println("TLSEmail Start");
        Properties props = new Properties();
        props.put("mail.smtp.host", context.getString(R.string.exception_smtp_host_server));//SMTP Host
        props.put("mail.smtp.port", context.getString(R.string.exception_smtp_port)); //TLS Port
        props.put("mail.smtp.auth", "true"); //enable authentication
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
        //create Authenticator object to pass in Session.getInstance argument
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        };
        final Session session = Session.getInstance(props, auth);
        Thread runnable = new Thread() {
            @Override
            public void run() {
                EmailUtil.sendEmail(session, toEmail, assunto, erro);
            }
        };
        runnable.start();
    }

    private static void sendEmail(Session session, String toAddress, String subject, String body) {
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");
            msg.setSubject(subject, "UTF-8");
            msg.setText(body, "UTF-8");
            msg.setSentDate(new Date());
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress, false));
            Transport.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

