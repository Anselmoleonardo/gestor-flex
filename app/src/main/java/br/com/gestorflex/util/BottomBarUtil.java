package br.com.gestorflex.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Aparelho;
import br.com.gestorflex.ui.activity.AutenticacaoActivity;

public class BottomBarUtil {



    public static void configurar(Context context) {
        Activity activity = (Activity) context;
        TextView _tvBottomBarAgencia = activity.findViewById(R.id.tv_bottom_bar_agencia_geral);
        TextView _tvBottomBarData = activity.findViewById(R.id.tv_bottom_bar_data);
        TextView _tvBottomBarVersaoApp = activity.findViewById(R.id.tv_bottom_bar_versao_app);
        TextView _tvBottomBarVersaoAndroid = activity.findViewById(R.id.tv_bottom_bar_versao_android);
        TextView _tvBottomBarIconeAndroid = activity.findViewById(R.id.tv_bottom_bar_icone_android);

        if (!(activity instanceof AutenticacaoActivity)) {
            _tvBottomBarAgencia.setText(UsuarioCTRL.getUsuario(context).getNomeAgencia());
            _tvBottomBarData.setText(UsuarioCTRL.getUsuario(context).getDataBD());
        }
        _tvBottomBarVersaoApp.setText("v" + Aparelho.getVersao());
        _tvBottomBarVersaoAndroid.setText("v" + Aparelho.getAndroidVersao());

        Typeface typeface = ResourcesCompat.getFont(context, R.font.fontawesome);
        _tvBottomBarIconeAndroid.setTypeface(typeface);
    }

}
