package br.com.gestorflex.util;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Aparelho;

public class ImageUtil {

    public static final int REQUEST_PHOTO_CAMERA = 2505;
    public static final int REQUEST_PHOTO_GALLERY = 1208;
    public static final String TEMP_PHOTO_FILE = "foto_temporaria.jpg";
    public static String CURRENT_IMAGE_PATH;

    public static Bitmap carregaBitmap(String caminhoFoto, Context context) throws IOException {
        ExifInterface exif = null;
        exif = new ExifInterface(caminhoFoto);
        String orientacao = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int codigoOrientacao = Integer.parseInt(orientacao);

        switch (codigoOrientacao) {
            case ExifInterface.ORIENTATION_NORMAL:
                return abreFotoERotaciona(caminhoFoto, 0, context);
            case ExifInterface.ORIENTATION_ROTATE_90:
                return abreFotoERotaciona(caminhoFoto, 90, context);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return abreFotoERotaciona(caminhoFoto, 180, context);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return abreFotoERotaciona(caminhoFoto, 270, context);
            default:
                return abreFotoERotaciona(caminhoFoto, 0, context);
        }
    }

    public static String converteBitmapParaBase64(Bitmap image) {
        String imgEmBase64 = "";
        if (image != null) {
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                imgEmBase64 = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return imgEmBase64;
    }


    private static Bitmap abreFotoERotaciona(String caminhoFoto, int angulo, Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        int targetW = metrics.heightPixels;
        int targetH = metrics.widthPixels;

        BitmapFactory.Options mBitmapOptions = new BitmapFactory.Options();
        mBitmapOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(caminhoFoto, mBitmapOptions);

        int photoW = mBitmapOptions.outWidth;
        int photoH = mBitmapOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        mBitmapOptions.inJustDecodeBounds = false;
        mBitmapOptions.inSampleSize = scaleFactor * 2;
        mBitmapOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto, mBitmapOptions);
        Matrix matrix = new Matrix();
        matrix.postRotate(angulo);

        return Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);
    }

    public static Intent criarIntentFotoCamera(Context context) {
        ErrorUtil.montarCaminhoErro();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            File photoFile;
            try {
                photoFile = createImageFile(context);
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(context, context.getString(R.string.file_provider), photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    return takePictureIntent;
                }
            } catch (IOException ex) {
                ErrorUtil.enviarErroPorEmail(context, ex);
            }
        }
        return null;
    }

    public static Bitmap getImagemFromIntent(Context context, Intent data, int requestCode, View view) {
        Bitmap foto = null;
        try {
            File file;
            if (requestCode == ImageUtil.REQUEST_PHOTO_CAMERA)
                file = new File(ImageUtil.CURRENT_IMAGE_PATH);
            else
                file = new File(getCaminho(context, data.getData()));
            if (file.exists()) {
                int rotacao = getRotacao(file);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2; // Reduz em 1/2 o tamanho da imagem
                foto = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                foto = ImageUtil.rotacionarFoto(foto, rotacao); // Corrige orientação da imagem
                if (foto == null)
                    Snackbar.make(view, R.string.message_snackbar_erro_criar_imagem, Snackbar.LENGTH_LONG)
                            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
            } else
                Snackbar.make(view, R.string.message_snackbar_erro_caminho_foto, Snackbar.LENGTH_LONG)
                        .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
        } catch (Exception ex) {
            foto = null;
            Snackbar.make(view, R.string.message_snackbar_erro_criar_imagem, Snackbar.LENGTH_LONG)
                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
        }
        return foto;
    }

    public static File createImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",   /* suffix */
                storageDir      /* directory */
        );
        CURRENT_IMAGE_PATH = image.getAbsolutePath();
        return image;
    }

    public static int getRotacao(File file) {
        int rotate = 0;
        try {
            ExifInterface exif = new ExifInterface(file.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static Bitmap rotacionarFoto(Bitmap bmp, int rotate) {
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    public static String converterBitmapParaBase64(Bitmap image) {
        String imgEmBase64 = "";
        try {
            if (image != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 75, stream);
                imgEmBase64 = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
                stream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imgEmBase64;
    }

    public static void adicionarImagemEmImageView(Context context, AppCompatImageView _ivFoto, String url) {
        if (!url.equals(""))
            Picasso.get().load(url).into(_ivFoto);
        else
            _ivFoto.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_sem_foto, null));
    }

    public static void exibirImagem(Context context, String url) {
        int padding = Aparelho.getPixelValueFromDp(context, 8);
        ImageView imageView = new ImageView(context);
        imageView.setAdjustViewBounds(true);
        imageView.setPadding(0, padding, 0, padding);
        Picasso.get().load(url).into(imageView);
        AlertDialog myDialog = new AlertDialog.Builder(context).setView(imageView).create();
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        myDialog.show();
    }

    public static void exibirImagem(Context context, Bitmap image) {
        if (image != null) {
            int padding = Aparelho.getPixelValueFromDp(context, 8);
            ImageView imageView = new ImageView(context);
            imageView.setAdjustViewBounds(true);
            imageView.setPadding(0, padding, 0, padding);
            imageView.setImageBitmap(image);
            AlertDialog myDialog = new AlertDialog.Builder(context).setView(imageView).create();
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            myDialog.show();
        }
    }

    public static String getCaminho(Context context, Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public static void excluirArquivo(String path) {
        File fileAux = new File(path);
        if (fileAux.exists()) {
            if (fileAux.delete()) {
                Log.e("EXCLUIR ARQUIVO", "Arquivo excluído com sucesso!");
            } else
                Log.e("EXCLUIR ARQUIVO", "Erro ao Excluir!");
        } else
            Log.e("EXCLUIR ARQUIVO", "Arquivo não encontrado!");
    }

    public static Bitmap getCircularBitmapFromUrl(Context context, String url) {
        Bitmap imagem;
        try {
            imagem = Picasso.get().load(url).get();
            imagem = getCircularBitmap(imagem);
        } catch (Exception e) {
            imagem = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_sem_foto);
            e.printStackTrace();
        }
        return imagem;
    }

    private static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;
        if (bitmap.getWidth() > bitmap.getHeight())
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        else
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        float r;
        if (bitmap.getWidth() > bitmap.getHeight())
            r = bitmap.getHeight() / 2;
        else
            r = bitmap.getWidth() / 2;
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    private static File getTempFile() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return file;
        } else {
            return null;
        }
    }

}
