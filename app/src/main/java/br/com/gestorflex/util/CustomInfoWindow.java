package br.com.gestorflex.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import br.com.gestorflex.R;


public class CustomInfoWindow {
    private static HashMap<Marker, View> hmInfoWindow;

    public static void criarInfoWindow(final Context context, GoogleMap mapa) {
        hmInfoWindow = new HashMap<>();

        mapa.setInfoWindowAdapter(new InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                if (marker.getTitle() == null)
                    return null;

                View v = hmInfoWindow.get(marker);
                if (v == null)
                    return exibirInfoWindow(context, marker);
                else
                    return v;
            }
        });
    }

    private static View exibirInfoWindow(Context context, Marker marker) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.info_window, null);

        TextView tvTitulo = (TextView) v.findViewById(R.id.tvTitulo);
        tvTitulo.setText(marker.getTitle());


        TextView tvEndereco = (TextView) v.findViewById(R.id.tvEndereco);
        if (marker.getSnippet()==null)
            tvEndereco.setVisibility(View.GONE);
        else
            tvEndereco.setVisibility(View.VISIBLE);
        tvEndereco.setText(marker.getSnippet());

        hmInfoWindow.put(marker, v);

        return v;
    }

    public static String coletarEndereco(Context context, LatLng latLng) {
        String endereco = "";
        try {
            Geocoder geo = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.isEmpty()) {
                endereco = "Endereço não encontrado";
            } else {
                if (addresses.size() > 0) {
                    String end = addresses.get(0).getAddressLine(0).toString();
                    String[] parts = end.split(",");
                    endereco = parts[0] + "\n" + parts[1];
                }
            }
        } catch (Exception e) {
            endereco = "Endereço não encontrado";
        }

        return endereco;
    }
}