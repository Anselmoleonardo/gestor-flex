package br.com.gestorflex.interfaces;

import android.view.View;

/**
 * Created by Vicente on 07/02/2019.
 */

public interface OnRecyclerItemClickListener {

    int i = 0;

    void onRecyclerItemClickListener(View view, int position, Object item);
}
