package br.com.gestorflex.interfaces;

import android.view.View;

public interface OnItemClickListener {
    void OnItemClickListener(View view, int position, Object item);
}
