package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Odometro;

public class OdometroCTRL {

    public static List<Odometro> listOdometro(Context context) {
        Cursor cursor = null;
        List<Odometro> listaOdometro = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(Odometro.TABELA, null, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listaOdometro.add(new Odometro(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return listaOdometro;
    }

    public static List<Odometro> listOdometroSincronizacao(Context context) {
        Cursor cursor = null;
        List<Odometro> listaOdometro = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(Odometro.TABELA, null, Odometro.CAMPO_IS_SINC + " = ?",
                    new String[]{"1"}, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listaOdometro.add(new Odometro(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return listaOdometro;
    }

}
