package br.com.gestorflex.database.internal.controller;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import br.com.gestorflex.database.internal.DBHelper;

/**
 * Created by Vicente on 29/03/2019.
 */

public class GenericCTRL {

    public static void save(Context context, String tabela, ContentValues valores) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            db.insert(tabela, null, valores);
        } catch (Exception e) {
            Log.e("excessao sqlite", e.getMessage());
        } finally {
            db.close();
        }
    }

    public static void deleteAll(Context context, String tabela) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            db.delete(tabela, null, null);
        } catch (Exception e) {
            Log.e("excessao sqlite", e.getMessage());
        } finally {
            db.close();
        }
    }

    public static int delete(Context context, String tabela, int id) {
        int response = 0;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            String[] parametros = {String.valueOf(id)};
            response = db.delete(tabela, "_id =?", parametros);
        } catch (Exception e) {
            Log.e("excessao sqlite", e.getMessage());
            return 0;
        } finally {
            db.close();
        }
        return response;
    }

    public static void update(Context context, String tabela, int id, ContentValues contentValues) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            db.update(tabela, contentValues, "_id = ?", new String[]{String.valueOf(id)});
        } catch (Exception e) {
            Log.e("excessao sqlite", e.getMessage());
        } finally {
            db.close();
        }
    }

    public static void update(Context context, String tabela, String where, String[] args, ContentValues contentValues) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            db.update(tabela, contentValues, where, args);
        } catch (Exception e) {
            Log.e("excessao sqlite", e.getMessage());
        } finally {
            db.close();
        }
    }

    public static void deleteSharedPreferences(Context context, String key) {
        SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
