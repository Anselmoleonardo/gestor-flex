package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.CategoriaDash;

public class CategoriaDashboardCTRL {

    public static List<CategoriaDash> listCategoriaDash(Context context) {
        Cursor c = null;
        List<CategoriaDash> listCategoriaDash = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            c = db.query(CategoriaDash.TABELA, null, null, null, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                listCategoriaDash = new ArrayList<>();
                do {
                    listCategoriaDash.add(new CategoriaDash(c));
                } while (c.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
        return listCategoriaDash;
    }

}
