package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.PerfilDashboard;

public class PerfilDashboardCTRL {

    public static PerfilDashboard getPerfilDashboard(Context context) {
        Cursor cursor = null;
        PerfilDashboard perfil = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(PerfilDashboard.TABELA, null, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                perfil = new PerfilDashboard(cursor);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return perfil;
    }

}
