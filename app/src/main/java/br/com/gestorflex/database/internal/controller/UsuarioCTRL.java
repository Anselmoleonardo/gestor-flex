package br.com.gestorflex.database.internal.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.database.internal.DBHelper;

/**
 * Created by paulo on 01/04/2019.
 */

public class UsuarioCTRL {

    public static Usuario getUsuario(Context context) {
        Cursor c = null;
        Usuario usuario = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            c = db.query(Usuario.TABELA, null, null, null, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                usuario = new Usuario(c);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
        return usuario;
    }

    public static void updateDataDb(Context context, Usuario usuario, String data) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            ContentValues valores = new ContentValues();
            valores.put(Usuario.CAMPO_DATA_BD, data);
            db.update(Usuario.TABELA, valores, Usuario.CAMPO_ID + " = ?", new String[]{usuario.getId() + ""});
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close();
        }
    }

}
