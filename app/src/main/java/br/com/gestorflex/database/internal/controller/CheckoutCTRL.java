package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Checkout;

public class CheckoutCTRL {

    public static  void salvarCheckout(Checkout checkout, Context context) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            db.insert(Checkout.TABELA, null, checkout.getContentValues());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public static List<Checkout> pegarCheckOuts(Context context) {
        List<Checkout> listaCheckouts = new ArrayList<Checkout>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = null;
        try {
            String sql = "Select * from "+ Checkout.TABELA;
            cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                Checkout checkout = new Checkout(cursor);
                listaCheckouts.add(checkout);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            cursor.close();
        }

        return listaCheckouts;
    }

    public static int pegarQuantidadeCheckout(Context context) {
        int quantidade = 0;
        Cursor cursor = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            String sql = "Select count(*) as quantidade from " + Checkout.TABELA;
            cursor = db.rawQuery(sql, null);
            if(cursor.moveToNext()){
                quantidade = cursor.getInt(cursor.getColumnIndex("quantidade"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return quantidade;
    }

    public static void removerCheckout(int id, Context context) {
        GenericCTRL.delete(context, Checkout.TABELA, id);
    }

}
