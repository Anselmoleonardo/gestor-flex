package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Monitoramento;

public class MonitoramentoCTRL {

    public static List<Monitoramento> listMonitoramentos(Context context) {
        Cursor cursor = null;
        List<Monitoramento> monitoramentos = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(Monitoramento.TABELA, null, null, null, null, null, Monitoramento.CAMPO_ID);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    monitoramentos.add(new Monitoramento(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return monitoramentos;
    }

}

