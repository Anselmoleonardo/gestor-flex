package br.com.gestorflex.database.internal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.model.Agencia;
import br.com.gestorflex.model.Botao;
import br.com.gestorflex.model.CategoriaDash;
import br.com.gestorflex.model.CheckInOut;
import br.com.gestorflex.model.Checkout;
import br.com.gestorflex.model.DDD;
import br.com.gestorflex.model.Dash;
import br.com.gestorflex.model.Estado;
import br.com.gestorflex.model.GrupoEconomico;
import br.com.gestorflex.model.ListarPor;
import br.com.gestorflex.model.Monitoramento;
import br.com.gestorflex.model.MotivoRecusa;
//import br.com.gestorflex.model.Notificacao;
import br.com.gestorflex.model.Notificacao;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.model.PDV;
import br.com.gestorflex.model.PerfilDashboard;
import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Pesquisa;
import br.com.gestorflex.model.Produto;
import br.com.gestorflex.model.Promotor;
import br.com.gestorflex.model.Regional;
import br.com.gestorflex.model.Resposta;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.model.SituacaoFuncional;
import br.com.gestorflex.model.TabMenu;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.model.UsuarioGerenciado;

/**
 * Created by Frann on 29/03/2019.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper dbInstance;

    private static final String DB_NAME = "gestorFlex.db";
    private static final int DB_VERSION = 138;

    public static synchronized DBHelper getInstance(Context context) {
        if (dbInstance == null)
            dbInstance = new DBHelper(context.getApplicationContext());
        return dbInstance;
    }

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createAllTables(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (oldVersion < DB_VERSION - 1) { // Verificar até 2 versões (Recomendado)
            // Query's
            // Execuções
        }
        if (oldVersion < DB_VERSION) {
            // Query's
            // Execuções
            dropAllTables(sqLiteDatabase);
            createAllTables(sqLiteDatabase);
        }
    }

//    retorno,usuario,agencia,botoes,produto,uf,ddd,grupo_economico,regional,listarPOR,pdv,promotor,situacaoFuncional,tabs,pesquisa,pergunta,resposta,jornada

    private void createAllTables(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_USUARIO);
        sqLiteDatabase.execSQL(CREATE_AGENCIA);
        sqLiteDatabase.execSQL(CREATE_BOTAO);
        sqLiteDatabase.execSQL(CREATE_PRODUTO);
        sqLiteDatabase.execSQL(CREATE_NOTIFICACAO);
//        sqLiteDatabase.execSQL(CREATE_MOTIVO_RECUSA);
        sqLiteDatabase.execSQL(CREATE_ESTADO);
        sqLiteDatabase.execSQL(CREATE_DDD);
        sqLiteDatabase.execSQL(CREATE_GRUPO_ECONOMICO);
        sqLiteDatabase.execSQL(CREATE_REGIONAL);
        sqLiteDatabase.execSQL(CREATE_LISTAR_POR);
        sqLiteDatabase.execSQL(CREATE_PDV);
        sqLiteDatabase.execSQL(CREATE_PROMOTOR);
        sqLiteDatabase.execSQL(CREATE_SITUACAO_FUNCIONAL);
        sqLiteDatabase.execSQL(CREATE_TABS);
        sqLiteDatabase.execSQL(CREATE_PERGUNTA);
        sqLiteDatabase.execSQL(CREATE_PESQUISA);
        sqLiteDatabase.execSQL(CREATE_RESPOSTA);
        sqLiteDatabase.execSQL(CREATE_RESPOSTA_PERGUNTA);
        sqLiteDatabase.execSQL(CREATE_CHECKOUT);
//        sqLiteDatabase.execSQL(CREATE_JORNADA);
        sqLiteDatabase.execSQL(CREATE_PERFIL_DASH);
        sqLiteDatabase.execSQL(CREATE_CATEGORIA_DASH);
        sqLiteDatabase.execSQL(CREATE_DASHBOARD);

//        sqLiteDatabase.execSQL(CREATE_NOTIFICACAO);
        sqLiteDatabase.execSQL(CREATE_ODOMETRO);
        sqLiteDatabase.execSQL(CREATE_CHECK_IN_OUT);
        sqLiteDatabase.execSQL(CREATE_USUARIO_GERENCIADO);
        sqLiteDatabase.execSQL(CREATE_MONITORAMENTO);
    }

    public void dropAllTables(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Usuario.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Botao.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Produto.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Notificacao.TABELA);
//        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MotivoRecusa.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Estado.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Agencia.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + GrupoEconomico.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Regional.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + UsuarioGerenciado.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ListarPor.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CategoriaDash.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Dash.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PerfilDashboard.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DDD.TABELA);
//        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Notificacao.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PDV.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Promotor.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SituacaoFuncional.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TabMenu.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Pergunta.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Pesquisa.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Resposta.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + RespostaPergunta.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Checkout.TABELA);
//        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Jornada.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Odometro.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CheckInOut.TABELA);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Monitoramento.TABELA);
    }

    public static void clearDatabase(Context context) {
        GenericCTRL.deleteAll(context, Usuario.TABELA);
        GenericCTRL.deleteAll(context, Agencia.TABELA);
        GenericCTRL.deleteAll(context, Botao.TABELA);
        GenericCTRL.deleteAll(context, Produto.TABELA);
        GenericCTRL.deleteAll(context, Notificacao.TABELA);
//        GenericCTRL.deleteAll(context, MotivoRecusa.TABELA);
        GenericCTRL.deleteAll(context, Estado.TABELA);
        GenericCTRL.deleteAll(context, GrupoEconomico.TABELA);
        GenericCTRL.deleteAll(context, UsuarioGerenciado.TABELA);
        GenericCTRL.deleteAll(context, Regional.TABELA);
        GenericCTRL.deleteAll(context, ListarPor.TABELA);
        GenericCTRL.deleteAll(context, CategoriaDash.TABELA);
        GenericCTRL.deleteAll(context, Dash.TABELA);
        GenericCTRL.deleteAll(context, PerfilDashboard.TABELA);
        GenericCTRL.deleteAll(context, DDD.TABELA);
//        GenericCTRL.deleteAll(context, Notificacao.TABELA);
        GenericCTRL.deleteAll(context, PDV.TABELA);
        GenericCTRL.deleteAll(context, Promotor.TABELA);
        GenericCTRL.deleteAll(context, SituacaoFuncional.TABELA);
        GenericCTRL.deleteAll(context, TabMenu.TABELA);
        GenericCTRL.deleteAll(context, Pergunta.TABELA);
        GenericCTRL.deleteAll(context, Pesquisa.TABELA);
        GenericCTRL.deleteAll(context, Resposta.TABELA);
        GenericCTRL.deleteAll(context, RespostaPergunta.TABELA);
        GenericCTRL.deleteAll(context, Checkout.TABELA);
//        GenericCTRL.deleteAll(context, Jornada.TABELA);
        GenericCTRL.deleteAll(context, Odometro.TABELA);
        GenericCTRL.deleteAll(context, CheckInOut.TABELA);
        GenericCTRL.deleteAll(context, Monitoramento.TABELA);
    }

    private static final String CREATE_USUARIO = "CREATE TABLE IF NOT EXISTS " +
            Usuario.TABELA + " (" +
            Usuario.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            Usuario.CAMPO_DDD_CIDADE + " INTEGER(3), " +
            Usuario.CAMPO_CPF + " TEXT(14), " +
            Usuario.CAMPO_ID_AGENCIA + " INTEGER(3), " +
            Usuario.CAMPO_NOME_AGENCIA + " TEXT(150), " +
            Usuario.CAMPO_NOME_REGIONAL + " TEXT(150), " +
            Usuario.CAMPO_PDV + " INTEGER(11), " +
            Usuario.CAMPO_MATRICULA + " INTEGER(20), " +
            Usuario.CAMPO_BOTIM + " INTEGER(20), " +
            Usuario.CAMPO_BO_LIBERAJORNADA + " INTEGER(20), " +
            Usuario.CAMPO_BO_LIBERAODOMETRO + " INTEGER(20), " +
            Usuario.CAMPO_NOME + " TEXT(150), " +
            Usuario.CAMPO_REFERENCIA + " TEXT(100), " +
            Usuario.CAMPO_FOTO + " TEXT(150), " +
            Usuario.CAMPO_EMAIL + " TEXT(100), " +
            Usuario.CAMPO_IDNOTIFY + " TEXT(200), " +
            Usuario.CAMPO_LATITUDE_PADRAO + " TEXT(50), " +
            Usuario.CAMPO_LONGITUDE_PADRAO + " TEXT(50), " +
            Usuario.CAMPO_ICCID_FUNCIONAL + " TEXT(100), " +
            Usuario.CAMPO_LATITUDE_PDV + " TEXT(50), " +
            Usuario.CAMPO_LONGITUDE_PDV + " TEXT(50), " +
            Usuario.CAMPO_DATA_BD + " TEXT(12), " +
            Usuario.CAMPO_DATA_NASCIMENTO + " TEXT(12), " +
            Usuario.CAMPO_DATA_ADMISSAO + " TEXT(12), " +
            Usuario.CAMPO_DATA_VALIDADE_URA + " TEXT(12), " +
            Usuario.CAMPO_URA + " TEXT(20), " +
            Usuario.CAMPO_BO_PESQUISA + " INTEGER(1), " +
            Usuario.CAMPO_SEXO + " INTEGER(1), " +
            Usuario.CAMPO_FLAG_PRIVILEGIO + " INTEGER, " +
            Usuario.CAMPO_BO_MONITORAMENTO + " INTEGER," +
            Usuario.CAMPO_DATA_INICIO_MONITORAMENTO + " VARCHAR (10)," +
            Usuario.CAMPO_DATA_FINAL_MONITORAMENTO + " VARCHAR (10)," +
            Usuario.CAMPO_FLAG_DOC_CONTROLE + " INTEGER, " +
            Usuario.CAMPO_RG + " TEXT(9), " +
            Usuario.CAMPO_FG_TIPO + " INTEGER(1), " +
            Usuario.CAMPO_TRAVAR_VENDA + " INTEGER(1))";

    private static final String CREATE_AGENCIA = "CREATE TABLE IF NOT EXISTS " +
            Agencia.TABELA + " (" +
            Agencia.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            Agencia.CAMPO_DESCRICAO + " TEXT)";

    private static final String CREATE_BOTAO = "CREATE TABLE IF NOT EXISTS " +
            Botao.TABELA + " (" +
            Botao.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            Botao.CAMPO_NOME + " TEXT(100)," +
            Botao.CAMPO_TAG + " TEXT(100)," +
            Botao.CAMPO_ICONE + " TEXT(100)," +
            Botao.CAMPO_ID_TAB + " INTEGER, " +
            Botao.CAMPO_BO_SITUACAO + " INTEGER)";

    private static final String CREATE_PRODUTO = "CREATE TABLE IF NOT EXISTS " +
            Produto.TABELA + " (" +
            Produto.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            Produto.CAMPO_DESCRICAO + " TEXT(100)," +
            Produto.CAMPO_ID_AGENCIA + " INTEGER)";

    private static final String CREATE_NOTIFICACAO = "CREATE TABLE IF NOT EXISTS " +
            Notificacao.TABELA + " (" +
            Notificacao.CAMPO_ID + " INTEGER PRIMARY KEY," +
            Notificacao.CAMPO_ID_DESTINATARIO + " INTEGER," +
            Notificacao.CAMPO_DATA_HORA + " TEXT(20)," +
            Notificacao.CAMPO_TIPO + " INTEGER(1)," +
            Notificacao.CAMPO_TITULO + " TEXT," +
            Notificacao.CAMPO_MENSAGEM + " TEXT," +
            Notificacao.CAMPO_URL + " TEXT," +
            Notificacao.CAMPO_CAMINHO_ARQUIVO + " TEXT," +
            Notificacao.CAMPO_IMAGEM + " BLOB," +
            Notificacao.CAMPO_FLAG_NOVA + " INTEGER(1)," +
            Notificacao.CAMPO_FLAG_COMPLETO + " INTEGER(1)," +
            Notificacao.CAMPO_FLAG_SINC + " INTEGER(1)," +
            Notificacao.CAMPO_FLAG_LIDO + " INTEGER(1))";


//    private static final String CREATE_MOTIVO_RECUSA = "CREATE TABLE IF NOT EXISTS " +
//            MotivoRecusa.TABELA + " (" +
//            MotivoRecusa.CAMPO_ID + " INTEGER PRIMARY KEY, " +
//            MotivoRecusa.CAMPO_DESCRICAO + " TEXT)";

    private static final String CREATE_ESTADO = "CREATE TABLE IF NOT EXISTS " +
            Estado.TABELA + " (" +
            Estado.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            Estado.CAMPO_DESCRICAO + " TEXT," +
            Estado.CAMPO_REGIONAL + " INTEGER)";



    private static final String CREATE_DDD = "CREATE TABLE IF NOT EXISTS " +
            DDD.TABELA + " (" +
            DDD.CAMPO_ID_UF + " INTEGER PRIMARY KEY," +
            DDD.CAMPO_DESCRICAO + " TEXT)";

    private static final String CREATE_GRUPO_ECONOMICO = "CREATE TABLE IF NOT EXISTS " +
            GrupoEconomico.TABELA + " (" +
            GrupoEconomico.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            GrupoEconomico.CAMPO_DESCRICAO + " TEXT)";

    private static final String CREATE_REGIONAL = "CREATE TABLE IF NOT EXISTS " +
            Regional.TABELA + " (" +
            Regional.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            Regional.CAMPO_ID_AGENCIA + " INTEGER," +
            Regional.CAMPO_DESCRICAO + " TEXT)";

    private static final String CREATE_LISTAR_POR = "CREATE TABLE IF NOT EXISTS " +
            ListarPor.TABELA + " (" +
            ListarPor.CAMPO_NR_ORDEM + " INTEGER," +
            ListarPor.CAMPO_DESCRICAO + " TEXT)";

    private static final String CREATE_CATEGORIA_DASH = "CREATE TABLE IF NOT EXISTS " + CategoriaDash.TABELA + " (" +
            CategoriaDash.CAMPO_ID + " INTEGER PRIMARY KEY," +
            CategoriaDash.CAMPO_NOME + " TEXT)";

    private static final String CREATE_DASHBOARD = "CREATE TABLE IF NOT EXISTS " + Dash.TABELA + " (" +
            Dash.CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            Dash.CAMPO_NOME + " TEXT," +
            Dash.CAMPO_VALOR + " TEXT," +
            Dash.CAMPO_DATA_HORA + " TEXT," +
            Dash.CAMPO_CATEGORIA_DASH_ID + " INTEGER)";

    private static final String CREATE_PDV = "CREATE TABLE IF NOT EXISTS " +
            PDV.TABELA + " (" +
            PDV.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            PDV.CAMPO_ID_REDE + " INTEGER, " +
            PDV.CAMPO_DESCRICAO + " TEXT, " +
            PDV.CAMPO_LATITUDE + " TEXT, " +
            PDV.CAMPO_LONGITUDE + " TEXT, " +
            PDV.CAMPO_IS_BLINDADO + " INTEGER)";

    private static final String CREATE_PROMOTOR = "CREATE TABLE IF NOT EXISTS " +
            Promotor.TABELA + " (" +
            Promotor.CAMPO_ID + " INTEGER PRIMARY KEY," +
            Promotor.CAMPO_DESCRICAO + " TEXT)";

    private static final String CREATE_SITUACAO_FUNCIONAL = "CREATE TABLE IF NOT EXISTS " +
            SituacaoFuncional.TABELA + " (" +
            SituacaoFuncional.CAMPO_ID + " INTEGER PRIMARY KEY," +
            SituacaoFuncional.CAMPO_NOME + " TEXT)";

    private static final String CREATE_TABS = "CREATE TABLE IF NOT EXISTS " +
            TabMenu.TABELA + " (" +
            TabMenu.CAMPO_ID + " INTEGER PRIMARY KEY," +
            TabMenu.CAMPO_NOME + " TEXT," +
            TabMenu.CAMPO_TAG + " TEXT)";

    private static final String CREATE_PERGUNTA = "CREATE TABLE IF NOT EXISTS " +
            Pergunta.TABELA + " (" +
            Pergunta.CAMPO_ID + " INTEGER PRIMARY KEY," +
            Pergunta.CAMPO_PERGUNTA + " TEXT," +
            Pergunta.CAMPO_ID_PESQUISA + " INTEGER," +
            Pergunta.CAMPO_TIPO + " INTEGER," +
            Pergunta.CAMPO_VALIDACAO + " INTEGER)";

    private static final String CREATE_RESPOSTA_PERGUNTA = "CREATE TABLE IF NOT EXISTS " + RespostaPergunta.TABELA + " (" +
            RespostaPergunta.CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            RespostaPergunta.CAMPO_DATA_ATUAL + " VARCHAR(20)," +
            RespostaPergunta.CAMPO_DATA_SINC + "  VARCHAR(20)," +
            RespostaPergunta.CAMPO_DESCRICAO + " TEXT," +
            RespostaPergunta.CAMPO_ID_PDV + " INTEGER," +
            RespostaPergunta.CAMPO_ID_PERGUNTA + " INTEGER," +
            RespostaPergunta.CAMPO_PERGUNTAS_CONCATENADAS + "  TEXT," +
            RespostaPergunta.CAMPO_PDVS_CONCATENADOS + "  TEXT," +
            RespostaPergunta.CAMPO_LATITUDE + "  VARCHAR(20)," +
            RespostaPergunta.CAMPO_LONGITUDE + "  VARCHAR(20))";

    private static final String CREATE_CHECKOUT = "CREATE TABLE IF NOT EXISTS " + Checkout.TABELA + "(" +
            Checkout.CAMPO_ID + " INTEGER PRIMARY KEY," +
            Checkout.CAMPO_ID_PDV + " INTEGER," +
            Checkout.CAMPO_DATA_CHECKOUT + " VARCHAR (25)," +
            Checkout.CAMPO_LATITUDE + " TEXT," +
            Checkout.CAMPO_LONGITUDE + " TEXT)";

    private static final String CREATE_PESQUISA = "CREATE TABLE IF NOT EXISTS " +
            Pesquisa.TABELA + " (" +
            Pesquisa.CAMPO_ID + " INTEGER PRIMARY KEY," +
            Pesquisa.CAMPO_NOME + " TEXT," +
            Pesquisa.CAMPO_DATA_CADASTRO + " TEXT)";

    private static final String CREATE_RESPOSTA = "CREATE TABLE IF NOT EXISTS " +
            Resposta.TABELA + " (" +
            Resposta.CAMPO_ID + " INTEGER PRIMARY KEY," +
            Resposta.CAMPO_ID_PERGUNTA + " INTEGER," +
            Resposta.CAMPO_RESPOSTA + " TEXT)";

//    private static final String CREATE_JORNADA = "CREATE TABLE IF NOT EXISTS " +
//            Jornada.TABELA + " (" +
//            Jornada.CAMPO_ID + " INTEGER PRIMARY KEY," +
//            Jornada.CAMPO_ATIVIDADE + " INTEGER," +
//            Jornada.CAMPO_DATA_ENVIO + " TEXT," +
//            Jornada.CAMPO_KM_DIA + " INTEGER," +
//            Jornada.CAMPO_KM_ODOMENTRO + " INTEGER," +
//            Jornada.CAMPO_CAMINHO_FOTO + " TEXT," +
//            Jornada.CAMPO_FOTO + " TEXT," +
//            Jornada.CAMPO_LATITUDE + " TEXT," +
//            Jornada.CAMPO_LONGITUDE + " TEXT)";

    private static final String CREATE_USUARIO_GERENCIADO = "CREATE TABLE IF NOT EXISTS " +
            UsuarioGerenciado.TABELA + " (" +
            UsuarioGerenciado.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            UsuarioGerenciado.CAMPO_NOME + " TEXT, " +
            UsuarioGerenciado.CAMPO_MATRICULA + " TEXT, " +
            UsuarioGerenciado.CAMPO_FLAG_PRIVILEGIO + " INTEGER, " +
            UsuarioGerenciado.CAMPO_ID_ESTADO + " INTEGER)";



    private static final String CREATE_PERFIL_DASH = "CREATE TABLE IF NOT EXISTS " +
            PerfilDashboard.TABELA + " (" +
            PerfilDashboard.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            PerfilDashboard.CAMPO_META + " TEXT, " +
            PerfilDashboard.CAMPO_REALIZADO + " TEXT, " +
            PerfilDashboard.CAMPO_GAP + " TEXT, " +
            PerfilDashboard.CAMPO_DATA + " TEXT)";



//    private static final String CREATE_NOTIFICACAO = "CREATE TABLE IF NOT EXISTS " +
//            Notificacao.TABELA + " (" +
//            Notificacao.CAMPO_ID + " INTEGER PRIMARY KEY," +
//            Notificacao.CAMPO_ID_DESTINATARIO + " INTEGER," +
//            Notificacao.CAMPO_DATA_HORA + " TEXT(20)," +
//            Notificacao.CAMPO_TIPO + " INTEGER(1)," +
//            Notificacao.CAMPO_TITULO + " TEXT," +
//            Notificacao.CAMPO_MENSAGEM + " TEXT," +
//            Notificacao.CAMPO_URL + " TEXT," +
//            Notificacao.CAMPO_CAMINHO_ARQUIVO + " TEXT," +
//            Notificacao.CAMPO_IMAGEM + " BLOB," +
//            Notificacao.CAMPO_FLAG_NOVA + " INTEGER(1)," +
//            Notificacao.CAMPO_FLAG_COMPLETO + " INTEGER(1)," +
//            Notificacao.CAMPO_FLAG_SINC + " INTEGER(1)," +
//            Notificacao.CAMPO_FLAG_LIDO + " INTEGER(1))";


    private static final String CREATE_ODOMETRO = "CREATE TABLE IF NOT EXISTS  " +
            Odometro.TABELA + "(" +
            Odometro.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            Odometro.CAMPO_KM_DIA + " INTEGER, " +
            Odometro.CAMPO_KM_ODOMENTRO + " INTEGER, " +
            Odometro.CAMPO_DATA_ENVIO + " VARCHAR (25), " +
            Odometro.CAMPO_CAMINHO_FOTO + " TEXT, " +
            Odometro.CAMPO_FOTO + " TEXT, " +
            Odometro.CAMPO_ATIVIDADE + " INTEGER, " +
            Odometro.CAMPO_LATITUDE + " TEXT, " +
            Odometro.CAMPO_LONGITUDE + " TEXT, " +
            Odometro.CAMPO_IS_SINC + " INTEGER)";

    private static final String CREATE_CHECK_IN_OUT = "CREATE TABLE IF NOT EXISTS " +
            CheckInOut.TABELA + "(" +
            CheckInOut.CAMPO_ID + " INTEGER PRIMARY KEY, " +
            CheckInOut.CAMPO_ID_PDV + " INTEGER, " +
            CheckInOut.CAMPO_NOME_PDV + " VARCHAR (250), " +
            CheckInOut.CAMPO_DATA + " VARCHAR (25), " +
            CheckInOut.CAMPO_LATITUDE + " TEXT, " +
            CheckInOut.CAMPO_LONGITUDE + " TEXT, " +
            CheckInOut.CAMPO_FLAG_TIPO_CHECK + " INTEGER, " +
            CheckInOut.CAMPO_IS_SINC + " INTEGER)";


    private static final String CREATE_MONITORAMENTO = "CREATE TABLE IF NOT EXISTS " + Monitoramento.TABELA + "  (" +
            Monitoramento.CAMPO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            Monitoramento.CAMPO_LATITUDE + " VARCHAR(30)," +
            Monitoramento.CAMPO_LONGITUDE + " VARCHAR(30)," +
            Monitoramento.CAMPO_DATA_HORA + " VARCHAR(25))";
}
