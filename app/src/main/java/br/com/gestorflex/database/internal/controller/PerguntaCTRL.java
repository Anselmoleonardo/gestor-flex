package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Pesquisa;

public class PerguntaCTRL {

//    public static List<PDV> listPdvs(Context context) {
//        Cursor cursor = null;
//        List<PDV> listaPdv = new ArrayList<>();
//        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
//        try {
//            cursor = db.query(PDV.TABELA, null, null, null, null, null, null);
//            if (cursor.getCount() > 0) {
//                cursor.moveToFirst();
//                do {
//                    listaPdv.add(new PDV(cursor));
//                } while (cursor.moveToNext());
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            cursor.close();
//            db.close();
//        }
//        return listaPdv;
//    }

    public static List<Pergunta> getPerguntasDaPesquisa(Context context, int idPesquisa) {
        Cursor cursor = null;
        List<Pergunta> listPergunta = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {

            cursor = db.query(Pergunta.TABELA, null, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listPergunta.add(new Pergunta(cursor));
                } while (cursor.moveToNext());
            }

        } catch (Exception ex) {
            Log.e("joinTeste", ex.getMessage());
            db.close();
            return null;
        } finally {
            cursor.close();
            db.close();
        }
        return listPergunta;
    }


//    public static List<Pergunta> getPerguntasDaPesquisa(Context context, int idPesquisa) {
//        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
//        Cursor c = null;
//        List<Pergunta> listPergunta = new ArrayList<>();
//        try {
//            String sql = "SELECT * " + "FROM " + Pergunta.TABELA + " JOIN " + Pesquisa.TABELA + " on   " +
//                    Pergunta.CAMPO_ID_PESQUISA + " = " + Pesquisa.CAMPO_ID + " AND " + Pesquisa.CAMPO_ID + " = " + idPesquisa + "";
//            c = db.rawQuery(sql, null);
//            if (c.getCount() > 0) {
//                c.moveToFirst();
//                do {
//                    listPergunta.add(new Pergunta(c));
//                } while (c.moveToNext());
//            }
//            return listPergunta;
//        } catch (Exception ex) {
//            Log.e("joinTeste", ex.getMessage());
//            db.close();
//            return null;
//        } finally {
//            db.close();
//        }
//    }

    public static List<Pergunta> getPerguntasDaPesquisaComOrdenacao(Context context, int idPesquisa) {
        List<Pergunta> perguntasDaPesquisa = null;
        try {
            perguntasDaPesquisa = getPerguntasDaPesquisa(context, idPesquisa);
            for (int i = 0; i < perguntasDaPesquisa.size(); i++) {
                String perguntaComNumero = (i + 1) + " - " + perguntasDaPesquisa.get(i).getPegunta();
                perguntasDaPesquisa.get(i).setPergunta(perguntaComNumero);
            }
            return perguntasDaPesquisa;
        } catch (Exception ex) {
            return null;
        }
    }

    public static Pergunta getPerguntaById(Context context, int id) {
        Cursor c = null;
        Pergunta pergunta = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            c = db.query(Pergunta.TABELA, null, Pergunta.CAMPO_ID + " = ?", new String[]{id+""}, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                pergunta = new Pergunta(c);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
        return pergunta;
    }
}

