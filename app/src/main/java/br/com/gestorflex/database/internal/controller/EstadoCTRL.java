package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Estado;

public class EstadoCTRL {

    public static List<Estado> listEstados(Context context) {
        Cursor cursor = null;
        List<Estado> estados = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(Estado.TABELA, null, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    estados.add(new Estado(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return estados;
    }

}
