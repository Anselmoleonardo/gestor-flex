package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.DDD;

public class DDDCTRL {

    public static List<DDD> listDDDs(Context context, int idUf) {
        Cursor cursor = null;
        List<DDD> DDDs = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(DDD.TABELA, null, DDD.CAMPO_ID_UF + " = ? OR ? = 0", new String[]{idUf + "", idUf + ""}, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    DDDs.add(new DDD(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return DDDs;
    }

}
