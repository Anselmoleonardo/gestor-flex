package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.RespostaPergunta;

public class RespostaPerguntaCTRL {

    public static List<RespostaPergunta> listRespostaPergunta(Context context) {
        Cursor c = null;
        List<RespostaPergunta> listResposta = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            c = db.query(RespostaPergunta.TABELA, null, null, null, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    listResposta.add(new RespostaPergunta(c));
                } while (c.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
        return listResposta;
    }

    public static List<RespostaPergunta> pegarCheckIn(Context context) {
        Cursor cursor = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        List<RespostaPergunta> respostaPerguntas = new ArrayList<>();
        try {
            String sql = "Select distinct * from " + RespostaPergunta.TABELA + "";

            cursor = db.rawQuery(sql, null);

            while (cursor.moveToNext()) {
                RespostaPergunta respostaPergunta = new RespostaPergunta(cursor);
                respostaPerguntas.add(respostaPergunta);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return respostaPerguntas;
    }

    public static JSONObject pegarRespostaPergunta(Context context, int idPdv, String data) {
        Cursor cursor = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        JSONObject jsonParametros = new JSONObject();
        try {
            String sql = "Select * from " + RespostaPergunta.TABELA + " where " + RespostaPergunta.CAMPO_ID_PDV + " = ?";
            cursor = db.rawQuery(sql, new String[]{String.valueOf(idPdv)});
            RespostaPergunta.converteParaJson(cursor, jsonParametros);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return jsonParametros;
    }

    public static JSONObject pegarJsonPesquisaCheckIn(Context context, RespostaPergunta registrosDePesquisa) {
        try {
            return  pegarRespostaPergunta (context,registrosDePesquisa.getIdPdv(),registrosDePesquisa.getDtAtual());
        }catch (Exception ex){
            return  null;
        }

    }

    public static int pegarQuantidadePesquisaCheckin(Context context) {
        int quantidade = 0;
        Cursor cursor = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            String sql = "Select distinct " + RespostaPergunta.CAMPO_ID_PDV + "," + "substr(" + RespostaPergunta.CAMPO_DATA_ATUAL + ", 0, 11) from " + RespostaPergunta.TABELA;
            cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            quantidade = cursor.getCount();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return quantidade;
    }
}
