package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.PDV;

public class PdvCTRL {

    public static List<PDV> listPdvs(Context context) {
        Cursor cursor = null;
        List<PDV> listaPdv = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(PDV.TABELA, null, null, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listaPdv.add(new PDV(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return listaPdv;
    }

    public static List<PDV> getPDVPorIdRede(Context context, int id){
        Cursor cursor = null;
        List<PDV> listaPdv = new ArrayList();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try{
            cursor = db.query(PDV.TABELA, null, PDV.CAMPO_ID_REDE + " = ?",new String[]{String.valueOf(id)}, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listaPdv.add(new PDV(cursor));
                } while (cursor.moveToNext());
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            cursor.close();
            db.close();
        }
        return listaPdv;
    }

    public static PDV getPdvById(Context context, int id) {
        Cursor c = null;
        PDV pdv = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            c = db.query(PDV.TABELA, null, PDV.CAMPO_ID + " = ?", new String[]{id+""}, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                pdv = new PDV(c);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
        return pdv;
    }

    public static List<PDV> listPdv(Context context, String notIn) {
        Cursor cursor = null;
        List<PDV> listaPdv = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(PDV.TABELA, null, PDV.CAMPO_ID + " NOT IN (" + notIn + ")",
                    null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listaPdv.add(new PDV(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return listaPdv;
    }

}
