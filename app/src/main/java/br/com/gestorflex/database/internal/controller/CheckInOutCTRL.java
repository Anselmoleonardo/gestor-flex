package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.CheckInOut;

public class CheckInOutCTRL {

    public static List<CheckInOut> listCheckInOutSincronizacao(Context context) {
        Cursor cursor = null;
        List<CheckInOut> listaCheckInOut = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(CheckInOut.TABELA, null, CheckInOut.CAMPO_IS_SINC + " = ?",
                    new String[]{"1"}, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listaCheckInOut.add(new CheckInOut(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return listaCheckInOut;
    }

    public static List<CheckInOut> listCheckOuts(Context context) {
        Cursor cursor = null;
        List<CheckInOut> listaCheckInOut = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(CheckInOut.TABELA, null, CheckInOut.CAMPO_FLAG_TIPO_CHECK + " = ?",
                    new String[]{String.valueOf(CheckInOut.TIPO_CHECK_OUT)}, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    listaCheckInOut.add(new CheckInOut(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return listaCheckInOut;
    }

    public static CheckInOut getCheckOutPendente(Context context) {
        Cursor cursor = null;
        CheckInOut checkInOut = null;
        int idPdvCheckInOut = getIdPdvCheckOutPendente(context);
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(CheckInOut.TABELA, null, CheckInOut.CAMPO_ID_PDV + " = ? AND "
                            + CheckInOut.CAMPO_FLAG_TIPO_CHECK + " = ?",
                    new String[]{String.valueOf(idPdvCheckInOut), String.valueOf(CheckInOut.TIPO_CHECK_IN)},
                    null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                checkInOut = new CheckInOut(cursor);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return checkInOut;
    }

    public static int getIdPdvCheckOutPendente(Context context) {
        Cursor cursor = null;
        int idPdvCheckInOut = 0;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            // SELECT id_pdv FROM check_in_out GROUP BY id_pdv HAVING COUNT(id_pdv) = 1 AND flag_tipo_check = 2
            cursor = db.query(CheckInOut.TABELA, new String[]{CheckInOut.CAMPO_ID_PDV}, null, null,
                    CheckInOut.CAMPO_ID_PDV, "COUNT(" + CheckInOut.CAMPO_ID_PDV + ") = 1 AND " +
                            CheckInOut.CAMPO_FLAG_TIPO_CHECK + " = " + CheckInOut.TIPO_CHECK_IN, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                idPdvCheckInOut = cursor.getInt(cursor.getColumnIndex(CheckInOut.CAMPO_ID_PDV));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return idPdvCheckInOut;
    }

}
