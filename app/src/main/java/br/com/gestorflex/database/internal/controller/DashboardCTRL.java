package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Dash;

public class DashboardCTRL {

    public static List<Dash> listDashboardPorCategoria(Context context, int id_categoria_dash) {
        Cursor c = null;
        List<Dash> listDashboard = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            c = db.query(Dash.TABELA, null, Dash.CAMPO_CATEGORIA_DASH_ID + " = ?", new String[]{"" + id_categoria_dash}, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                listDashboard = new ArrayList<>();
                do {
                    listDashboard.add(new Dash(c));
                } while (c.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
        return listDashboard;
    }

}
