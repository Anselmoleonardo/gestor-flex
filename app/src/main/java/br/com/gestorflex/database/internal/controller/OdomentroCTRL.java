package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Odometro;

public class OdomentroCTRL {

    public static List<Odometro> pegarJornadas(Context context) {
        Cursor cursor = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        List<Odometro> jornadas = new ArrayList<Odometro>();
        try {
            String sql = "Select * from " + Odometro.TABELA;
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                Odometro jornada = new Odometro(cursor);
                jornadas.add(jornada);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return jornadas;
    }

    public static int pegarQuantidade(Context context) {
        int quantidade = 0;
        Cursor cursor = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            String sql = "Select count(*) as quantidade from " + Odometro.TABELA;
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                quantidade = cursor.getInt(cursor.getColumnIndex("quantidade"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return quantidade;
    }

}
