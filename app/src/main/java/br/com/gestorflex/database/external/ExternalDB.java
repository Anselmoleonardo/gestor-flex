package br.com.gestorflex.database.external;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.collection.LruCache;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Aparelho;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.ErrorUtil;

/**
 * Created Frann on 29/03/2019.
 */

public class ExternalDB {

    public static final String URL_CONEXAO = "urlConexao";
    public static final String RETORNO = "retorno";
    public static final String COD_RETORNO = "cdRetorno";
    public static final String MSG_RETORNO = "msgRetorno";

    public static int CD_RETORNO_SUCESSO = 200;

    private static ExternalDB mInstance;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mCtx;

    private ExternalDB(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized ExternalDB getInstance(Context context) {
        if (mInstance == null)
            mInstance = new ExternalDB(context);
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public static void addDbRequest(final Context context, final JSONObject logParams, final String procedure,
                                    Response.Listener<String> responseListener, Response.ErrorListener errorListener, String tag) throws Exception {
        getLogPadrao(context, logParams);
        Request request = new StringRequest(Request.Method.POST, getUrl(context), responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                try {
                    logParams.put("procedure", procedure);
                    JSONObject log = new JSONObject();
                    log.put("log", logParams);
                    params.put("concentrador", log.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(context, e);
                }
                Log.e("PARAMS", params.toString());
                return params;
            }
        };
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)); // Este código força o Volley a realizar apenas uma requisição, pois para essa procedure ele estava, estranhamente, realizando duas.
        if (tag != null && !tag.equals(""))
            request.setTag(tag);
        getInstance(context).addToRequestQueue(request);
    }

    public static void getLogPadrao(Context context, JSONObject params) throws Exception {
        Usuario usuario = UsuarioCTRL.getUsuario(context);
        if (usuario != null) usuario.getLogUsuarioGeral(params);
        Aparelho.getLog(context, params);
        Geolocalizacao.getLog(context, params);
    }

    public static String getUrl(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(ExternalDB.URL_CONEXAO, context.MODE_PRIVATE);
        return sharedPreferences.getString("url", context.getResources().getString(R.string.url_producao));
    }
}
