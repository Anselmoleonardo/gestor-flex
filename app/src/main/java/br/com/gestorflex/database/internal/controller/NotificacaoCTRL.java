package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Notificacao;

/**
 * Created by Frann on 09/04/2019.
 */

public class NotificacaoCTRL {


    public static List<Notificacao> getList(Context context) {
        Cursor cursor = null;
        List<Notificacao> notificacaos = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(Notificacao.TABELA, null, Notificacao.CAMPO_FLAG_NOVA + " = ?  AND " + Notificacao.CAMPO_FLAG_LIDO + " = ?", new String[]{"" + 1, "" +0},null,null, Notificacao.CAMPO_ID + " DESC" , null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    notificacaos.add(new Notificacao(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return notificacaos;
    }

    public static Notificacao getNotificacao(Context context, int id) throws Exception {
            Notificacao notificacao = null;
            SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();

            Cursor cursor = db.rawQuery("SELECT * FROM " + Notificacao.TABELA + " WHERE (" + Notificacao.CAMPO_ID + " = " + id + " OR " + id + " = 0)",
                            null);

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                notificacao = new Notificacao(cursor);
            }

            return notificacao;

    }

    public static Cursor getCursorNotificacao(Context context, int isNova) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            return db.rawQuery(
                    "SELECT *" +
                            " FROM " + Notificacao.TABELA +
                            " WHERE (" + Notificacao.CAMPO_FLAG_NOVA + " = " + isNova + " OR " + isNova + " = -1)" +
                            " ORDER BY " + Notificacao.CAMPO_ID + " DESC",
                    null);
        } catch (Exception ex) {
            throw ex;
        } finally {
            //db.close();
        }
    }

    public static void desatualizar(Context context, int id) throws Exception {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            String sql = "UPDATE " + Notificacao.TABELA + " SET " + Notificacao.CAMPO_FLAG_NOVA + " = 0 WHERE (" + Notificacao.CAMPO_ID + " = " + id + " OR " + id + " = 0)";
            db.execSQL(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void setCompleto(Context context, int id) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            String sql = "UPDATE " + Notificacao.TABELA + " SET " + Notificacao.CAMPO_FLAG_COMPLETO + " = 1 WHERE " + Notificacao.CAMPO_ID + "= " + id;
            db.execSQL(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            db.close();
        }
    }

    public static void setSync(Context context, int id, int isSync) throws Exception {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            String sql = "UPDATE " + Notificacao.TABELA + " SET " + Notificacao.CAMPO_FLAG_SINC + " = " + isSync + " WHERE " + Notificacao.CAMPO_ID + " = " + id;
            db.execSQL(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }

    }

    public static void updateNotificacaoLida(Context context, int isLido, int id) {
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();

        try {
            String sql = "UPDATE " + Notificacao.TABELA + " SET " + Notificacao.CAMPO_FLAG_LIDO + " = " + isLido + " WHERE " + Notificacao.CAMPO_ID + " = " + id;
            db.execSQL(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }
    }

    public Boolean getNotificacaoLidas(Context context, int id) {
        Notificacao notificacao = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        Cursor cursor = null;

        try {
            cursor = db.rawQuery(
                    "SELECT *" +
                            " FROM  " + Notificacao.TABELA + " WHERE (" + Notificacao.CAMPO_ID + "= " + id + " AND " + Notificacao.CAMPO_FLAG_SINC + "  = 1)",
                    null);

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();

                notificacao = new Notificacao(cursor);

                return true;
            } else {
                return false;
            }

        } catch (Exception ex) {
            Log.e("cursor noti", "" + ex.getMessage());
            return false;
        } finally {
            cursor.close();
            db.close();
        }


    }

}
