package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Resposta;

public class RespostaCTRL {
    public static List<Resposta> listResposta(Context context, int idPergunta) throws Exception {
        Cursor c = null;
        List<Resposta> listResposta = null;
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            c = db.query(Resposta.TABELA, null, Resposta.CAMPO_ID_PERGUNTA + "=?", new String[]{String.valueOf(idPergunta)}, null, null, null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                listResposta = new ArrayList<>();
                do {
                    listResposta.add(new Resposta(c));
                } while (c.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
        return listResposta;
    }

}
