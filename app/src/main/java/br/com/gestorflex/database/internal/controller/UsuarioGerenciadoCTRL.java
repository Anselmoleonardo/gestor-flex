package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.UsuarioGerenciado;

public class UsuarioGerenciadoCTRL {

    public static List<UsuarioGerenciado> listUsuariosGerenciados(Context context, int privilegio, int idEstado) {
        Cursor cursor = null;
        List<UsuarioGerenciado> usuariosGerenciados = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(UsuarioGerenciado.TABELA, null, UsuarioGerenciado.CAMPO_FLAG_PRIVILEGIO + " = ? AND (" +
                    UsuarioGerenciado.CAMPO_ID_ESTADO + " = ? OR " + idEstado + " = 0)", new String[]{privilegio + "", idEstado + ""}, null, null, UsuarioGerenciado.CAMPO_NOME);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    usuariosGerenciados.add(new UsuarioGerenciado(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return usuariosGerenciados;
    }

}
