package br.com.gestorflex.database.internal.controller;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.model.Botao;

/**
 * Created by paulo on 02/04/2019.
 */

public class BotaoCTRL {

    public static List<Botao> getList(Context context, int idTab) {
        Cursor cursor = null;
        List<Botao> botoes = new ArrayList<>();
        SQLiteDatabase db = DBHelper.getInstance(context).getWritableDatabase();
        try {
            cursor = db.query(Botao.TABELA, null, Botao.CAMPO_ID_TAB + " = ?", new String[]{"" + idTab}, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    botoes.add(new Botao(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return botoes;
    }

}
