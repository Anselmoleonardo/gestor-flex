package br.com.gestorflex.notificacao;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.NotificacaoCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Notificacao;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.activity.AutenticacaoActivity;
import br.com.gestorflex.ui.activity.MenuActivity;
import br.com.gestorflex.ui.activity.NotificacaoActivity;

//import br.com.gestorflex.model.Login;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static int idNotification = 0;
    protected String caminhoFotoNotificacao;
    private Bitmap bigPicture;
    private Notificacao notificacao;
    public static NotificationManager notificationManager;
    Usuario usuario;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        usuario = UsuarioCTRL.getUsuario(this);
        if (remoteMessage.getData().size() > 0 && UsuarioCTRL.getUsuario(this) != null) {
//            int idDestinatario = Integer.parseInt(remoteMessage.getData().get("id_destinatario"));
//            if (idDestinatario == usuario.getId())
            showNotification(remoteMessage);
        }
    }

    private void showNotification(RemoteMessage remoteMessage) {
        String channelId = getString(R.string.default_notification_channel_id);
//        if (usuario.getFlagAutenticado() == 0) {
//            usuario.setFlagAutenticado(1);
//            GenericCTRL.update(this, Usuario.TABELA, Usuario.CAMPO_ID + " = ?", new String[]{"" + usuario.getId()}, usuario.getContetValues());
//        }
        notificacao = new Notificacao(remoteMessage);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(notificacao.getMensagem())
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setContentIntent(getPendingIntent());
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(idNotification, notificationBuilder.build());
        try {
            if (notificacao.getTipo() == 1) {
                coletarImagem(notificacao.getUrl());
            }
            Notificacao notificacaoValidacao = NotificacaoCTRL.getNotificacao(this, notificacao.getId());
            if (notificacaoValidacao == null)
                GenericCTRL.save(this, Notificacao.TABELA, notificacao.getContetValues());


        } catch (Exception e) {
            e.printStackTrace();
        }
        idNotification++;
    }

    public void coletarImagem(String strURL) throws Exception {
        try {
            // ACESSA A URL
            URL url = new URL(strURL);
            URLConnection connection = url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            // COLETA A IMAGEM DA URL E SALVA NO APARELHO
            InputStream input = connection.getInputStream();
            salvarImagem(input);
            // CONVERTE IMAGEM PARA BITMAP
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            if (bigPicture == null) {
                Toast.makeText(getApplicationContext(),
                        "A imagem enviada por notificação não foi encontrada!",
                        Toast.LENGTH_LONG).show();
                return;
            }
            byte[] blImagem = notificacao.convertBitmapToByteArray(bigPicture);
            Bitmap bitmap = BitmapFactory.decodeByteArray(blImagem, 0,
                    blImagem.length, options);
            blImagem = notificacao.convertBitmapToByteArray(bitmap);
            notificacao.setImagem(blImagem);
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void salvarImagem(InputStream input) throws Exception {
        try {
            File image = null;
            File storageDir = null;
            Uri photoURI = null;

            BufferedInputStream bis = new BufferedInputStream(input);
            ByteArrayBuffer baf = new ByteArrayBuffer(5000);
            int bytesRead = 0;
            while ((bytesRead = bis.read()) != -1) {
                baf.append((byte) bytesRead);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                image = File.createTempFile(notificacao.getId() + "_notificacao", "_foto.jpg", storageDir);
                caminhoFotoNotificacao = image.getAbsolutePath();
            } else {
                storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                caminhoFotoNotificacao = storageDir.getAbsolutePath() + "/" + notificacao.getId() + "_notificacao_foto.jpg";
                image = new File(caminhoFotoNotificacao);
            }
            File arquivo = new File(caminhoFotoNotificacao);
            // SALVA A IMAGEM NO ARQUIVO
            FileOutputStream fos = new FileOutputStream(arquivo);
            fos.write(baf.toByteArray());
            fos.flush();
            fos.close();
            // COLETA O BITMAP DA IMAGEM
            bigPicture = BitmapFactory.decodeStream(new FileInputStream(caminhoFotoNotificacao));
            notificacao.setCaminhoArquivo(caminhoFotoNotificacao);
        } finally {
            input.close();
        }
    }


    private PendingIntent getPendingIntent() {
        Intent intent;
        if (UsuarioCTRL.getUsuario(this) != null) {
            intent = new Intent(this, NotificacaoActivity.class);
        } else
            intent = new Intent(this, AutenticacaoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("boNotificacao", 1);
        intent.putExtra(MenuActivity.TAG_TITLE, "Notificação");
        return PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
    }

}
