package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class PerfilDashboard {

    public static final String TABELA = "perfil_dashboard";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_META = "meta";
    public static final String CAMPO_REALIZADO = "realizado";
    public static final String CAMPO_GAP = "gap";
    public static final String CAMPO_DATA = "data";

    private int id;
    private String meta;
    private String realizado;
    private String gap;
    private String data;

    public PerfilDashboard() {
    }

    public PerfilDashboard(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.meta = cursor.getString(cursor.getColumnIndex(CAMPO_META));
        this.realizado = cursor.getString(cursor.getColumnIndex(CAMPO_REALIZADO));
        this.gap = cursor.getString(cursor.getColumnIndex(CAMPO_GAP));
        this.data = cursor.getString(cursor.getColumnIndex(CAMPO_DATA));
    }

    public PerfilDashboard(JSONObject jsonObject) {
        this.id = 1;
        this.meta = jsonObject.optString("txMeta", "0");
        this.realizado = jsonObject.optString("txRealizado", "0");
        this.gap = jsonObject.optString("txGap", "0");
        this.data = jsonObject.optString("txData", "Visita");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getRealizado() {
        return realizado;
    }

    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }

    public String getGap() {
        return gap;
    }

    public void setGap(String gap) {
        this.gap = gap;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_META, getMeta());
        valores.put(CAMPO_REALIZADO, getRealizado());
        valores.put(CAMPO_GAP, getGap());
        valores.put(CAMPO_DATA, getData());
        return valores;
    }
}
