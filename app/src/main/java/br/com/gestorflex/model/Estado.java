package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Estado {

    public static final String TABELA = "estado";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DESCRICAO = "descricao";
    public static final String CAMPO_REGIONAL = "regional";

    private int id;
    private String descricao;
    private int regional;

    public Estado(){
        this.id = 0;
        this.descricao = "";
        this.regional = 0;
    }

    public Estado(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
        this.regional = regional;
    }

    public int getRegional() {
        return regional;
    }

    public void setRegional(int regional) {
        this.regional = regional;
    }

    public Estado(JSONObject jsonEstado) {
        this.id = jsonEstado.optInt("d02_cdid");
        this.descricao = jsonEstado.optString("d02_uf");
        this.regional = jsonEstado.optInt("d02_cdregiao");
    }

    public Estado(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
        this.regional = cursor.getInt(cursor.getColumnIndex(CAMPO_REGIONAL));
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_DESCRICAO, getDescricao());
        valores.put(CAMPO_REGIONAL, getRegional());
        return valores;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

}
