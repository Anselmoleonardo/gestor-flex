package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

/**
 * Created by Frann on 29/03/2019.
 */

public class Botao {

    public static final String TABELA = "botao";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NOME = "nome";
    public static final String CAMPO_TAG = "tag";
    public static final String CAMPO_ICONE = "icone";
    public static final String CAMPO_ID_TAB = "id_tab";
    public static final String CAMPO_BO_SITUACAO = "bo_situacao";

    private int id;
    private String nome;
    private String tag;
    private String icone;
    private int idTab;
    private int boSituacao;

    public Botao(int id, String nome, String tag, String icone, int idTab, int boSituacao) {
        this.id = id;
        this.nome = nome;
        this.tag = tag;
        this.icone = icone;
        this.idTab = idTab;
        this.boSituacao = boSituacao;
    }

    public Botao(JSONObject jsonBotao) {
        this.id = jsonBotao.optInt("id");
        this.nome = jsonBotao.optString("nome");
        this.tag = jsonBotao.optString("tag");
        this.icone = jsonBotao.optString("icone");
        this.idTab = jsonBotao.optInt("id_tab");
        this.boSituacao = jsonBotao.optInt("bosituacao", 1);
    }

    public Botao(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.nome = cursor.getString(cursor.getColumnIndex(CAMPO_NOME));
        this.tag = cursor.getString(cursor.getColumnIndex(CAMPO_TAG));
        this.icone = cursor.getString(cursor.getColumnIndex(CAMPO_ICONE));
        this.idTab = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_TAB));
        this.boSituacao = cursor.getInt(cursor.getColumnIndex(CAMPO_BO_SITUACAO));
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getTag() {
        return tag;
    }

    public String getIcone() {
        return icone;
    }

    public int getIdTab() {
        return idTab;
    }

    public int getBoSituacao() {
        return boSituacao;
    }

    public void setBoSituacao(int boSituacao) {
        this.boSituacao = boSituacao;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_NOME, getNome());
        valores.put(CAMPO_TAG, getTag());
        valores.put(CAMPO_ICONE, getIcone());
        valores.put(CAMPO_ID_TAB, getIdTab());
        valores.put(CAMPO_BO_SITUACAO, getBoSituacao());
        return valores;
    }
}
