package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class PDV {

    public static final String TABELA = "pdv";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_ID_REDE = "id_rede";
    public static final String CAMPO_DESCRICAO = "descricao";
    public static final String CAMPO_LATITUDE = "latitude";
    public static final String CAMPO_LONGITUDE = "longitude";
    public static final String CAMPO_IS_BLINDADO = "is_blindado";

    private int id;
    private int idRede;
    private String descricao;
    private int isBlindado;
    private String latitude;
    private String longitude;


    public PDV() {
        this.id = 0;
        this.idRede = 0;
        this.descricao = "";
        this.isBlindado = 0;
        this.latitude = "";
        this.longitude = "";

    }

    public PDV(int id, Double latitude, Double longitude) {
        this.id = id;
        this.latitude = latitude.toString();
        this.longitude = longitude.toString();
    }

    public PDV(JSONObject jsonObject) {
        this.id = jsonObject.optInt("d03_cdid");
        this.idRede = jsonObject.optInt("d03_cdrede");
        this.descricao = jsonObject.optString("d03_nmdescricao");
        this.isBlindado = jsonObject.optInt("d03_boblindado");
        this.latitude = jsonObject.optString("d03_latitude");
        this.longitude = jsonObject.optString("d03_longitude");

    }

    public PDV(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.idRede = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_REDE));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
        this.isBlindado = cursor.getInt(cursor.getColumnIndex(CAMPO_IS_BLINDADO));
        this.latitude = cursor.getString(cursor.getColumnIndex(CAMPO_LATITUDE));
        this.longitude = cursor.getString(cursor.getColumnIndex(CAMPO_LONGITUDE));

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getIdRede() {
        return idRede;
    }

    public void setIdRede(int idRede) {
        this.idRede = idRede;
    }

    public int getIsBlindado() {
        return isBlindado;
    }

    public void setIsBlindado(int isBlindado) {
        this.isBlindado = isBlindado;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_ID_REDE, getIdRede());
        valores.put(CAMPO_DESCRICAO, getDescricao());
        valores.put(CAMPO_IS_BLINDADO, getIsBlindado());
        valores.put(CAMPO_LATITUDE, getLatitude());
        valores.put(CAMPO_LONGITUDE, getLongitude());

        return valores;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

}
