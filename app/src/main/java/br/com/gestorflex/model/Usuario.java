package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Frann on 29/03/2019.
 */

public class Usuario {

    public static final String TABELA = "usuario";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DDD_CIDADE = "ddd_cidade";
    public static final String CAMPO_CPF = "cpf";
    public static final String CAMPO_ID_AGENCIA = "id_agencia";
    public static final String CAMPO_NOME_AGENCIA = "nome_agencia";
    public static final String CAMPO_NOME_REGIONAL = "nome_regional";

    public static final String CAMPO_PDV = "pdv";
    public static final String CAMPO_MATRICULA = "matricula";
    public static final String CAMPO_FLAG_PRIVILEGIO = "flag_privilegio";
    public static final String CAMPO_BOTIM = "botim";
    public static final String CAMPO_BO_LIBERAJORNADA = "bo_liberajornada";
    public static final String CAMPO_BO_LIBERAODOMETRO = "bo_liberaodometro";
    public static final String CAMPO_NOME = "nome";
    public static final String CAMPO_REFERENCIA = "referencia";
    public static final String CAMPO_FOTO = "foto";
    public static final String CAMPO_EMAIL = "email";
    public static final String CAMPO_IDNOTIFY = "id_notify";
    public static final String CAMPO_LATITUDE_PADRAO = "latitude_padrao";
    public static final String CAMPO_LONGITUDE_PADRAO = "longitude_padrao";
    public static final String CAMPO_ICCID_FUNCIONAL = "iccid_funcional";
    public static final String CAMPO_LATITUDE_PDV = "latitude_pdv";
    public static final String CAMPO_LONGITUDE_PDV = "longitude_pdv";
    public static final String CAMPO_DATA_BD = "data_bd";
    public static final String CAMPO_DATA_NASCIMENTO = "data_nascimento";
    public static final String CAMPO_DATA_ADMISSAO = "data_admissao";
    public static final String CAMPO_DATA_VALIDADE_URA = "data_validadeUra";
    public static final String CAMPO_URA = "ura";
    public static final String CAMPO_BO_PESQUISA = "bo_pesquisa";
    public static final String CAMPO_SEXO = "sexo";
    public static final String CAMPO_RG = "rg";
    public static final String CAMPO_TRAVAR_VENDA = "bo_travar_venda";
    public static final String CAMPO_FLAG_DOC_CONTROLE = "flag_doc_controle";
    public static final String CAMPO_FG_TIPO = "fgtipo";
    public static final String CAMPO_BO_MONITORAMENTO = "bo_monitoramento";
    public static final String CAMPO_DATA_INICIO_MONITORAMENTO = "data_inicio_monitoramento";
    public static final String CAMPO_DATA_FINAL_MONITORAMENTO = "data_final_monitoramento";
    public static final String CAMPO_BO_LIBERA_ODOMETRO = "bo_libera_odometro";

    private int id;
    private int dddCidade;
    private String cpf;
    private int idAgencia;
    private String nomeAgencia;
    private String nomeRegional;
    private int pdv;
    private int matricula;
    private int flagPrivilegio;
    private int boTim;
    private int boLiberaJornada;
    private int boLiberaOdometro;
    private String nome;
    private String referencia;
    private String foto;
    private String email;
    private String idNotify;
    private String latitudePadrao;
    private String longitudePadrao;
    private String iccidFuncional;
    private String latitudePDV;
    private String longitudePDV;
    private String dataBD;
    private String dataNascimento;
    private String dataValidadeUra;
    private String dataAdmissao;
    private String URA;
    private int boPesquisa;
    private int sexo;
    private String rg;
    private int boTravaVenda;
    private int flagDocControle;
    private int fgTipo;
    private int boMonitoramento;

    private String dataInicioMonitoramento;
    private String dataFinalMonitoramento;

    public Usuario() {
        this.id = 0;
        this.dddCidade = 0;
        this.cpf = "";
        this.idAgencia = 0;
        this.nomeAgencia = "";
        this.nomeRegional = "";
        this.fgTipo = 0;
        this.pdv = 0;
        this.matricula = 0;
        this.flagPrivilegio = 0;
        this.boTim = 0;
        this.boLiberaJornada = 0;
        this.boLiberaOdometro = 0;
        this.nome = "";
        this.referencia = "";
        this.foto = "";
        this.email = "";
        this.idNotify = "";
        this.latitudePadrao = "";
        this.longitudePadrao = "";
        this.iccidFuncional = "";
        this.latitudePDV = "";
        this.longitudePDV = "";
        this.dataBD = "";
        this.dataNascimento = "";
        this.dataValidadeUra = "";
        this.URA = "";
        this.boPesquisa = 0;
        this.sexo = 0;
        this.rg = "";
        this.boTravaVenda = 0;
        this.flagDocControle = 0;
        this.boMonitoramento = 0;

    }

    public Usuario(Cursor c) {
        this.setId(c.getInt(c.getColumnIndex(CAMPO_ID)));
        this.setCpf(c.getString(c.getColumnIndex(CAMPO_CPF)));
        this.setDddCidade(c.getInt(c.getColumnIndex(CAMPO_DDD_CIDADE)));
        this.setIdAgencia(c.getInt(c.getColumnIndex(CAMPO_ID_AGENCIA)));
        this.setNomeAgencia(c.getString(c.getColumnIndex(CAMPO_NOME_AGENCIA)));
        this.setPdv(c.getInt(c.getColumnIndex(CAMPO_PDV)));
        this.setNomeRegional(c.getString(c.getColumnIndex(CAMPO_NOME_REGIONAL)));
        this.setMatricula(c.getInt(c.getColumnIndex(CAMPO_MATRICULA)));
        this.setBoTim(c.getInt(c.getColumnIndex(CAMPO_BOTIM)));
        this.setBoLiberaJornada(c.getInt(c.getColumnIndex(CAMPO_BO_LIBERAJORNADA)));
        this.setBoLiberaOdometro(c.getInt(c.getColumnIndex(CAMPO_BO_LIBERAODOMETRO)));
        this.setNome(c.getString(c.getColumnIndex(CAMPO_NOME)));
        this.setReferencia(c.getString(c.getColumnIndex(CAMPO_REFERENCIA)));
        this.setFoto(c.getString(c.getColumnIndex(CAMPO_FOTO)));
        this.setEmail(c.getString(c.getColumnIndex(CAMPO_EMAIL)));
        this.setIdNotify(c.getString(c.getColumnIndex(CAMPO_IDNOTIFY)));
        this.setLatitudePadrao(c.getString(c.getColumnIndex(CAMPO_LATITUDE_PADRAO)));
        this.setLongitudePadrao(c.getString(c.getColumnIndex(CAMPO_LONGITUDE_PADRAO)));
        this.setIccidFuncional(c.getString(c.getColumnIndex(CAMPO_ICCID_FUNCIONAL)));
        this.setLatitudePDV(c.getString(c.getColumnIndex(CAMPO_LATITUDE_PDV)));
        this.setLongitudePDV(c.getString(c.getColumnIndex(CAMPO_LONGITUDE_PDV)));
        this.setDataBD(c.getString(c.getColumnIndex(CAMPO_DATA_BD)));
        this.setDataNascimento(c.getString(c.getColumnIndex(CAMPO_DATA_NASCIMENTO)));
        this.setDataAdmissao(c.getString(c.getColumnIndex(CAMPO_DATA_ADMISSAO)));
        this.setBoPesquisa(c.getInt(c.getColumnIndex(CAMPO_BO_PESQUISA)));
        this.setRg(c.getString(c.getColumnIndex(CAMPO_RG)));
        this.setSexo(c.getInt(c.getColumnIndex(CAMPO_SEXO)));

        this.setFlagDocControle(c.getInt(c.getColumnIndex(CAMPO_FLAG_DOC_CONTROLE)));
        this.setFlagDocControle(c.getInt(c.getColumnIndex(CAMPO_FLAG_DOC_CONTROLE)));
        this.setFlagPrivilegio(c.getInt(c.getColumnIndex(CAMPO_FLAG_PRIVILEGIO)));
        this.setFgTipo(c.getInt(c.getColumnIndex(CAMPO_FG_TIPO)));
        this.setDataInicioMonitoramento(c.getString(c.getColumnIndex(CAMPO_DATA_INICIO_MONITORAMENTO)));
        this.setDataFinalMonitoramento(c.getString(c.getColumnIndex(CAMPO_DATA_FINAL_MONITORAMENTO)));
        this.setBoMonitoramento(c.getInt(c.getColumnIndex(CAMPO_BO_MONITORAMENTO)));
    }

    public Usuario(JSONObject jsonUsuario) {
        this.setId(jsonUsuario.optInt("s01_cdid"));
        this.setCpf(jsonUsuario.optString("s01_txcpf"));
        this.setEmail(jsonUsuario.optString("s01_txemail"));
        this.setDddCidade(jsonUsuario.optInt("d01_nrddd"));
        this.setIdAgencia(jsonUsuario.optInt("d05_cdid"));
        this.setNomeAgencia(jsonUsuario.optString("d05_nmdescricao"));
        this.setPdv(jsonUsuario.optInt("d03_cdid"));
        this.setNomeRegional(jsonUsuario.optString("d02_nmdescricao"));
        this.setMatricula(jsonUsuario.optInt("s01_nrmatriculaparceiro"));
        this.setFlagPrivilegio(jsonUsuario.optInt("s02_fgprivilegio"));
        this.setBoTim(jsonUsuario.optInt("botim"));
        this.setBoLiberaJornada(jsonUsuario.optInt("s01_boliberaJornada"));
        this.setBoLiberaOdometro(jsonUsuario.optInt("s01_boLiberaOdometro"));
        this.setNome(jsonUsuario.optString("s02_nmdescricao"));
        this.setReferencia(jsonUsuario.optString("d05_nrreferencia"));
        this.setFoto(jsonUsuario.optString("r01_txfoto"));
        this.setIdNotify(jsonUsuario.optString("r01_idnotify"));
        this.setLatitudePadrao(jsonUsuario.optString("d02_latitude"));
        this.setLongitudePadrao(jsonUsuario.optString("d02_longitude"));
        this.setLatitudePDV(jsonUsuario.optString("d03_latitude"));
        this.setLongitudePDV(jsonUsuario.optString("d03_longitude"));
        this.setDataBD(jsonUsuario.optString("dataBD"));
        this.setDataNascimento(jsonUsuario.optString("dataNascimento"));
        this.setDataAdmissao(jsonUsuario.optString("s01_dtadmissao"));
        this.setBoPesquisa(jsonUsuario.optInt("d05_boverPesquisa"));
        this.setRg(jsonUsuario.optString("s01_txidentidade"));
        this.setBoTravaVenda(jsonUsuario.optInt("boTravaVenda"));

        this.setFlagDocControle(jsonUsuario.optInt("d05_boDoctoControle"));

        this.setFlagDocControle(jsonUsuario.optInt("d05_boDoctoControle"));
        this.setFgTipo(jsonUsuario.optInt("s02_fgtipo"));
        this.setDataInicioMonitoramento(jsonUsuario.optString("d05_horaInicioMonitoramento"));
        this.setDataFinalMonitoramento(jsonUsuario.optString("d05_horafinalMonitoramento"));
        this.setBoMonitoramento(jsonUsuario.optInt("d05_boMonitoramento"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDddCidade() {
        return dddCidade;
    }

    public void setDddCidade(int dddCidade) {
        this.dddCidade = dddCidade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getIdAgencia() {
        return idAgencia;
    }

    public void setIdAgencia(int idAgencia) {
        this.idAgencia = idAgencia;
    }

    public String getNomeAgencia() {
        return nomeAgencia;
    }

    public void setNomeAgencia(String nomeAgencia) {
        this.nomeAgencia = nomeAgencia;
    }

    public String getNomeRegional() {
        return nomeRegional;
    }

    public void setNomeRegional(String nomeRegional) {
        this.nomeRegional = nomeRegional;
    }

    public int getPdv() {
        return pdv;
    }

    public void setPdv(int pdv) {
        this.pdv = pdv;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public int getBoTim() {
        return boTim;
    }

    public void setBoTim(int boTim) {
        this.boTim = boTim;
    }

    public int getBoLiberaJornada() {
        return boLiberaJornada;
    }

    public void setBoLiberaJornada(int boLiberaJornada) {
        this.boLiberaJornada = boLiberaJornada;
    }

    public int getBoLiberaOdometro() {
        return boLiberaOdometro;
    }

    public void setBoLiberaOdometro(int boLiberaOdometro) {
        this.boLiberaOdometro = boLiberaOdometro;
    }

    public int getFgTipo() {
        return fgTipo;
    }

    public void setFgTipo(int fgTipo) {
        this.fgTipo = fgTipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdNotify() {
        return idNotify;
    }

    public void setIdNotify(String idNotify) {
        this.idNotify = idNotify;
    }

    public String getLatitudePadrao() {
        return latitudePadrao;
    }

    public void setLatitudePadrao(String latitudePadrao) {
        this.latitudePadrao = latitudePadrao;
    }

    public String getLongitudePadrao() {
        return longitudePadrao;
    }

    public void setLongitudePadrao(String longitudePadrao) {
        this.longitudePadrao = longitudePadrao;
    }

    public String getIccidFuncional() {
        return iccidFuncional;
    }

    public void setIccidFuncional(String iccidFuncional) {
        this.iccidFuncional = iccidFuncional;
    }

    public String getLatitudePDV() {
        return latitudePDV;
    }

    public void setLatitudePDV(String latitudePDV) {
        this.latitudePDV = latitudePDV;
    }

    public String getLongitudePDV() {
        return longitudePDV;
    }

    public void setLongitudePDV(String longitudePDV) {
        this.longitudePDV = longitudePDV;
    }

    public String getDataBD() {
        return dataBD;
    }

    public void setDataBD(String dataBD) {
        this.dataBD = dataBD;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getDataValidadeUra() {
        return dataValidadeUra;
    }

    public void setDataValidadeUra(String dataValidadeUra) {
        this.dataValidadeUra = dataValidadeUra;
    }

    public String getURA() {
        return URA;
    }

    public void setURA(String URA) {
        this.URA = URA;
    }

    public int getBoPesquisa() {
        return boPesquisa;
    }

    public void setBoPesquisa(int boPesquisa) {
        this.boPesquisa = boPesquisa;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public int getBoTravaVenda() {
        return boTravaVenda;
    }

    public void setBoTravaVenda(int boTravaVenda) {
        this.boTravaVenda = boTravaVenda;
    }

    public String getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(String dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public int getFlagDocControle() {
        return flagDocControle;
    }

    public void setFlagDocControle(int flagDocControle) {
        this.flagDocControle = flagDocControle;

    }

//    public int getFlagAutenticado() {
//        return flagAutenticado;
//    }
//
//    public void setFlagAutenticado(int flagAutenticado) {
//        this.flagAutenticado = flagAutenticado;
//    }

    public int getFlagPrivilegio() {
        return flagPrivilegio;
    }

    public void setFlagPrivilegio(int flagPrivilegio) {
        this.flagPrivilegio = flagPrivilegio;
    }

    public String getMatriculaNome() {
        return String.format("%s - %s", this.matricula, this.nome);
    }

    public String getDataInicioMonitoramento() {
        return dataInicioMonitoramento;
    }

    public void setDataInicioMonitoramento(String dataInicioMonitoramento) {
        this.dataInicioMonitoramento = dataInicioMonitoramento;
    }

    public String getDataFinalMonitoramento() {
        return dataFinalMonitoramento;
    }

    public void setDataFinalMonitoramento(String dataFinalMonitoramento) {
        this.dataFinalMonitoramento = dataFinalMonitoramento;
    }

    public int getBoMonitoramento() {

        return boMonitoramento;
    }

    public void setBoMonitoramento(int boMonitoramento) {
        this.boMonitoramento = boMonitoramento;
    }


    public ContentValues getContetValues() {
        ContentValues valores = new ContentValues();
        valores.put(Usuario.CAMPO_ID, getId());
        valores.put(Usuario.CAMPO_CPF, getCpf());
        valores.put(Usuario.CAMPO_DDD_CIDADE, getDddCidade());
        valores.put(Usuario.CAMPO_ID_AGENCIA, getIdAgencia());
        valores.put(Usuario.CAMPO_NOME_AGENCIA, getNomeAgencia());
        valores.put(Usuario.CAMPO_NOME_REGIONAL, getNomeRegional());
        valores.put(Usuario.CAMPO_PDV, getPdv());
        valores.put(Usuario.CAMPO_MATRICULA, getMatricula());
        valores.put(Usuario.CAMPO_FLAG_PRIVILEGIO, getFlagPrivilegio());
        valores.put(Usuario.CAMPO_BOTIM, getBoTim());
        valores.put(Usuario.CAMPO_NOME, getNome());
        valores.put(Usuario.CAMPO_BO_LIBERAJORNADA, getBoLiberaJornada());
        valores.put(Usuario.CAMPO_BO_LIBERAODOMETRO, getBoLiberaOdometro());
        valores.put(Usuario.CAMPO_REFERENCIA, getReferencia());
        valores.put(Usuario.CAMPO_FOTO, getFoto());
        valores.put(Usuario.CAMPO_EMAIL, getEmail());
        valores.put(Usuario.CAMPO_IDNOTIFY, getIdNotify());
        valores.put(Usuario.CAMPO_LATITUDE_PADRAO, getLatitudePadrao());
        valores.put(Usuario.CAMPO_LONGITUDE_PADRAO, getLongitudePadrao());
        valores.put(Usuario.CAMPO_ICCID_FUNCIONAL, getIccidFuncional());
        valores.put(Usuario.CAMPO_LATITUDE_PDV, getLatitudePDV());
        valores.put(Usuario.CAMPO_LONGITUDE_PDV, getLongitudePDV());
        valores.put(Usuario.CAMPO_DATA_BD, getDataBD());
        valores.put(Usuario.CAMPO_DATA_NASCIMENTO, getDataNascimento());
        valores.put(Usuario.CAMPO_DATA_ADMISSAO, getDataAdmissao());
        valores.put(Usuario.CAMPO_DATA_VALIDADE_URA, getDataValidadeUra());
        valores.put(Usuario.CAMPO_URA, getURA());
        valores.put(Usuario.CAMPO_BO_PESQUISA, getBoPesquisa());
        valores.put(Usuario.CAMPO_SEXO, getSexo());
        valores.put(Usuario.CAMPO_RG, getRg());
        valores.put(Usuario.CAMPO_TRAVAR_VENDA, getBoTravaVenda());

        valores.put(Usuario.CAMPO_FLAG_DOC_CONTROLE, getFlagDocControle());

        valores.put(Usuario.CAMPO_FG_TIPO, getFgTipo());
        valores.put(Usuario.CAMPO_DATA_INICIO_MONITORAMENTO, getDataInicioMonitoramento());
        valores.put(Usuario.CAMPO_DATA_FINAL_MONITORAMENTO, getDataFinalMonitoramento());
        //valores.put(Usuario.CAMPO_BO_AUTENTICADO, getFlagAutenticado());
        valores.put(Usuario.CAMPO_BO_MONITORAMENTO, getBoMonitoramento());
        return valores;
    }


    public ContentValues getContetValuesMeusDados() {
        ContentValues valores = new ContentValues();
        valores.put(Usuario.CAMPO_EMAIL, getEmail());
        //valores.put(Usuario.CAMPO_FOTO, getFoto());
        valores.put(Usuario.CAMPO_DATA_NASCIMENTO, getDataNascimento());
        valores.put(Usuario.CAMPO_SEXO, getSexo());
        valores.put(Usuario.CAMPO_RG, getRg());
        valores.put(Usuario.CAMPO_FOTO, getFoto());
        return valores;
    }

    public JSONObject getLogUsuario() throws JSONException {
        JSONObject params = new JSONObject();
        params.put("idUsuario", getId());
        params.put("idAgencia", getIdAgencia());
        params.put("cpf", getCpf());
        params.put("matricula", getMatricula());
        params.put("email", getEmail());
        params.put("idNotify", getIdNotify());
        return params;
    }

    public void getLogUsuarioGeral(JSONObject params) throws JSONException {
        params.put("idUsuario", getId());
        params.put("idAgencia", getIdAgencia());
        params.put("cpf", getCpf());
        params.put("fgPrivilegio", getFlagPrivilegio());
        params.put("matricula", getMatricula());
        params.put("email", getEmail());
        params.put("idNotify", getIdNotify());
    }

}
