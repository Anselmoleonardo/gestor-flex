package br.com.gestorflex.model;

import org.json.JSONObject;

import java.io.Serializable;

public class Cabecalho implements Serializable {

    private String preCabecalho;
    private String recarga;
    private String ctrl;
    private String ctrlAv;
    private String pos;
    private String meta;
    private String vendas;
    private String ranking;

    public Cabecalho(){
        this.preCabecalho = "";
        this.recarga = "";
        this.ctrl = "";
        this.ctrlAv = "";
        this.pos = "";
        this.meta = "";
        this.vendas = "";
        this.ranking = "";
    }

    public Cabecalho(JSONObject jsonObject) {
        this.preCabecalho = jsonObject.optString("PRE_CABECALHO");
        this.recarga = jsonObject.optString("RECARGA_CABECALHO");
        this.ctrl = jsonObject.optString("CTRL_CABECALHO");
        this.ctrlAv = jsonObject.optString("CTRL_AV_CABECALHO");
        this.pos = jsonObject.optString("POS_CABECALHO");
        this.meta = jsonObject.optString("META_CABECALHO");
        this.vendas = jsonObject.optString("VENDAS_CABECALHO");
        this.ranking = jsonObject.optString("RANKING_CABECALHO");

    }

    public String getPreCabecalho() {
        return preCabecalho;
    }

    public void setPreCabecalho(String preCabecalho) {
        this.preCabecalho = preCabecalho;
    }

    public String getRecarga() {
        return recarga;
    }

    public void setRecarga(String recarga) {
        this.recarga = recarga;
    }

    public String getCtrl() {
        return ctrl;
    }

    public void setCtrl(String ctrl) {
        this.ctrl = ctrl;
    }

    public String getCtrlAv() {
        return ctrlAv;
    }

    public void setCtrlAv(String ctrlAv) {
        this.ctrlAv = ctrlAv;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public String getVendas() {
        return vendas;
    }

    public void setVendas(String vendas) {
        this.vendas = vendas;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }
}
