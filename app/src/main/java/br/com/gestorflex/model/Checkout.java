package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

public class Checkout {

    public static final String TABELA = "checkout";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_ID_PDV = "id_pdv";
    public static final String CAMPO_DATA_CHECKOUT = "data_checkout";
    public static final String CAMPO_LATITUDE = "latitude";
    public static final String CAMPO_LONGITUDE = "longitude";
    public static final String EH_CHECOUT = "1";
//  public static final String CAMPO_FLAG_TIPO_CHECK = "flag_tipo_check";
//  public static final String CAMPO_IS_SINC = "is_sinc";

    private int id;
    private String idPdv;
    private String dataCheckout;
    private String latitude;
    private String longitude;
//  private int flagTipoCheck;
//  private int isSinc;

    public Checkout() {
        this.id = 0;
        this.idPdv = "";
        this.dataCheckout = "";
        this.latitude = "";
        this.longitude = "";
//    this.flagTipoCheck = 2;
//    this.isSinc = 0;
    }

    public Checkout(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.idPdv = cursor.getString(cursor.getColumnIndex(CAMPO_ID_PDV));
        this.dataCheckout = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_CHECKOUT));
        this.latitude = cursor.getString(cursor.getColumnIndex(CAMPO_LATITUDE));
        this.longitude = cursor.getString(cursor.getColumnIndex(CAMPO_LONGITUDE));
//    this.flagTipoCheck = cursor.getInt(cursor.getColumnIndex(CAMPO_FLAG_TIPO_CHECK));
//    this.isSinc = cursor.getInt(cursor.getColumnIndex(CAMPO_IS_SINC));
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Checkout(String idPdv, String dataCheckout) {
        this.idPdv = idPdv;
        this.dataCheckout = dataCheckout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdPdv() {
        return idPdv;
    }

    public String getDataCheckout() {
        return dataCheckout;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

//  public int getFlagTipoCheck() {
//    return flagTipoCheck;
//  }
//
//  public void setFlagTipoCheck(int flagTipoCheck) {
//    this.flagTipoCheck = flagTipoCheck;
//  }
//
//  public int getIsSinc() {
//    return isSinc;
//  }
//
//  public void setIsSinc(int isSinc) {
//    this.isSinc = isSinc;
//  }

    public JSONObject pegarJson() throws JSONException {
        JSONObject jsonParametros = new JSONObject();
        jsonParametros.put("ehCheckout", EH_CHECOUT);
        jsonParametros.put("idPdv", this.idPdv);
        jsonParametros.put("dataResposta", this.dataCheckout);
        jsonParametros.put("latitude", this.latitude);
        jsonParametros.put("longitude", this.longitude);
        return jsonParametros;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(CAMPO_ID_PDV, getIdPdv());
        values.put(CAMPO_DATA_CHECKOUT, getDataCheckout());
        values.put(CAMPO_LATITUDE, getLatitude());
        values.put(CAMPO_LONGITUDE , getLongitude());
        return values;
    }

}
