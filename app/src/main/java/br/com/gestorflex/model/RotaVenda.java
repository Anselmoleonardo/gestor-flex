package br.com.gestorflex.model;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import org.json.JSONObject;

import java.io.Serializable;

public class RotaVenda implements Serializable {

    private String nomeFantasia;
    private String endereco;
    private String numero;
    private String bairro;
    private String telefone;
    private double latitude;
    private double longitude;
    private int aceite;

    public RotaVenda(JSONObject jsonObject) {
        this.nomeFantasia = jsonObject.optString("d03_txcomercialFantasia");
        this.endereco = jsonObject.optString("d03_nmendereco");
        this.numero = jsonObject.optString("d03_nrendereco");
        this.bairro = jsonObject.optString("d03_bairro");
        this.telefone = jsonObject.optString("d03_nrtelefone");
        this.latitude = jsonObject.optDouble("d03_latitude");
        this.longitude = jsonObject.optDouble("d03_longitude");
        this.aceite = jsonObject.optInt("d03_cdaceitevenda");
    }

    public String getEndereco() {
        return endereco;
    }

    public String getNumero() {
        return numero;
    }

    public String getBairro() {
        return bairro;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getAceite() {
        return aceite;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getEnderecoCompleto() {
        String enderecoCompleto = "Endereço: ";
        if (!this.endereco.equals(""))
            enderecoCompleto += this.endereco;
        if (!this.numero.equals(""))
            enderecoCompleto += ", " + this.numero;
        if (!this.bairro.equals(""))
            enderecoCompleto += " - " + this.bairro;
        if (!this.telefone.equals(""))
            enderecoCompleto += "\nTelefone: " + this.telefone;
        if (enderecoCompleto.equals("Endereço: "))
            return "Endereço indisponível";
        return enderecoCompleto;
    }

    public BitmapDescriptor getMarker() {
        if (this.aceite == 1)
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
        else
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
    }

}
