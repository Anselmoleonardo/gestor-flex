package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Agencia {

    public static final String TABELA = "agencia";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DESCRICAO = "descricao";

    private int id;
    private String descricao;

    public Agencia() {
        this.id = 0;
        this.descricao = "";
    }

    public Agencia(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;

    }

    public Agencia(JSONObject jsonAgencia) {
        this.id = jsonAgencia.optInt("d05_dcid");
        this.descricao = jsonAgencia.optString("d05_nmdescricao");

    }

    public Agencia(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));

    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_DESCRICAO, getDescricao());

        return valores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
