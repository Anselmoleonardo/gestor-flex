package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Produto {

    public static final String TABELA = "produto";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DESCRICAO = "descricao";
    public static final String CAMPO_ID_AGENCIA = "id_agencia";

    private int id;
    private String descricao;
    private  int idAgencia=0;

    public Produto() {
        this.id = 0;
        this.descricao = "";
        this.idAgencia=0;
    }

    public Produto(int id, String descricao, int idAgencia) {
        this.id = id;
        this.descricao = descricao;
        this.idAgencia= idAgencia;

    }

    public Produto(Cursor c) {
        this.setId(c.getInt(c.getColumnIndex(CAMPO_ID)));
        this.setDescricao(c.getString(c.getColumnIndex(CAMPO_DESCRICAO)));
        this.setIdAgencia(c.getInt(c.getColumnIndex(CAMPO_ID_AGENCIA)));
    }

    public Produto(JSONObject jsonProduto){
        this.setId(jsonProduto.optInt("d07_cdid"));
        this.setDescricao(jsonProduto.optString("d07_nmdescricao"));
        this.setIdAgencia(jsonProduto.optInt("d05_cdid"));

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getIdAgencia() {
        return idAgencia;
    }

    public void setIdAgencia(int idAgencia) {
        this.idAgencia = idAgencia;
    }

    @Override
    public String toString() {
        return getDescricao();
    }

    public ContentValues getContetValues() {
        ContentValues valores = new ContentValues();
        valores.put(Produto.CAMPO_ID, getId());
        valores.put(Produto.CAMPO_DESCRICAO, getDescricao());
        valores.put(Produto.CAMPO_ID_AGENCIA, getIdAgencia());
        return valores;
        }


}
