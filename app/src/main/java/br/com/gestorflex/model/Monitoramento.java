package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

public class Monitoramento {

    public static String TABELA = "monitoramento";
    public static String CAMPO_ID = "_id";
    public static String CAMPO_LATITUDE = "latitude";
    public static String CAMPO_LONGITUDE = "longitude";
    public static String CAMPO_DATA_HORA = "data_hora";

    private int id;
    private String latitude;
    private String longitude;
    private String dataHora;

    public Monitoramento(String latitude, String longitude, String dataHora) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.dataHora = dataHora;
    }

    public Monitoramento(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.latitude = cursor.getString(cursor.getColumnIndex(CAMPO_LATITUDE));
        this.longitude = cursor.getString(cursor.getColumnIndex(CAMPO_LONGITUDE));
        this.dataHora = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_HORA));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDataHora() {
        return dataHora;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_LATITUDE, getLatitude());
        valores.put(CAMPO_LONGITUDE, getLongitude());
        valores.put(CAMPO_DATA_HORA, getDataHora());
        return valores;
    }

}

