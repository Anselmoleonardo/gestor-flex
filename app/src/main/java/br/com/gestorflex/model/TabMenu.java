package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class TabMenu {

    public static final String TABELA = "SituacaoFuncional";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NOME = "nome";
    public static final String CAMPO_TAG = "tag";

    private int id;
    private String nome;
    private String tag;

    public TabMenu(){
        this.id = 0;
        this.nome = "";
        this.tag = "";
    }

    public TabMenu(int id, String nome, String tag) {
        this.id = id;
        this.nome = nome;
        this.tag = tag;
    }

    public TabMenu(JSONObject jsonTab) {
        this.id = jsonTab.optInt("id");
        this.nome = jsonTab.optString("nome");
        this.tag = jsonTab.optString("tag");
    }

    public TabMenu(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.nome = cursor.getString(cursor.getColumnIndex(CAMPO_NOME));
        this.tag = cursor.getString(cursor.getColumnIndex(CAMPO_TAG));
    }


    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getTag() {
        return tag;
    }

    public ContentValues getContentValues(){
        ContentValues valores = new ContentValues();
        valores.put("id",getId());
        valores.put("nome",getNome());
        valores.put("tag", getTag());

        return valores;
    }
}

