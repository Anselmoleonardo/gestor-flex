package br.com.gestorflex.model;

import org.json.JSONObject;

import java.io.Serializable;

public class UsuarioOfensor implements Serializable {

    private String nome;
    private String dataAdmissao;
    private String ddd;
    private String vendas;
    private String matricula;
    private String telefone;
    private String nomeSupervisor;

    public UsuarioOfensor(JSONObject jsonObject) {
        this.nome = jsonObject.optString("NOME");
        this.dataAdmissao = jsonObject.optString("ADMISSAO");
        this.ddd = jsonObject.optString("DDD");
        this.vendas = jsonObject.optString("VENDAS");
        this.matricula = jsonObject.optString("MATRICULA");
        this.telefone = jsonObject.optString("TELEFONE");
        this.nomeSupervisor = jsonObject.optString("SUPERVISOR");
    }

    public String getNome() {
        return nome;
    }

    public String getDataAdmissao() {
        return dataAdmissao;
    }

    public String getDdd() {
        return ddd;
    }

    public String getVendas() {
        return vendas;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getNomeSupervisor() {
        return nomeSupervisor;
    }
}
