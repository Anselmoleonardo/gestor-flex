package br.com.gestorflex.model;

import org.json.JSONObject;

import java.io.Serializable;

public class Cliente implements Serializable {

    private String idPedido;
    private String data;
    private String oferta;
    private String cnaeramo;
    private String cliente;
    private String motivo;
    private String idCliente;
    private String telefone;
    private String celular;
    private String situacao;
    private String qtdTecnologias;
    private String consultor;
    private int aceite;

    public Cliente(JSONObject jsonObject) {
        this.data = jsonObject.optString("data");
        this.cliente = jsonObject.optString("cliente");
        this.aceite = jsonObject.optInt("boAceite");
        this.consultor = jsonObject.optString("consultor");
        if (aceite == 0) {
            this.oferta = jsonObject.optString("oferta");
            this.cnaeramo = jsonObject.optString("cnaeramo");
            this.idPedido = jsonObject.optString("idpedido");
            this.telefone = jsonObject.optString("telefone");
            this.celular = jsonObject.optString("celular");
            this.situacao = jsonObject.optString("situacao");
            this.qtdTecnologias = jsonObject.optString("qtdTecnologias");
        } else {
            this.idCliente = jsonObject.optString("idcliente");
            this.motivo = jsonObject.optString("motivo");
            this.situacao = "VENDA REJEITADA";
        }
    }

    public String getIdPedido() {
        return idPedido;
    }

    public String getData() {
        return data;
    }

    public String getOferta() {
        return oferta;
    }

    public String getCnaeramo() {
        return cnaeramo;
    }

    public String getCliente() {
        return cliente;
    }

    public String getMotivo() {
        return motivo;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCelular() {
        return celular;
    }

    public String getSituacao() {
        return situacao;
    }

    public String getQtdTecnologias() {
        return qtdTecnologias;
    }

    public int getAceite() {
        return aceite;
    }

    public String getConsultor() {
        return consultor;
    }
}
