package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import br.com.gestorflex.util.DateUtil;

public class RespostaPergunta {

    public static final String TABELA = "respota_pergunta";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DATA_ATUAL = "dtAtual";
    public static final String CAMPO_DESCRICAO = "descricaoResposta";
    public static final String CAMPO_ID_PERGUNTA = "idPergunta";
    public static final String CAMPO_DATA_SINC = "dataSinc";
    public static final String CAMPO_ID_PDV = "idPdv";
    public static final String CAMPO_LATITUDE = "latitude";
    public static final String CAMPO_LONGITUDE = "longitude";
    public static final String CAMPO_PERGUNTAS_CONCATENADAS = "perguntas_concatenadas";
    public static final String CAMPO_PDVS_CONCATENADOS = "pdvs_concatenados";

    private static final String NAO_EH_CHECKOUT = "0";
    private static final int APENAS_PERGUNTA_PDV = 1;
    private static final int NAO_EH_SINCRONIZACAO = 0;

    private int id;
    private String dtAtual;
    private String descricaoResposta;
    private int idPergunta;
    private String dataSinc;
    private int idPdv;
    private String latitude;
    private String longitude;
    private String idPerguntasConcatenadas;
    private String idPdvsConcatenados;

    public RespostaPergunta() {
        this.id = 0;
        this.dtAtual = "";
        this.descricaoResposta = "";
        this.idPergunta = 0;
        this.dataSinc = "";
        this.idPdv = 0;
        this.latitude = "";
        this.longitude = "";
        this.idPerguntasConcatenadas = "";
        this.idPdvsConcatenados = "";
    }

    public RespostaPergunta(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.dtAtual = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_ATUAL));
        this.descricaoResposta = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
        this.idPergunta = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_PERGUNTA));
        this.dataSinc = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_SINC));
        this.idPdv = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_PDV));
        this.latitude = cursor.getString(cursor.getColumnIndex(CAMPO_LATITUDE));
        this.longitude = cursor.getString(cursor.getColumnIndex(CAMPO_LONGITUDE));
        this.idPerguntasConcatenadas = cursor.getString(cursor.getColumnIndex(CAMPO_PERGUNTAS_CONCATENADAS));
        this.idPdvsConcatenados = cursor.getString(cursor.getColumnIndex(CAMPO_PDVS_CONCATENADOS));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDtAtual() {
        return dtAtual;
    }

    public void setDtAtual(String dtAtual) {
        this.dtAtual = dtAtual;
    }

    public int getIdPergunta() {
        return idPergunta;
    }

    public void setIdPergunta(int idPergunta) {
        this.idPergunta = idPergunta;
    }

    public String getDescricaoResposta() {
        return descricaoResposta;
    }

    public void setDescricaoResposta(String descricaoResposta) {
        this.descricaoResposta = descricaoResposta;
    }

    public String getDataSinc() {
        return dataSinc;
    }

    public void setDataSinc(String dataSinc) {
        this.dataSinc = dataSinc;
    }

    public int getIdPdv() {
        return idPdv;
    }

    public void setIdPdv(int idPdv) {
        this.idPdv = idPdv;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIdPerguntasConcatenadas() {
        return idPerguntasConcatenadas;
    }

    public void setIdPerguntasConcatenadas(String idPerguntasConcatenadas) {
        this.idPerguntasConcatenadas = idPerguntasConcatenadas;
    }

    public String getIdPdvsConcatenados() {
        return idPdvsConcatenados;
    }

    public void setIdPdvsConcatenados(String idPdvsConcatenados) {
        this.idPdvsConcatenados = idPdvsConcatenados;
    }

    public static JSONObject getLogResposta(List<RespostaPergunta> respostasDoUsuario) throws JSONException {
        JSONObject params = new JSONObject();
        String idResposta = "";
        String idPdv = "";
        String txResposta = "";
        String dataHora = "";
        int quantidadeRespostas = respostasDoUsuario.size();
        for (int i = 0; i < quantidadeRespostas; i++) {
            RespostaPergunta resposta = respostasDoUsuario.get(i);
            idResposta = idResposta + resposta.getId() + "|";
            txResposta = txResposta + resposta.getDescricaoResposta() + "|";
            dataHora = dataHora + resposta.getDtAtual() + "|";
        }
        params.put("idResposta", idResposta);
        params.put("txResposta", txResposta);
        params.put("dataResposta", dataHora);
        return params;
    }

    public static RespostaPergunta concatenarRespostas(List<RespostaPergunta> respostasDoUsuario) {
        RespostaPergunta respostaPergunta = new RespostaPergunta();
        String idPdv =(respostasDoUsuario.get(respostasDoUsuario.size() - 1).getDescricaoResposta().split("-"))[0].trim();
        respostaPergunta.setIdPdv(Integer.parseInt(idPdv.replace("(B)", "").trim()));
        for (int i = 0; i < respostasDoUsuario.size() - 1; i++) {
            RespostaPergunta resposta = respostasDoUsuario.get(i);
            respostaPergunta.idPerguntasConcatenadas += resposta.getId() + "|";
            respostaPergunta.idPdvsConcatenados += idPdv + "|";
            respostaPergunta.descricaoResposta += resposta.getDescricaoResposta() + "|";
            respostaPergunta.dtAtual += resposta.getDtAtual() + "|";
        }
        respostaPergunta.setLatitude(Geolocalizacao.getLatitude() + "");
        respostaPergunta.setLongitude(Geolocalizacao.getLongitude() + "");
        return respostaPergunta;
    }

    public JSONObject getLogRespostaOdomentro() throws JSONException {
        JSONObject jsonParametros = new JSONObject();
        jsonParametros.put("idResposta", this.idPerguntasConcatenadas);
        jsonParametros.put("idPdv", this.idPdv);
        jsonParametros.put("txResposta", this.descricaoResposta);
        jsonParametros.put("dataResposta", this.dtAtual);
        jsonParametros.put("ehCheckout", NAO_EH_CHECKOUT);
        jsonParametros.put("latitude", this.latitude);
        jsonParametros.put("longitude", this.longitude);
        return jsonParametros;
    }

    public static void converteParaJson(Cursor cursor, JSONObject jsonParametros) throws JSONException {
        String idResposta = "";
        String idPdvResposta = "";
        String txResposta = "";
        String dataResposta = "";
        String latitude = "";
        String longitude = "";
        while (cursor.moveToNext()) {
            idResposta = idResposta + cursor.getInt(cursor.getColumnIndex(CAMPO_ID)) + "|";
            idPdvResposta = idPdvResposta + cursor.getInt(cursor.getColumnIndex(CAMPO_ID_PDV)) + "|";
            txResposta = txResposta + cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO)) + "|";
            dataResposta = dataResposta + cursor.getString(cursor.getColumnIndex(CAMPO_DATA_ATUAL)) + "|";
            latitude = "";
            longitude = "";
        }
        jsonParametros.put("idResposta", idResposta);
        jsonParametros.put("idPdv", idPdvResposta);
        jsonParametros.put("txResposta", txResposta);
        jsonParametros.put("dataResposta", dataResposta);
        jsonParametros.put("latitude", latitude);
        jsonParametros.put("longitude", longitude);
    }

    public ContentValues getContentValues() {
        try {
            ContentValues dados = new ContentValues();
            dados.put(CAMPO_ID_PDV, this.getIdPdv());
            dados.put(CAMPO_ID_PERGUNTA, this.getIdPergunta());
            dados.put(CAMPO_DATA_ATUAL, this.getDtAtual());
            dados.put(CAMPO_DESCRICAO, this.getDescricaoResposta());
            dados.put(CAMPO_LATITUDE, this.getLatitude());
            dados.put(CAMPO_LONGITUDE, this.getLongitude());
            dados.put(CAMPO_DATA_SINC, DateUtil.getDateTimeAsString("yyyy-MM-dd HH:mm:ss"));
            dados.put(CAMPO_PDVS_CONCATENADOS, this.getIdPdvsConcatenados());
            dados.put(CAMPO_PERGUNTAS_CONCATENADAS, this.getIdPerguntasConcatenadas());
            return dados;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }
}
