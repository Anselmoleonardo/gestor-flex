package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class CheckInOut {

    public static final int TIPO_CHECK_IN = 2;
    public static final int TIPO_CHECK_OUT = 5;

    public static final String TABELA = "check_in_out";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_ID_PDV = "id_pdv";
    public static final String CAMPO_NOME_PDV = "nome_pdv";
    public static final String CAMPO_DATA = "data";
    public static final String CAMPO_LATITUDE = "latitude";
    public static final String CAMPO_LONGITUDE = "longitude";
    public static final String CAMPO_FLAG_TIPO_CHECK = "flag_tipo_check";
    public static final String CAMPO_IS_SINC = "is_sinc";

    private int id;
    private int idPdv;
    private String nomePdv;
    private String data;
    private String latitude;
    private String longitude;
    private int flagTipoCheck;
    private int isSinc;

    public CheckInOut() {
        this.id = 0;
        this.idPdv = 0;
        this.nomePdv = "";
        this.data = "";
        this.latitude = "";
        this.longitude = "";
        this.flagTipoCheck = 2;
        this.isSinc = 0;
    }

    public CheckInOut(JSONObject jsonObject) {
        this.idPdv = jsonObject.optInt("r22_cdpdvagencia");
        this.nomePdv = jsonObject.optString("d03_nmdescricao");
        this.data = jsonObject.optString("r22_dtatividade");
        this.latitude = jsonObject.optString("r22_txlatitude");
        this.longitude = jsonObject.optString("r22_txlongitude");
        this.flagTipoCheck = jsonObject.optInt("r22_fgtipo");
        this.isSinc = 0;
    }

    public CheckInOut(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.idPdv = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_PDV));
        this.nomePdv = cursor.getString(cursor.getColumnIndex(CAMPO_NOME_PDV));
        this.data = cursor.getString(cursor.getColumnIndex(CAMPO_DATA));
        this.latitude = cursor.getString(cursor.getColumnIndex(CAMPO_LATITUDE));
        this.longitude = cursor.getString(cursor.getColumnIndex(CAMPO_LONGITUDE));
        this.flagTipoCheck = cursor.getInt(cursor.getColumnIndex(CAMPO_FLAG_TIPO_CHECK));
        this.isSinc = cursor.getInt(cursor.getColumnIndex(CAMPO_IS_SINC));
    }

    public CheckInOut(int idPdv, String data) {
        this.idPdv = idPdv;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPdv() {
        return idPdv;
    }

    public void setIdPdv(int idPdv) {
        this.idPdv = idPdv;
    }

    public String getNomePdv() {
        return nomePdv;
    }

    public void setNomePdv(String nomePdv) {
        this.nomePdv = nomePdv;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getFlagTipoCheck() {
        return flagTipoCheck;
    }

    public void setFlagTipoCheck(int flagTipoCheck) {
        this.flagTipoCheck = flagTipoCheck;
    }

    public int getIsSinc() {
        return isSinc;
    }

    public void setIsSinc(int isSinc) {
        this.isSinc = isSinc;
    }

    public JSONObject getJSONParams() throws Exception {
        JSONObject jsonParametros = new JSONObject();
        jsonParametros.put("pdv_rota", getIdPdv());
        jsonParametros.put("latitudeok", getLatitude());
        jsonParametros.put("longitudeok", getLongitude());
        jsonParametros.put("km", 0);
        jsonParametros.put("atividade", getFlagTipoCheck());
        jsonParametros.put("idRota", 0);
        jsonParametros.put("data_visita", getData());
        return jsonParametros;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(CAMPO_ID_PDV, getIdPdv());
        values.put(CAMPO_NOME_PDV, getNomePdv());
        values.put(CAMPO_DATA, getData());
        values.put(CAMPO_FLAG_TIPO_CHECK, getFlagTipoCheck());
        values.put(CAMPO_LATITUDE, getLatitude());
        values.put(CAMPO_LONGITUDE, getLongitude());
        values.put(CAMPO_IS_SINC, getIsSinc());
        return values;
    }

}
