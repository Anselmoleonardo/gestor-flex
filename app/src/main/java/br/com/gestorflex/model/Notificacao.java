package br.com.gestorflex.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.util.DateUtil;

/**
 * Created by Frann on 09/04/2019.
 */

public class Notificacao {

    public static final String TABELA = "notificacao";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_ID_DESTINATARIO = "id_notificacao";
    public static final String CAMPO_DATA_HORA = "data_hora";
    public static final String CAMPO_TIPO = "tipo";
    public static final String CAMPO_TITULO = "titulo";
    public static final String CAMPO_MENSAGEM = "mensagem";
    public static final String CAMPO_URL = "url";
    public static final String CAMPO_CAMINHO_ARQUIVO = "caminho_arquivo";
    public static final String CAMPO_IMAGEM = "imagem";
    public static final String CAMPO_FLAG_NOVA = "flag_nova";
    public static final String CAMPO_FLAG_COMPLETO = "flag_completo";
    public static final String CAMPO_FLAG_LIDO = "flag_lido";
    public static final String CAMPO_FLAG_SINC = "flag_sinc";
    public static String tokenId;

    private int id;
    private int idDestinatario;
    private String dataHora;
    private int tipo;
    private String titulo;
    private String mensagem;
    private String url;
    private String caminhoArquivo;
    private byte[] imagem;
    private int flagNova;
    private int flagCompleto;
    private int flagLido;
    private int flagSinc;

    public Notificacao() {
        this.id = 0;
        this.idDestinatario = 0;
        this.dataHora = "";
        this.tipo = 0;
        this.titulo = "";
        this.mensagem = "";
        this.url = "";
        this.caminhoArquivo = "";
        this.imagem = new byte[0];
        this.flagNova = 0;
        this.flagCompleto = 0;
        this.flagLido = 0;
        this.flagSinc = 0;
    }

    public Notificacao(RemoteMessage remoteMessage) {
//        String[] textoSeparadoOK = remoteMessage.getData().get("body").split(";");
////        Log.e("texto result body ", remoteMessage.getData().get("body"));
////        for (String a : textoSeparadoOK) {
////            Log.e("texto result ", a.trim());
////        }

        //this.id = Integer.parseInt(textoSeparadoOK[0].trim());
        this.id = Integer.parseInt(remoteMessage.getData().get("id_notificacao"));
        this.idDestinatario = Integer.parseInt(remoteMessage.getData().get("id_destinatario"));
        this.dataHora = DateUtil.getCurrentDate();
        this.mensagem = remoteMessage.getData().get("body");
        //this.mensagem = textoSeparadoOK[1].trim();
        this.titulo = remoteMessage.getData().get("title");
        this.tipo = Integer.parseInt(remoteMessage.getData().get("tipo_notificacao"));
        this.url = remoteMessage.getData().get("url_imagem");
        this.flagNova = 1;
    }

    public Notificacao(JSONObject jsonNotificacao) {
        this.id = jsonNotificacao.optInt("id");
        this.dataHora = jsonNotificacao.optString("dataHora");
        this.mensagem = jsonNotificacao.optString("body");
        this.titulo = jsonNotificacao.optString("title");
        this.url = jsonNotificacao.optString("url");
        this.tipo = jsonNotificacao.optInt("tipo");
        this.flagLido = jsonNotificacao.optInt("boLido");
        this.flagNova = flagLido == 0 ? 1 : 0;
    }

    public Notificacao(Cursor c) {
        this.setId(c.getInt(c.getColumnIndex(this.CAMPO_ID)));
        this.setIdDestinatario(c.getInt(c.getColumnIndex(this.CAMPO_ID_DESTINATARIO)));
        this.setDataHora(c.getString(c.getColumnIndex(this.CAMPO_DATA_HORA)));
        this.setTipo(c.getInt(c.getColumnIndex(this.CAMPO_TIPO)));
        this.setTitulo(c.getString(c.getColumnIndex(this.CAMPO_TITULO)));
        this.setMensagem(c.getString(c.getColumnIndex(this.CAMPO_MENSAGEM)));
        this.setUrl(c.getString(c.getColumnIndex(this.CAMPO_URL)));
        this.setCaminhoArquivo(c.getString(c.getColumnIndex(this.CAMPO_CAMINHO_ARQUIVO)));
        this.setImagem(c.getBlob(c.getColumnIndex(this.CAMPO_IMAGEM)));
        this.setTipo(c.getInt(c.getColumnIndex(this.CAMPO_TIPO)));
        this.setFlagNova(c.getInt(c.getColumnIndex(this.CAMPO_FLAG_NOVA)));
        this.setFlagCompleto(c.getInt(c.getColumnIndex(this.CAMPO_FLAG_COMPLETO)));
        this.setFlagLido(c.getInt(c.getColumnIndex(this.CAMPO_FLAG_LIDO)));
        this.setFlagSinc(c.getInt(c.getColumnIndex(this.CAMPO_FLAG_SINC)));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdDestinatario(int idDestinatario) {
        this.idDestinatario = idDestinatario;
    }

    public int getIdDestinatario() {return idDestinatario;
    }


    public String getDataHora() {
        return dataHora;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    public int getFlagNova() {
        return flagNova;
    }

    public void setFlagNova(int flagNova) {
        this.flagNova = flagNova;
    }

    public int getFlagCompleto() {
        return flagCompleto;
    }

    public void setFlagCompleto(int flagCompleto) {
        this.flagCompleto = flagCompleto;
    }

    public int getFlagLido() {
        return flagLido;
    }

    public void setFlagLido(int flagLido) {
        this.flagLido = flagLido;
    }

    public int getFlagSinc() {
        return flagSinc;
    }

    public void setFlagSinc(int flagSinc) {
        this.flagSinc = flagSinc;
    }

    public String getCaminhoArquivo() {
        return caminhoArquivo;
    }

    public void setCaminhoArquivo(String caminhoArquivo) {
        this.caminhoArquivo = caminhoArquivo;
    }

    public byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, blob);
        return blob.toByteArray();
    }

    public JSONObject getLog(String endereco, String latitude, String longitude) {
        JSONObject jsonParametros = new JSONObject();
        try {
            jsonParametros.put("idNotificacao", getId());
            jsonParametros.put("endereco", endereco);
            jsonParametros.put("latitude", latitude);
            jsonParametros.put("longitude", longitude);
            jsonParametros.put("isLocalizacao", 1);
            return jsonParametros;
        } catch (Exception ex) {
            ex.printStackTrace();
            return jsonParametros;
        }
    }

    public ContentValues getContetValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_ID_DESTINATARIO, getIdDestinatario());
        valores.put(CAMPO_DATA_HORA, getDataHora());
        valores.put(CAMPO_TIPO, getTipo());
        valores.put(CAMPO_TITULO, getTitulo());
        valores.put(CAMPO_MENSAGEM, getMensagem());
        valores.put(CAMPO_URL, getUrl());
        valores.put(CAMPO_CAMINHO_ARQUIVO, getCaminhoArquivo());
        valores.put(CAMPO_IMAGEM, getImagem());
        valores.put(CAMPO_FLAG_NOVA, getFlagNova());
        valores.put(CAMPO_FLAG_COMPLETO, getFlagCompleto());
        valores.put(CAMPO_FLAG_LIDO, getFlagLido());
        return valores;
    }

    public static String getTokenId() {
        tokenId = "";
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.e("getTokenId", "getInstanceId failed", task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    tokenId = task.getResult().getToken();
                    Log.e("getTokenId", tokenId);
                });
        return tokenId;
    }

    public static void criarTeste(Context context) {
        Notificacao n = new Notificacao();
        n.setId(1164360);
        n.setDataHora("02/05/2019 16:05:36");
        n.setTipo(2);
        n.setTitulo("TESTE");
        n.setMensagem("meupdv");
        n.setUrl("http://pdvdemo.com.br/notificacao/video/9fe4cbfd-10d9-488c-b4a6-e6a848859634.mp4");
        n.setFlagNova(0);
        n.setFlagCompleto(1);
        n.setFlagLido(0);
        GenericCTRL.save(context, Notificacao.TABELA, n.getContetValues());
        n.setId(1164361);
        GenericCTRL.save(context, Notificacao.TABELA, n.getContetValues());
        n.setId(1164362);
        GenericCTRL.save(context, Notificacao.TABELA, n.getContetValues());
        n.setId(1164363);
        GenericCTRL.save(context, Notificacao.TABELA, n.getContetValues());
    }
}
