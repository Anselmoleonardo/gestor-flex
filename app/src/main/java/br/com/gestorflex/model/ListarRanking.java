package br.com.gestorflex.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ListarRanking implements Serializable {

    private String cdEstado;
    private int nrDDD;
    private String nomeListarPor;
    private String fgPeriodo;
    private int fgListarPor;
    private String txData;
    private String dataInicio;
    private String dataFim;
    private boolean isEmail;
    private int tipo;
    private int produto;
    private int cdRegional;
    private  int idParceiro;

    public ListarRanking() {
        this.cdEstado = "";
        this.nrDDD = 0;
        this.nomeListarPor = "";
        this.fgPeriodo = "";
        this.fgListarPor = 0;
        this.tipo = 0;
        this.produto = 0;
        this.idParceiro = 0;
        this.txData = "";
        this.dataInicio = "";
        this.dataFim = "";
        this.isEmail = false;
    }

    public String getCdEstado() {
        return cdEstado;
    }

    public void setCdEstado(String cdEstado) {
        this.cdEstado = cdEstado;
    }

    public int getNrDDD() {
        return nrDDD;
    }

    public void setNrDDD(int nrDDD) {
        this.nrDDD = nrDDD;
    }

    public String getFgPeriodo() {
        return fgPeriodo;
    }

    public void setFgPeriodo(String fgPeriodo) {
        this.fgPeriodo = fgPeriodo;
    }

    public int getFgListarPor() {
        return fgListarPor;
    }

    public void setFgListarPor(int fgListarPor) {
        this.fgListarPor = fgListarPor;
    }

    public String getTxData() {
        return txData;
    }

    public void setTxData(String txData) {
        this.txData = txData;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public boolean isEmail() {
        return isEmail;
    }

    public void setEmail(boolean email) {
        isEmail = email;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getProduto() {
        return produto;
    }

    public void setProduto(int produto) {
        this.produto = produto;
    }

    public int getCdRegional() {
        return cdRegional;
    }

    public void setCdRegional(int cdRegional) {
        this.cdRegional = cdRegional;
    }

    public int getIdParceiro() {
        return idParceiro;
    }

    public void setIdParceiro(int idParceiro) {
        this.idParceiro = idParceiro;
    }

    public String getNomeListarPor() {
        return nomeListarPor;
    }

    public void setNomeListarPor(String nomeListarPor) {
        this.nomeListarPor = nomeListarPor;
    }

    public JSONObject pegarJson() {
        JSONObject jsonRanking = new JSONObject();
        try {
            jsonRanking.put("estado", this.cdEstado);
            jsonRanking.put("regional", this.cdRegional);

            jsonRanking.put("ddd", this.nrDDD);
            jsonRanking.put("periodo", this.fgPeriodo);
            jsonRanking.put("listarPor", this.fgListarPor);
            jsonRanking.put("produto", this.produto);
            jsonRanking.put("tipo", this.tipo);
            jsonRanking.put("data", this.txData);
            jsonRanking.put("dataInicial", this.dataInicio);
            jsonRanking.put("dataFinal", this.dataFim);
            jsonRanking.put("isEmail", (this.isEmail() ? 1 : 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonRanking;
    }
}
