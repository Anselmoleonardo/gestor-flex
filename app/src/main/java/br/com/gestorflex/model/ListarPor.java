package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class ListarPor {

    public static final String TABELA = "listar_por";
    public static final String CAMPO_NR_ORDEM = "ordem";
    public static final String CAMPO_DESCRICAO = "descricao";

    private int nrOrdem;
    private String descricao;

    public ListarPor(int nrOrdem, String descricao){
        this.nrOrdem = nrOrdem;
        this.descricao = descricao;
    }

    public ListarPor(JSONObject jsonObject) {
        this.nrOrdem = jsonObject.optInt("d39_nrordem");
        this.descricao = jsonObject.optString("d39_nmdescricao");
    }

    public ListarPor(Cursor cursor) {
        this.nrOrdem = cursor.getInt(cursor.getColumnIndex(CAMPO_NR_ORDEM));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
    }

    public int getNrOrdem() {
        return nrOrdem;
    }

    public String getDescricao() {
        return descricao;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_NR_ORDEM, getNrOrdem());
        valores.put(CAMPO_DESCRICAO, getDescricao());
        return valores;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

}
