package br.com.gestorflex.model;

import org.json.JSONObject;

import java.io.Serializable;

public class RetornoPonto implements Serializable {
    //private int idUsuario;
    private String nome;
    private int matricula;
    private String data;
    private String funcional;
    private int DDD;
    private int PDV;
    private String nomePDV;
    private int idPromotor;


    public RetornoPonto(){
        //idUsuario = 0;
        nome = "teste";
        matricula = 0;
        data = "";
        funcional = "";
        DDD = 0;
        PDV = 0;
        nomePDV = "";
        idPromotor = 0;
    }


    public RetornoPonto(JSONObject jsonPonto){
        //this.setIdUsuario(jsonPonto.optInt("idUsuario"));
        this.setNome(jsonPonto.optString("nome"));
        this.setMatricula(jsonPonto.optInt("matricula"));
        this.setData(jsonPonto.optString("admissao"));
        this.setFuncional(jsonPonto.optString("funcional"));
        this.setDDD(jsonPonto.optInt("DDD"));
        this.setPDV(jsonPonto.optInt("PDV"));
        this.setNomePDV(jsonPonto.optString("NOME_PDV"));
        this.setIdPromotor(jsonPonto.optInt("PROMOTOR"));
    }

//    //public int getIdUsuario() {
//        return idUsuario;
//    }

//    public void setIdUsuario(int idUsuario) {
//        this.idUsuario = idUsuario;
//    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getFuncional() {
        return funcional;
    }

    public void setFuncional(String funcional) {
        this.funcional = funcional;
    }

    public int getDDD() {
        return DDD;
    }

    public void setDDD(int DDD) {
        this.DDD = DDD;
    }

    public int getPDV() {
        return PDV;
    }

    public void setPDV(int PDV) {
        this.PDV = PDV;
    }

    public String getNomePDV() {
        return nomePDV;
    }

    public void setNomePDV(String nomePDV) {
        this.nomePDV = nomePDV;
    }

    public int getIdPromotor() {
        return idPromotor;
    }

    public void setIdPromotor(int idPromotor) {
        this.idPromotor = idPromotor;
    }
}
