//package br.com.gestorflex.model;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//
//import org.json.JSONObject;
//
//public class Jornada {
//
//    public static final String TABELA = "odometro";
//    public static final String CAMPO_ID = "_id";
//    public static final String CAMPO_ATIVIDADE = "atividade";
//    public static final String CAMPO_DATA_ENVIO = "dataEnvio";
//    public static final String CAMPO_KM_DIA = "kmDia";
//    public static final String CAMPO_KM_ODOMENTRO = "kmOdometro";
//    public static final String CAMPO_CAMINHO_FOTO = "caminhoFoto";
//    public static final String CAMPO_FOTO = "foto";
//    public static final String CAMPO_LATITUDE = "latitude";
//    public static final String CAMPO_LONGITUDE = "longitude";
//
//    private int id;
//    private int atividade;
//    private String dataEnvio;
//    private String caminhoFoto;
//    private String bitmapFoto;
//    private int kmDoDia;
//    private int kmDoOdometro;
//    private String latitude;
//    private String longitude;
//
//    public Jornada(){
//        this.id = 0;
//        this.atividade = 0;
//        this.dataEnvio = "";
//        this.caminhoFoto = "";
//        this.bitmapFoto = "";
//        this.kmDoDia = 0;
//        this.kmDoOdometro = 0;
//        this.latitude = "";
//        this.longitude = "";
//    }
//
////    public Jornada(int id, int atividade, String data, String caminhoFoto, Bitmap bitmapFoto, int kmDoDia, int kmDoOdometro, String latitude, String longitude){
////        this.id = id;
////        this.atividade = atividade;
////        this.data = data;
////        this.caminhoFoto = caminhoFoto;
////        this.bitmapFoto = bitmapFoto;
////        this.kmDoDia = kmDoDia;
////        this.kmDoOdometro = kmDoOdometro;
////        this.latitude = latitude;
////        this.longitude = longitude;
////    }
//
//    public Jornada(Cursor cursor){
//        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
//        this.atividade = cursor.getInt(cursor.getColumnIndex(CAMPO_ATIVIDADE));
//        this.dataEnvio = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_ENVIO));
//        this.caminhoFoto = cursor.getString(cursor.getColumnIndex(CAMPO_CAMINHO_FOTO));
//        this.bitmapFoto = cursor.getString(cursor.getColumnIndex(CAMPO_FOTO));
//        this.kmDoDia = cursor.getInt(cursor.getColumnIndex(CAMPO_KM_DIA));
//        this.kmDoOdometro = cursor.getInt(cursor.getColumnIndex(CAMPO_KM_ODOMENTRO));
//        this.latitude = cursor.getString(cursor.getColumnIndex(CAMPO_LATITUDE));
//        this.longitude = cursor.getString(cursor.getColumnIndex(CAMPO_LONGITUDE));
//
//    }
//
//    public JSONObject pegarJson() {
//        JSONObject json = new JSONObject();
//        try {
//            json.put("atividade", this.atividade);
//            json.put("dataEnvio", this.dataEnvio);
//            json.put("kmDia", this.kmDoDia);
//            json.put("kmOdometro", this.kmDoOdometro);
//            json.put("latitude", this.latitude);
//            json.put("longitude", this.longitude);
//            json.put("foto", this.bitmapFoto);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return json;
//    }
//
//    public ContentValues getContentValues() {
//        ContentValues values = new ContentValues();
//        values.put(CAMPO_ATIVIDADE, this.getAtividade());
//        values.put(CAMPO_DATA_ENVIO, this.getDataEnvio());
//        values.put(CAMPO_KM_DIA, this.getKmDoDia());
//        values.put(CAMPO_KM_ODOMENTRO, this.getKmDoOdometro());
//        values.put(CAMPO_CAMINHO_FOTO, this.getCaminhoFoto());
//        values.put(CAMPO_FOTO, this.getBitmapFoto());
//        values.put(CAMPO_LATITUDE, this.getLatitude());
//        values.put(CAMPO_LONGITUDE, this.getLongitude());
//        return values;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getAtividade() {
//        return atividade;
//    }
//
//    public void setAtividade(int atividade) {
//        this.atividade = atividade;
//    }
//
//    public String getDataEnvio() {
//        return dataEnvio;
//    }
//
//    public void setDataEnvio(String dataEnvio) {
//        this.dataEnvio = dataEnvio;
//    }
//
//    public String getCaminhoFoto() {
//        return caminhoFoto;
//    }
//
//    public void setCaminhoFoto(String foto) {
//        this.caminhoFoto = foto;
//    }
//
//    public String getBitmapFoto() {
//        return bitmapFoto;
//    }
//
//    public void setBitmapFoto(String bitmapFoto) {
//        this.bitmapFoto = bitmapFoto;
//    }
//
//    public int getKmDoDia() {
//        return kmDoDia;
//    }
//
//    public void setKmDoDia(int kmDoDia) {
//        this.kmDoDia = kmDoDia;
//    }
//
//    public int getKmDoOdometro() {
//        return kmDoOdometro;
//    }
//
//    public void setKmDoOdometro(int kmDoOdometro) {
//        this.kmDoOdometro = kmDoOdometro;
//    }
//
//    @Override
//    public String toString() {
//        return "Odomentro: " + kmDoOdometro + " - dataEnvio: " + dataEnvio;
//    }
//
//    public String getLatitude() {
//        return latitude;
//    }
//
//    public void setLatitude(String latitude) {
//        this.latitude = latitude;
//    }
//
//    public String getLongitude() {
//        return longitude;
//    }
//
//    public void setLongitude(String longitude) {
//        this.longitude = longitude;
//    }
//
//
//}
