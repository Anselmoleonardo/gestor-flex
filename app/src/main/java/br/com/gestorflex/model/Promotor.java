package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Promotor {

    public static final String TABELA = "promotor";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DESCRICAO = "descricao";

    // VARIÁVEIS
    private int id;
    private String descricao;

    public Promotor() {
        this.id = 0;
        this.descricao = "";
    }

    public Promotor(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }


    public Promotor(JSONObject jsonPromotor) {
        this.id = jsonPromotor.optInt("s01_cdid");
        this.descricao = jsonPromotor.optString("s01_txnome");

    }

    public Promotor(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_DESCRICAO, getDescricao());

        return valores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
