package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

import java.io.Serializable;

import br.com.gestorflex.util.DateUtil;

public class Dash implements Serializable {

    public static final String TABELA = "dashboard";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NOME = "nome";
    public static final String CAMPO_VALOR = "valor";
    public static final String CAMPO_DATA_HORA = "data_hora";
    public static final String CAMPO_CATEGORIA_DASH_ID = "categoria_dash_id";

    private int id;
    private String nome;
    private String valor;
    private String dataHora;
    private int categoriaDash;

    public Dash() {
    }

    public Dash(int id, String nome, String valor, int categoriaDash) {
        this.id = id;
        this.nome = nome;
        this.valor = valor;
        this.categoriaDash = categoriaDash;
    }

    public Dash(String nome, String valor, int categoriaDash) {
        this.nome = nome;
        this.valor = valor;
        this.categoriaDash = categoriaDash;
    }

    public Dash(JSONObject jsDash) {
        this.nome = jsDash.optString("NOME");
        this.valor = jsDash.optString("VALOR");
        this.dataHora = DateUtil.getCurrentDateTimeBR();
        this.categoriaDash = jsDash.optInt("CATEGORIA");
    }

    public Dash(Cursor cursor) {
        this.nome = cursor.getString(cursor.getColumnIndex(CAMPO_NOME));
        this.valor = cursor.getString(cursor.getColumnIndex(CAMPO_VALOR));
        this.dataHora = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_HORA));
        this.categoriaDash = cursor.getInt(cursor.getColumnIndex(CAMPO_CATEGORIA_DASH_ID));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDataHora() {
        return dataHora;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }

    public int getCategoriaDash() {
        return categoriaDash;
    }

    public void setCategoriaDash(int categoriaDash) {
        this.categoriaDash = categoriaDash;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
//        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_NOME, getNome());
        valores.put(CAMPO_VALOR, getValor());
        valores.put(CAMPO_DATA_HORA, getDataHora());
        valores.put(CAMPO_CATEGORIA_DASH_ID, getCategoriaDash());
        return valores;
    }
}
