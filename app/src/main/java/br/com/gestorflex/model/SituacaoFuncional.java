package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class SituacaoFuncional {

    public static final String TABELA = "SituacaoFuncional";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NOME = "nome";

    private int id;
    private String nome;

    public SituacaoFuncional(){
        this.id = 0;
        this.nome = "";
    }

    public SituacaoFuncional(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public SituacaoFuncional(JSONObject jsonPromotor) {
        this.id = jsonPromotor.optInt("id");
        this.nome = jsonPromotor.optString("nome");

    }

    public SituacaoFuncional(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.nome = cursor.getString(cursor.getColumnIndex(CAMPO_NOME));
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_NOME, getNome());

        return valores;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    @Override
    public String toString() {
        return nome;
    }
}
