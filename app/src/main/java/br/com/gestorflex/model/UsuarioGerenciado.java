package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class UsuarioGerenciado {

    public static final String TABELA = "usuario_gerenciado";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NOME = "nome";
    public static final String CAMPO_MATRICULA = "matricula";
    public static final String CAMPO_FLAG_PRIVILEGIO = "flag_privilegio";
    public static final String CAMPO_ID_ESTADO = "id_estado";

    private int id;
    private String nome;
    private String matricula;
    private int flagPrivilegio;
    private int idEstado;

    public UsuarioGerenciado(int id, String nome, String matricula, int flagPrivilegio, int idEstado) {
        this.id = id;
        this.nome = nome;
        this.matricula = matricula;
        this.flagPrivilegio = flagPrivilegio;
        this.idEstado = idEstado;
    }

    public UsuarioGerenciado(JSONObject jsonObject) {
        this.id = jsonObject.optInt("s01_cdid");
        this.nome = jsonObject.optString("s01_txnome");
        this.matricula = jsonObject.optString("s01_nrmatriculaparceiro");
        this.flagPrivilegio = jsonObject.optInt("s02_fgprivilegio");
        this.idEstado = jsonObject.optInt("d02_cdid");
    }

    public UsuarioGerenciado(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.nome = cursor.getString(cursor.getColumnIndex(CAMPO_NOME));
        this.matricula = cursor.getString(cursor.getColumnIndex(CAMPO_MATRICULA));
        this.flagPrivilegio = cursor.getInt(cursor.getColumnIndex(CAMPO_FLAG_PRIVILEGIO));
        this.idEstado = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_ESTADO));
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getMatricula() {
        return matricula;
    }

    public int getFlagPrivilegio() {
        return flagPrivilegio;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_NOME, getNome());
        valores.put(CAMPO_MATRICULA, getMatricula());
        valores.put(CAMPO_FLAG_PRIVILEGIO, getFlagPrivilegio());
        valores.put(CAMPO_ID_ESTADO, getIdEstado());
        return valores;
    }

    @Override
    public String toString() {
        return this.nome;
    }

}
