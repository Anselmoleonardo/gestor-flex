package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Pergunta {

    public static final String TABELA = "Pergunta";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_PERGUNTA = "pergunta";
    public static final String CAMPO_TIPO = "tipo";
    public static final String CAMPO_ID_PESQUISA = "idpesquisa";
    public static final String CAMPO_VALIDACAO = "validacao";

    private int id;
    private String pergunta;
    private int tipo;
    private  int idPesquisa;
    private  int validacao;

    public Pergunta() {
        this.id = 0;
        this.tipo = 0;
        this.pergunta ="";
        this.idPesquisa =0;
        this.validacao =0;
    }

    public Pergunta(int id, int tipo, String pergunta, int idPesquisa, int validacao) {
        this.id = id;
        this.tipo = tipo;
        this.pergunta = pergunta;
        this.idPesquisa = idPesquisa;
        this.validacao = validacao;
    }

    public Pergunta(JSONObject jsonEstado) {
        this.id = jsonEstado.optInt("id");
        this.pergunta = jsonEstado.optString("r16_txpergunta");
        this.tipo = jsonEstado.optInt("r16_fgtipo");
        this.idPesquisa = jsonEstado.optInt("r16_cdpesquisa");
        this.validacao = jsonEstado.optInt("r16_fgvalidacao");
    }

    public Pergunta(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.pergunta = cursor.getString(cursor.getColumnIndex(CAMPO_PERGUNTA));
        this.tipo = cursor.getInt(cursor.getColumnIndex(CAMPO_TIPO));
        this.idPesquisa = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_PESQUISA));
        this.validacao = cursor.getInt(cursor.getColumnIndex(CAMPO_VALIDACAO));
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPegunta() {
        return pergunta;
    }

    public void setPergunta(String pegunta) {
        this.pergunta = pegunta;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getIdPesquisa() {
        return idPesquisa;
    }

    public void setIdPesquisa(int idPesquisa) {
        this.idPesquisa = idPesquisa;
    }

    public int getValidacao() {
        return validacao;
    }

    public void setValidacao(int validacao) {
        this.validacao = validacao;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_PERGUNTA, getPegunta());
        valores.put(CAMPO_TIPO, getTipo());
        valores.put(CAMPO_ID_PESQUISA, getIdPesquisa());
        valores.put(CAMPO_VALIDACAO, getValidacao());
        return valores;
    }
}
