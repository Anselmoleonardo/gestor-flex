package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Regional {

    public static final String TABELA = "regional";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DESCRICAO = "descricao";
    public static final String CAMPO_ID_AGENCIA  = "idagencia";

    private int id;
    private String descricao;
    private int idAgencia;

    public  Regional(){
        this.id=0;
        this.descricao="";
        this.idAgencia=0;
    }

    public Regional(int id, String descricao, int idAgencia) {
        this.id = id;
        this.descricao = descricao;
        this.idAgencia = idAgencia;
    }

    public Regional(JSONObject jsonRegional) {
        this.id = jsonRegional.optInt("d06_cdid");
        this.descricao = jsonRegional.optString("d06_nmdescricao");
        this.idAgencia = jsonRegional.optInt("d05_cdid");

    }

    public Regional(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
        this.idAgencia = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_AGENCIA));
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_DESCRICAO, getDescricao());
        valores.put(CAMPO_ID_AGENCIA, getIdAgencia());

        return valores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getIdAgencia() {
        return idAgencia;
    }

    public void setIdAgencia(int idAgencia) {
        this.idAgencia = idAgencia;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
