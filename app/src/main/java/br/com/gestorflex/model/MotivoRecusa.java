package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class MotivoRecusa {

    public static final String TABELA = "motivo_recusa";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DESCRICAO = "descricao";

    private int id;
    private String descricao;

    public MotivoRecusa(JSONObject jsonObject) {
        this.id = jsonObject.optInt("id");
        this.descricao = jsonObject.optString("descricao");
    }

    public MotivoRecusa(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_DESCRICAO, getDescricao());
        return valores;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

}
