package br.com.gestorflex.model;

public class ComparativoVenda {

    private String nomeListagem;
    private int quantidadeAntes;
    private int quantidadeDepois;

    public ComparativoVenda(String nomeOpcaoListagem, int quantidadeAntes, int quantidadeDepois) {
        this.nomeListagem = nomeOpcaoListagem;
        this.quantidadeAntes = quantidadeAntes;
        this.quantidadeDepois = quantidadeDepois;
    }

    public String getNomeListagem() {
        return nomeListagem;
    }

    public int getQuantidadeAntes() {
        return quantidadeAntes;
    }

    public int getQuantidadeDepois() {
        return quantidadeDepois;
    }

    public int getDiferenca() {
        return quantidadeDepois - quantidadeAntes;
    }

    public float getPorcentagem() {
        float porcentagem = 0;
        if (quantidadeAntes == 0) {
            return 100;
        }
        if (quantidadeDepois == 0) {
            return -100;
        }
        return ((quantidadeDepois - quantidadeAntes) * 100) / quantidadeAntes;
    }
}
