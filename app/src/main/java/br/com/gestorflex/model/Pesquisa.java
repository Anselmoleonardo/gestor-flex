package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Pesquisa {
    public static final String TABELA = "Pesquisa";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NOME = "nome";
    public static final String CAMPO_DATA_CADASTRO = "data_cadastro";

    private int id;
    private String nome;
    private  String dataCadastro;

    public Pesquisa() {
        this.id = 0;
        this.nome = "";
        this.dataCadastro ="";
    }

    public Pesquisa(int id, String nome, String dataCadastro) {
        this.id = id;
        this.nome = nome;
        this.dataCadastro = dataCadastro;
    }

    public Pesquisa(JSONObject jsonPesquisa) {
        this.id = jsonPesquisa.optInt("id");
        this.nome = jsonPesquisa.optString("p13_nmpesquisa");
        this.dataCadastro = jsonPesquisa.optString("dataPesquisa");

    }

    public Pesquisa(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.nome = cursor.getString(cursor.getColumnIndex(CAMPO_NOME));
        this.dataCadastro = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_CADASTRO));
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_NOME, getNome());
        valores.put(CAMPO_DATA_CADASTRO, getDataCadastro());

        return valores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(String dataCadastro) {
        this.dataCadastro = dataCadastro;
    }
}