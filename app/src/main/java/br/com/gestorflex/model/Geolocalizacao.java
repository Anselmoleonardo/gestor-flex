package br.com.gestorflex.model;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

public class Geolocalizacao {

    private static double latitude;
    private static double longitude;
    private static String descricao;

    public Geolocalizacao() {
        latitude = 0;
        longitude = 0;
        descricao = "";
    }

    public static double getLatitude() {
        return latitude;
    }

    public static void setLatitude(double latitude) {
        Geolocalizacao.latitude = latitude;
    }

    public static double getLongitude() {
        return longitude;
    }

    public static void setLongitude(double longitude) {
        Geolocalizacao.longitude = longitude;
    }

    public static String getDescricao() {
        return descricao;
    }

    public static void setDescricao(String descricao) {
        Geolocalizacao.descricao = descricao;
    }

    public static LatLng coletarPosicaoAtual() {
        return new LatLng(Geolocalizacao.getLatitude(), Geolocalizacao.getLongitude());
    }

    public static void getLog(Context context, JSONObject params) throws Exception {
        params.put("latitude", getLatitude());
        params.put("longitude", getLongitude());
    }
}
