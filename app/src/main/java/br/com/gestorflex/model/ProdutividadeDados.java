package br.com.gestorflex.model;

import org.json.JSONObject;

import java.io.Serializable;

public class ProdutividadeDados implements Serializable {
    private String listar;
    private int headCount;
    private int preMeta;
    private int recargaMeta;
    private int ctrlMeta;
    private int ctrlAvMEta;
    private int posMeta;
    private int preVenda;
    private int recargaVenda;
    private int ctlrVenda;
    private int ctrlAvVenda;
    private int posVenda;
    private String preRanking;
    private String recargaRanking;
    private String ctrlRanking;
    private String ctrlAvRanking;
    private String posRanking;

    public ProdutividadeDados(JSONObject jsonObject) {
        this.listar = jsonObject.optString("LISTAR");
        this.headCount = jsonObject.optInt("HEADCOUNT");
        this.preMeta = jsonObject.optInt("PRE_META");
        this.recargaMeta = jsonObject.optInt("RECARGA_META");
        this.ctrlMeta = jsonObject.optInt("CTRL_META");
        this.ctrlAvMEta = jsonObject.optInt("CTRL_AV_META");
        this.posMeta = jsonObject.optInt("POS_META");
        this.preVenda = jsonObject.optInt("PRE_VENDA");
        this.recargaVenda = jsonObject.optInt("RECARGA_VENDA");
        this.ctlrVenda = jsonObject.optInt("CTRL_VENDA");
        this.ctrlAvVenda = jsonObject.optInt("CTRL_AV_VENDA");
        this.posVenda = jsonObject.optInt("POS_VENDA");
        this.preRanking = jsonObject.optString("PRE_RANKING");
        this.recargaRanking = jsonObject.optString("RECARGA_RANKING");
        this.ctrlRanking = jsonObject.optString("CTRL_RANKING");
        this.ctrlAvRanking = jsonObject.optString("CTRL_AV_RANKING");
        this.posRanking = jsonObject.optString("POS_RANKING");
    }

    public String getListar() {
        return listar;
    }

    public void setListar(String listar) {
        this.listar = listar;
    }

    public int getHeadCount() {
        return headCount;
    }

    public void setHeadCount(int headCount) {
        this.headCount = headCount;
    }

    public int getPreMeta() {
        return preMeta;
    }

    public void setPreMeta(int preMeta) {
        this.preMeta = preMeta;
    }

    public int getRecargaMeta() {
        return recargaMeta;
    }

    public void setRecargaMeta(int recargaMeta) {
        this.recargaMeta = recargaMeta;
    }

    public int getCtrlMeta() {
        return ctrlMeta;
    }

    public void setCtrlMeta(int ctrlMeta) {
        this.ctrlMeta = ctrlMeta;
    }

    public int getCtrlAvMEta() {
        return ctrlAvMEta;
    }

    public void setCtrlAvMEta(int ctrlAvMEta) {
        this.ctrlAvMEta = ctrlAvMEta;
    }

    public int getPosMeta() {
        return posMeta;
    }

    public void setPosMeta(int posMeta) {
        this.posMeta = posMeta;
    }

    public int getPreVenda() {
        return preVenda;
    }

    public void setPreVenda(int preVenda) {
        this.preVenda = preVenda;
    }

    public int getRecargaVenda() {
        return recargaVenda;
    }

    public void setRecargaVenda(int recargaVenda) {
        this.recargaVenda = recargaVenda;
    }

    public int getCtlrVenda() {
        return ctlrVenda;
    }

    public void setCtlrVenda(int ctlrVenda) {
        this.ctlrVenda = ctlrVenda;
    }

    public int getCtrlAvVenda() {
        return ctrlAvVenda;
    }

    public void setCtrlAvVenda(int ctrlAvVenda) {
        this.ctrlAvVenda = ctrlAvVenda;
    }

    public int getPosVenda() {
        return posVenda;
    }

    public void setPosVenda(int posVenda) {
        this.posVenda = posVenda;
    }

    public String getPreRanking() {
        return preRanking;
    }

    public void setPreRanking(String preRanking) {
        this.preRanking = preRanking;
    }

    public String getRecargaRanking() {
        return recargaRanking;
    }

    public void setRecargaRanking(String recargaRanking) {
        this.recargaRanking = recargaRanking;
    }

    public String getCtrlRanking() {
        return ctrlRanking;
    }

    public void setCtrlRanking(String ctrlRanking) {
        this.ctrlRanking = ctrlRanking;
    }

    public String getCtrlAvRanking() {
        return ctrlAvRanking;
    }

    public void setCtrlAvRanking(String ctrlAvRanking) {
        this.ctrlAvRanking = ctrlAvRanking;
    }

    public String getPosRanking() {
        return posRanking;
    }

    public void setPosRanking(String posRanking) {
        this.posRanking = posRanking;
    }
}


