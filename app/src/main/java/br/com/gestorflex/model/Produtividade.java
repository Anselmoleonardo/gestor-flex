package br.com.gestorflex.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Produtividade implements Serializable {

    private String estado;
    private int ddd;
    private int periodo;
    private int listarPor;
    private String data;
    private String dataInicio;
    private String dataFim;
    private int cdRegional;
    private int idParceiro;

    public Produtividade() {
        this.estado = "";
        this.ddd = 0;
        this.periodo = 0;
        this.listarPor = 0;
        this.data = "";
        this.dataInicio = "";
        this.dataFim = "";
        this.idParceiro=0;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getDdd() {
        return ddd;
    }

    public void setDdd(int ddd) {
        this.ddd = ddd;
    }

    public int getPeriodo() {
        return periodo;
    }

    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    public int getListarPor() {
        return listarPor;
    }

    public void setListarPor(int listarPor) {
        this.listarPor = listarPor;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public int getCdRegional() {
        return cdRegional;
    }

    public void setCdRegional(int cdRegional) {
        this.cdRegional = cdRegional;
    }

    public int getIdParceiro() {
        return idParceiro;
    }

    public void setIdParceiro(int idParceiro) {
        this.idParceiro = idParceiro;
    }

    public JSONObject pegarJson() {
        JSONObject jsonRanking = new JSONObject();
        try {
            jsonRanking.put("estado", this.estado);
            jsonRanking.put("regional", this.cdRegional);

            jsonRanking.put("ddd", this.ddd);
            jsonRanking.put("periodo", this.periodo);
            jsonRanking.put("listarPor", this.listarPor);


            jsonRanking.put("data", this.data);
            jsonRanking.put("dataInicial", this.dataInicio);
            jsonRanking.put("dataFinal", this.dataFim);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonRanking;
    }
}