package br.com.gestorflex.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Ponto implements Serializable {

    private boolean isComRegistro;
    private String data;
    private String dataAlternada;
    private int promotor;
    private int pdv;
    private int cdRegional;
    private  int idParceiro;

    public Ponto() {
        this.isComRegistro = true;
        this.data = "";
        this.dataAlternada = "";
        this.promotor = 0;
        this.pdv = 0;
        this.idParceiro=0;
    }


    public JSONObject pegarJson() {
        JSONObject jsonRanking = new JSONObject();
        try {
            jsonRanking.put("idParceiro", this.idParceiro);
            jsonRanking.put("regional", this.cdRegional);
            jsonRanking.put("isComRegistro", this.isComRegistro?1:0);
            jsonRanking.put("data", this.data);
            jsonRanking.put("data", this.dataAlternada);
            jsonRanking.put("promotor", this.promotor);
            jsonRanking.put("pdv", this.pdv);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonRanking;
    }

    public boolean isComRegistro() {
        return isComRegistro;
    }

    public void setComRegistro(boolean isComRegistro) {
        this.isComRegistro = isComRegistro;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataAlternada() {
        return dataAlternada;
    }

    public void setDataAlternada(String dataAlternada) {
        this.dataAlternada = dataAlternada;
    }

    public int getPromotor() {
        return promotor;
    }

    public void setPromotor(int promotor) {
        this.promotor = promotor;
    }

    public int getPdv() {
        return pdv;
    }

    public void setPdv(int pdv) {
        this.pdv = pdv;
    }

    public int getCdRegional() {
        return cdRegional;
    }

    public void setCdRegional(int cdRegional) {
        this.cdRegional = cdRegional;
    }

    public int getIdParceiro() {
        return idParceiro;
    }

    public void setIdParceiro(int idParceiro) {
        this.idParceiro = idParceiro;
    }
}
