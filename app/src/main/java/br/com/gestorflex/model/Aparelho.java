package br.com.gestorflex.model;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.view.Display;

import androidx.appcompat.app.AlertDialog;

import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.List;

import br.com.gestorflex.BuildConfig;
import br.com.gestorflex.R;

/**
 * Created by Frann on 29/03/2019.
 */

public class Aparelho {

    public static int getComprimentoTela(Context context) {
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        return display.getWidth();
    }

    public static int getAlturaTela(Context context) {
        Display display = ((Activity) context).getWindowManager()
                .getDefaultDisplay();
        return display.getHeight();
    }

    public static String getImei(Context context) {
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return tm.getImei();
        } else {
            return tm.getDeviceId();
        }
    }

    public static String getIccid(Context context) {
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getSimSerialNumber();
    }

    public static String getModelo() {
        return Build.MODEL;
    }

    public static int getApi() {
        return Build.VERSION.SDK_INT;
    }

    public static String getAndroidVersao() {
        return Build.VERSION.RELEASE;
    }

    public static int getCodigoVersao() {
        return BuildConfig.VERSION_CODE;
    }

    public static String getVersao() {
        return BuildConfig.VERSION_NAME;
    }

    private static String coletarDadosChips(Context context) {
        String txt = "";
        if (Build.VERSION.SDK_INT >= 22) {
            List<SubscriptionInfo> lista = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
            if (lista != null) {
                for (SubscriptionInfo s : lista) {
                    txt += "Operadora:" + s.getCarrierName() + "|Slot:" + (s.getSimSlotIndex() + 1) + "|ICCID:"
                            + s.getIccId() + "||";
                }
            }
        }
        return txt;
    }

    public static int coletarSlotPadrao(Context con) {
        Object tm = con.getSystemService(Context.TELEPHONY_SERVICE);
        Method method_getDefaultSim;
        int defaultSimm = 0;
        try {
            method_getDefaultSim = tm.getClass().getDeclaredMethod("getDefaultSim");
            method_getDefaultSim.setAccessible(true);
            defaultSimm = (Integer) method_getDefaultSim.invoke(tm);
            defaultSimm++;
        } catch (Exception e) {
            return defaultSimm;
        }
        return defaultSimm;
    }

    public static int coletarSlotSMS(Context con) {
        Object tm = con.getSystemService(Context.TELEPHONY_SERVICE);
        Method method_getSmsDefaultSim;
        int smsDefaultSim = 0;
        try {
            method_getSmsDefaultSim = tm.getClass().getDeclaredMethod("getSmsDefaultSim");
            smsDefaultSim = (Integer) method_getSmsDefaultSim.invoke(tm);
            smsDefaultSim++;
        } catch (Exception e) {
            return smsDefaultSim;
        }
        return smsDefaultSim;
    }

    private static String coletarMemoriaLivre(Context context) {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return Formatter.formatFileSize(context, (long) stat.getBlockSize() * (long) stat.getAvailableBlocks())
                .replace(",", ".");
    }

    private static String coletarMemoriaTotal(Context context) {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return Formatter.formatFileSize(context, (long) stat.getBlockSize() * (long) stat.getBlockCount()).replace(",",
                ".");
    }

    public static boolean validarICCID(Context context, String iccid) {
        if (!iccid.startsWith("8955")) {
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso).setPositiveButton(R.string.label_alert_button_ok, null).
                    setMessage("O prefixo do ICCID deve ser 8955!" +
                            context.getString(R.string.title_tab_venda_geral)).show();
            return false;
        }

        if (iccid.length() < 18 || iccid.length() > 20) {
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso).setPositiveButton(R.string.label_alert_button_ok, null).
                    setMessage("A quantidade de dígitos do ICCID é inválida!" +
                            context.getString(R.string.title_tab_venda_geral)).show();
            return false;
        }
        String workStr;
        int strLength;
        int i = 0;
        int sum;
        int prod;
        String digit;
        int evenInd;
        int result;
        if (!iccid.matches("[0-9]+")) {
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso).setPositiveButton(R.string.label_alert_button_ok, null).
                    setMessage("ICCID Inválido!" +
                            context.getString(R.string.title_tab_venda_geral)).show();
            return false;
        }
        if (iccid.trim().length() < 18) {
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso).setPositiveButton(R.string.label_alert_button_ok, null).
                    setMessage("Número mínimo do ICCID é 18 dígitos" +
                            context.getString(R.string.title_tab_venda_geral)).show();
            return false;
        }
        strLength = iccid.length();

        workStr = "";
        while (i < strLength) {
            digit = iccid.substring(i, i + 1);

            if (digit.matches("[0-9]+"))
                workStr = workStr + digit;
            i++;
        }

        sum = 0;
        evenInd = 0;
        if (strLength % 2 == 0)
            evenInd = 1;
        i = 0;

        while (i < strLength) {
            digit = workStr.substring(i, i + 1);
            prod = Integer.parseInt(digit);
            if ((evenInd == 1 && i % 2 == 0) || (evenInd == 0 && i % 2 == 1))
                prod = prod * 2;
            if (prod >= 10)
                prod = prod / 10 + prod - 10;
            sum = sum + prod;
            i++;
        }

        if (sum % 10 == 0)
            result = 1;
        else
            result = 0;

        if (result == 0) {
            new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso).setPositiveButton(R.string.label_alert_button_ok, null).
                    setMessage("ICCID Inválido!" +
                            context.getString(R.string.title_tab_venda_geral)).show();
            return false;
        }
        return true;
    }

    public static int getPixelValueFromDp(Context context, int paddingDp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int)(paddingDp * density);
    }


    public static void getLog(Context context, JSONObject params) throws Exception {
        params.put("versao", getCodigoVersao());
        params.put("fgapp", context.getResources().getString(R.string.fgapp));
        params.put("dadosChip", coletarDadosChips(context));
        params.put("slotPadrao", coletarSlotPadrao(context));
        params.put("slotSMS", coletarSlotSMS(context));
        params.put("memLivre", coletarMemoriaLivre(context));
        params.put("memTotal", coletarMemoriaTotal(context));
        params.put("altura", getAlturaTela(context));
        params.put("comprimento", getComprimentoTela(context));
        params.put("iccid", getIccid(context));
        params.put("imei", getImei(context));
        params.put("modelo", getModelo());
        params.put("api", getApi());
        params.put("iccid_Sec", getIccid(context));
    }

}
