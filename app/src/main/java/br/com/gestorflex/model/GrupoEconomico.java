package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class GrupoEconomico {

    public static final String TABELA = "grupo_economico";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_DESCRICAO = "descricao";

    private int id;
    private String descricao;

    public GrupoEconomico(){
        this.id = 0;
        this.descricao = "";
    }

    public GrupoEconomico(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public GrupoEconomico(JSONObject jsonObject) {
        this.id = jsonObject.optInt("d04_cdid");
        this.descricao = jsonObject.optString("d04_nmdescricao");
    }

    public GrupoEconomico(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.descricao = cursor.getString(cursor.getColumnIndex(CAMPO_DESCRICAO));
    }

    public int getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_DESCRICAO, getDescricao());
        return valores;
    }

    @Override
    public String toString() {
        return this.descricao;
    }

}
