package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class Resposta {

    public static final String TABELA = "Resposta";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_ID_PERGUNTA = "id_pergunta";
    public static final String CAMPO_RESPOSTA = "resposta";
    public static final String CAMPO_CDDEPENDENCIA = "dependencia";

    private int id;
    private int idPergunta;
    private String resposta;
    private  int cdDependicia;

    public Resposta() {
        this.id = 0;
        this.idPergunta = 0;
        this.resposta = "";
        this.cdDependicia=0;
    }

    public Resposta(int id,int idPergunta, String resposta, int cdDependicia) {
        this.id = id;
        this.idPergunta = idPergunta;
        this.resposta = resposta;
        this.cdDependicia = cdDependicia;
    }

    public Resposta(JSONObject jsonPesquisa) {
        this.id = jsonPesquisa.optInt("id");
        this.idPergunta = jsonPesquisa.optInt("r17_cdpergunta");
        this.resposta = jsonPesquisa.optString("r17_txresposta");


    }

    public Resposta(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.idPergunta = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_PERGUNTA));
        this.resposta = cursor.getString(cursor.getColumnIndex(CAMPO_RESPOSTA));
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_ID_PERGUNTA, getIdPergunta());
        valores.put(CAMPO_RESPOSTA, getResposta());

        return valores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPergunta() {
        return idPergunta;
    }

    public void setIdPergunta(int idPergunta) {
        this.idPergunta = idPergunta;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public int getCdDependicia() {
        return cdDependicia;
    }

    public void setCdDependicia(int cdDependicia) {
        this.cdDependicia = cdDependicia;
    }
}
