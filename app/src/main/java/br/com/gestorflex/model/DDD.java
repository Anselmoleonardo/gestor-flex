package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class DDD {

    public static final String TABELA = "ddd";
    public static final String CAMPO_ID_UF = "id_uf";
    public static final String CAMPO_DESCRICAO = "descricao";

    private int idUf;
    private int descricao;

    public DDD(int idUf, int descricao) {
        this.idUf = idUf;
        this.descricao = descricao;
    }

    public DDD(JSONObject jsonObject) {
        this.idUf = jsonObject.optInt("d01_cdestado");
        this.descricao = jsonObject.optInt("d01_nrddd");
    }

    public DDD(Cursor cursor) {
        this.idUf = cursor.getInt(cursor.getColumnIndex(CAMPO_ID_UF));
        this.descricao = cursor.getInt(cursor.getColumnIndex(CAMPO_DESCRICAO));
    }

    public int getIdUf() {
        return idUf;
    }

    public void setIdUf(int idUf) {
        this.idUf = idUf;
    }

    public int getDescricao() {
        return descricao;
    }

    public void setDescricao(int descricao) {
        this.descricao = descricao;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID_UF, getIdUf());
        valores.put(CAMPO_DESCRICAO, getDescricao());
        return valores;
    }

    @Override
    public String toString() {
        return String.valueOf(this.descricao);
    }
}