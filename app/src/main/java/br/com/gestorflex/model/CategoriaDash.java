package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONObject;

public class CategoriaDash {

    public static final String TABELA = "categoria_dash";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NOME = "nome";

    private int id;
    private String nome;

    public CategoriaDash(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public CategoriaDash(JSONObject jsonObject) {
        this.id = jsonObject.optInt("ID");
        this.nome = jsonObject.optString("NOME");
    }

    public CategoriaDash(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.nome = cursor.getString(cursor.getColumnIndex(CAMPO_NOME));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ContentValues getContentValues() {
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, getId());
        valores.put(CAMPO_NOME, getNome());
        return valores;
    }
}

