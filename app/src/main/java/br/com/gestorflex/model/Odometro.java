package br.com.gestorflex.model;

import android.content.ContentValues;
import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

public class Odometro {

    public static final int INICIAR_JORNADA = 1;
    public static final int FINALIZAR_JORNADA = 0;

    public static final String TABELA = "odometro";
    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_ATIVIDADE = "atividade";
    public static final String CAMPO_DATA_ENVIO = "data_envio";
    public static final String CAMPO_KM_DIA = "km_dia";
    public static final String CAMPO_KM_ODOMENTRO = "km_odometro";
    public static final String CAMPO_CAMINHO_FOTO = "caminho_foto";
    public static final String CAMPO_FOTO = "foto";
    public static final String CAMPO_LATITUDE = "latitude";
    public static final String CAMPO_LONGITUDE = "longitude";
    public static final String CAMPO_IS_SINC = "is_sinc";

    private int id;
    private int atividade;
    private String dataEnvio;
    private String caminhoFoto;
    private String foto;
    private int kmDoDia;
    private int kmDoOdometro;
    private String latitude;
    private String longitude;
    private int isSinc;

    public Odometro() {
        this.id = 0;
        this.atividade = 0;
        this.dataEnvio = "";
        this.caminhoFoto = "";
        this.foto = "";
        this.kmDoDia = 0;
        this.kmDoOdometro = 0;
        this.latitude = "";
        this.longitude = "";
        this.isSinc = 0;
    }

    public Odometro(JSONObject jsonObject) {
        this.atividade = jsonObject.optInt("r24_fgatividade");
        this.dataEnvio = jsonObject.optString("r24_dtatividade");
        this.kmDoDia = jsonObject.optInt("r24_nrKMdia");
        this.kmDoOdometro = jsonObject.optInt("r24_nrOdometro");
        this.caminhoFoto = jsonObject.optString("r24_txfoto");
        this.latitude = jsonObject.optString("r24_txlatitude");
        this.longitude = jsonObject.optString("r24_txlongitude");
        this.foto = "";
        this.isSinc = 0;
    }

    public Odometro(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(CAMPO_ID));
        this.atividade = cursor.getInt(cursor.getColumnIndex(CAMPO_ATIVIDADE));
        this.dataEnvio = cursor.getString(cursor.getColumnIndex(CAMPO_DATA_ENVIO));
        this.caminhoFoto = cursor.getString(cursor.getColumnIndex(CAMPO_CAMINHO_FOTO));
        this.foto = cursor.getString(cursor.getColumnIndex(CAMPO_FOTO));
        this.kmDoDia = cursor.getInt(cursor.getColumnIndex(CAMPO_KM_DIA));
        this.kmDoOdometro = cursor.getInt(cursor.getColumnIndex(CAMPO_KM_ODOMENTRO));
        this.latitude = cursor.getString(cursor.getColumnIndex(CAMPO_LATITUDE));
        this.longitude = cursor.getString(cursor.getColumnIndex(CAMPO_LONGITUDE));
        this.isSinc = cursor.getInt(cursor.getColumnIndex(CAMPO_IS_SINC));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAtividade() {
        return atividade;
    }

    public void setAtividade(int atividade) {
        this.atividade = atividade;
    }

    public String getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(String dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public String getCaminhoFoto() {
        return caminhoFoto;
    }

    public void setCaminhoFoto(String foto) {
        this.caminhoFoto = foto;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getKmDoDia() {
        return kmDoDia;
    }

    public void setKmDoDia(int kmDoDia) {
        this.kmDoDia = kmDoDia;
    }

    public int getKmDoOdometro() {
        return kmDoOdometro;
    }

    public void setKmDoOdometro(int kmDoOdometro) {
        this.kmDoOdometro = kmDoOdometro;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getIsSinc() {
        return isSinc;
    }

    public void setIsSinc(int isSinc) {
        this.isSinc = isSinc;
    }

    @Override
    public String toString() {
        return "Odomentro: " + kmDoOdometro + " - dataEnvio: " + dataEnvio;
    }



    public JSONObject getJSONParams() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("atividade", this.atividade);
        json.put("dataEnvio", this.dataEnvio);
        json.put("kmDia", this.kmDoDia);
        json.put("kmOdometro", this.kmDoOdometro);
        json.put("latitude", this.latitude);
        json.put("longitude", this.longitude);
        json.put("foto", this.foto);
        json.put("isSync", this.isSinc);
        return json;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(CAMPO_ATIVIDADE, this.getAtividade());
        values.put(CAMPO_DATA_ENVIO, this.getDataEnvio());
        values.put(CAMPO_KM_DIA, this.getKmDoDia());
        values.put(CAMPO_KM_ODOMENTRO, this.getKmDoOdometro());
        values.put(CAMPO_CAMINHO_FOTO, this.getCaminhoFoto());
        values.put(CAMPO_FOTO, this.getFoto());
        values.put(CAMPO_LATITUDE, this.getLatitude());
        values.put(CAMPO_LONGITUDE, this.getLongitude());
        values.put(CAMPO_IS_SINC, this.getIsSinc());
        return values;
    }

}
