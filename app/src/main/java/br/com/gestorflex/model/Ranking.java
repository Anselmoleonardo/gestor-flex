package br.com.gestorflex.model;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class Ranking implements Serializable {

    private String titulo;
    private List<String> valores;

//    private int isHeader;
//    private String produto;
//    private String qtd;

//    public Ranking(JSONObject jsonObject) {
//        this.isHeader = jsonObject.optInt("isHeader");
//        this.produto = jsonObject.optString("produto");
//        this.qtd = jsonObject.optString("dados");
//    }

//    public int getIsHeader() {
//        return isHeader;
//    }
//
//    public void setIsHeader(int isHeader) {
//        this.isHeader = isHeader;
//    }
//
//    public String getProduto() {
//        return produto;
//    }
//
//    public void setProduto(String produto) {
//        this.produto = produto;
//    }
//
//    public String getQtd() {
//        return qtd;
//    }
//
//    public void setQtd(String qtd) {
//        this.qtd = qtd;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<String> getValores() {
        return valores;
    }

    public void setValores(List<String> valores) {
        this.valores = valores;
    }
}
