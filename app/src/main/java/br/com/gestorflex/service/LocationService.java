package br.com.gestorflex.service;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.MonitoramentoCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.Monitoramento;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;

public class LocationService extends Service {

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;

    private Handler handlerRecuperaLocalizacao;
    private Runnable repetirRecuperaLocalizacao;

    private int idNotification = 0;
    private int intervaloRecuperacaoLocalizacao = 10 * 1000; // 10 Segundos
    private int qtdParaEnvio = 80;
    private int qtdParaEnvioAposFalha = 40;
    private int qtdGeralParaEnvio = 0;
    private int qtdParaCasoErro = 1500;
    private String latitudes;
    private String longitudes;
    private String datas;

    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        inicializarQtdGeralEnvio();
        setupVolleyListeners();
        configurarRepeticaoLocalizacao();
        return START_STICKY;
    }

    private void inicializarQtdGeralEnvio() {
        List<Monitoramento> monitoramentos = MonitoramentoCTRL.listMonitoramentos(this);
        qtdGeralParaEnvio = qtdGeralParaEnvio < monitoramentos.size() ? (monitoramentos.size() + 2) : qtdParaEnvio;
    }

    private void startMyOwnForeground() {
        startForeground(2, buildNotification(getString(R.string.text_notification_servico_localizacao_ligado)));
    }

    private Notification buildNotification(String texto) {
        String channelId = "tradeflex_notification";
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.icone_app_notif).setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_logo))
                .setContentTitle(getString(R.string.app_name)).setContentText(texto).setPriority(NotificationCompat.PRIORITY_HIGH);
        notificationBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Serviço geolocalização", NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(channel);
        }
        idNotification++;
        return notificationBuilder.build();
    }

    private void configurarRepeticaoLocalizacao() {
        if (handlerRecuperaLocalizacao == null && repetirRecuperaLocalizacao == null) {
            handlerRecuperaLocalizacao = new Handler();
            repetirRecuperaLocalizacao = new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.checkSelfPermission(LocationService.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                            adicionarGeolocalizacao();
                        else
                            abrirTelaPreferenciasApp();
                    } else
                        adicionarGeolocalizacao();
                    handlerRecuperaLocalizacao.postDelayed(this, intervaloRecuperacaoLocalizacao);
                }
            };
            handlerRecuperaLocalizacao.postDelayed(repetirRecuperaLocalizacao, intervaloRecuperacaoLocalizacao);
        }
    }

    private void adicionarGeolocalizacao() {
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        if (GeolocalizacaoUtil.verificarGeolocalizacao(LocationService.this)) {
            if (Geolocalizacao.getLatitude() != 0 && Geolocalizacao.getLatitude() != 0 &&
                    DateUtil.isHourInInterval(usuario.getDataInicioMonitoramento(), usuario.getDataFinalMonitoramento())) {
                Monitoramento monitoramento = new Monitoramento(String.valueOf(Geolocalizacao.getLatitude()),
                        String.valueOf(Geolocalizacao.getLongitude()), DateUtil.getCurrentDateTimeUS());
                GenericCTRL.save(LocationService.this, Monitoramento.TABELA, monitoramento.getContentValues());
                List<Monitoramento> monitoramentos = MonitoramentoCTRL.listMonitoramentos(LocationService.this);
                if (monitoramentos.size() == qtdGeralParaEnvio || monitoramentos.size() >= qtdParaCasoErro)
                    enviarMonitoramento(monitoramentos);
//                Toast.makeText(LocationService.this, "Registro: " + monitoramentos.size() + "\nLatitude: " + Geolocalizacao.getLatitude() + "\nLongitude: " +
//                        Geolocalizacao.getLongitude(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(LocationService.this, R.string.message_toast_ligar_geolocalizacao, Toast.LENGTH_LONG).show();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        if (usuario != null && usuario.getBoMonitoramento() == 1) {
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("restartservice");
//        broadcastIntent.setClass(this, RestartServiceBroadcast.class);
            this.sendBroadcast(broadcastIntent);
        } else {
            GeolocalizacaoUtil.pararAtualizacoes(this);
            handlerRecuperaLocalizacao.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }

    public static boolean isLocationServiceRunning(Context context) {
        Class<?> serviceClass = LocationService.class;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.e("Service status", "Running");
                return true;
            }
        }
        Log.e("Service status", "Not running");
        return false;
    }


    private void getValoresConcatenados(List<Monitoramento> list) {
        latitudes = list.get(0).getLatitude();
        longitudes = list.get(0).getLongitude();
        datas = list.get(0).getDataHora();
        for (int i = 1; i < list.size(); i++) {
            latitudes += "||" + list.get(i).getLatitude();
            longitudes += "||" + list.get(i).getLongitude();
            datas += "||" + list.get(i).getDataHora();
        }
    }

    public static void tratarRetornoMonitoramento(Context context, int boMonitoramento) {
        Usuario usuario = UsuarioCTRL.getUsuario(context);
        if (!LocationService.isLocationServiceRunning(context) && boMonitoramento == 1) {
            atualizarUsuario(context, usuario, boMonitoramento);
            LocationService locationService = new LocationService();
            Intent serviceIntent = new Intent(context, locationService.getClass());
            context.startService(serviceIntent);
        }
    }

    private static void atualizarUsuario(Context context, Usuario usuario, int boMonitoramento) {
        if (usuario.getBoMonitoramento() != boMonitoramento) {
            usuario.setBoMonitoramento(boMonitoramento);
            GenericCTRL.update(context, Usuario.TABELA, usuario.getId(), usuario.getContetValues());
        }
    }

    private void tratarErrorVolley() {
        qtdGeralParaEnvio += qtdParaEnvioAposFalha;
    }

    private void abrirTelaPreferenciasApp() {
        Toast.makeText(LocationService.this, R.string.message_toast_conceder_permissao_localizacao, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    // --------------------------------- VOLLEY ---------------------------------

    private void enviarMonitoramento(List<Monitoramento> monitoramentos) {
        ErrorUtil.montarCaminhoErro();
        try {
            Log.e("LOG_M", "Enviando...");
            latitudes = longitudes = datas = "";
            getValoresConcatenados(monitoramentos);
            JSONObject logParams = new JSONObject();
            logParams.put("qtd_envios", monitoramentos.size());
            logParams.put("latitudes", latitudes);
            logParams.put("longitudes", longitudes);
            logParams.put("datas", datas);
            ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_gravar_monitoramento), responseListener, errorListener, null);
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        responseListener = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    Log.e(ExternalDB.MSG_RETORNO, jsonResposta.optString(ExternalDB.MSG_RETORNO) + "teste");
                    if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                        qtdGeralParaEnvio = qtdParaEnvio;
                        GenericCTRL.deleteAll(LocationService.this, Monitoramento.TABELA);
                        // Atualiza Usuario
                        JSONArray jArrayMonitoramento = jsonRetorno.getJSONArray("monitoramento");
                        int boMonitoramento = jArrayMonitoramento.getJSONObject(0).optInt("d05_bomonitoramento", 1);
                        Usuario usuario = UsuarioCTRL.getUsuario(LocationService.this);
                        atualizarUsuario(LocationService.this, usuario, boMonitoramento);
                        if (boMonitoramento == 0)
                            stopSelf();
                    } else
                        tratarErrorVolley();
                } else
                    tratarErrorVolley();
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(LocationService.this, e);
            }
        };
        errorListener = error -> tratarErrorVolley();
    }

}
