package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Cliente;
import br.com.gestorflex.ui.holder.ClienteHolder;

public class ClienteAdapter extends RecyclerView.Adapter<ClienteHolder> {

    private List<Cliente> listaVendas;
    private Context context;

    public ClienteAdapter(Context context, List<Cliente> listaVendas) {
        this.listaVendas = new ArrayList<>(listaVendas);
        this.context = context;
    }

    @NonNull
    @Override
    public ClienteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ClienteHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_cliente_venda, parent, false));
    }

    @Override
    public void onBindViewHolder(ClienteHolder holder, int position) {
        Cliente cliente = listaVendas.get(position);
        holder.bindCliente(context, cliente);
    }

    public void insertItem(Cliente venda) {
        listaVendas.add(venda);
        notifyItemInserted(getItemCount());
    }

    public void removeItem(int position) {
        listaVendas.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listaVendas.size());
    }

    @Override
    public int getItemCount() {
        return listaVendas != null ? listaVendas.size() : 0;
    }

}
