package br.com.gestorflex.ui.activity;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.model.UsuarioOfensor;
import br.com.gestorflex.ui.adapter.UsuarioOfensorAdapter;
import br.com.gestorflex.util.BottomBarUtil;

public class UsuarioOfensorResultadoActivity extends AppCompatActivity {

    private TextView _tvTotal;
    private RecyclerView _rvUsuariosOfensores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_ofensor_resultado);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        Toolbar _toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(_toolbar);
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        _tvTotal = findViewById(R.id.tv_resultado_usuarios_total);
        _rvUsuariosOfensores = findViewById(R.id.rv_resultado_usuarios_ofensores);
        configurarViews();
    }

    private void configurarViews() {
        List<UsuarioOfensor> usuariosOfensores = (List<UsuarioOfensor>) getIntent().getSerializableExtra("usuariosOfensores");
        UsuarioOfensorAdapter adapter = new UsuarioOfensorAdapter(this, usuariosOfensores);
        _rvUsuariosOfensores.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        _rvUsuariosOfensores.setAdapter(adapter);
        _tvTotal.setText(String.format("Total: %s", adapter.getItemCount()));
    }
}
