package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Response;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.CheckoutCTRL;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.OdomentroCTRL;
import br.com.gestorflex.database.internal.controller.RespostaPerguntaCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Checkout;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.ViewPagerAdapter;
import br.com.gestorflex.ui.fragment.fragments_odometro_sinc.TabOdometroSincCheckIn;
import br.com.gestorflex.ui.fragment.fragments_odometro_sinc.TabOdometroSincCheckOut;
import br.com.gestorflex.ui.fragment.fragments_odometro_sinc.TabOdometroSincJornada;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.ErrorUtil;

public class SincronizacaoOdomActivity extends AppCompatActivity implements View.OnClickListener {

    static final int TAB_JORNADA = 0;
    static final int TAB_CHECK_IN = 1;
    static final int TAB_CHECK_OUT = 2;

    private ViewPager _viewPager;
    private TabLayout _tabLayout;
    private Button _btnEnviarDados;
    private TabOdometroSincJornada _fgTabOdometroSincJornada;
    private TabOdometroSincCheckIn _fgTabOdometroSincCheckIn;
    private TabOdometroSincCheckOut _fgTabOdometroSincCheckOut;

    private ProgressDialog progressDialog;

    private Response.Listener<String> responseListenerJornada;
    private Response.ErrorListener errorListenerJornada;

    private Response.Listener<String> responseListenerChekIn;
    private Response.ErrorListener errorListenerChekIn;

    private Response.Listener<String> responseListenerCheckOut;
    private Response.ErrorListener errorListenerCheckOut;

    private ViewPagerAdapter adapter;

    private int qtdJornada = 0;
    private int qtdCheckIn = 0;
    private int qtdCheckOut = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sincronizacao_odom);
        BottomBarUtil.configurar(this);
        inicializarViews();
    }

    private void inicializarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(_toolbar);
            //getSupportActionBar().setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
            Usuario usuario = UsuarioCTRL.getUsuario(this);
            _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());

            _viewPager = findViewById(R.id.viewpager);
            _btnEnviarDados = findViewById(R.id.btn_enviar_dados_sinc);
            setupViewPager(_viewPager);
            _viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tabLayout));

            _tabLayout = findViewById(R.id.tabs);
            _tabLayout.setupWithViewPager(_viewPager);
            inicializarDados();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void inicializarDados() throws Exception {
        setupVolleyListenersJornada();
        setupVolleyListenersCheckIn();
        setupVolleyListenersCheckOut();
        _btnEnviarDados.setOnClickListener(this);
    }

    private void setupVolleyListenersJornada() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(SincronizacaoOdomActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        responseListenerJornada = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
                        int result = GenericCTRL.delete(SincronizacaoOdomActivity.this, Odometro.TABELA, _fgTabOdometroSincJornada.sincJornadaAdapter.getVendas().get(0).getId());
                        if (result > 0) {
                            _fgTabOdometroSincJornada.sincJornadaAdapter.removeItem(0);
                            if (_fgTabOdometroSincJornada.sincJornadaAdapter.getItemCount() > 0) {
                                enviarJornada();
                            } else {
                                if (qtdCheckIn > 0) {
                                    _viewPager.setCurrentItem(TAB_CHECK_IN);
                                    enviarCheckIn();
                                } else if (qtdCheckOut > 0) {
                                    _viewPager.setCurrentItem(TAB_CHECK_OUT);
                                    enviarCheckOut();
                                } else {
                                    progressDialog.dismiss();
                                    new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                                            .setMessage("Dados sincronizados com sucesso!").setPositiveButton("OK", null)
                                            .setOnDismissListener(dialog -> finish()).show();
                                }
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(SincronizacaoOdomActivity.this, "Erro ao excluir do Banco de Dados Interno", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        int result = GenericCTRL.delete(SincronizacaoOdomActivity.this, Odometro.TABELA, _fgTabOdometroSincJornada.sincJornadaAdapter.getVendas().get(0).getId());
                        if (result > 0) {
                            _fgTabOdometroSincJornada.sincJornadaAdapter.removeItem(0);
                        }
                        progressDialog.dismiss();

                        new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                                .setMessage(getString(R.string.message_error_sinc_ponto, jsonResposta.optString("msgRetorno")))
                                .setPositiveButton("OK", null).show();
                    }
                } else {
                    progressDialog.dismiss();
                    new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                            .setMessage("Ocorreu uma FALHA na conexão!").setPositiveButton("OK", null)
                            .setOnDismissListener(dialog -> finish()).show();
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(SincronizacaoOdomActivity.this, e);
            }
        };
        errorListenerJornada = error -> {
            progressDialog.dismiss();
            new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                    .setMessage("Ocorreu uma FALHA na conexão!")
                    .setPositiveButton("OK", null).show();
        };
    }

    private void setupVolleyListenersCheckIn() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(SincronizacaoOdomActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        responseListenerChekIn = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
                        int result = GenericCTRL.delete(SincronizacaoOdomActivity.this, RespostaPergunta.TABELA, _fgTabOdometroSincCheckIn.sincCheckInAdapter.getCheckIns().get(0).getId());
                        if (result > 0) {
                            _fgTabOdometroSincCheckIn.sincCheckInAdapter.removeItem(0);
                            if (_fgTabOdometroSincCheckIn.sincCheckInAdapter.getItemCount() > 0) {
                                enviarCheckIn();
                            } else {
                                if (qtdCheckOut > 0) {
                                    _viewPager.setCurrentItem(TAB_CHECK_OUT);
                                    enviarCheckOut();
                                } else {
                                    progressDialog.dismiss();
                                    new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                                            .setMessage("Dados sincronizados com sucesso!").setPositiveButton("OK", null)
                                            .setOnDismissListener(dialog -> finish()).show();
                                }
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(SincronizacaoOdomActivity.this, "Erro ao excluir do Banco de Dados Interno", Toast.LENGTH_SHORT).show();
                        }
                    } else {

                        int result = GenericCTRL.delete(SincronizacaoOdomActivity.this, RespostaPergunta.TABELA, _fgTabOdometroSincCheckIn.sincCheckInAdapter.getCheckIns().get(0).getId());
                        if (result > 0) {
                            _fgTabOdometroSincCheckIn.sincCheckInAdapter.removeItem(0);
                        }

                        progressDialog.dismiss();
                        new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this)
                                .setTitle("Aviso")
                                .setMessage(getString(R.string.message_error_sinc_ponto, jsonResposta.optString("msgRetorno")))
                                .setPositiveButton("OK", null)
                                .show();
                    }
                } else {
                    progressDialog.dismiss();
                    new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                            .setMessage("Ocorreu uma FALHA na conexão!")
                            .setPositiveButton("OK", null).show();
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(SincronizacaoOdomActivity.this, e);
            }
        };
        errorListenerChekIn = error -> {
            progressDialog.dismiss();
            new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                    .setMessage("Ocorreu uma FALHA na conexão!")
                    .setPositiveButton("OK", null).show();
        };
    }

    private void setupVolleyListenersCheckOut() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(SincronizacaoOdomActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        responseListenerCheckOut = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
                        int result = GenericCTRL.delete(SincronizacaoOdomActivity.this, Checkout.TABELA, _fgTabOdometroSincCheckOut.sincCheckOutAdapter.getCheckOuts().get(0).getId());
                        if (result > 0) {
                            _fgTabOdometroSincCheckOut.sincCheckOutAdapter.removeItem(0);
                            if (_fgTabOdometroSincCheckOut.sincCheckOutAdapter.getItemCount() > 0) {
                                enviarCheckOut();
                            } else {
                                progressDialog.dismiss();
                                new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                                        .setMessage("Dados sincronizados com sucesso!").setPositiveButton("OK", null)
                                        .setOnDismissListener(dialog -> finish()).show();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(SincronizacaoOdomActivity.this, "Erro ao excluir do Banco de Dados Interno", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        int result = GenericCTRL.delete(this, Checkout.TABELA, _fgTabOdometroSincCheckOut.sincCheckOutAdapter.getCheckOuts().get(0).getId());
                        if (result > 0) {
                            _fgTabOdometroSincCheckOut.sincCheckOutAdapter.removeItem(0);
                        }
                        progressDialog.dismiss();
                        new androidx.appcompat.app.AlertDialog.Builder(this)
                                .setTitle("Aviso")
                                .setMessage(getString(R.string.message_error_sinc_ponto, jsonResposta.optString("msgRetorno")))
                                .setPositiveButton("OK", null)
                                .show();
                    }

                } else {
                    progressDialog.dismiss();
                    new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                            .setMessage("Ocorreu uma FALHA na conexão!")
                            .setPositiveButton("OK", null).show();
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(SincronizacaoOdomActivity.this, e);
            }
        };
        errorListenerCheckOut = error -> {
            progressDialog.dismiss();
            new androidx.appcompat.app.AlertDialog.Builder(SincronizacaoOdomActivity.this).setTitle("Aviso")
                    .setMessage("Ocorreu uma FALHA na conexão!")
                    .setPositiveButton("OK", null).show();
        };
    }

    private void enviarJornada() throws Exception {
        ErrorUtil.montarCaminhoErro();
        progressDialog.setMessage("Enviando jornada...");
        if (!progressDialog.isShowing())
            progressDialog.show();
        Odometro odometro = _fgTabOdometroSincJornada.sincJornadaAdapter.getVendas().get(0);
        final JSONObject logParams = odometro.getJSONParams();
        logParams.put("isSinc", 1);
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_jornada_lad), responseListenerJornada, errorListenerJornada, null);
    }

    private void enviarCheckIn() throws Exception {
        ErrorUtil.montarCaminhoErro();
        progressDialog.setMessage("Enviando Check-in...");
        if (!progressDialog.isShowing())
            progressDialog.show();
        final JSONObject logParams = _fgTabOdometroSincCheckIn.sincCheckInAdapter.getCheckIns().get(0).getLogRespostaOdomentro();
        logParams.put("isSinc", 1);
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_questionario_odomentro), responseListenerChekIn, errorListenerChekIn, null);
    }

    private void enviarCheckOut() throws Exception {
        ErrorUtil.montarCaminhoErro();
        progressDialog.setMessage("Enviando Check-out...");
        if (!progressDialog.isShowing())
            progressDialog.show();
        Checkout odometro = _fgTabOdometroSincCheckOut.sincCheckOutAdapter.getCheckOuts().get(0);
        final JSONObject logParams = odometro.pegarJson();
        logParams.put("isSinc", 1);
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_questionario_odomentro), responseListenerCheckOut, errorListenerCheckOut, null);
    }

    private void setupViewPager(ViewPager viewPager) throws Exception {
        _fgTabOdometroSincJornada = new TabOdometroSincJornada();
        _fgTabOdometroSincCheckIn = new TabOdometroSincCheckIn();
        _fgTabOdometroSincCheckOut = new TabOdometroSincCheckOut();
        qtdJornada = OdomentroCTRL.pegarJornadas(this) != null ? OdomentroCTRL.pegarJornadas(this).size() : 0;
        qtdCheckIn = RespostaPerguntaCTRL.listRespostaPergunta(this) != null ? RespostaPerguntaCTRL.listRespostaPergunta(this).size() : 0;
        qtdCheckOut = CheckoutCTRL.pegarCheckOuts(this) != null ? CheckoutCTRL.pegarCheckOuts(this).size() : 0;
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(_fgTabOdometroSincJornada, getString(R.string.title_tab_odometro_sinc_jornada) + " " + qtdJornada);
        adapter.addFragment(_fgTabOdometroSincCheckIn, getString(R.string.title_tab_odometro_sinc_check_in) + " " + qtdCheckIn);
        adapter.addFragment(_fgTabOdometroSincCheckOut, getString(R.string.title_tab_odometro_sinc_check_out) + " " + qtdCheckOut);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_enviar_dados_sinc:
//                if (GeolocalizacaoUtil.verificarGeolocalizacao(SincronizacaoOdomActivity.this)) {
                try {
                    qtdJornada = OdomentroCTRL.pegarJornadas(this) != null ? OdomentroCTRL.pegarJornadas(this).size() : 0;
                    qtdCheckIn = RespostaPerguntaCTRL.listRespostaPergunta(this) != null ? RespostaPerguntaCTRL.listRespostaPergunta(this).size() : 0;
                    qtdCheckOut = CheckoutCTRL.pegarCheckOuts(this) != null ? CheckoutCTRL.pegarCheckOuts(this).size() : 0;
                    if (qtdJornada > 0)
                        enviarJornada();
                    else if (qtdCheckIn > 0)
                        enviarCheckIn();
                    else if (qtdCheckOut > 0)
                        enviarCheckOut();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                }
        }
    }
}


