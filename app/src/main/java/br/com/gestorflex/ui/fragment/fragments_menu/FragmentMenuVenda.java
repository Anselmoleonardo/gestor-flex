package br.com.gestorflex.ui.fragment.fragments_menu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.BotaoCTRL;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Botao;
import br.com.gestorflex.ui.activity.ClientesFiltroActivity;
import br.com.gestorflex.ui.activity.ComparativoVendaFiltroActivity;
import br.com.gestorflex.ui.activity.DefaultActivity;
import br.com.gestorflex.ui.activity.MenuActivity;
import br.com.gestorflex.ui.activity.ProdutividadeActivity;
import br.com.gestorflex.ui.activity.RankingFiltroActivity;
import br.com.gestorflex.ui.activity.RotaVendaFiltroActivity;
import br.com.gestorflex.ui.activity.UsuarioOfensorFiltroActivity;
import br.com.gestorflex.ui.adapter.BotaoMenuAdapter;
import br.com.gestorflex.util.ErrorUtil;

/**
 * Created by Vicente on 21/05/2019.
 */

public class FragmentMenuVenda extends Fragment implements OnRecyclerItemClickListener {

    public static final int TAB_VENDA = 2;

    public static final String TAG_CLIENTES = "consuClientes";
    public static final String TAG_ROTAS = "consulRotas";
    public static final String TAG_USUARIO_OFENSOR = "usuarioOfensor";
    public static final String TAG_RANKING = "ranking";
    public static final String TAG_PRODUTIVIDADE = "produtividade";
    public static final String TAG_COMPARATIVO = "comparativoVenda";
    public static final String TAG_EXCLUIR_VENDA = "excluirVenda";

    private BotaoMenuAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_venda, container, false);
        getLayoutVenda(view);
        return view;
    }

    private int getLayoutVenda(View view) {
        int layout = R.layout.fragment_menu_venda;
        if (adapter == null || adapter.getItemCount() == 0) {
            List<Botao> botoes = BotaoCTRL.getList(getActivity(), TAB_VENDA);
            if (botoes.isEmpty())
                layout = R.layout.fragment_sem_permissao;
            else
                configuraRecyclerView(view, botoes);
        }
        return layout;
    }

    private void configuraRecyclerView(View view, List<Botao> botoes) {
        RecyclerView rvBotoes = view.findViewById(R.id.menu_venda_lista);
        adapter = new BotaoMenuAdapter(getContext(), botoes);
        adapter.setOnRecyclerItemClickListener(this);
        rvBotoes.setAdapter(adapter);
    }

    private int getPreferencesIsCheckin() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(this.getString(R.string.isCheckin), Context.MODE_PRIVATE);
        String count = sharedPreferences.getString(this.getString(R.string.isCheckin), "0");
        return Integer.parseInt(count);
    }

    private int getPreferencesIsChere() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(this.getString(R.string.isChere), Context.MODE_PRIVATE);
        String count = sharedPreferences.getString(this.getString(R.string.isChere), "0");
        return Integer.parseInt(count);
    }

    @Override
    public void onRecyclerItemClickListener(View view, int position, Object item) {
        try {
            Botao botao = (Botao) item;
            Intent intent = null;
            switch (botao.getTag()) {
                case TAG_CLIENTES:
                    intent = new Intent(getActivity(), ClientesFiltroActivity.class);
                    break;

                case TAG_ROTAS:
                    intent = new Intent(getActivity(), RotaVendaFiltroActivity.class);
                    break;
                case TAG_PRODUTIVIDADE:
                    intent = new Intent(getActivity(), ProdutividadeActivity.class);
                    break;
                case TAG_USUARIO_OFENSOR:
                    intent = new Intent(getActivity(), UsuarioOfensorFiltroActivity.class);
                    break;
                case TAG_RANKING:
                    intent = new Intent(getActivity(), RankingFiltroActivity.class);
                    break;
                case TAG_COMPARATIVO:
                intent = new Intent(getActivity(), ComparativoVendaFiltroActivity.class);
                    break;
                case TAG_EXCLUIR_VENDA:
//                intent = new Intent(getActivity(), SimuladorFiltroActivity.class);
                    break;
                default:
                    intent = new Intent(getActivity(), DefaultActivity.class); // Intent Default
            }
            if (intent != null) {
                intent.putExtra(MenuActivity.TAG_TITLE, botao.getNome());
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }
}
