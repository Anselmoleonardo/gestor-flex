package br.com.gestorflex.ui.holder;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.gestorflex.R;
import br.com.gestorflex.model.CheckInOut;


public class SincronizacaoCheckInOutHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    // Valores
    private AppCompatTextView _tvDescricao;
    private AppCompatTextView _tvTipo;

    public SincronizacaoCheckInOutHolder(@NonNull View itemView) {
        super(itemView);
        _tvDescricao = itemView.findViewById(R.id.tv_item_rota_sincronizacao_descricao);
        _tvTipo = itemView.findViewById(R.id.tv_item_rota_sincronizacao_tipo);
        itemView.setOnClickListener(this);
    }

    public void bindCheckInOut(Context context, CheckInOut checkInOut) {
        String tipo = checkInOut.getFlagTipoCheck() == CheckInOut.TIPO_CHECK_IN ?
                context.getString(R.string.text_check_in) : context.getString(R.string.text_check_out);
        _tvDescricao.setText(checkInOut.getNomePdv());
        _tvTipo.setText(tipo);
    }

    @Override
    public void onClick(View v) {
    }

}

