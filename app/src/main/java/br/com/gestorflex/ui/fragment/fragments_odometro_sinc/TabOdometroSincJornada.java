package br.com.gestorflex.ui.fragment.fragments_odometro_sinc;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.Response;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.OdomentroCTRL;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.ui.adapter.SincJornadaAdapter;
import br.com.gestorflex.util.ErrorUtil;


public class TabOdometroSincJornada extends Fragment {

    private RecyclerView _rvSinc;
    private Button _btnSinc;

    public SincJornadaAdapter sincJornadaAdapter;

    private Request request;
    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_odometro_sinc_jornada, container, false);
        instanciarViews(rootView);
        return rootView;
    }

    private void instanciarViews(View view) {
        _rvSinc = view.findViewById(R.id.rv_odometro_sinc_jornada);
        configurarViews();
    }

    private void configurarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            configurarRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }

    private void configurarRecyclerView() {
        List<Odometro> odometros = OdomentroCTRL.pegarJornadas(getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        _rvSinc.setLayoutManager(layoutManager);
        sincJornadaAdapter = new SincJornadaAdapter(getActivity(), odometros);
//        sincJornadaAdapter.setRecyclerViewOnClickListenerHack(this);
        _rvSinc.setAdapter(sincJornadaAdapter);
    }


//    @Override
//    public void OnItemClickListener(View view, final int position, Object item) {
//        final Odometro odometro = (Odometro) item;
//        String[] opcoes = {"Excluir jornada"};
//        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getActivity());
//        alert.setItems(opcoes, (dialog, which) -> {
//            switch (which) {
//                case 0:
//                    int result = GenericCTRL.delete(getActivity(), Odometro.TABELA, odometro.getId());
//                    if (result > 0) {
//                        sincJornadaAdapter.removeItem(position);
//                        Toast.makeText(getActivity(), "Jornada removida com sucesso!", Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//            }
//
//        });
//        alert.show();
//    }

}
