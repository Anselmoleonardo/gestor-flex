package br.com.gestorflex.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Response;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.EstadoCTRL;
import br.com.gestorflex.database.internal.controller.GrupoEconomicoCTRL;
import br.com.gestorflex.database.internal.controller.ListarPorCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioGerenciadoCTRL;
import br.com.gestorflex.model.Cliente;
import br.com.gestorflex.model.Estado;
import br.com.gestorflex.model.GrupoEconomico;
import br.com.gestorflex.model.ListarPor;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.model.UsuarioGerenciado;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;

/**
 * Created by Vicente on 21/05/2019.
 */

public class ClientesFiltroActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private AppCompatSpinner _spEstado;
    private AppCompatSpinner _spGrupoEconomico;
    private AppCompatSpinner _spDia;
    private AppCompatSpinner _spMes;
    private AppCompatSpinner _spAno;
    private AppCompatSpinner _spOpcao;
    private AppCompatSpinner _spListarPor;
    private AppCompatSpinner _spUsuariosGerenciados;
    private FloatingActionButton _fabPesquisar;

    private Response.Listener<String> responseListener;
    private Response.Listener<String> responseListenerDadosUsuario;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrar_busca_clientes);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            _toolbar.setSubtitle(UsuarioCTRL.getUsuario(this).getMatriculaNome());
            setSupportActionBar(_toolbar);
            getSupportActionBar().setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
            _spEstado = findViewById(R.id.sp_filtrar_clientes_uf);
            _spGrupoEconomico = findViewById(R.id.sp_filtrar_clientes_grupo_econ);
            _spDia = findViewById(R.id.sp_filtrar_clientes_dia);
            _spMes = findViewById(R.id.sp_filtrar_clientes_mes);
            _spAno = findViewById(R.id.sp_filtrar_clientes_ano);
            _spOpcao = findViewById(R.id.sp_filtrar_clientes_opcao);
            _spListarPor = findViewById(R.id.sp_filtrar_clientes_listar_por);
            _spUsuariosGerenciados = findViewById(R.id.sp_filtrar_clientes_consultor);
            _fabPesquisar = findViewById(R.id.fab_filtrar_usuario_ofensor_pesquisar);
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() {
        preencherSpinnerUf();
        preencherSpinnerGrupoEconomico();
        preencherSpinnerDia();
        preencherSpinnerMes();
        preencherSpinnerAno();
        preencherSpinnerOpcao();
        preencherSpinnerListarPor();
        setupVolleyListeners();
        _fabPesquisar.setOnClickListener(this);
        _spEstado.setOnItemSelectedListener(this);
        _spListarPor.setOnItemSelectedListener(this);
    }

    public void preencherSpinnerUf() {
        List<Estado> listEstados = EstadoCTRL.listEstados(this);
        if (listEstados.size() > 1)
            listEstados.add(0, new Estado(0, "TODOS"));
        ArrayAdapter<Estado> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listEstados);
        _spEstado.setAdapter(dataAdapter);
    }

    public void preencherSpinnerGrupoEconomico() {
        try {
            List<GrupoEconomico> listGrupoEconomicos = GrupoEconomicoCTRL.listGruposEconomicos(this);
            if (listGrupoEconomicos.size() > 1)
                listGrupoEconomicos.add(0, new GrupoEconomico(0, "TODOS"));
            ArrayAdapter<GrupoEconomico> dataAdapter = new ArrayAdapter<>(this,
                    R.layout.support_simple_spinner_dropdown_item, listGrupoEconomicos);
            _spGrupoEconomico.setAdapter(dataAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void preencherSpinnerDia() {
        List<String> dias = DateUtil.getTodosDiasDoMes(_spAno.getSelectedItemPosition(), _spMes.getSelectedItemPosition() );
        dias.add(0, "TODOS");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, dias);
        _spDia.setAdapter(dataAdapter);
    }

    public void preencherSpinnerMes() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, DateUtil.getTodosMeses());
        _spMes.setAdapter(dataAdapter);
        _spMes.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.MONTH));
    }

    public void preencherSpinnerAno() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String[] gruposEcon = {(year - 1) + "", year + "", (year + 1) + ""};
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, gruposEcon);
        _spAno.setAdapter(dataAdapter);
        _spAno.setSelection(1);
    }

    public void preencherSpinnerOpcao() {
        try {
            String[] opcoes;
            if (usuario.getIdAgencia() == 15)
                opcoes = new String[]{"Credenciamento Aceito", "Credenciamento Recusado"};
            else if (usuario.getIdAgencia() == 18)
                opcoes = new String[]{"Proposta Prospectada", "PCAE Recusada"};
            else
                opcoes = new String[]{"Proposta Prospectada", "Proposta Recusada"};

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                    R.layout.support_simple_spinner_dropdown_item, opcoes);
            _spOpcao.setAdapter(dataAdapter);
//            _spOpcao.setOnItemSelectedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void preencherSpinnerListarPor() {
        List<ListarPor> listListarPor = ListarPorCTRL.listListarPor(this);
        ArrayAdapter<ListarPor> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, listListarPor);
        _spListarPor.setAdapter(dataAdapter);
    }

    public void preencherSpinnerUsuarioGerenciado(int privilegio, int idEstado) {
        try {
            List<UsuarioGerenciado> listConsultores = UsuarioGerenciadoCTRL.listUsuariosGerenciados(this, privilegio, idEstado);
            if (listConsultores.size() > 0) {
                if (listConsultores.size() > 1)
                    listConsultores.add(0, new UsuarioGerenciado(0, "TODOS", "00000", privilegio, 0));
            } else
                listConsultores.add(0, new UsuarioGerenciado(-1, "SEM USUÁRIOS", "00000", privilegio, 0));
            ArrayAdapter<UsuarioGerenciado> dataAdapter = new ArrayAdapter<>(this,
                    R.layout.support_simple_spinner_dropdown_item, listConsultores);
            _spUsuariosGerenciados.setAdapter(dataAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void recuperarDados() throws Exception {
        if (validarDataSelecionada()) {
            progressDialog.show();
            String dataParaEnviar = _spAno.getSelectedItem().toString() + "-" + _spMes.getSelectedItem().toString() + "-" +
                    (_spDia.getSelectedItemPosition() == 0 ? _spDia.getSelectedItemPosition() : _spDia.getSelectedItem().toString());
            JSONObject logParams = new JSONObject();
            logParams.put("cdUF", ((Estado) _spEstado.getSelectedItem()).getId() + "");
            logParams.put("cdGRUPO", ((GrupoEconomico) _spGrupoEconomico.getSelectedItem()).getId() + "");
            logParams.put("dtVENDA", dataParaEnviar);
            logParams.put("fgACEITE", _spOpcao.getSelectedItemPosition() == -1 ? 0 : _spOpcao.getSelectedItemPosition() + "");
            logParams.put("ordemLISTARPOR", ((ListarPor) _spListarPor.getSelectedItem()).getNrOrdem() + "");
            logParams.put("cdUSUARIOGERENCIADO", ((UsuarioGerenciado) _spUsuariosGerenciados.getSelectedItem()).getId() + "");
            ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_listar_clientes), responseListener, errorListener, null);
        }
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_buscando_clientes));
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                JSONObject jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                    JSONArray jArrayClientes = jsonRetorno.getJSONArray("clientes");
                    if (jArrayClientes.length() > 0) {
                        List<Cliente> clientes = new ArrayList<>();
                        for (int i = 0; i < jArrayClientes.length(); i++) {
                            clientes.add(new Cliente(jArrayClientes.getJSONObject(i)));
                        }
                        Intent intent = new Intent(ClientesFiltroActivity.this, ClientesResultadoActivity.class);
                        intent.putExtra("clientes", (Serializable) clientes);
                        startActivity(intent);
                    } else {
                        new AlertDialog.Builder(ClientesFiltroActivity.this)
                                .setTitle(R.string.title_alert_aviso)
                                .setMessage(R.string.message_alert_sem_resultados)
                                .setPositiveButton(R.string.label_alert_button_ok, null)
                                .show();
                    }
                } else {
                    new AlertDialog.Builder(ClientesFiltroActivity.this)
                            .setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                            .setPositiveButton(R.string.label_alert_button_ok, null)
                            .show();
                }
                progressDialog.dismiss();
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(ClientesFiltroActivity.this, e);
            }
        };
        responseListenerDadosUsuario = response -> {

        };

        errorListener = error -> {
            progressDialog.dismiss();
            Toast.makeText(ClientesFiltroActivity.this, "Erro ao conectar ao servidor", Toast.LENGTH_LONG).show();
        };
    }

    private boolean validarDataSelecionada() {
        if (_spDia.getSelectedItemPosition() != 0) {
            String dataParaValidar = _spDia.getSelectedItem().toString() + "/" +
                    _spMes.getSelectedItem().toString() + "/" +
                    _spAno.getSelectedItem().toString();
            if (!DateUtil.validateDate(dataParaValidar)) {
                new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso)
                        .setMessage(R.string.message_alert_data_invalida)
                        .setPositiveButton(R.string.label_alert_button_ok, null).show();
                return false;
            }
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab_filtrar_usuario_ofensor_pesquisar) {
            try {
                recuperarDados();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        ListarPor listarPor;
        Estado estado;
        switch (adapterView.getId()) {
            case R.id.sp_filtrar_clientes_listar_por:
                listarPor = (ListarPor) _spListarPor.getSelectedItem();
                estado = (Estado) _spEstado.getSelectedItem();
                preencherSpinnerUsuarioGerenciado(listarPor.getNrOrdem() - 1, estado.getId());
                break;
            case R.id.sp_filtrar_clientes_uf:
                listarPor = (ListarPor) _spListarPor.getSelectedItem();
                estado = (Estado) _spEstado.getSelectedItem();
                preencherSpinnerUsuarioGerenciado(listarPor.getNrOrdem() - 1, estado.getId());
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

}
