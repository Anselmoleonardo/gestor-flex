package br.com.gestorflex.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Response;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.GrupoEconomicoCTRL;
import br.com.gestorflex.database.internal.controller.PdvCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.GrupoEconomico;
import br.com.gestorflex.model.PDV;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.CustomInfoWindow;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;

public class BlindarPdvActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private GoogleMap mapa_blindar_pdv;
    private Toolbar _toolbar;
    private AppCompatSpinner _spGrupoEcon;
    private AppCompatSpinner _spPdv;
    private FloatingActionButton _fabPosicaoAtual;
    private FloatingActionButton _fabBlindarPvd;
    private ProgressDialog _pdCarregando;
    private SupportMapFragment mapFragment;


    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private JSONObject jsonRetorno;

    private GeolocalizacaoUtil geolocalizacaoUtil;
    private Marker markerNovo;
    private Circle circle;
    private Marker markerPDV;
    private PDV pdv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blindar_pdv);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        _spGrupoEcon = findViewById(R.id.sp_blindar_pdv_rede);
        _spPdv = findViewById(R.id.sp_blindar_pdv);
        _fabPosicaoAtual = findViewById(R.id.fab_blindar_pdv_posicao_atual);
        _fabBlindarPvd = findViewById(R.id.fab_blindar_pdv_enviar);
        _toolbar = findViewById(R.id.toolbar);
        configurarViews();
    }

    private void configurarViews() {
        mapFragment.getMapAsync(this);
        _toolbar.setSubtitle(UsuarioCTRL.getUsuario(this).getMatriculaNome());
        setSupportActionBar(_toolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
        setupVolleyListeners();
        geolocalizacaoUtil = new GeolocalizacaoUtil(this);
        geolocalizacaoUtil.getGeolocalizacao();
        _fabBlindarPvd.setOnClickListener(this);
        _fabPosicaoAtual.setOnClickListener(this);
        _spGrupoEcon.setOnItemSelectedListener(this);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mapa_blindar_pdv = googleMap;
        mapa_blindar_pdv.setInfoWindowAdapter(this);
        addMarkerUsuario();
    }

    private void addMarkerUsuario() {
        preencherSpinnerRede();
        MapsInitializer.initialize(getApplicationContext());
        LatLng latLng = new LatLng(Geolocalizacao.getLatitude(), Geolocalizacao.getLongitude());
        mapa_blindar_pdv.getUiSettings().setAllGesturesEnabled(true);
        CustomInfoWindow.criarInfoWindow(this, mapa_blindar_pdv);
        if (markerNovo != null)
            markerNovo.remove();

        markerNovo = mapa_blindar_pdv.addMarker(new MarkerOptions()
                .position(new LatLng(Geolocalizacao.getLatitude(), Geolocalizacao.getLongitude()))
                .title("POSIÇÃO ATUAL")
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        mapa_blindar_pdv.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Geolocalizacao.getLatitude(), Geolocalizacao.getLongitude()), 15f));
        markerNovo.showInfoWindow();

        criarCirculo();

        mapa_blindar_pdv.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                Vibrator v = (Vibrator) BlindarPdvActivity.this
                        .getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(100);
            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

                if (markerNovo != null)
                    markerNovo.remove();

                markerNovo = marker;
                LatLng pos = markerNovo.getPosition();

                String end = CustomInfoWindow.coletarEndereco(getApplicationContext(), pos);

                mapa_blindar_pdv.animateCamera(CameraUpdateFactory
                        .newLatLngZoom(markerNovo.getPosition(), 15));

                markerNovo = mapa_blindar_pdv.addMarker(new MarkerOptions().position(pos).title("BLINDAR AQUI").draggable(true));

                markerNovo.setSnippet(end);
                markerNovo.showInfoWindow();

                criarCirculo();
            }
        });
    }

    public void criarCirculo() {
        if (circle != null)
            circle.remove();
        circle = mapa_blindar_pdv.addCircle(new CircleOptions().center(markerNovo.getPosition()).radius(80)
                .fillColor(Color.argb(50, 255, 0, 0)).strokeWidth(0));
    }

    private void preencherSpinnerRede() {
        List<GrupoEconomico> list = GrupoEconomicoCTRL.listGruposEconomicos(this);
        ArrayAdapter<GrupoEconomico> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        _spGrupoEcon.setAdapter(adapter);
    }

    private void preencherSpinnerPdv(int idRede) {
        List<PDV> listaPdvs = PdvCTRL.getPDVPorIdRede(this, idRede);
        if (!listaPdvs.isEmpty()) {
            ArrayAdapter<PDV> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaPdvs);
            _spPdv.setAdapter(dataAdapter);
            _spPdv.setOnItemSelectedListener(this);
        }
    }

    private void verificarPDVBlindado(PDV pdv) {
        if (pdv.getIsBlindado() == 1) {
            _fabBlindarPvd.setVisibility(View.GONE);
            if (markerPDV != null)
                markerPDV.remove();
            LatLng latLngPDV = new LatLng(Double.parseDouble(pdv.getLatitude()), Double.parseDouble(pdv.getLongitude()));
            if (markerPDV != null)
                markerPDV.remove();
            markerPDV = mapa_blindar_pdv.addMarker(new MarkerOptions().position(latLngPDV).title(pdv.getDescricao())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            mapa_blindar_pdv.animateCamera(CameraUpdateFactory.newLatLngZoom(markerPDV.getPosition(), 15));
            markerPDV.showInfoWindow();
        } else {
            _fabBlindarPvd.setVisibility(View.VISIBLE);
            mapa_blindar_pdv.animateCamera(CameraUpdateFactory.newLatLngZoom(markerNovo.getPosition(), 15));
            markerNovo.showInfoWindow();
        }
    }

//    private void fixarPdv{
//        if(pdv.getId()==)
//    }

    private void blindarPdv() {
        ErrorUtil.montarCaminhoErro();
        _pdCarregando.show();
        try {
            if (markerNovo == null || markerNovo.getPosition().latitude == 0) {
                new AlertDialog.Builder(BlindarPdvActivity.this).setTitle(R.string.title_alert_aviso)
                        .setMessage(R.string.message_alert_erro_geolocalizacao)
                        .setPositiveButton(R.string.label_alert_button_ok, null).show();
            } else {
                pdv = (PDV) _spPdv.getSelectedItem();
                pdv.setLatitude(markerNovo.getPosition().latitude + "");
                pdv.setLongitude(markerNovo.getPosition().longitude + "");
                JSONObject logParams = new JSONObject();
                logParams.put("pdv", pdv.getId());
                logParams.put("latitudePDV", pdv.getLatitude());
                logParams.put("longitudePDV", pdv.getLongitude());
                ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_blindar_pdv), responseListener, errorListener, null);
            }
        } catch (Exception e) {
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    // SETUP VOLLEY LISTENERS

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        _pdCarregando = new ProgressDialog(this);
        _pdCarregando.setIndeterminate(true);
        _pdCarregando.setMessage(getString(R.string.message_progress_realizando_blindagem));
        _pdCarregando.setCancelable(false);
        responseListener = response -> {
            try {
                jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt(ExternalDB.COD_RETORNO) != ExternalDB.CD_RETORNO_SUCESSO) {
                    _pdCarregando.dismiss();
                    new AlertDialog.Builder(BlindarPdvActivity.this).setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
                } else {
                    JSONArray jArrayPDV = jsonRetorno.getJSONArray("pdv");
                    if (jArrayPDV.length() > 0) {
                        int pos = 0;
                        GenericCTRL.deleteAll(BlindarPdvActivity.this, PDV.TABELA);
                        for (int i = 0; i < jArrayPDV.length(); i++) {
                            PDV pdvRetorno = new PDV(jArrayPDV.getJSONObject(i));
                            GenericCTRL.save(BlindarPdvActivity.this, PDV.TABELA, pdvRetorno.getContentValues());
                            if (pdv.getId() == pdvRetorno.getId()) {
                                pdv = pdvRetorno;
                                pos = i;
                                verificarPDVBlindado(pdv);
                          }
                        }
                        preencherSpinnerPdv(pdv.getIdRede());
                        _spPdv.setSelection(pos);
                    }
                    new AlertDialog.Builder(BlindarPdvActivity.this).setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
                    _pdCarregando.dismiss();
                }
            } catch (Exception e) {
                _pdCarregando.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(BlindarPdvActivity.this, e);
            }
        };

        errorListener = error -> _pdCarregando.dismiss();
    }
//int pos1 =_spPdv.getSelectedItemPosition();
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.sp_blindar_pdv_rede:
                GrupoEconomico rede = (GrupoEconomico) adapterView.getSelectedItem();
                preencherSpinnerPdv(rede.getId());
                break;
            case R.id.sp_blindar_pdv:
                pdv = (PDV) adapterView.getSelectedItem();
                verificarPDVBlindado(pdv);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_blindar_pdv_posicao_atual:
                if (markerNovo != null) {
                    mapa_blindar_pdv.animateCamera(CameraUpdateFactory.newLatLngZoom(markerNovo.getPosition(), 15));
                    markerNovo.showInfoWindow();
                }
                break;
            case R.id.fab_blindar_pdv_enviar:
                new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_blndarpdv)
                        .setPositiveButton(R.string.label_alert_button_sim, (dialogInterface, i) -> blindarPdv())
                        .setNegativeButton(R.string.label_alert_button_nao, null).show();
                break;
        }
    }
}
