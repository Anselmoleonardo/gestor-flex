package br.com.gestorflex.ui.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Ranking;

public class RankingHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


    private AppCompatTextView _tvRankingTitulo;
    private LinearLayoutCompat _llValores;
    private ImageView _ivListarRanking;
    private Context context;

    public RankingHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        _tvRankingTitulo = itemView.findViewById(R.id.tv_ranking_titulo);
        _llValores = itemView.findViewById(R.id.ll_item_valores);
        _ivListarRanking = itemView.findViewById(R.id.iv_listar_ranking);
    }

    public void bindRanking(Ranking ranking) {
        _tvRankingTitulo.setText(ranking.getTitulo());
        _tvRankingTitulo.setOnClickListener(this);
        _ivListarRanking.setOnClickListener(this);
        if (_llValores.getChildCount() == 0) {// Adiciona valores no Linear Layout
            for (String valor : ranking.getValores()) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View _vwValor = inflater.inflate(R.layout.layout_valores_ranking, null);

                TextView _tvValor = _vwValor.findViewById(R.id.tv_valor_ranking_descricao);
                _tvValor.setText(valor);

                _llValores.addView(_vwValor);
            }
        }
    }

    @Override
    public void onClick(View v) {

        if (_llValores.getVisibility() == View.GONE) {
            _llValores.setVisibility(View.VISIBLE);
            _ivListarRanking.setImageResource(R.drawable.ic_seta_cima);
        } else {
            _llValores.setVisibility(View.GONE);
            _ivListarRanking.setImageResource(R.drawable.ic_seta_baixo);
        }
    }
}
