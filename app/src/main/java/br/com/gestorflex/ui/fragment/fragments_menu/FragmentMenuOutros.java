package br.com.gestorflex.ui.fragment.fragments_menu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.BotaoCTRL;
import br.com.gestorflex.database.internal.controller.PesquisaCTRL;
import br.com.gestorflex.database.internal.controller.RespostaPerguntaCTRL;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Botao;
import br.com.gestorflex.ui.activity.BlindarPdvActivity;
import br.com.gestorflex.ui.activity.DefaultActivity;
import br.com.gestorflex.ui.activity.JornadaMenuActivity;
import br.com.gestorflex.ui.activity.MenuActivity;
import br.com.gestorflex.ui.activity.NotificacaoActivity;
import br.com.gestorflex.ui.activity.PontoActivity;
import br.com.gestorflex.ui.adapter.BotaoMenuAdapter;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;

/**
 * Created by Vicente on 21/05/2019.
 */

public class FragmentMenuOutros extends Fragment implements OnRecyclerItemClickListener {

    public static final int TAB_OUTROS = 3;
    public static final String TAG_PERFIL = "perfilPromotor";
    public static final String TAG_ALOCACAO = "alocacao";
    public static final String TAG_NOTIFICACAO = "notificacao";
    public static final String TAG_FUNCIONAL = "funcional";
    public static final String TAG_JORNADA = "menuJornada";
    public static final String TAG_BLINDAR = "blindarPDV";
    public static final String TAG_PONTO = "ponto";


    private RecyclerView _rvBotoes;

    private BotaoMenuAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_outros, container, false);
        inicializarObjetos(view);
        configuraViews();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null)
            adapter.notifyDataSetChanged();  // Chamar aqui para atualizar o contador em Sincronização
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        configuraViews();
//    }

    public void inicializarObjetos(View view) {
        _rvBotoes = view.findViewById(R.id.menu_outros_lista);

    }

    public void configuraViews() {
        ErrorUtil.montarCaminhoErro();
        try {
//            if (adapter == null || adapter.getItemCount() == 0)
            configuraRecyclerView(BotaoCTRL.getList(getActivity(), TAB_OUTROS));
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }

    private void configuraRecyclerView(List<Botao> botoes) {
        adapter = new BotaoMenuAdapter(getContext(), botoes);
        adapter.setOnRecyclerItemClickListener(this);
        _rvBotoes.setAdapter(adapter);
    }

    private int getPreferencesIsCheckin() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(this.getString(R.string.isCheckin), Context.MODE_PRIVATE);
        String count = sharedPreferences.getString(this.getString(R.string.isCheckin), "0");
        return Integer.parseInt(count);
    }

    private String getPreferencesIsValidate() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(this.getString(R.string.isHabiliteVenda), Context.MODE_PRIVATE);
        String valueVersion = sharedPreferences.getString(this.getString(R.string.isHabiliteVenda), "");
        return valueVersion;
    }

    private int getPreferencesIsChere() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(this.getString(R.string.isChere), Context.MODE_PRIVATE);
        String count = sharedPreferences.getString(this.getString(R.string.isChere), "0");
        return Integer.parseInt(count);
    }



    private int getQtdDadosParaSincronizar() {
        //int numSincs = VendaCTRL.listVenda(getActivity()).size();
        int numResposta = RespostaPerguntaCTRL.listRespostaPergunta(getActivity()).size();
       // int numJornada = JornadaCTRL.listJornadaSinc(getActivity()).size();
        //int numVendaFixa = VendaFixoCTRL.listVendaFixo(getActivity()).size();
        return numResposta;
    }

    @Override
    public void onRecyclerItemClickListener(View view, int position, Object item) {
        ErrorUtil.montarCaminhoErro();
        try {

            Botao botao = (Botao) item;
            Intent intent = null;
            switch (botao.getTag()) {
                case TAG_PERFIL:
//                    intent = new Intent(getActivity(), JornadaActivity.class);
                    break;
                case TAG_ALOCACAO:
//                    intent = new Intent(getActivity(), MenuOdomentroActivity.class);
                    break;
                case TAG_NOTIFICACAO:
                    intent = new Intent(getActivity(), NotificacaoActivity.class);
                    break;
                case TAG_FUNCIONAL:
//                    intent = new Intent(getActivity(), PesquisaActivity.class);
                    break;
                case TAG_JORNADA:
                    intent = new Intent(getActivity(), JornadaMenuActivity.class);
                    break;
                case TAG_BLINDAR:
                    intent = new Intent(getActivity(), BlindarPdvActivity.class);
                    break;
                case TAG_PONTO:
                    intent = new Intent(getActivity(), PontoActivity.class);
                    break;
                default:
                    intent = new Intent(getActivity(), DefaultActivity.class); // Intent Default
            }
            if (intent != null) {
                intent.putExtra(MenuActivity.TAG_TITLE, botao.getNome());
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }
}
