package br.com.gestorflex.ui.fragment.fragments_odometro_sinc;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.CheckoutCTRL;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.model.Checkout;
import br.com.gestorflex.ui.adapter.SincCheckOutAdapter;
import br.com.gestorflex.util.ErrorUtil;


public class TabOdometroSincCheckOut extends Fragment implements View.OnClickListener {

    private RecyclerView _rvSinc;
    private Button _btnSinc;

    public SincCheckOutAdapter sincCheckOutAdapter;

    private Request request;
    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_odometro_sinc_check_out, container, false);
        instanciarViews(rootView);
        return rootView;
    }

    private void instanciarViews(View view) {
        _rvSinc = view.findViewById(R.id.rv_odometro_sinc_check_out);
        _btnSinc = view.findViewById(R.id.btn_odometro_sinc_check_out);
        configurarViews();
    }

    private void configurarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            configurarRecyclerView();
            setupVolleyListeners();
            _btnSinc.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }

    private void configurarRecyclerView() {
        List<Checkout> checkouts = CheckoutCTRL.pegarCheckOuts(getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        _rvSinc.setLayoutManager(layoutManager);
        sincCheckOutAdapter = new SincCheckOutAdapter(getActivity(), checkouts);
//        sincCheckOutAdapter.setRecyclerViewOnClickListenerHack(this);
        _rvSinc.setAdapter(sincCheckOutAdapter);
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Enviando Check-out...");
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
                        int result = GenericCTRL.delete(getActivity(), Checkout.TABELA, sincCheckOutAdapter.getCheckOuts().get(0).getId());
                        if (result > 0) {
                            sincCheckOutAdapter.removeItem(0);
                            if (sincCheckOutAdapter.getItemCount() > 0) {
                                enviarCheckOut();
                            } else {
                                progressDialog.dismiss();
                                new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                                        .setTitle("Aviso")
                                        .setMessage(jsonResposta.optString("msgRetorno"))
                                        .setPositiveButton("OK", null)
                                        .show();
                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Erro ao excluir do Banco de Dados Interno", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        int result = GenericCTRL.delete(getActivity(), Checkout.TABELA, sincCheckOutAdapter.getCheckOuts().get(0).getId());
                        if (result > 0) {
                            sincCheckOutAdapter.removeItem(0);}
                        progressDialog.dismiss();
                        new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                                .setTitle("Aviso")
                                .setMessage(getString(R.string.message_error_sinc_ponto,jsonResposta.optString("msgRetorno")))
                                .setPositiveButton("OK", null)
                                .show();
                    }
                } else {
                    progressDialog.dismiss();
                    new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                            .setTitle("Aviso")
                            .setMessage("Ocorreu uma FALHA na conexão!")
                            .setPositiveButton("OK", null)
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(getActivity(), e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                    .setTitle("Aviso")
                    .setMessage("Ocorreu uma FALHA na conexão!")
                    .setPositiveButton("OK", null)
                    .show();
        };
    }

    private void enviarCheckOut() throws Exception {
        ErrorUtil.montarCaminhoErro();
        if (!progressDialog.isShowing())
            progressDialog.show();
        Checkout odometro = sincCheckOutAdapter.getCheckOuts().get(0);
        final JSONObject logParams = odometro.pegarJson();
        logParams.put("isSinc", 1);
        ExternalDB.getLogPadrao(getActivity(), logParams);
        request = new StringRequest(Request.Method.POST, ExternalDB.getUrl(getActivity()), responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                try {
                    logParams.put("procedure", getString(R.string.procedure_questionario_odomentro));
                    JSONObject log = new JSONObject();
                    log.put("log", logParams);
                    params.put("concentrador", log.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(getActivity(), e);
                }
                Log.e("PARAMS", params.toString());
                return params;
            }
        };
        ExternalDB.getInstance(getActivity()).addToRequestQueue(request);
    }

//    @Override
//    public void OnItemClickListener(View view, final int position, Object item) {
//        final Checkout odometro = (Checkout) item;
//        String[] opcoes = {"Excluir check-out"};
//        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getActivity());
//        alert.setItems(opcoes, (dialog, which) -> {
//            switch (which) {
//                case 0:
//                    int result = GenericCTRL.delete(getActivity(), Checkout.TABELA, odometro.getId());
//                    if (result > 0) {
//                        sincCheckOutAdapter.removeItem(position);
//                        Toast.makeText(getActivity(), "Check-out removido com sucesso!", Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//            }
//        });
//        alert.show();
//    }

    @Override
    public void onClick(View v) {
        ErrorUtil.montarCaminhoErro();
        try {
//            if (GeolocalizacaoUtil.verificarGeolocalizacao(getActivity()))
            enviarCheckOut();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }
}
