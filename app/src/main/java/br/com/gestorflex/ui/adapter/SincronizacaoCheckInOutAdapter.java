package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.CheckInOut;
import br.com.gestorflex.ui.holder.SincronizacaoCheckInOutHolder;

public class SincronizacaoCheckInOutAdapter extends RecyclerView.Adapter<SincronizacaoCheckInOutHolder> {

    private Context context;
    private List<CheckInOut> listaCheckInOut;

    public SincronizacaoCheckInOutAdapter(Context context, List<CheckInOut> listaCheckInOut) {
        this.context = context;
        this.listaCheckInOut = listaCheckInOut;
    }

    @NonNull
    @Override
    public SincronizacaoCheckInOutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SincronizacaoCheckInOutHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_jornada_sincronizacao, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SincronizacaoCheckInOutHolder holder, int position) {
        holder.bindCheckInOut(context, listaCheckInOut.get(position));
    }

    public List<CheckInOut> getList() {
        return listaCheckInOut;
    }

    public void insertItem(CheckInOut checkInOut) {
        listaCheckInOut.add(checkInOut);
        notifyItemInserted(getItemCount());
    }

    public void removeItem(int position) {
        listaCheckInOut.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listaCheckInOut.size());
    }

    @Override
    public int getItemCount() {
        return listaCheckInOut == null ? 0 : listaCheckInOut.size();
    }

}
