package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.ui.holder.SincronizacaoOdometroHolder;

public class SincronizacaoOdometroAdapter extends RecyclerView.Adapter<SincronizacaoOdometroHolder> {

    private Context context;
    private List<Odometro> listaOdometro;

    public SincronizacaoOdometroAdapter(Context context, List<Odometro> listaOdometro) {
        this.context = context;
        this.listaOdometro = listaOdometro;
    }

    @NonNull
    @Override
    public SincronizacaoOdometroHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SincronizacaoOdometroHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_jornada_sincronizacao, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SincronizacaoOdometroHolder holder, int position) {
        holder.bindOdometro(context, listaOdometro.get(position));
    }

    public List<Odometro> getList() {
        return listaOdometro;
    }

    public void insertItem(Odometro odometro) {
        listaOdometro.add(odometro);
        notifyItemInserted(getItemCount());
    }

    public void removeItem(int position) {
        listaOdometro.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listaOdometro.size());
    }

    @Override
    public int getItemCount() {
        return listaOdometro == null ? 0 : listaOdometro.size();
    }

}
