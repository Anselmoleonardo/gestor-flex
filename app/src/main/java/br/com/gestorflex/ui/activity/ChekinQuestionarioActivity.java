package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.BottomBarUtil;

public class ChekinQuestionarioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chekin_questionario);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        Toolbar _toolbar = findViewById(R.id.toolbar);
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_toolbar);
        configurarViews();
    }

    private void configurarViews() {

    }
}
