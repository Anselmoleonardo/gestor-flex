package br.com.gestorflex.ui.fragment.fragments_odometro_sinc;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.RespostaPerguntaCTRL;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.ui.adapter.SincCheckInAdapter;
import br.com.gestorflex.util.ErrorUtil;


public class TabOdometroSincCheckIn extends Fragment {

    private RecyclerView _rvSinc;
    public SincCheckInAdapter sincCheckInAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_odometro_sinc_check_in, container, false);
        instanciarViews(rootView);
        return rootView;
    }

    private void instanciarViews(View view) {
        _rvSinc = view.findViewById(R.id.rv_odometro_sinc_check_in);
        configurarViews();
    }

    private void configurarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            configurarRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }

    private void configurarRecyclerView() {
        List<RespostaPergunta> checkouts = RespostaPerguntaCTRL.pegarCheckIn(getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        _rvSinc.setLayoutManager(layoutManager);
        sincCheckInAdapter = new SincCheckInAdapter(getActivity(), checkouts);
//        sincCheckInAdapter.setRecyclerViewOnClickListenerHack(this);
        _rvSinc.setAdapter(sincCheckInAdapter);
    }

//    @Override
//    public void OnItemClickListener(View view, final int position, Object item) {
//        final RespostaPergunta odometro = (RespostaPergunta) item;
//        String[] opcoes = {"Excluir check-in"};
//        final android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(getActivity());
//        alert.setItems(opcoes, (dialog, which) -> {
//            switch (which) {
//                case 0:
//                    int result = GenericCTRL.delete(getActivity(), RespostaPergunta.TABELA, odometro.getId());
//                    if (result > 0) {
//                        sincCheckInAdapter.removeItem(position);
//                        Toast.makeText(getActivity(), "Check-in removido com sucesso!", Toast.LENGTH_SHORT).show();
//                    }
//                    break;
//            }
//        });
//        alert.show();
//    }

}
