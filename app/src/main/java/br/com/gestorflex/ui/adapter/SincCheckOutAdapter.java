package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.Checkout;
import br.com.gestorflex.ui.holder.SincCheckOutHolder;

public class SincCheckOutAdapter extends RecyclerView.Adapter<SincCheckOutHolder> {

    private Context context;
    private List<Checkout> checkouts;
    private OnItemClickListener recyclerViewOnClickListenerHack;

    public SincCheckOutAdapter(Context context, List<Checkout> checkouts) {
        this.context = context;
        this.checkouts = checkouts;
    }

    public void setRecyclerViewOnClickListenerHack(OnItemClickListener recyclerViewOnClickListenerHack) {
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
    }

    @NonNull
    @Override
    public SincCheckOutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SincCheckOutHolder(LayoutInflater.from(context).inflate(R.layout.item_sinc_odometro,
                parent, false), this.recyclerViewOnClickListenerHack, checkouts);
    }

    @Override
    public void onBindViewHolder(@NonNull SincCheckOutHolder sincCheckOutHoder, int position) {
        sincCheckOutHoder.bindCheckOut(context, checkouts.get(position));
    }

    @Override
    public int getItemCount() {
        return checkouts != null ? checkouts.size() : 0;
    }

    public void insertItem(Checkout checkout) {
        checkouts.add(checkout);
        notifyItemInserted(getItemCount());
    }

    public void removeItem(int position) {
        checkouts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    public List<Checkout> getCheckOuts() {
        return checkouts;
    }
}
