package br.com.gestorflex.ui.fragment.fragments_jornada_sincronizacao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response;

import org.json.JSONObject;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.OdometroCTRL;
import br.com.gestorflex.model.CheckInOut;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.ui.activity.JornadaSincronizacaoActivity;
import br.com.gestorflex.ui.adapter.SincronizacaoOdometroAdapter;
import br.com.gestorflex.util.ErrorUtil;

import static androidx.recyclerview.widget.DividerItemDecoration.VERTICAL;

public class FragmentJornadaSincOdometro extends Fragment {

    private JornadaSincronizacaoActivity _actJornadaSinc;
    private RecyclerView _rvSincronizacaoOdometro;
    private AppCompatTextView _tvNenhumOdometro;
    private ProgressDialog _pdCarregando;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;

    private SincronizacaoOdometroAdapter adapter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        _actJornadaSinc = (JornadaSincronizacaoActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jornada_sinc_odometro, container, false);
        inicializarViews(view);
        return view;
    }

    private void inicializarViews(View view) {
        _rvSincronizacaoOdometro = view.findViewById(R.id.rv_jornada_sinc_odometro);
        _tvNenhumOdometro = view.findViewById(R.id.tv_jornada_sinc_odometro_nenhum);
        configurarViews();
    }

    private void configurarViews() {
        configurarRecyclerView();
        setupVolleyListeners();
    }

    private void configurarRecyclerView() {
        List<Odometro> odometros = OdometroCTRL.listOdometroSincronizacao(getActivity());
        if (!odometros.isEmpty()) {
            DividerItemDecoration itemDecor = new DividerItemDecoration(getActivity(), VERTICAL);
            _rvSincronizacaoOdometro.addItemDecoration(itemDecor);
            adapter = new SincronizacaoOdometroAdapter(getActivity(), odometros);
            _rvSincronizacaoOdometro.setAdapter(adapter);
        } else {
            _tvNenhumOdometro.setVisibility(View.VISIBLE);
            _rvSincronizacaoOdometro.setVisibility(View.GONE);
        }
    }

    public SincronizacaoOdometroAdapter getAdapter() {
        return adapter;
    }

    // ---------------------------------- VOLLEY ----------------------------------

    public void enviarOdometro() {
        ErrorUtil.montarCaminhoErro();
        try {
            _pdCarregando.show();
            Odometro odometro = adapter.getList().get(0);
            ExternalDB.addDbRequest(getActivity(), odometro.getJSONParams(), getString(R.string.procedure_enviar_odometro), responseListener, errorListener, null);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        _pdCarregando = new ProgressDialog(getActivity());
        _pdCarregando.setIndeterminate(true);
        _pdCarregando.setMessage(getString(R.string.message_progress_enviando_dados));
        _pdCarregando.setCancelable(false);
        responseListener = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONObject jsonResposta = jsonRetorno.getJSONArray("retorno").getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
                        Odometro odometro = adapter.getList().get(0);
                        if (odometro.getAtividade() == Odometro.INICIAR_JORNADA) {
                            odometro.setIsSinc(0);
                            GenericCTRL.update(getActivity(), Odometro.TABELA, odometro.getId(), odometro.getContentValues());
                        } else {
                            GenericCTRL.deleteAll(getActivity(), Odometro.TABELA);
                            GenericCTRL.deleteAll(getActivity(), CheckInOut.TABELA);
                        }
                        adapter.removeItem(0);
                        if (adapter.getItemCount() > 0)
                            enviarOdometro();
                        else {
                            _pdCarregando.dismiss();
                            new AlertDialog.Builder(getActivity()).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_sincronizacao_sucesso)
                                    .setPositiveButton(R.string.label_alert_button_ok, null).setOnDismissListener(dialog -> _actJornadaSinc.finish()).show();
                        }
                    } else {
                        _pdCarregando.dismiss();
                        Odometro odometro = adapter.getList().get(0);
                        GenericCTRL.delete(getActivity(), Odometro.TABELA, odometro.getId());
                        adapter.removeItem(0);
                        new AlertDialog.Builder(getActivity()).setTitle(R.string.title_alert_aviso)
                                .setMessage(getString(R.string.message_error_sinc_ponto, jsonResposta.optString("msgRetorno")))
                                .setPositiveButton(R.string.label_alert_button_ok, null).setOnDismissListener(dialog -> {
                                    if (_actJornadaSinc.getQtdDeRegistrosSinc() == 0)
                                        _actJornadaSinc.finish();
                                }).show();
                    }
                } else {
                    _pdCarregando.dismiss();
                    new AlertDialog.Builder(getActivity()).setTitle(R.string.title_alert_aviso)
                            .setMessage(R.string.message_alert_erro_servidor)
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(getActivity(), e);
            }
        };
        errorListener = error -> {
            _pdCarregando.dismiss();
            new AlertDialog.Builder(getActivity()).setTitle(R.string.title_alert_aviso)
                    .setMessage(R.string.message_alert_erro_servidor)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
        };
    }

}
