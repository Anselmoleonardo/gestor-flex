package br.com.gestorflex.ui.holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.util.DateUtil;

public class SincJornadaHoder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView _tvDescricao;
    private TextView _tvData;

    private OnItemClickListener recyclerViewOnClickListenerHack;
    private List<Odometro> odometros;

    public SincJornadaHoder(@NonNull View itemView, OnItemClickListener recyclerViewOnClickListenerHack, List<Odometro> odometros) {
        super(itemView);
        _tvDescricao = itemView.findViewById(R.id.tv_sinc_odometro_descricao);
        _tvData = itemView.findViewById(R.id.tv_sinc_odometro_data);

        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
        this.odometros = odometros;
        itemView.setOnClickListener(this);
    }

    public void bindOdometro(Odometro odometro) {
        _tvDescricao.setText(odometro.getAtividade() == 1 ? R.string.label_sinc_odometro_jornada_iniciada : R.string.label_sinc_odometro_jornada_finalizada);
        _tvData.setText(DateUtil.convertDateTimeBrPattern(odometro.getDataEnvio()));
    }

    @Override
    public void onClick(View v) {
        if (this.recyclerViewOnClickListenerHack != null)
            this.recyclerViewOnClickListenerHack.OnItemClickListener(v, getLayoutPosition(), odometros.get(getLayoutPosition()));
    }
}

