package br.com.gestorflex.ui.fragment.fragments_ranking_filtro;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.ListarRanking;
import br.com.gestorflex.model.Ranking;
import br.com.gestorflex.ui.activity.RankingResultadoActivity;

public class FragmentRankingWeb extends Fragment {


    private RecyclerView _rvRanking;
    private TextView _tvDataRanking;
    private TextView _tvUFRanking;
    private TextView _tvDDDRanking;
    private TextView _tvListarPorRanking;
    private TextView _tvProdutoRanking;
    private TextView _tvComSemRegargaRanking;

    private RankingResultadoActivity _actRankingResultado;

    private List<Ranking> ranking;
    private ListarRanking listarRanking;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        _actRankingResultado = (RankingResultadoActivity) context;
    }

    public FragmentRankingWeb() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ranking_web, container, false);
    }
}
