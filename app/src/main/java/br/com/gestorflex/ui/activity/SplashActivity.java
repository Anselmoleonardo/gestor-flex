package br.com.gestorflex.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import br.com.gestorflex.R;

import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.ErrorUtil;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                irTela();
            }
        }, 2000);
    }

    private void irTela() {
        ErrorUtil.montarCaminhoErro();
        try {
            Usuario usuario = UsuarioCTRL.getUsuario(this);
            Intent intent = null;
            if (usuario != null)
                intent = new Intent(this, MenuActivity.class);
            else
                intent = new Intent(this, AutenticacaoActivity.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }
}