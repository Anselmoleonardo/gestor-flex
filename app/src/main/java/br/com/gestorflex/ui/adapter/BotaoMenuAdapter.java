package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Botao;
import br.com.gestorflex.ui.holder.BotaoHolder;

/**
 * Created by Frann on 30/11/2018.
 */

public class BotaoMenuAdapter extends RecyclerView.Adapter<BotaoHolder> {

    private Context context;
    private List<Botao> botoes;
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    public BotaoMenuAdapter(Context context, List<Botao> botoes) {
        this.context = context;
        this.botoes = botoes;
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }

    @NonNull
    @Override
    public BotaoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BotaoHolder(LayoutInflater.from(context).inflate(R.layout.item_menu_botao,
                parent, false), this.onRecyclerItemClickListener, botoes);
    }

    @Override
    public void onBindViewHolder(@NonNull BotaoHolder holder, int position) {
        holder.bindBotao(context, botoes.get(position));
    }

    public Botao getBotaoAtPosition(int position) {
        return botoes != null ? botoes.get(position) : null;
    }

    @Override
    public int getItemCount() {
        return botoes != null ? botoes.size() : 0;
    }

}
