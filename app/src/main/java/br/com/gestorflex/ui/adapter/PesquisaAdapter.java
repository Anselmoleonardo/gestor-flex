package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Pesquisa;
import br.com.gestorflex.ui.holder.PesquisaHolder;
import br.com.gestorflex.ui.holder.ProdutividadeHolder;


public class PesquisaAdapter extends RecyclerView.Adapter<PesquisaHolder> {
    private Context context;
    private List<Pesquisa> pesquisas;
    private OnItemClickListener recyclerViewOnClickListenerHack;


    public PesquisaAdapter(Context context, List<Pesquisa> pesquisas) {
        this.context = context;
        this.pesquisas = pesquisas;
    }
    public void setRecyclerViewOnClickListenerHack (OnItemClickListener recyclerViewOnClickListenerHack) {
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
    }

    @NonNull
    @Override
    public PesquisaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PesquisaHolder(LayoutInflater.from(context).inflate(R.layout.item_pesquisa,
                parent, false), this.recyclerViewOnClickListenerHack, pesquisas);
    }

    @Override
    public void onBindViewHolder(@NonNull PesquisaHolder pesquisaHolder, int i) {
        Pesquisa pesquisa = pesquisas.get(i);
        pesquisaHolder.bindPesquisa(pesquisa);
    }

    @Override
    public int getItemCount() {
        return pesquisas != null ? pesquisas.size() : 0;
    }
}

//public class PesquisaAdapter extends RecyclerView.Adapter<PesquisaHolder>{
//    private List<Pesquisa> pesquisas;
//    private Context context;
//    private OnRecyclerItemClickListener onRecyclerItemClickListener;
//
//
//    public PesquisaAdapter(Context context, List<Pesquisa> pesquisas) {
//        this.pesquisas = pesquisas;
//        this.context = context;
//    }
//
//    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
//        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
//    }
//
//    @NonNull
//    @Override
//    public PesquisaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new PesquisaHolder(LayoutInflater.from(parent.getContext()).
//                inflate(R.layout.item_pesquisa, parent, false),this.onRecyclerItemClickListener, pesquisas);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull PesquisaHolder holder, int position) {
//        Pesquisa pesquisa = pesquisas.get(position);
//        holder.bindPesquisa(pesquisa);
//    }
//
//    @Override
//    public int getItemCount() {
//        return pesquisas != null ? pesquisas.size() : 0;
//    }
//}
