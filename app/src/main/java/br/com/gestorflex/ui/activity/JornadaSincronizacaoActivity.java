package br.com.gestorflex.ui.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.CheckInOutCTRL;
import br.com.gestorflex.database.internal.controller.OdometroCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.CheckInOut;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.ui.adapter.ViewPagerAdapter;
import br.com.gestorflex.ui.fragment.fragments_jornada_sincronizacao.FragmentJornadaSincCheckInOut;
import br.com.gestorflex.ui.fragment.fragments_jornada_sincronizacao.FragmentJornadaSincOdometro;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.ErrorUtil;

public class JornadaSincronizacaoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int TAB_ODOMETRO = 0;
    private static final int TAB_CHECK_IN_OUT = 1;

    private ViewPager _vpVenda;
    private TabLayout _tlVenda;
    private Toolbar _toolbar;
    private FloatingActionButton _btnEnviarVenda;
    private FragmentJornadaSincOdometro _fgSincronizacaoOdometro;
    private FragmentJornadaSincCheckInOut _fgSincronizacaoCheckInOut;

    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jornada_sincronizacao);
        BottomBarUtil.configurar(this);
        inicializarViews();
    }

    private void inicializarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            _toolbar = findViewById(R.id.toolbar);
            _btnEnviarVenda = findViewById(R.id.fab_enviar_venda);
            _tlVenda = findViewById(R.id.tabs);
            _vpVenda = findViewById(R.id.viewpager);
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() {
        _toolbar.setSubtitle(UsuarioCTRL.getUsuario(this).getMatriculaNome());
        setSupportActionBar(_toolbar);
        setupViewPager();
        _btnEnviarVenda.setOnClickListener(this);
    }

    private void setupViewPager() {
        _fgSincronizacaoOdometro = new FragmentJornadaSincOdometro();
        _fgSincronizacaoCheckInOut = new FragmentJornadaSincCheckInOut();
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(_fgSincronizacaoCheckInOut, getString(R.string.title_aba_sinc_check_in_out));
        adapter.addFragment(_fgSincronizacaoOdometro, getString(R.string.title_aba_sinc_odometro));
        _vpVenda.setAdapter(adapter);
        _vpVenda.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tlVenda));
        _vpVenda.setOffscreenPageLimit(2);
        _tlVenda.setTabMode(TabLayout.MODE_FIXED);
        _tlVenda.setupWithViewPager(_vpVenda);
    }

    public FragmentJornadaSincOdometro getFgSincronizacaoOdometro() {
        return _fgSincronizacaoOdometro;
    }

    public int getQtdDeRegistrosSinc() {
        int qtdDeRegistrosSinc = 0;
        qtdDeRegistrosSinc += _fgSincronizacaoOdometro.getAdapter() == null ? 0 : _fgSincronizacaoOdometro.getAdapter().getItemCount();
        qtdDeRegistrosSinc += _fgSincronizacaoCheckInOut.getAdapter() == null ? 0 : _fgSincronizacaoOdometro.getAdapter().getItemCount();
        return qtdDeRegistrosSinc;
    }

    // ----------------------------------- EVENTOS -----------------------------------

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_enviar_venda:
                List<CheckInOut> listCheckInOut = CheckInOutCTRL.listCheckInOutSincronizacao(this);
                if (!listCheckInOut.isEmpty())
                    _fgSincronizacaoCheckInOut.enviarCheckInOut();
                else {
                    List<Odometro> listOdometro = OdometroCTRL.listOdometroSincronizacao(this);
                    if (!listOdometro.isEmpty())
                        _fgSincronizacaoOdometro.enviarOdometro();
                }
                break;
        }
    }

}
