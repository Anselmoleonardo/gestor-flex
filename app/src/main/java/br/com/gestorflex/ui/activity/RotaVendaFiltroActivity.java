package br.com.gestorflex.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Response;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioGerenciadoCTRL;
import br.com.gestorflex.model.RotaVenda;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.model.UsuarioGerenciado;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;

public class RotaVendaFiltroActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatSpinner _spDia;
    private AppCompatSpinner _spMes;
    private AppCompatSpinner _spAno;
    private AppCompatSpinner _spUsuariosGerenciados;
    private FloatingActionButton _fabPesquisar;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    private Usuario usuario;
    private String dataRota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rota_venda_filtro);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(_toolbar);
            usuario = UsuarioCTRL.getUsuario(this);
            _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
            getSupportActionBar().setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
            _spDia = findViewById(R.id.sp_filtrar_rota_dia);
            _spMes = findViewById(R.id.sp_filtrar_rota_mes);
            _spAno = findViewById(R.id.sp_filtrar_rota_ano);
            _spUsuariosGerenciados = findViewById(R.id.sp_filtrar_rota_consultor);
            _fabPesquisar = findViewById(R.id.fab_filtrar_rota_pesquisar);
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() {
        preencherSpinnerDia();
        preencherSpinnerMes();
        preencherSpinnerAno();
        setupVolleyListeners();
        preencherSpinnerUsuarioGerenciado();
        _fabPesquisar.setOnClickListener(this);
    }

    public void preencherSpinnerDia() {
        List<String> dias = DateUtil.getTodosDiasDoMes(_spAno.getSelectedItemPosition(), _spMes.getSelectedItemPosition() );
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, dias);
        _spDia.setAdapter(dataAdapter);
        _spDia.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
    }

    public void preencherSpinnerMes() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, DateUtil.getTodosMeses());
        _spMes.setAdapter(dataAdapter);
        _spMes.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.MONTH));
    }

    public void preencherSpinnerAno() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String[] gruposEcon = {(year - 1) + "", year + "", (year + 1) + ""};
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, gruposEcon);
        _spAno.setAdapter(dataAdapter);
        _spAno.setSelection(1);
    }

    public void preencherSpinnerUsuarioGerenciado() {
        try {
            List<UsuarioGerenciado> listConsultores = UsuarioGerenciadoCTRL.listUsuariosGerenciados(this, 0, 0);
            if (listConsultores.size() == 0)
                listConsultores.add(0, new UsuarioGerenciado(-1, "SEM USUÁRIOS", "00000", 0, 0));
            ArrayAdapter<UsuarioGerenciado> dataAdapter = new ArrayAdapter<>(this,
                    R.layout.support_simple_spinner_dropdown_item, listConsultores);
            _spUsuariosGerenciados.setAdapter(dataAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void recuperarRota() throws Exception {
        if (validarDataSelecionada()) {
            progressDialog.show();
            dataRota = DateUtil.converterDataPadraoUs(dataRota);
            JSONObject logParams = new JSONObject();
            logParams.put("dtROTA", dataRota);
            logParams.put("cdUSUARIOGERENCIADO", ((UsuarioGerenciado) _spUsuariosGerenciados.getSelectedItem()).getId() + "");
            ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_rota), responseListener, errorListener, null);
        }
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_buscando_clientes));
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                JSONObject jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                    JSONArray jArrayRota = jsonRetorno.getJSONArray("rota");
                    if (jArrayRota.length() > 0) {
                        List<RotaVenda> rotaVendas = new ArrayList<>();
                        for (int i = 0; i < jArrayRota.length(); i++) {
                            rotaVendas.add(new RotaVenda(jArrayRota.getJSONObject(i)));
                        }
                        Intent intent = new Intent(RotaVendaFiltroActivity.this, RotaVendaResultadoActivity.class);
                        intent.putExtra("rota", (Serializable) rotaVendas);
                        startActivity(intent);
                    } else
                        new AlertDialog.Builder(RotaVendaFiltroActivity.this).setTitle(R.string.title_alert_aviso)
                                .setMessage(R.string.message_alert_sem_resultados)
                                .setPositiveButton(R.string.label_alert_button_ok, null).show();
                } else
                    new AlertDialog.Builder(RotaVendaFiltroActivity.this).setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
                progressDialog.dismiss();
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(RotaVendaFiltroActivity.this, e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            Toast.makeText(RotaVendaFiltroActivity.this, "Erro ao conectar ao servidor", Toast.LENGTH_LONG).show();
        };
    }

    private boolean validarDataSelecionada() {
        String dia = _spDia.getSelectedItem().toString();
        String mes = _spMes.getSelectedItem().toString();
        String ano = _spAno.getSelectedItem().toString();
        dataRota = String.format(Locale.getDefault(), "%s/%s/%s", dia, mes, ano);
        if (!DateUtil.validateDate(dataRota)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso)
                    .setMessage(R.string.message_alert_data_invalida)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        try {
            if (GeolocalizacaoUtil.verificarGeolocalizacao(this))
                recuperarRota();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
