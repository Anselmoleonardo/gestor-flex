package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Response;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.FotoUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;
import br.com.gestorflex.util.ImageUtil;

public class OdomentroActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int INICIAR_JORNADA = 1;
    private static final int FINALIZAR_JORNADA = 0;
    public static final String JORNADA_INICIADA = "jornada";
    private static final String DATA_JORNADA = "dataJornada";
    private static final String KM_JORNADA = "kmJornada";

    private Toolbar _toolbar;
    private TextInputLayout etJornadaKmDia;
    private TextInputLayout etJornadaKmOdometro;
    private ImageView ivJornadaImg;
    private CardView cvJornadaImgCard;
    private Button btJornadaFazerFoto;
    private Button btJornadaAtividade;
    private Bitmap bitmap;
    private int atividade;

    private Usuario usuario;
    private Odometro odometro;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_odomentro);
        BottomBarUtil.configurar(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        atividade = getIntent().getExtras().getInt("atividade");
        instanciarViews();
    }

    private void instanciarViews() {
        _toolbar = findViewById(R.id.toolbar);
        etJornadaKmDia = findViewById(R.id.til_odometro_jornada_km_dia);
        etJornadaKmOdometro = findViewById(R.id.til_odometro_jornada_km);
        cvJornadaImgCard = findViewById(R.id.jornada_img_card);
        ivJornadaImg = findViewById(R.id.jornada_img_odometro);
        btJornadaFazerFoto = findViewById(R.id.jornada_fazer_foto);
        btJornadaAtividade = findViewById(R.id.jornada_atividade);
        configurarViews();
    }

    private void configurarViews() {
        setSupportActionBar(_toolbar);
        getSupportActionBar().setTitle(getIntent().getExtras().getString("titulo"));
        usuario = UsuarioCTRL.getUsuario(this);
        _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        if (atividade == FINALIZAR_JORNADA) {
            etJornadaKmDia.setVisibility(View.GONE);
            btJornadaAtividade.setText("Finalizar Jornada");
        }
        if (usuario.getBoLiberaOdometro() == 1) {
            etJornadaKmDia.setVisibility(View.GONE);
            etJornadaKmOdometro.setVisibility(View.GONE);
            btJornadaFazerFoto.setVisibility(View.GONE);
        }
        btJornadaFazerFoto.setOnClickListener(this);
        btJornadaAtividade.setOnClickListener(this);
        setupListener();
    }

    // VOLLEY**********************************

    private void setupListener() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Enviando Odometro...");
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                btJornadaAtividade.setEnabled(true);
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    Log.e("retornoJson ", "" + jsonRetorno.toString());
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") != ExternalDB.CD_RETORNO_SUCESSO) {
                        new AlertDialog.Builder(OdomentroActivity.this).setTitle(R.string.title_alert_aviso)
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton(R.string.label_alert_button_ok, null).show();
                    } else {
                        salvarStatusAtividade(odometro);
                        if (usuario.getBoLiberaOdometro() == 0)
                            apagarFotoAparelho(odometro.getCaminhoFoto());
                        new AlertDialog.Builder(OdomentroActivity.this).setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", (dialogInterface, i) -> finish())
                                .setCancelable(false).show();
                    }
                } else {
                    GenericCTRL.save(OdomentroActivity.this, Odometro.TABELA, odometro.getContentValues());
                    salvarStatusAtividade(odometro);
                    new AlertDialog.Builder(OdomentroActivity.this)
                            .setTitle("Aviso")
                            .setMessage("Ocorreu uma FALHA na conexão! PONTO salvo para SINCRONIZAÇÃO!")
                            .setPositiveButton("OK", (dialog, which) -> finish())
                            .setCancelable(false)
                            .show();
                }
                progressDialog.dismiss();
            } catch (Exception e) {
                progressDialog.dismiss();
                ErrorUtil.enviarErroPorEmail(OdomentroActivity.this, e);
                e.printStackTrace();
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            try {
                btJornadaAtividade.setEnabled(true);
                GenericCTRL.save(OdomentroActivity.this, Odometro.TABELA, odometro.getContentValues());
                salvarStatusAtividade(odometro);
                new AlertDialog.Builder(OdomentroActivity.this).setTitle("Aviso")
                        .setMessage("Ocorreu uma FALHA na conexão! PONTO salvo para SINCRONIZAÇÃO!")
                        .setPositiveButton("OK", (dialog, which) -> finish()).setCancelable(false).show();
            } catch (Exception e) {
                ErrorUtil.enviarErroPorEmail(OdomentroActivity.this, e);
                e.printStackTrace();
            }
        };
    }

    private void enviarJornadaCheckout() {
        ErrorUtil.montarCaminhoErro();
        try {
            progressDialog.show();
            btJornadaAtividade.setEnabled(false);
            odometro = coletarJornada(atividade);
            final JSONObject logParams = odometro.getJSONParams();
            ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_jornada_lad), responseListener, errorListener, null);
        } catch (Exception ex) {
            new AlertDialog.Builder(OdomentroActivity.this).setTitle("Aviso").setMessage(ex.getMessage())
                    .setPositiveButton("OK", null).show();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private Odometro coletarJornada(int atividade) throws Exception {
        SimpleDateFormat formatoData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.mmm");
        String jornadaKmDia = etJornadaKmDia.getEditText().getText().toString();
        String jornadakmOdometro = etJornadaKmOdometro.getEditText().getText().toString();
        if (atividade == FINALIZAR_JORNADA)
            jornadaKmDia = "0";
        Odometro odometro = new Odometro();
        odometro.setAtividade(atividade);
        odometro.setDataEnvio(formatoData.format(Calendar.getInstance().getTime()));
        odometro.setCaminhoFoto(FotoUtil.CAMINHO_FOTO);
        odometro.setFoto(ImageUtil.converteBitmapParaBase64(bitmap));
        odometro.setKmDoDia(jornadaKmDia.isEmpty() ? 0 : Integer.parseInt(jornadaKmDia));
        odometro.setKmDoOdometro(jornadakmOdometro.isEmpty() ? 0 : Integer.parseInt(jornadakmOdometro));
        odometro.setLatitude(String.valueOf(Geolocalizacao.getLatitude()));
        odometro.setLongitude(String.valueOf(Geolocalizacao.getLongitude()));
        return odometro;
    }

    private boolean validarDadosUsuario(int atividade) {
        if (!GeolocalizacaoUtil.verificarGeolocalizacao(this)) {
            return false;
        }
        if (usuario.getBoLiberaOdometro() == 0) {
            String jornadaKmDia = etJornadaKmDia.getEditText().getText().toString();
            String jornadakmOdometro = etJornadaKmOdometro.getEditText().getText().toString();
            if (jornadakmOdometro.equals("") || bitmap == null ||
                    (atividade != FINALIZAR_JORNADA && jornadaKmDia.equals(""))) {
                new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso)
                        .setMessage("Prencha todos os campos e faça uma foto do odometro.")
                        .setPositiveButton(R.string.label_alert_button_ok, null)
                        .show();
                return false;
            }
            if (Integer.parseInt(jornadakmOdometro) < 1 || (atividade != FINALIZAR_JORNADA && Integer.parseInt(jornadaKmDia) < 1)) {
                new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso)
                        .setMessage("A distância em Km do odometro não pode ser zero")
                        .setPositiveButton(R.string.label_alert_button_ok, null)
                        .show();
                return false;
            }
            if (!validarOdomentroFinal())
                return false;
        }
        return true;
    }

    private boolean validarOdomentroFinal() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(JORNADA_INICIADA, Context.MODE_PRIVATE);
        String jornadakmOdometroFinal = etJornadaKmOdometro.getEditText().getText().toString();
        String jornadakmOdometroInicial = sharedPreferences.getString(KM_JORNADA, "1");
        if (atividade == FINALIZAR_JORNADA && Integer.parseInt(jornadakmOdometroInicial) > Integer.parseInt(jornadakmOdometroFinal)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso)
                    .setMessage(String.format("O ODÔMETRO FINAL [%s] não pode ser MENOR que o ODÔMETRO INICIAL [%s]",
                            jornadakmOdometroFinal, jornadakmOdometroInicial))
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        return true;
    }

    public void salvarStatusAtividade(Odometro odometro) {
        boolean ehInicio = odometro.getAtividade() == 1;
        SharedPreferences sharedPreferences = this.getSharedPreferences(JORNADA_INICIADA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(JORNADA_INICIADA, ehInicio);
        editor.putString(DATA_JORNADA, DateUtil.getDataAtualBr());
        editor.putString(KM_JORNADA, etJornadaKmOdometro.getEditText().getText().toString());
        editor.apply();
    }

    public void apagarFotoAparelho(String caminhoFoto) {
        File file = new File(caminhoFoto);
        file.delete();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (FotoUtil.CAMINHO_FOTO != null)
            savedInstanceState.putString("caminho_foto", FotoUtil.CAMINHO_FOTO);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey("caminho_foto")) {
            FotoUtil.CAMINHO_FOTO = savedInstanceState.getString("caminho_foto");
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    //  MÉTODOS SOBRE TIRAR FOTOS
    private void tirarFotoOdometro() {
        ErrorUtil.montarCaminhoErro();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                Uri fotoURI = FotoUtil.criarFotoURI_Odometro(this);
                if (fotoURI != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fotoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } catch (IOException e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(this, e);
            }
        }
    }

    private void salvarImagem() {
        try {
            File file = new File(FotoUtil.CAMINHO_FOTO);
            if (file.exists()) {
                int rotate = FotoUtil.getRotation(file);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                receberFoto(bitmap, rotate);
            }
        } catch (Exception e) {
            Log.e("erroFOTO", "" + e.getMessage());
            Toast.makeText(this, "Não foi possível carregar a foto!", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, OdomentroActivity.class);
            Log.e("retorno", "" + "ddd");
            i.putExtra("telaOriginal", "");
            this.startActivity(i);
        }
    }

    private void receberFoto(Bitmap bmp, int rotate) {
        ErrorUtil.montarCaminhoErro();
        try {
            byte[] foto = criarFoto(true, bmp, rotate);
            if (foto.length == 0)
                return;
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private byte[] criarFoto(boolean isEditar, Bitmap bmp, int rotate) throws Exception {
        Bitmap bmp2 = null;
        if (bmp.getWidth() >= bmp.getHeight())
            bmp2 = Bitmap.createBitmap(bmp, bmp.getWidth() / 2 - bmp.getHeight() / 2, 0, bmp.getHeight(),
                    bmp.getHeight());
        else
            bmp2 = Bitmap.createBitmap(bmp, 0, bmp.getHeight() / 2 - bmp.getWidth() / 2, bmp.getWidth(),
                    bmp.getWidth());
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp2, bmp2.getWidth(), bmp2.getHeight(), true);
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(),
                scaledBitmap.getHeight(), matrix, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ErrorUtil.montarCaminhoErro();
        try {
            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                salvarImagem();
                FotoUtil.exibirFotoOdometro(this, ivJornadaImg, bitmap, cvJornadaImgCard);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    @Override
    public void onClick(View v) {
        ErrorUtil.montarCaminhoErro();
        switch (v.getId()) {
            case R.id.jornada_atividade:
                try {
                    if (validarDadosUsuario(atividade))
                        enviarJornadaCheckout();
                } catch (Exception e) {
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(this, e);
                }
                break;
            case R.id.jornada_fazer_foto:
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.checkSelfPermission(OdomentroActivity.this,
                                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                || ActivityCompat.checkSelfPermission(OdomentroActivity.this,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            String[] permissoes = {android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                            requestPermissions(permissoes, 7);
                        } else {
                            tirarFotoOdometro();
                        }
                    } else {
                        tirarFotoOdometro();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(this, ex);
                }
                break;
        }
    }
}

