package br.com.gestorflex.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.CheckInOutCTRL;
import br.com.gestorflex.database.internal.controller.CheckoutCTRL;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.OdomentroCTRL;
import br.com.gestorflex.database.internal.controller.OdometroCTRL;
import br.com.gestorflex.database.internal.controller.PdvCTRL;
import br.com.gestorflex.database.internal.controller.RespostaPerguntaCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Botao;
import br.com.gestorflex.model.CheckInOut;
import br.com.gestorflex.model.Checkout;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.model.PDV;
import br.com.gestorflex.model.Usuario;

import br.com.gestorflex.util.Animacao;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;

public class JornadaMenuActivity extends AppCompatActivity{

    private static final int INICIAR_JORNADA = 1;
    private static final int FINALIZAR_JORNADA = 0;
    public static final String PREFERENCIA_CHECKOUT = "checkout";
    private static final String JORNADA_INICIADA = "jornada";
    private static final String ID_PDV = "idPdv";
    public static final String TAG_MENU_SINCRONIZACAO = "rotaSincronizacao";

    private static final String INICIAR_JORNADA_TITLE = "Iniciar Jornada";
    private static final String FINALIZAR_JORNADA_TITLE = "Finalizar Jornada";
    private static final String CHECK_IN_PDV_TITLE = "Check In PDV";
    private static final String SINCRONIZAÇÃO_TITLE = "Sincronização";

    private TextView _tvJornadaIniciarIcon;
    private TextView _tvJornadaFinalizarIcon;
    private LinearLayout _llJornadaInicio;
    private LinearLayout _llJornadaFinizar;
    private LinearLayout _llCheckinPesquisaPdv;
    private TextView _tvCheckinPesquisaPdvIcon;
    private LinearLayout _llCheckoutPdv;
    private TextView _tvCheckoutPdvIcon;
    private LinearLayout _llSincronizacao;
    private TextView _tvSincronizacaoIcon;
    private TextView _tvSincronizacaoQuantidade;

    private Usuario usuario;
    private Checkout checkout;

    private Response.Listener<String> responseListener;

    private Response.ErrorListener errorListener;

    private ProgressDialog progressDialog;

    private String titulo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jornada_menu);
        BottomBarUtil.configurar(this);
        ErrorUtil.montarCaminhoErro();
        try {
            inicializarObjetos();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater = getMenuInflater();
//        menuInflater.inflate(R.menu.menu_info, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_info:
//                new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso)
//                        .setMessage(R.string.info_activity_menu_odometro)
//                        .setPositiveButton(R.string.label_alert_button_ok, null).show();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    private void inicializarObjetos() {
        Toolbar _toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(_toolbar);
        titulo = getIntent().getStringExtra(MenuActivity.TAG_TITLE);
        getSupportActionBar().setTitle(titulo);
        usuario = UsuarioCTRL.getUsuario(this);
        _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setIniciarJornada(jornadaIniciada());
        setFinalizarJornada(jornadaIniciada());
        setCheckInPdv(jornadaIniciada());
        setCheckOutPdv(jornadaIniciada(), temPdv());
        setSincronizacao();
        setupListener();
        setarFontes();
    }

    private void setarFontes() {
        inserirFonte(this, _tvJornadaIniciarIcon, getResources().getString(R.string.icone_iniciar_jornada), R.font.fa_solid);
        inserirFonte(this, _tvJornadaFinalizarIcon, getResources().getString(R.string.icone_finalizar_jornada), R.font.fa_solid);
        inserirFonte(this, _tvSincronizacaoIcon, getResources().getString(R.string.icone_sincronizacao), R.font.fontawesome);
        inserirFonte(this, _tvCheckinPesquisaPdvIcon, getResources().getString(R.string.icone_checkin), R.font.fontawesome);
        inserirFonte(this, _tvCheckoutPdvIcon, getResources().getString(R.string.icone_checkout), R.font.fontawesome);
    }

    private void inserirFonte(Context context, TextView textView, String unicode, int fonteId) {
        Typeface font = ResourcesCompat.getFont(context, fonteId);
        textView.setTypeface(font);
        textView.setText(unicode);
    }

    private void setupListener() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Enviando Check-out...");
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") != ExternalDB.CD_RETORNO_SUCESSO) {
                        new AlertDialog.Builder(JornadaMenuActivity.this)
                                .setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", null)
                                .show();
                    } else {
                        removeCheckOutPreferencias();
                        new AlertDialog.Builder(JornadaMenuActivity.this).setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", (dialogInterface, i) -> finish()).setCancelable(false).show();
                    }
                } else {
                    checkout.setLatitude(String.valueOf(Geolocalizacao.getLatitude()));
                    checkout.setLongitude(String.valueOf(Geolocalizacao.getLongitude()));
                    GenericCTRL.save(JornadaMenuActivity.this, Checkout.TABELA, checkout.getContentValues());
                    new AlertDialog.Builder(JornadaMenuActivity.this).setTitle(R.string.title_alert_aviso)
                            .setMessage("Ocorreu uma FALHA na conexão! CHECK-OUT salvo para SINCRONIZAÇÃO!")
                            .setPositiveButton(R.string.label_alert_button_ok, (dialog, which) -> atualizarTelaMenuJornada())
                            .setCancelable(false).show();
                }
                progressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(JornadaMenuActivity.this, e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            try {
                checkout.setLatitude(String.valueOf(Geolocalizacao.getLatitude()));
                checkout.setLongitude(String.valueOf(Geolocalizacao.getLongitude()));
                GenericCTRL.save(JornadaMenuActivity.this, Checkout.TABELA, checkout.getContentValues());
                new AlertDialog.Builder(JornadaMenuActivity.this).setTitle(R.string.title_alert_aviso)
                        .setMessage("Ocorreu uma FALHA na conexão! VENDA salva para SINCRONIZAÇÃO!")
                        .setPositiveButton(R.string.label_alert_button_ok, (dialog, which) -> atualizarTelaMenuJornada())
                        .setCancelable(false).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    private boolean jornadaIniciada() {
        SharedPreferences preferenciasDaJornada = getSharedPreferences(JORNADA_INICIADA, this.MODE_PRIVATE);
        return preferenciasDaJornada.getBoolean(JORNADA_INICIADA, false);
    }

    private boolean temPdv() {
        SharedPreferences preferenciasDoCheckout = getSharedPreferences(PREFERENCIA_CHECKOUT, this.MODE_PRIVATE);
        return preferenciasDoCheckout.contains(ID_PDV);
    }

    private void setIniciarJornada(boolean foiIniciada) {
        _tvJornadaIniciarIcon = findViewById(R.id.jornada_inicio_icon);
        _llJornadaInicio = findViewById(R.id.jornada_inicio);
        _llJornadaInicio.setOnClickListener(abrirTelaIniciarJornada());
        ativarOuDesativarBotao(_tvJornadaIniciarIcon, _llJornadaInicio, !foiIniciada);
    }

    private void setFinalizarJornada(boolean foiIniciada) {
        _tvJornadaFinalizarIcon = findViewById(R.id.jornada_finalizar_icon);
        _llJornadaFinizar = findViewById(R.id.jornada_finalizar);
        _llJornadaFinizar.setOnClickListener(AbrirTelaFinalizarJornada());
        ativarOuDesativarBotao(_tvJornadaFinalizarIcon, _llJornadaFinizar, foiIniciada);
    }

    private void setCheckInPdv(boolean foiIniciada) {
        _tvCheckinPesquisaPdvIcon = findViewById(R.id.jornada_checkin_pesquisa_icon);
        _llCheckinPesquisaPdv = findViewById(R.id.jornada_checkin_pesquisa_pdv);
        _llCheckinPesquisaPdv.setOnClickListener(abrirListaDePesquisas());

        ativarOuDesativarBotao(_tvCheckinPesquisaPdvIcon, _llCheckinPesquisaPdv, foiIniciada);

    }

    private void setCheckOutPdv(boolean foiIniciada, boolean temPdv) {
        _tvCheckoutPdvIcon = findViewById(R.id.checkout_pdv_icon);
        _llCheckoutPdv = findViewById(R.id.checkout_pdv);
        _llCheckoutPdv.setOnClickListener(fazerOuSalvarCheckout());
        if (foiIniciada) {
            ativarOuDesativarBotao(_tvCheckoutPdvIcon, _llCheckoutPdv, temPdv);
        }
        else {
            ativarOuDesativarBotao(_tvCheckoutPdvIcon, _llCheckoutPdv, foiIniciada);
        }

    }

    private void setSincronizacao() {

        _tvSincronizacaoQuantidade = findViewById(R.id.sincronizacao_quantidade);
        _tvSincronizacaoIcon = findViewById(R.id.sincronizacao_icon);
        _llSincronizacao = findViewById(R.id.sincronizacao);
        _llSincronizacao.setOnClickListener(abrirTelaSincronizacao());
        ativarOuDesativarBotaoSincronizacao();
    }

    @NonNull
    private View.OnClickListener abrirTelaIniciarJornada() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!GeolocalizacaoUtil.verificarGeolocalizacao(JornadaMenuActivity.this)) {
                    return;
                } else {
                    Intent irTelaIniciarJornada = criarIntent(OdomentroActivity.class, INICIAR_JORNADA_TITLE);
                    irTelaIniciarJornada.putExtra("atividade", INICIAR_JORNADA);
                    startActivity(irTelaIniciarJornada);
                    finish();
                }
            }
        };
    }

    @NonNull
    private View.OnClickListener AbrirTelaFinalizarJornada() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!GeolocalizacaoUtil.verificarGeolocalizacao(JornadaMenuActivity.this)) {
                    return;
                } else {
                    Intent irTelaFinalizarJornada = criarIntent(OdomentroActivity.class, FINALIZAR_JORNADA_TITLE);
                    irTelaFinalizarJornada.putExtra("atividade", FINALIZAR_JORNADA);
                    startActivity(irTelaFinalizarJornada);
                    finish();
                }
            }
        };
    }

    @NonNull
    private View.OnClickListener abrirListaDePesquisas() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!GeolocalizacaoUtil.verificarGeolocalizacao(JornadaMenuActivity.this)) {
                    return;
                } else {
                    Intent irCheckInPesquisa = criarIntent(PesquisaOdomentroActivity.class, CHECK_IN_PDV_TITLE);
                    startActivity(irCheckInPesquisa);
                    finish();
                }
            }
        };
    }

    @NonNull
    private View.OnClickListener abrirTelaSincronizacao() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent irSincronizacao = criarIntent(SincronizacaoOdomActivity.class, SINCRONIZAÇÃO_TITLE);
                startActivity(irSincronizacao);
                finish();
            }
        };
    }

    @NonNull
    private View.OnClickListener fazerOuSalvarCheckout() {
        ErrorUtil.montarCaminhoErro();
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Checkout checkout = pegarCheckOut();
                    fazerCheckOut(checkout);
                } catch (Exception e) {
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(JornadaMenuActivity.this, e);
                }
            }
        };
    }

    private void fazerCheckOut(Checkout checkout) throws Exception {
        this.checkout = checkout;
        if (!GeolocalizacaoUtil.verificarGeolocalizacao(JornadaMenuActivity.this)) {
            return;
        } else {
            enviarJornadaCheckout(checkout);
        }
    }

    public Checkout pegarCheckOut() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(PREFERENCIA_CHECKOUT, Context.MODE_PRIVATE);
        String idPdv = sharedPreferences.getString(ID_PDV, "");
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.mmm");
        String dataResposta = formato.format(Calendar.getInstance().getTime());
        return new Checkout(idPdv, dataResposta);
    }

    public void atualizarTelaMenuJornada() {
//        removeCheckOutPreferencias();
        Intent intent = criarIntent(JornadaMenuActivity.class, "");
        intent.putExtra(MenuActivity.TAG_TITLE, titulo);
        this.startActivity(intent);
        finish();
    }

    public void removeCheckOutPreferencias() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(PREFERENCIA_CHECKOUT, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences.contains(ID_PDV)) {
            editor.remove(ID_PDV);
            editor.apply();
        }
    }

    @NonNull
    private Intent criarIntent(Class clazz, String descricaoActivity) {
        Intent irTelaIniciarJornada = new Intent(JornadaMenuActivity.this, clazz);
        irTelaIniciarJornada.putExtra("telaOriginal", this.getClass().getName());
        irTelaIniciarJornada.putExtra("titulo", descricaoActivity);
        irTelaIniciarJornada.putExtra("isSenhaPadrao", false);
        irTelaIniciarJornada.putExtra("isMenu", false);
        return irTelaIniciarJornada;
    }

    private void ativarOuDesativarBotao (TextView textView, LinearLayout linearLayout, boolean jornadaIniciada) {
        if (jornadaIniciada) {
            textView.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            desativarBotao(textView, linearLayout);
        }
    }

    private void ativarOuDesativarBotao (TextView textView, LinearLayout linearLayout, boolean jornadaIniciada, boolean temPdv) {
        if (jornadaIniciada && temPdv) {
            textView.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            desativarBotao(textView, linearLayout);
        }
    }


    private void desativarBotao(TextView textView, LinearLayout linearLayout) {
        linearLayout.setClickable(false);
        textView.setEnabled(false);
        textView.setTextColor(getResources().getColor(R.color.bloqueio));
    }

    private void ativarOuDesativarBotaoSincronizacao() {
        ErrorUtil.montarCaminhoErro();
        int quantidadeJornada = OdomentroCTRL.pegarQuantidade(this);
        int quantidadePesquisaCheckin = RespostaPerguntaCTRL.pegarQuantidadePesquisaCheckin(this);
        int quantidadeRegistrosCheckout = CheckoutCTRL.pegarQuantidadeCheckout(this);
        int quantidadeRegistros = quantidadeJornada + quantidadePesquisaCheckin + quantidadeRegistrosCheckout;
        if (quantidadeRegistros > 0) {
            _tvSincronizacaoIcon.setTextColor(getResources().getColor(R.color.colorPrimary));
            try {
                Animacao.piscarView(_tvSincronizacaoQuantidade, 250, 750);
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(this, e);
            }
            _tvSincronizacaoQuantidade.setText(String.valueOf(quantidadeRegistros));
            _tvSincronizacaoQuantidade.setVisibility(View.VISIBLE);
        } else
            desativarBotao(_tvSincronizacaoIcon, _llSincronizacao);
    }

    private void enviarJornadaCheckout(Checkout checkout) throws Exception {
        ErrorUtil.montarCaminhoErro();
        progressDialog.show();
        final JSONObject logParams = checkout.pegarJson();
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_questionario_odomentro), responseListener, errorListener, null);
    }

}
