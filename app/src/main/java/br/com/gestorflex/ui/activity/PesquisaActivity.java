package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.PesquisaCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Pesquisa;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.PesquisaAdapter;
import br.com.gestorflex.ui.adapter.RankingAdapter;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.ErrorUtil;

public class PesquisaActivity extends AppCompatActivity implements OnItemClickListener {

    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisa);
        instanciarViews();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater = getMenuInflater();
//        menuInflater.inflate(R.menu.menu_info, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_info:
//                new AlertDialog.Builder(this)
//                        .setTitle(R.string.title_alert_aviso)
//                        .setMessage(R.string.info_activity_pesquisa)
//                        .setPositiveButton(R.string.label_alert_button_ok, null)
//                        .show();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
            usuario = UsuarioCTRL.getUsuario(this);
            toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
            getSupportActionBar().setTitle("Pesquisa");
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() throws Exception {
        List<Pesquisa> pesquisas = PesquisaCTRL.listPesquisas(this);
        if (pesquisas != null && pesquisas.size() > 0)
            configurarRecyclerView(pesquisas);
        else {
            Intent intent = new Intent(PesquisaActivity.this, QuestionarioPesquisaActivity.class);
            intent.putExtra("telaOriginal", "");
            intent.putExtra("idPesquisa", "");
            startActivity(intent);
            finish();
        }
    }

    private void configurarRecyclerView(List<Pesquisa> pesquisas) {
        RecyclerView _rvPesquisa = findViewById(R.id.rv_pesquisa);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        PesquisaAdapter pesquisaAdapter = new PesquisaAdapter(this, pesquisas);
        pesquisaAdapter.setRecyclerViewOnClickListenerHack(this);
        _rvPesquisa.setLayoutManager(layoutManager);
        _rvPesquisa.setAdapter(pesquisaAdapter);
    }

    @Override
    public void OnItemClickListener(View view, int position, Object item) {
        ErrorUtil.montarCaminhoErro();
        try {
            Pesquisa pesquisa = (Pesquisa) item;
            Intent intent = null;
            intent = new Intent(PesquisaActivity.this, QuestionarioPesquisaActivity.class);
            intent.putExtra("telaOriginal", "");
            intent.putExtra("idPesquisa", pesquisa.getId());
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }
}
