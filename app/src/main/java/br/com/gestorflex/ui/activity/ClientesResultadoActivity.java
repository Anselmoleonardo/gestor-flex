package br.com.gestorflex.ui.activity;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Cliente;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.ClienteAdapter;
import br.com.gestorflex.util.BottomBarUtil;

public class ClientesResultadoActivity extends AppCompatActivity {

    private TextView _tvTotal;
    private RecyclerView _rvClientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes_resultado);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        Toolbar _toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(_toolbar);
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        _tvTotal = findViewById(R.id.tv_resultado_clientes_total);
        _rvClientes = findViewById(R.id.rv_resultado_clientes);
        configurarViews();
    }

    private void configurarViews() {
        List<Cliente> clientes = (List<Cliente>) getIntent().getSerializableExtra("clientes");
        ClienteAdapter adapter = new ClienteAdapter(this, clientes);
        _rvClientes.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        _rvClientes.setAdapter(adapter);
        _tvTotal.setText(String.format("Total: %s", adapter.getItemCount()));
    }

}
