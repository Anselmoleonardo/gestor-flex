package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Cabecalho;
import br.com.gestorflex.model.ListarRanking;
import br.com.gestorflex.model.Produtividade;
import br.com.gestorflex.model.ProdutividadeDados;
import br.com.gestorflex.model.Ranking;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.ProdutividadeAdapter;
import br.com.gestorflex.util.BottomBarUtil;

public class ProdutividadeResultadoActivity extends AppCompatActivity {

    private RecyclerView _rvProdutividade;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView _tvTotal;
    private Toolbar _tbRankingFiltro;
    private TextView _tvProdutividadeSemDados;

    private Cabecalho cabecalho;
    private List<ProdutividadeDados> produtividadeDados;
    private Produtividade produtividade;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtividade_resultado);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        _tbRankingFiltro = findViewById(R.id.toolbar);
        _tvTotal = findViewById(R.id.tv_produtividade_total);
        _rvProdutividade = findViewById(R.id.rv_resultado_ranking);
        _tvProdutividadeSemDados = findViewById(R.id.tv_produtividade_sem_dados);
        _rvProdutividade.setHasFixedSize(true);
        configurarViews();
    }

    private void configurarViews() {
        usuario = UsuarioCTRL.getUsuario(this);
        _tbRankingFiltro.setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
        _tbRankingFiltro.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_tbRankingFiltro);
        produtividadeDados = (List<ProdutividadeDados>) getIntent().getSerializableExtra("produtividade_dados");
        cabecalho = (Cabecalho) getIntent().getSerializableExtra("cabecalho");
        produtividade = (Produtividade) getIntent().getSerializableExtra("produtividade");

        if (produtividadeDados.size() <= 0)
            _tvProdutividadeSemDados.setVisibility(View.VISIBLE);

        mAdapter = new ProdutividadeAdapter(this, produtividadeDados, cabecalho, produtividade);
        _rvProdutividade.setAdapter(mAdapter);

        _tvTotal.setText(getString(R.string.text_total_valor, produtividadeDados.size()));

//        _rvProdutividade.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));

    }
}
