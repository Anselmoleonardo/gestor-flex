package br.com.gestorflex.ui.holder;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Pesquisa;

public class PesquisaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final TextView _tvIdPesquisa;
    private final TextView _tvDescricaoPesquisa;

    private List<Pesquisa> pesquisas;
    private Pesquisa pesquisa;
    private OnItemClickListener recyclerViewOnClickListenerHack;


    public PesquisaHolder(@NonNull View itemView, OnItemClickListener recyclerViewOnClickListenerHack, List<Pesquisa> pesquisas) {
        super(itemView);
        _tvIdPesquisa = itemView.findViewById(R.id.tv_pesquisa_id);
        _tvDescricaoPesquisa = itemView.findViewById(R.id.tv_pesquisa_nome);
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
        this.pesquisas = pesquisas;
        itemView.setOnClickListener(this);
    }

    public void bindPesquisa(Pesquisa pesquisa) {
        this.pesquisa = pesquisa;
        _tvIdPesquisa.setText(pesquisa.getId() + "");
        _tvDescricaoPesquisa.setText(pesquisa.getNome());
    }

    @Override
    public void onClick(View v) {
        if (this.recyclerViewOnClickListenerHack != null) {
            this.recyclerViewOnClickListenerHack.OnItemClickListener(v, getLayoutPosition(), pesquisas.get(getLayoutPosition()));
        }
    }
}

//public class PesquisaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//
//    private AppCompatTextView _tvId;
//    private AppCompatTextView _tvNome;
//
//    private OnRecyclerItemClickListener recyclerViewOnClickListenerHack;
//    Pesquisa pesquisas = new Pesquisa();
//    List<Pesquisa> listPesquisas;
//
//
//    public PesquisaHolder(@NonNull View itemView, OnRecyclerItemClickListener recyclerViewOnClickListenerHack, List<Pesquisa> listPesquisas) {
//        super(itemView);
//        _tvId = itemView.findViewById(R.id.tv_pesquisa_id);
//        _tvNome = itemView.findViewById(R.id.tv_pesquisa_nome);
//
//        this.listPesquisas =  listPesquisas;
//        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
//        itemView.setOnClickListener(this);
//    }
//
//    public void bindPesquisa(Pesquisa pesquisa){
//        this.pesquisas = pesquisa;
//
//        _tvId.setText(String.valueOf(pesquisas.getId()));
//        _tvId.setOnClickListener(this);
//        _tvNome.setText(pesquisas.getNome());
//        _tvNome.setOnClickListener(this);
//
//    }
//
//    @Override
//    public void onClick(View v) {
//        if (this.recyclerViewOnClickListenerHack != null) {
//            this.recyclerViewOnClickListenerHack.onRecyclerItemClickListener(v, getLayoutPosition(), listPesquisas.get(getLayoutPosition()));
//        }
//
//    }
//}
