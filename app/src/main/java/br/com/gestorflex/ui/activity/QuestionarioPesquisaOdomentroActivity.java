package br.com.gestorflex.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.PdvCTRL;
import br.com.gestorflex.database.internal.controller.PerguntaCTRL;
import br.com.gestorflex.database.internal.controller.RespostaCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.PDV;
import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Resposta;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.pergunta.PerguntaInterface;
import br.com.gestorflex.ui.adapter.PesquisaDialagAdapater;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.ErrorUtil;

public class QuestionarioPesquisaOdomentroActivity extends AppCompatActivity implements OnItemClickListener {

    private static final int PERGUNTA_OBJETIVA = 0;
    public static final int APENAS_PERGUNTA_PDV = 1;
    private static final String PREFERENCIA_CHECKOUT = "checkout";
    private static final String ID_PDV = "idPdv";

    private TextView _tvSemPesquisa;

    private int indexPergunta = 0;
    private int qunatidadeDePerguntas = 0;
    private int idPesquisa;
    private Usuario usuario;
    private PerguntaInterface perguntaView;
    private List<RespostaPergunta> respostasDoUsuario;
    private List<Pergunta> perguntasDaPesquisa;
    private int perguntaSubjetivaTipo3 = 3;
    public PesquisaDialagAdapater dialagAdapater;

    int idPdv;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionario_pesquisa_odomentro);
        BottomBarUtil.configurar(this);
        Intent pesquisaIntent = getIntent();
        inicializarDados(pesquisaIntent);
    }

    private void inicializarDados(Intent pesquisaIntent) {
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(_toolbar);
            getSupportActionBar().setTitle(getIntent().getExtras().getString("titulo"));
            usuario = UsuarioCTRL.getUsuario(this);
            _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());

            idPesquisa = pesquisaIntent.getExtras().getInt("idPesquisa");
            _tvSemPesquisa = findViewById(R.id.tv_check_in_sem_pesquisa);
            perguntasDaPesquisa = PerguntaCTRL.getPerguntasDaPesquisaComOrdenacao(this, idPesquisa);
            respostasDoUsuario = new ArrayList<>();
            Pergunta ultimaPergunta = new Pergunta();
            ultimaPergunta.setPergunta((perguntasDaPesquisa.size() + 1) + " - Selecione o PDV:");
            perguntasDaPesquisa.add(ultimaPergunta);
            qunatidadeDePerguntas = perguntasDaPesquisa.size();
            Button avancar = findViewById(R.id.questionario_pergunta_avancar);
            avancar.setOnClickListener(proximaPerguntaOuFazerCheckin());
            Button voltar = findViewById(R.id.questionario_pergunta_voltar);
            voltar.setOnClickListener(perguntaAnterior());
            exibirPergunta(indexPergunta, null);
            setupListener();

        } catch (Exception e) {
            Log.e("erro", "" + e.getMessage());
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void setupListener() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Enviando Questionário...");
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") != ExternalDB.CD_RETORNO_SUCESSO) {
                        if (respostasDoUsuario != null && respostasDoUsuario.size() > 0)
                            respostasDoUsuario.remove(respostasDoUsuario.size() - 1);
                        new AlertDialog.Builder(QuestionarioPesquisaOdomentroActivity.this)
                                .setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", null)
                                .show();
                    } else {
                        int ultimaRespostaPdv = respostasDoUsuario.size() - 1;
                        idPdv = respostasDoUsuario.get(ultimaRespostaPdv).getId();
                        salvarIdPdv(String.valueOf(idPdv));
                        new AlertDialog.Builder(QuestionarioPesquisaOdomentroActivity.this)
                                .setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                    }
                                })
                                .show();
                    }
                } else {
                    salvarParaSincronizacao();
                    new AlertDialog.Builder(QuestionarioPesquisaOdomentroActivity.this).setTitle("Aviso")
                            .setMessage("Ocorreu uma FALHA na conexão! PESQUISA salva para SINCRONIZAÇÃO!")
                            .setPositiveButton("OK", (dialogInterface, i) -> finish()).show();
                }
                progressDialog.dismiss();
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(QuestionarioPesquisaOdomentroActivity.this, e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            try {
                salvarParaSincronizacao();
                new AlertDialog.Builder(QuestionarioPesquisaOdomentroActivity.this).setTitle("Aviso")
                        .setMessage("Ocorreu uma FALHA na conexão! PESQUISA salva para SINCRONIZAÇÃO!")
                        .setPositiveButton("OK", (dialog, which) -> finish()).show();
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(QuestionarioPesquisaOdomentroActivity.this, e);
            }
        };
    }

    private void exibirPergunta(int indexPergunta, String respostaUsuario) {
        ErrorUtil.montarCaminhoErro();
        try {
            perguntaView = instanciarPerguntaView(perguntasDaPesquisa.get(indexPergunta).getTipo());
            Pergunta pergunta = perguntasDaPesquisa.get(indexPergunta);
            int idPergunta = perguntasDaPesquisa.get(indexPergunta).getId();
            List<Resposta> respostas = null;
            if (naoHaPerguntas(indexPergunta)) {
                perguntaView = instanciarPerguntaView(PERGUNTA_OBJETIVA);
                respostas = adicionarRespostasDoPdv();
            } else {
                respostas = RespostaCTRL.listResposta(this, idPergunta);
            }
            adicionarPerguntaNaView(perguntaView, pergunta, respostas, respostaUsuario);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private boolean naoHaPerguntas(int indexPergunta) {
        return indexPergunta == qunatidadeDePerguntas - 1;
    }

    private PerguntaInterface instanciarPerguntaView(int tipoImplementacao) throws Exception {
        //Reflexão
        PerguntaInterface perguntaView = null;
        Field campo = R.string.class.getField("pergunta_impl_" + tipoImplementacao);
        int idCampo = campo.getInt(null);
        String caminhoImplementacao = getResources().getString(idCampo);
        Class<PerguntaInterface> perguntaViewClass = (Class<PerguntaInterface>) Class.forName(caminhoImplementacao);
        perguntaView = perguntaViewClass.newInstance();

        return perguntaView;
    }

    private void adicionarPerguntaNaView(PerguntaInterface perguntaView, Pergunta pergunta, List<Resposta> respostas, String respostaUsuario) {
        View view = perguntaView.criarPerguntaView(this, pergunta, respostas, respostaUsuario, this);
        LinearLayout questionario = findViewById(R.id.questionario_pesquisa);
        questionario.removeAllViews();
        questionario.addView(view);
    }

    private List<Resposta> adicionarRespostasDoPdv() {
        ErrorUtil.montarCaminhoErro();
        try {
            List<Resposta> respostas = new ArrayList<>();
            List<PDV> pdvsDoUsuario = PdvCTRL.listPdvs(this);
            for (PDV pdv : pdvsDoUsuario) {
                Resposta resposta = new Resposta();
                resposta.setId(pdv.getId());
                resposta.setResposta(pdv.getDescricao());
                respostas.add(resposta);
            }
            return respostas;
        } catch (Exception ex) {
            ErrorUtil.enviarErroPorEmail(this, ex);
            return null;
        }
    }

    @NonNull
    private View.OnClickListener proximaPerguntaOuFazerCheckin() {
        ErrorUtil.montarCaminhoErro();
        return view -> {
            try {
                int ultimaPergunta = qunatidadeDePerguntas - 1;
                boolean podeAvancar = indexPergunta < ultimaPergunta;
                int tipo = perguntasDaPesquisa.get(indexPergunta).getTipo();
                int fgValidate = perguntasDaPesquisa.get(indexPergunta).getValidacao();
                int retornoQtdVenda = perguntaView.pegaNumDeVendas();
                if (tipo == perguntaSubjetivaTipo3 && retornoQtdVenda != 0) {
                    retornoQtdVenda = perguntaView.pegaNumDeVendas();
                    montarDialog(retornoQtdVenda, ultimaPergunta, podeAvancar, fgValidate);
                } else
                    irPerguntaNormal(ultimaPergunta, podeAvancar);
            } catch (Exception ex) {
                new AlertDialog.Builder(QuestionarioPesquisaOdomentroActivity.this).setTitle(R.string.title_alert_aviso)
                        .setMessage(ex.getMessage()).setPositiveButton(R.string.label_alert_button_ok, null).show();
            }
        };
    }

    private void irPerguntaNormal(int ultimaPergunta, boolean podeAvancar) throws Exception {
        ErrorUtil.montarCaminhoErro();
        RespostaPergunta respostaDoUsuario;
        try {
            respostaDoUsuario = perguntaView.getRespostaDoUsuario();
            if (podeAvancar) {
                avancarProximaPergunta(respostaDoUsuario);
            } else if (indexPergunta == ultimaPergunta) {
                fazerCheckin(respostaDoUsuario);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            new AlertDialog.Builder(QuestionarioPesquisaOdomentroActivity.this)
                    .setTitle("Aviso")
                    .setMessage(ex.getMessage())
                    .setPositiveButton("OK", null)
                    .show();
        }
    }

    public void avancarProximaPergunta(RespostaPergunta respostaDoUsuario) {
        respostasDoUsuario.add(respostaDoUsuario);
        indexPergunta++;
        exibirPergunta(indexPergunta, null);
    }

    public void fazerCheckin(RespostaPergunta respostaDoUsuario) throws Exception {
        int quantidadeDeRepostas = respostasDoUsuario.size();
        if (quantidadeDeRepostas < qunatidadeDePerguntas)
            respostasDoUsuario.add(respostaDoUsuario);
        enviarQuestionario(respostasDoUsuario);
    }

    @NonNull
    private View.OnClickListener perguntaAnterior() {
        return view -> {
            if (indexPergunta > 0) {
                int indexUltimaResposta = respostasDoUsuario.size() - 1;
                String resposta = respostasDoUsuario.get(indexUltimaResposta).getDescricaoResposta();
                respostasDoUsuario.remove(indexUltimaResposta);
                indexUltimaResposta = respostasDoUsuario.size() - 1;
                if (respostasDoUsuario.size() == qunatidadeDePerguntas - 1) {
                    resposta = respostasDoUsuario.get(indexUltimaResposta).getDescricaoResposta();
                    respostasDoUsuario.remove(indexUltimaResposta);
                }
                indexPergunta--;
                exibirPergunta(indexPergunta, resposta);
            }
            if (indexPergunta == 0) {
                respostasDoUsuario.clear();
            }
        };
    }

    private void montarDialog(int qtdVendidos, final int ultimaPergunta, final boolean avancar, final int validate) {
        ErrorUtil.montarCaminhoErro();
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.item_pesquisa_layout, null);
        dialogBuilder.setView(dialogView);
        if (validate == 1) {
            dialogBuilder.setTitle("Informe CPF");
        } else if (validate == 2) {
            dialogBuilder.setTitle("Informe GSM");
        } else {
            dialogBuilder.setTitle("Informe corretamente");
        }
        dialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> {
            try {
                irPerguntaNormal(ultimaPergunta, avancar);
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(QuestionarioPesquisaOdomentroActivity.this, e);
            }
        });
        configurarRecyclerView(dialogView, qtdVendidos);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private void configurarRecyclerView(View view, int count) {
        RecyclerView _rvPesquisa = view.findViewById(R.id.rv_pesquisa_dialag);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dialagAdapater = new PesquisaDialagAdapater(this, count, 0);
        dialagAdapater.setRecyclerViewOnClickListenerHack(this);
        _rvPesquisa.setLayoutManager(layoutManager);
        _rvPesquisa.setAdapter(dialagAdapater);
    }

    public void salvarIdPdv(String idPdv) {
        SharedPreferences sharedPreferences = this.getSharedPreferences(PREFERENCIA_CHECKOUT, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ID_PDV, idPdv);
        editor.apply();
    }

    private void enviarQuestionario(List<RespostaPergunta> respostaPerguntas) throws Exception {
        ErrorUtil.montarCaminhoErro();
        progressDialog.show();
        SharedPreferences sharedPreferences = this.getSharedPreferences(ExternalDB.URL_CONEXAO, this.MODE_PRIVATE);
        String url = sharedPreferences.getString("url", this.getResources().getString(R.string.url_producao));
        Log.e("lista de Questionario ", respostaPerguntas.get(0).toString());
        RespostaPergunta respostaPergunta = RespostaPergunta.concatenarRespostas(respostaPerguntas);
        final JSONObject logParams = respostaPergunta.getLogRespostaOdomentro();
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_questionario_odomentro), responseListener, errorListener, null);
    }

    private void salvarParaSincronizacao() {
        int ultimaRespostaPdv = respostasDoUsuario.size() - 1;
        String pdv = (respostasDoUsuario.get(ultimaRespostaPdv).getDescricaoResposta().split("-"))[0].trim();
        pdv = pdv.replace("(B)", "");
        idPdv = Integer.parseInt(pdv.trim());
        salvarIdPdv(String.valueOf(idPdv));
        RespostaPergunta respostaPergunta = RespostaPergunta.concatenarRespostas(respostasDoUsuario);
        GenericCTRL.save(this, RespostaPergunta.TABELA, respostaPergunta.getContentValues());
    }

    @Override
    public void OnItemClickListener(View view, int position, Object item) {

    }

}
