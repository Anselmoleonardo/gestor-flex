package br.com.gestorflex.ui.fragment.fragments_menu;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Response;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.CategoriaDashboardCTRL;
import br.com.gestorflex.database.internal.controller.DashboardCTRL;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.PerfilDashboardCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.CategoriaDash;
import br.com.gestorflex.model.Dash;
import br.com.gestorflex.model.PerfilDashboard;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.service.LocationService;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;

/**
 * Created by Vicente on 22/05/2019.
 */

public class FragmentMenuDashboard extends Fragment implements View.OnClickListener {

    // VIEWS
    private TextView tvUltimaAtualizacao;
    private FloatingActionButton fabAtualizarPerfil;
    private TextView tvValidacaoGross;
    private LinearLayout llDashContent;

    // CLASSES
    private Usuario usuario;

    //DADOS
    private List<Dash> dadosDash;
    private List<CategoriaDash> categoriasDash;

    // VOLLEY
    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    // OUTROS
    private Boolean isDash = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu_dashboard, container, false);
        instanciarObjects(rootView);
        return rootView;
    }

    private void instanciarObjects(View rootView) {
        ErrorUtil.montarCaminhoErro();
        try {
            fabAtualizarPerfil = rootView.findViewById(R.id.fab_perfil_atualizar);
            tvValidacaoGross = rootView.findViewById(R.id.tv_perfil_validacao_gross);
            tvUltimaAtualizacao = rootView.findViewById(R.id.tv_perfil_ultima_atualizacao);
            llDashContent = rootView.findViewById(R.id.ll_perfil_dash_content);
            setupVolleyListeners();
            inicializarDados();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(getActivity(), e);
        }
    }

    //dash gestor
    public void inicializarDados() {
        categoriasDash = CategoriaDashboardCTRL.listCategoriaDash(getActivity());
        usuario = UsuarioCTRL.getUsuario(getActivity());
        if (categoriasDash == null) {
            tvUltimaAtualizacao.setText("Nenhum REGISTRO selecionado em " + DateUtil.getCurrentDate());
        } else {
            dadosDash = DashboardCTRL.listDashboardPorCategoria(getActivity(), categoriasDash.get(0).getId());
            tvUltimaAtualizacao.setText((dadosDash.get(0).getDataHora().length() > 0) ?
                    "Atualizado em: " + dadosDash.get(0).getDataHora() :
                    "O dashboard ainda NÃO foi atualizado");
            inserirValoresDashboard();
        }
        fabAtualizarPerfil.setOnClickListener(this);
    }

    //dash gestor
    private void inserirValoresDashboard() {
        ErrorUtil.montarCaminhoErro();
        llDashContent.removeAllViews();
        isDash = true;
        try {
            for (int i = 0; i < categoriasDash.size(); i++) {
                dadosDash = DashboardCTRL.listDashboardPorCategoria(getActivity(), categoriasDash.get(i).getId());
                TableRow.LayoutParams lpCategoria = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                TableRow.LayoutParams lpValor = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                TableLayout tlCategoriaDash = new TableLayout(getActivity());
                TableRow trHeaderCategoria = new TableRow(getActivity());
                TextView txtHeaderCategoriaDash = new TextView(getActivity());

                txtHeaderCategoriaDash.setText(categoriasDash.get(i).getNome());
                txtHeaderCategoriaDash.setTextColor(Color.WHITE);
                txtHeaderCategoriaDash.setTextSize(16);
                txtHeaderCategoriaDash.setGravity(Gravity.CENTER_HORIZONTAL);
                txtHeaderCategoriaDash.setLayoutParams(lpCategoria);

                trHeaderCategoria.setBackgroundResource(R.color.colorPrimary);
                trHeaderCategoria.addView(txtHeaderCategoriaDash);
                trHeaderCategoria.setLayoutParams(lpCategoria);

                tlCategoriaDash.setStretchAllColumns(true);
                tlCategoriaDash.setLayoutParams(lpCategoria);
                tlCategoriaDash.addView(trHeaderCategoria);

                llDashContent.addView(tlCategoriaDash);

                if (dadosDash.size() > 0) {
                    TableLayout tlValoresDash = new TableLayout(getActivity());
                    TableRow trValoresDash = new TableRow(getActivity());

                    for (int j = 0; j < dadosDash.size(); j++) {
                        String v = dadosDash.get(j).getValor();
                        TextView txValoresDash = new TextView(getActivity());

                        txValoresDash.setTextColor(Color.BLACK);
                        txValoresDash.setBackgroundColor(Color.WHITE);
                        txValoresDash.setTextSize(12);
                        txValoresDash.setGravity(Gravity.CENTER_HORIZONTAL);
                        txValoresDash.setText(v);
                        txValoresDash.setPadding(12, 12, 12, 12);
                        txValoresDash.setLayoutParams(lpValor);

                        trValoresDash.addView(txValoresDash);
                        trValoresDash.setLayoutParams(lpCategoria);
                    }

                    tlValoresDash.setLayoutParams(lpCategoria);
                    tlValoresDash.setStretchAllColumns(true);
                    tlValoresDash.addView(trValoresDash);
                    llDashContent.addView(tlValoresDash);

                    //adiciona os valores
                    //TableLayout tlTituloDash = new TableLayout(getActivity());
                    TableRow trTituloDash = new TableRow(getActivity());

                    for (int x = 0; x < dadosDash.size(); x++) {
                        String v = dadosDash.get(x).getNome();
                        TextView txTituloDash = new TextView(getActivity());
                        txTituloDash.setTextColor(Color.BLACK);
                        txTituloDash.setBackgroundColor(Color.WHITE);
                        txTituloDash.setTextSize(12);
                        txTituloDash.setGravity(Gravity.CENTER_HORIZONTAL);
                        txTituloDash.setText(v);
                        txTituloDash.setPadding(12, 12, 12, 12);
                        txTituloDash.setLayoutParams(lpValor);

                        trTituloDash.addView(txTituloDash);
                        trTituloDash.setLayoutParams(lpCategoria);
                    }

                    //tlTituloDash.setLayoutParams(lpCategoria);
                    //tlTituloDash.setStretchAllColumns(true);
                    tlValoresDash.addView(trTituloDash);
                    //llDashContent.addView(tlValoresDash);
                } else {
                    TableLayout tlTituloDash = new TableLayout(getActivity());
                    TableRow trTituloDash = new TableRow(getActivity());

                    tlTituloDash.setLayoutParams(lpCategoria);
                    tlTituloDash.setStretchAllColumns(true);

                    TextView txAlert = new TextView(getActivity());
                    txAlert.setGravity(Gravity.CENTER_HORIZONTAL);
                    txAlert.setText("Nenhum resultado");
                    txAlert.setTextSize(12);
                    trTituloDash.addView(txAlert);
                    tlTituloDash.addView(trTituloDash);
                    llDashContent.addView(tlTituloDash);
                }
            }
        } catch (Exception ex) {
            ErrorUtil.enviarErroPorEmail(getActivity(), ex);
        }
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Atualizando dashboard...");
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                JSONObject jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt("cdRetorno") >= ExternalDB.CD_RETORNO_SUCESSO) {
//                    JSONArray jdadosUsuario = jsonRetorno.getJSONArray("usuario");
//                    JSONObject jsonUsuario = jdadosUsuario.getJSONObject(0);
//                    Usuario userId = UsuarioCTRL.getUsuario(getActivity());
//                    Usuario usuarioData = new Usuario(jsonUsuario);
//                    String DataBD = jsonUsuario.getString("dataBD");
//                    if (!userId.getDataBD().equals(DataBD)) {
//
//                    }
//                    usuarioData.setDataBD(DataBD);
//                    GenericCTRL.update(getActivity(), Usuario.TABELA, Usuario.CAMPO_ID + "= ?", new String[]{"" + userId.getId()}, usuarioData.getContetValuesUpdate());

                    JSONArray jArrayHeader = jsonRetorno.getJSONArray("header");
                    JSONArray jArrayDashboard = jsonRetorno.getJSONArray("dashboard");
                    categoriasDash = new ArrayList<>();
                    dadosDash = new ArrayList<>();
                    GenericCTRL.deleteAll(getActivity(), Dash.TABELA);
                    GenericCTRL.deleteAll(getActivity(), CategoriaDash.TABELA);
                    if (jArrayHeader.length() > 0 && jArrayDashboard.length() > 0) {
                        for (int i = 0; i < jArrayHeader.length(); i++) {
                            CategoriaDash categoriaDash = new CategoriaDash(jArrayHeader.getJSONObject(i));
                            categoriasDash.add(categoriaDash);
                            GenericCTRL.save(getActivity(), CategoriaDash.TABELA, categoriaDash.getContentValues());
                        }
                        for (int i = 0; i < jArrayDashboard.length(); i++) {
                            Dash dash = new Dash(jArrayDashboard.getJSONObject(i));
                            dadosDash.add(dash);
                            GenericCTRL.save(getActivity(), Dash.TABELA, dash.getContentValues());
                        }
                    }
                    inserirValoresDashboard();
                    if (dadosDash.size() > 0 && dadosDash.get(0).getDataHora().length() > 0)
                        tvUltimaAtualizacao.setText("Atualizado em: " + dadosDash.get(0).getDataHora());
                    else
                        tvUltimaAtualizacao.setText("O dashboard ainda NÃO foi atualizado");
                    BottomBarUtil.configurar(getActivity());
                    // Atualiza e inicia monitoramento
                    JSONArray jArrayMonitoramento = jsonRetorno.getJSONArray("monitoramento");
                    int boMonitoramento = jArrayMonitoramento.getJSONObject(0).optInt("d05_bomonitoramento", 1);
                    LocationService.tratarRetornoMonitoramento(getActivity(), boMonitoramento);
                    Snackbar.make(llDashContent, "Dashboard atualizado.", Snackbar.LENGTH_SHORT)
                            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
                } else {
                    new AlertDialog.Builder(getActivity()).setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString("msgRetorno"))
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
                }
                progressDialog.dismiss();
            } catch (Exception e) {
                progressDialog.dismiss();
                ErrorUtil.enviarErroPorEmail(getActivity(), e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            Snackbar.make(llDashContent, "Erro ao conectar ao servidor", Snackbar.LENGTH_SHORT)
                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
        };
    }

    private void atualizarDashboard() throws Exception {
        ErrorUtil.montarCaminhoErro();
        progressDialog.show();
        final JSONObject logParams = new JSONObject();
        ExternalDB.addDbRequest(getContext(), logParams, getString(R.string.procedure_atualizar_perfil_dash), responseListener, errorListener, null);
    }

    @Override
    public void onClick(View v) {
        ErrorUtil.montarCaminhoErro();
        switch (v.getId()) {
            case R.id.fab_perfil_atualizar:
                try {
                    atualizarDashboard();
                } catch (Exception e) {
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(getActivity(), e);
                }
                break;
        }
    }
}
