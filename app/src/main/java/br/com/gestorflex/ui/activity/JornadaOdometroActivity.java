package br.com.gestorflex.ui.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.android.volley.Response;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.CheckInOutCTRL;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.OdometroCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.CheckInOut;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;
import br.com.gestorflex.util.ImageUtil;
import br.com.gestorflex.util.ValidacaoUtil;

public class JornadaOdometroActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar _toolbar;
    private TextInputLayout _tilKmDia;
    private TextInputLayout _tilOdometroKm;
    private AppCompatImageView _ivOdometro;
    private AppCompatTextView _tvOdometro;
    private CardView _cvImagemOdometro;
    private FloatingActionButton _fabTirarFoto;
    private FloatingActionButton _fabEnviar;
    private ProgressDialog progressDialog;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;

    private Odometro odometro;
    private Bitmap foto;
    private String caminhoFoto;
    private GeolocalizacaoUtil geolocalizacaoUtil;
    private int atividade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jornada_odometro);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        _toolbar = findViewById(R.id.toolbar);
        _tilKmDia = findViewById(R.id.til_jornada_odometro_km_dia);
        _tilOdometroKm = findViewById(R.id.til_jornada_odometro_km);
        _cvImagemOdometro = findViewById(R.id.cv_jornada_odometro_imagem_odometro);
        _ivOdometro = findViewById(R.id.iv_jornada_odometro);
        _tvOdometro = findViewById(R.id.tv_jornada_odometro);
        _fabEnviar = findViewById(R.id.fab_jornada_odometro_enviar);
        _fabTirarFoto = findViewById(R.id.fab_jornada_odometro_tirar_foto);
        configurarViews();
    }

    private void configurarViews() {
        _toolbar.setSubtitle(UsuarioCTRL.getUsuario(this).getMatriculaNome());
        setSupportActionBar(_toolbar);
        geolocalizacaoUtil = new GeolocalizacaoUtil(this);
        atividade = getIntent().getIntExtra("atividade", Odometro.INICIAR_JORNADA);
        if (atividade == Odometro.FINALIZAR_JORNADA) {
            getSupportActionBar().setTitle(R.string.text_finalizar_jornada);
            _tilKmDia.setVisibility(View.GONE);
            _fabEnviar.setImageResource(R.drawable.ic_finalizar_jornada);
        }
        setupVolleyListeners();
        _ivOdometro.setOnClickListener(this);
        _fabEnviar.setOnClickListener(this);
        _fabTirarFoto.setOnClickListener(this);
    }

    private void abrirCamera() {
        Intent intentCamera = ImageUtil.criarIntentFotoCamera(this);
        if (intentCamera != null)
            startActivityForResult(intentCamera, ImageUtil.REQUEST_PHOTO_CAMERA);
        else
            Snackbar.make(_fabTirarFoto, R.string.message_snackbar_erro_abrir_camera, Snackbar.LENGTH_LONG)
                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
    }

    private boolean validarOdometro() {
        List<Odometro> listOdometro = OdometroCTRL.listOdometro(this);
        if (atividade == Odometro.FINALIZAR_JORNADA && listOdometro.get(0).getKmDoOdometro() >
                Integer.parseInt(_tilOdometroKm.getEditText().getText().toString().trim())) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_error_odometro_final_menor)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        if (!GeolocalizacaoUtil.verificarGeolocalizacao(this))
            return false;
        if (atividade == Odometro.INICIAR_JORNADA && !ValidacaoUtil.validarCamposVazios(this, _tilKmDia))
            return false;
        if (!ValidacaoUtil.validarCamposVazios(this, _tilOdometroKm))
            return false;
        if (foto == null) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_error_sem_imagem_odometro)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        return true;
    }

    private void removerErros() {
        ValidacaoUtil.removerErroTextInputLayout(_tilKmDia);
        ValidacaoUtil.removerErroTextInputLayout(_tilOdometroKm);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ImageUtil.REQUEST_PHOTO_CAMERA || requestCode == ImageUtil.REQUEST_PHOTO_GALLERY) {
                foto = ImageUtil.getImagemFromIntent(this, data, requestCode, _fabEnviar);
                if (foto != null) {
                    caminhoFoto = ImageUtil.CURRENT_IMAGE_PATH;
                    _tvOdometro.setVisibility(View.GONE);
                    _ivOdometro.setVisibility(View.VISIBLE);
                    _ivOdometro.setImageBitmap(foto);
                }
            }
        }
    }

    private void preencherOdometro() {
        odometro = new Odometro();
        odometro.setDataEnvio(DateUtil.getCurrentDateTimeUS());
        odometro.setLatitude(String.valueOf(Geolocalizacao.getLatitude()));
        odometro.setLongitude(String.valueOf(Geolocalizacao.getLongitude()));
        odometro.setKmDoDia(atividade == Odometro.INICIAR_JORNADA ? Integer.parseInt(_tilKmDia.getEditText().getText().toString()) : 0);
        odometro.setKmDoOdometro(Integer.parseInt(_tilOdometroKm.getEditText().getText().toString()));
        odometro.setFoto(ImageUtil.converterBitmapParaBase64(foto));
        odometro.setCaminhoFoto(caminhoFoto);
        odometro.setAtividade(atividade);
    }

    private void mostrarDialogSincronizacao() {
        odometro.setIsSinc(1);
        GenericCTRL.save(JornadaOdometroActivity.this, Odometro.TABELA, odometro.getContentValues());
        new AlertDialog.Builder(JornadaOdometroActivity.this).setTitle(R.string.title_alert_aviso)
                .setMessage(R.string.message_alert_erro_odometro_sincronizacao).setPositiveButton(R.string.label_alert_button_ok, null)
                .setOnDismissListener(dialog -> finish()).show();
    }

    private boolean possuiDadosSincronizacao() {
        List<Odometro> listOdometros = OdometroCTRL.listOdometroSincronizacao(this);
        List<CheckInOut> listCheckInOut = CheckInOutCTRL.listCheckInOutSincronizacao(this);
        if (!listOdometros.isEmpty() || !listCheckInOut.isEmpty()) {
            new AlertDialog.Builder(JornadaOdometroActivity.this).setTitle(R.string.title_alert_aviso)
                    .setMessage(R.string.message_alert_erro_existe_sincronizacao).setPositiveButton(R.string.label_alert_button_ok, null)
                    .setOnDismissListener(dialog -> finish()).show();
            return true;
        }
        return false;
    }

    // --------------------------------- VOLLEY ---------------------------------

    public void enviarOdometro() {
        ErrorUtil.montarCaminhoErro();
        try {
            progressDialog.show();
            preencherOdometro();
            ExternalDB.addDbRequest(this, odometro.getJSONParams(), getString(R.string.procedure_enviar_odometro), responseListener, errorListener, null);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_enviando_odometro));
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONObject jsonResposta = jsonRetorno.getJSONArray("retorno").getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") != ExternalDB.CD_RETORNO_SUCESSO) {
                        new AlertDialog.Builder(JornadaOdometroActivity.this).setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", null).show();
                    } else {
                        if (atividade == Odometro.INICIAR_JORNADA) {
                            odometro.setIsSinc(0);
                            GenericCTRL.save(JornadaOdometroActivity.this, Odometro.TABELA, odometro.getContentValues());
                        } else {
                            GenericCTRL.deleteAll(this, Odometro.TABELA);
                            GenericCTRL.deleteAll(this, CheckInOut.TABELA);
                        }
                        new AlertDialog.Builder(JornadaOdometroActivity.this).setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno")).setPositiveButton("OK", null)
                                .setOnDismissListener(dialog -> finish()).show();
                    }
                } else
                    mostrarDialogSincronizacao();
                progressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(JornadaOdometroActivity.this, e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            mostrarDialogSincronizacao();
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_jornada_odometro_enviar:
                geolocalizacaoUtil.getGeolocalizacao();
                removerErros();
                if (validarOdometro()) {
                    if (atividade == Odometro.FINALIZAR_JORNADA) {
                        if (!possuiDadosSincronizacao())
                            enviarOdometro();
                    } else
                        enviarOdometro();
                }
                break;
            case R.id.iv_jornada_odometro:
                if (foto != null)
                    ImageUtil.exibirImagem(this, foto);
                break;
            case R.id.fab_jornada_odometro_tirar_foto:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                            ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 4);
                    } else
                        abrirCamera();
                } else
                    abrirCamera();
                break;
        }
    }
}
