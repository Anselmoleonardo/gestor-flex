package br.com.gestorflex.ui.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.PerguntaCTRL;
import br.com.gestorflex.database.internal.controller.RespostaCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Resposta;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.pergunta.PerguntaInterface;
import br.com.gestorflex.ui.adapter.PesquisaDialagAdapater;
import br.com.gestorflex.util.ErrorUtil;

public class QuestionarioPesquisaActivity extends AppCompatActivity implements OnItemClickListener {

    private Button _btAvancar;
    private Button _btvoltar;
    private LinearLayout _llFormPesquisa;
    private TextView _tvSemPesquisa;

    private PerguntaInterface perguntaView;
    private int idPesquisa;
    private List<Pergunta> perguntasDaPesquisa;
    private List<RespostaPergunta> respostasDoUsuario;
    private int indexPergunta = 0;
    private int qunatidadeDePerguntas = 0;
    private int perguntaSubjetivaTipo3 = 3;

    private Usuario usuario;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;

    private Response.Listener<String> responseListenerPesquisa;
    private Response.ErrorListener errorListenerPesquisa;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editorSharedPreferences;
    private ProgressDialog progressDialog;

    public PesquisaDialagAdapater dialagAdapater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionario_pesquisa);
        instanciarObjetos();
    }

    public void instanciarObjetos() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            usuario = UsuarioCTRL.getUsuario(this);
            respostasDoUsuario = new ArrayList<>();
            toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
            getSupportActionBar().setTitle("Questionário");
            _btAvancar = findViewById(R.id.bt_avancar);
            _btvoltar = findViewById(R.id.bt_voltar);
            _llFormPesquisa = findViewById(R.id.llform_pesquisa);
            _tvSemPesquisa = findViewById(R.id.tv_pesquisa_sem_pesquisa);

            inicializarDados();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    public void inicializarDados() throws Exception {
        Intent pesquisaIntent = getIntent();
        idPesquisa = pesquisaIntent.getExtras().getInt("idPesquisa");
        perguntasDaPesquisa = PerguntaCTRL.getPerguntasDaPesquisa(this, idPesquisa);
        if (perguntasDaPesquisa != null && perguntasDaPesquisa.size() > 0) {
            qunatidadeDePerguntas = perguntasDaPesquisa.size();
            _btAvancar.setOnClickListener(proximaPergunta(perguntasDaPesquisa.get(indexPergunta).getTipo()));
            _btvoltar.setOnClickListener(perguntaAnterior());
            exibirPergunta(indexPergunta, null);
            setupListener();
        } else {
            _tvSemPesquisa.setVisibility(View.VISIBLE);
            _llFormPesquisa.setVisibility(View.GONE);
        }
//        setupVolleyListeners();
    }

    public void savePreferencesValidacaoPesquisa(int situacaoFunc, int isHabilit, String data) {
        ErrorUtil.montarCaminhoErro();
        try {
            sharedPreferences = getSharedPreferences(getString(R.string.situacaoFunc), Context.MODE_MULTI_PROCESS);
            editorSharedPreferences = sharedPreferences.edit();
            editorSharedPreferences.putString(getString(R.string.situacaoFunc), String.valueOf(situacaoFunc));
            editorSharedPreferences.apply();

            sharedPreferences = getSharedPreferences(getString(R.string.isHabiliteVenda), Context.MODE_MULTI_PROCESS);
            editorSharedPreferences = sharedPreferences.edit();
            editorSharedPreferences.putString(getString(R.string.isHabiliteVenda), String.valueOf(isHabilit));
            editorSharedPreferences.apply();

            sharedPreferences = getSharedPreferences(getString(R.string.isDateLiberacaoPesquisa), Context.MODE_MULTI_PROCESS);
            editorSharedPreferences = sharedPreferences.edit();
            editorSharedPreferences.putString(getString(R.string.isDateLiberacaoPesquisa), data);
            editorSharedPreferences.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }
//    private void loginRequest() throws Exception {
//        ErrorUtil.montarCaminhoErro();
//        ExternalDB.addDbRequest(this, new JSONObject(), getString(R.string.procedure_travar_venda), responseListenerPesquisa, errorListenerPesquisa, null);
//    }
    public void savePreferencesValidacaoExportacao(int boPeriodoExportacao, int boCheckin, int boChere) {
        ErrorUtil.montarCaminhoErro();
        try {
            SharedPreferences sharedPreferencesExp = getSharedPreferences(getString(R.string.isPeriodoExportado), Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editorExp = sharedPreferencesExp.edit();
            editorExp.putString(getString(R.string.isPeriodoExportado), String.valueOf(boPeriodoExportacao));
            editorExp.apply();

            SharedPreferences sharedPreferencesChere = getSharedPreferences(getString(R.string.isChere), Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editorChere = sharedPreferencesChere.edit();
            editorChere.putString(getString(R.string.isChere), String.valueOf(boChere));
            editorChere.apply();

            sharedPreferences = getSharedPreferences(getString(R.string.isCheckin), Context.MODE_MULTI_PROCESS);
            editorSharedPreferences = sharedPreferences.edit();
            editorSharedPreferences.putString(getString(R.string.isCheckin), String.valueOf(boCheckin));
            editorSharedPreferences.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }
//    private void setupVolleyListeners() {
//        ErrorUtil.montarCaminhoErro();
//        responseListenerPesquisa = response -> {
//            try {
//                if (response != null) {
//                    JSONObject jsonRetorno = new JSONObject(response);
//                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
//                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
//
//                    if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
//                        JSONArray jDadosValidatePesquisa = jsonRetorno.getJSONArray("validate_pesquisa");
//                        int boSituacaoFunc = jDadosValidatePesquisa.optJSONObject(0).getInt("boSituacao");
//                        int boValidateVenda = jDadosValidatePesquisa.optJSONObject(0).getInt("isHabilitadoVenda");
//                        String data = jDadosValidatePesquisa.optJSONObject(0).getString("dataLiberacao");
//                        savePreferencesValidacaoPesquisa(boSituacaoFunc, boValidateVenda, data);
//
//                        JSONArray jDadosExportacao = jsonRetorno.getJSONArray("travar_Venda");
//                        int boPeriodoExportacao = jDadosExportacao.optJSONObject(0).getInt("boPeriodoExportacao");
//                        int boCheckin = jDadosExportacao.optJSONObject(0).getInt("boCheckin");
//                        int boChere = jDadosExportacao.optJSONObject(0).getInt("boShare");
//                        savePreferencesValidacaoExportacao(boPeriodoExportacao, boCheckin, boChere);
//
//                        JSONArray jDadosFeed = jsonRetorno.getJSONArray("feed_notice");
//                        for (int i = 0; i < jDadosFeed.length(); i++) {
//                            FeedNoticia feed = new FeedNoticia(jDadosFeed.getJSONObject(i));
//                            GenericCTRL.save(QuestionarioPesquisaActivity.this, FeedNoticia.TABELA, feed.getContentValues());
//                        }
//
//                        JSONArray jDadosExcluirFeeds = jsonRetorno.getJSONArray("feedsExcluir");
//                        for (int i = 0; i < jDadosExcluirFeeds.length(); i++) {
//                            JSONObject jsonPesq = jDadosExcluirFeeds.getJSONObject(i);
//                            GenericCTRL.delete(QuestionarioPesquisaActivity.this, FeedNoticia.TABELA, jsonPesq.getInt("id"));
//                        }
//                    } else {
//                        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
//                        String datasAnterior = df.format(Calendar.getInstance().getTime());
//                        savePreferencesValidacaoPesquisa(1, 1, datasAnterior);
//                        savePreferencesValidacaoExportacao(0, 1, 0);
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                ErrorUtil.enviarErroPorEmail(QuestionarioPesquisaActivity.this, e);
//            }
//        };
//        errorListenerPesquisa = error -> {
//            DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
//            String datasAnterior = df.format(Calendar.getInstance().getTime());
//            savePreferencesValidacaoPesquisa(1, 1, datasAnterior);
//            savePreferencesValidacaoExportacao(0, 1, 0);
//        };
//    }

    public void exibirPergunta(int indexPergunta, String respostaUsuario) {
        ErrorUtil.montarCaminhoErro();
        try {
            perguntaView = instanciarPerguntaView(perguntasDaPesquisa.get(indexPergunta).getTipo());
            Pergunta pergunta = perguntasDaPesquisa.get(indexPergunta);
            int idPergunta = perguntasDaPesquisa.get(indexPergunta).getId();
            List<Resposta> respostas = null;
            respostas = RespostaCTRL.listResposta(this, idPergunta);
            adicionarPerguntaNaView(perguntaView, pergunta, respostas, respostaUsuario);
        } catch (Exception e) {
            Log.e("erro", "" + e.getMessage());
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private PerguntaInterface instanciarPerguntaView(int tipoImplementacao) throws Exception {
        PerguntaInterface perguntaView = null;
        Field campo = R.string.class.getField("pergunta_impl_" + tipoImplementacao);
        int idCampo = campo.getInt(null);
        String caminhoImplementacao = getResources().getString(idCampo);
        Class<PerguntaInterface> perguntaViewClass = (Class<PerguntaInterface>) Class.forName(caminhoImplementacao);
        perguntaView = perguntaViewClass.newInstance();
        return perguntaView;
    }

    private void adicionarPerguntaNaView(PerguntaInterface perguntaView, Pergunta pergunta, List<Resposta> respostas, String respostaUsuario) {
        View view = perguntaView.criarPerguntaView(this, pergunta, respostas, respostaUsuario, this);
        LinearLayout questionario = findViewById(R.id.llform_pesquisa);
        questionario.removeAllViews();
        questionario.addView(view);
    }

    @NonNull
    public View.OnClickListener proximaPergunta(final int tipoa) {
        ErrorUtil.montarCaminhoErro();
        return view -> {
            try {
                int ultimaPergunta = qunatidadeDePerguntas - 1;
                boolean podeAvancar = indexPergunta < ultimaPergunta;
                int tipo = perguntasDaPesquisa.get(indexPergunta).getTipo();
                int fgValidate = perguntasDaPesquisa.get(indexPergunta).getValidacao();
                Log.e("validacao act ", fgValidate + "");
                int retornoQtdVenda = perguntaView.pegaNumDeVendas();
                if (tipo == perguntaSubjetivaTipo3 && retornoQtdVenda > 0) {
                    retornoQtdVenda = perguntaView.pegaNumDeVendas();
                    montarDialog(retornoQtdVenda, ultimaPergunta, podeAvancar, fgValidate);
                } else
                    irPerguntaNormal(ultimaPergunta, podeAvancar, fgValidate);
            } catch (Exception ex) {
                new AlertDialog.Builder(QuestionarioPesquisaActivity.this)
                        .setTitle("Aviso")
                        .setMessage(ex.getMessage())
                        .setPositiveButton("OK", null)
                        .show();
            }
        };
    }

    public void irPerguntaNormal(int ultimaPergunta, boolean podeAvancar, int fgValidate) {
        ErrorUtil.montarCaminhoErro();
        RespostaPergunta respostaDoUsuario = null;
        try {
            respostaDoUsuario = perguntaView.getRespostaDoUsuario();
            if (podeAvancar) {
                respostasDoUsuario.add(respostaDoUsuario);
                indexPergunta++;
                Log.e("index", "" + indexPergunta);
                exibirPergunta(indexPergunta, null);
            } else if (indexPergunta == ultimaPergunta) {
                if (qunatidadeDePerguntas > 1)
                    respostasDoUsuario.add(respostaDoUsuario);
                int quantidadeDeRepostas = respostasDoUsuario.size();
                if (quantidadeDeRepostas < qunatidadeDePerguntas) {
                    respostasDoUsuario.add(respostaDoUsuario);
                }
                enviarQuestionario(respostasDoUsuario);
            }
        } catch (Exception ex) {
            new AlertDialog.Builder(QuestionarioPesquisaActivity.this)
                    .setTitle("Aviso")
                    .setMessage(ex.getMessage())
                    .setPositiveButton("OK", null)
                    .show();
        }
    }

    private View.OnClickListener perguntaAnterior() {
        return view -> {
            if (indexPergunta > 0) {
                int indexUltimaResposta = respostasDoUsuario.size() - 1;
                String resposta = respostasDoUsuario.get(indexUltimaResposta).getDescricaoResposta();
                respostasDoUsuario.remove(indexUltimaResposta);
                indexUltimaResposta = respostasDoUsuario.size() - 1;
                if (respostasDoUsuario.size() == qunatidadeDePerguntas - 1) {
                    resposta = respostasDoUsuario.get(indexUltimaResposta).getDescricaoResposta();
                    respostasDoUsuario.remove(indexUltimaResposta);
                }
                indexPergunta--;
                exibirPergunta(indexPergunta, resposta);
            }
            if (indexPergunta == 0) {
                respostasDoUsuario.clear();
            }
        };
    }

    private void montarDialog(int qtdVendidos, final int ultimaPergunta, final boolean avancar, final int validate) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.item_pesquisa_layout, null);
        dialogBuilder.setView(dialogView);
        if (validate == 1) {
            dialogBuilder.setTitle("Informe CPF");
        } else if (validate == 2) {
            dialogBuilder.setTitle("Informe GSM");
        } else {
            dialogBuilder.setTitle("Informe corretamente");
        }
        dialogBuilder.setPositiveButton("OK", (dialogInterface, i) -> irPerguntaNormal(ultimaPergunta, avancar, validate));
        configurarRecyclerView(dialogView, qtdVendidos, validate);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        Window window = alertDialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private void configurarRecyclerView(View view, int count, int validacao) {
        RecyclerView _rvPesquisa = view.findViewById(R.id.rv_pesquisa_dialag);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dialagAdapater = new PesquisaDialagAdapater(this, count, validacao);
        dialagAdapater.setRecyclerViewOnClickListenerHack(this);
        _rvPesquisa.setLayoutManager(layoutManager);
        _rvPesquisa.setAdapter(dialagAdapater);
    }

    private void setupListener() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Enviando Questionário...");
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                if (response != null) {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt("cdRetorno") != ExternalDB.CD_RETORNO_SUCESSO) {
                        new AlertDialog.Builder(QuestionarioPesquisaActivity.this).setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", null).show();
                    } else {
//                        loginRequest();
                        new AlertDialog.Builder(QuestionarioPesquisaActivity.this).setTitle("Aviso")
                                .setMessage(jsonResposta.optString("msgRetorno"))
                                .setPositiveButton("OK", (dialogInterface, i) -> finish()).show();
                    }
                } else {
                    salvarRespostas(respostasDoUsuario);
                    new AlertDialog.Builder(QuestionarioPesquisaActivity.this).setTitle("Aviso")
                            .setMessage("Ocorreu uma FALHA na conexão! Pesquisa salva para SINCRONIZAÇÃO!")
                            .setPositiveButton("OK", (dialogInterface, i) -> finish()).show();
                }
                progressDialog.dismiss();
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(QuestionarioPesquisaActivity.this, e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            try {
                salvarRespostas(respostasDoUsuario);
                new AlertDialog.Builder(QuestionarioPesquisaActivity.this).setTitle("Aviso")
                        .setMessage("Ocorreu uma FALHA na conexão! VENDA salva para SINCRONIZAÇÃO!")
                        .setPositiveButton("OK", (dialogInterface, i) -> finish()).show();
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(QuestionarioPesquisaActivity.this, e);
            }
        };
    }

    private void enviarQuestionario(List<RespostaPergunta> respostaPerguntas) throws Exception {
        ErrorUtil.montarCaminhoErro();
        progressDialog.show();
        JSONObject logParams = RespostaPergunta.getLogResposta(respostaPerguntas);
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_questionario_odomentro), responseListener, errorListener, null);
    }

    @Override
    public void OnItemClickListener(View view, int position, Object item) {

    }

    private void salvarRespostas(List<RespostaPergunta> respostas) {
        for (RespostaPergunta resposta : respostas) {
            GenericCTRL.save(this, RespostaPergunta.TABELA, resposta.getContentValues());
        }
    }
}
