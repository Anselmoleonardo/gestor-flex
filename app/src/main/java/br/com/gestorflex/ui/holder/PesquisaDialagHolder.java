package br.com.gestorflex.ui.holder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;

public class PesquisaDialagHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextInputLayout _tilPesquisaTipo3;
    private OnItemClickListener recyclerViewOnClickListenerHack;
    private List<TextInputLayout> list;

    public PesquisaDialagHolder(@NonNull View itemView, OnItemClickListener recyclerViewOnClickListenerHack, int qtdVendas) {
        super(itemView);
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
        _tilPesquisaTipo3 = itemView.findViewById(R.id.til_pesquisa_tipo3);
        list = new ArrayList<>();
        itemView.setOnClickListener(this);
    }

    public void bindPesquisaDialag() {
        list.clear();
        list.add(_tilPesquisaTipo3);
    }

    public List<TextInputLayout> getList() {
        return list;
    }

    @Override
    public void onClick(View view) {

    }
}
