package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.PesquisaCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.Pesquisa;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.PesquisaAdapter;
import br.com.gestorflex.util.ErrorUtil;

public class PesquisaOdomentroActivity extends AppCompatActivity implements OnItemClickListener {

    private static final String QUESTIONARIO_TITLE = "Questionário";
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesquisa);
        instanciarViews();
    }

//    @Override
////    public boolean onCreateOptionsMenu(Menu menu) {
////        MenuInflater menuInflater = getMenuInflater();
////        menuInflater.inflate(R.menu.menu_info, menu);
////        return true;
////    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menu_info:
//                new AlertDialog.Builder(this)
//                        .setTitle(R.string.title_alert_aviso)
//                        .setMessage(R.string.info_activity_pesquisa)
//                        .setPositiveButton(R.string.label_alert_button_ok, null)
//                        .show();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(_toolbar);
            getSupportActionBar().setTitle(getIntent().getExtras().getString("titulo"));
            usuario = UsuarioCTRL.getUsuario(this);
            _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() throws Exception {
        List<Pesquisa> pesquisas = PesquisaCTRL.listPesquisas(this);

        if (pesquisas.size() > 0 && usuario.getBoPesquisa()==1)
            configurarRecyclerView(pesquisas);
        else {
            Intent intent = new Intent(PesquisaOdomentroActivity.this, QuestionarioPesquisaOdomentroActivity.class);
            intent.putExtra("titulo", QUESTIONARIO_TITLE);
            intent.putExtra("telaOriginal", "");
            intent.putExtra("idPesquisa", "");
            startActivity(intent);
            finish();
        }
    }

    private void configurarRecyclerView(List<Pesquisa> pesquisas) {
        RecyclerView _rvPesquisa = findViewById(R.id.rv_pesquisa);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        PesquisaAdapter pesquisaAdapter = new PesquisaAdapter(this, pesquisas);
        pesquisaAdapter.setRecyclerViewOnClickListenerHack(this);
        _rvPesquisa.setLayoutManager(layoutManager);
        _rvPesquisa.setAdapter(pesquisaAdapter);
    }

    @Override
    public void OnItemClickListener(View view, int position, Object item) {
        ErrorUtil.montarCaminhoErro();
        try {
            Pesquisa pesquisa = (Pesquisa) item;
            Intent intent = null;
            intent = new Intent(PesquisaOdomentroActivity.this, QuestionarioPesquisaOdomentroActivity.class);//intent = new Intent(PesquisaOdomentroActivity.this, QuestionarioPesquisaActivity.class);
            intent.putExtra("titulo", QUESTIONARIO_TITLE);
            intent.putExtra("idPesquisa", pesquisa.getId());
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }

    }
}

