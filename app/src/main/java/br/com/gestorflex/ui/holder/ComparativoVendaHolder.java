package br.com.gestorflex.ui.holder;

import android.text.method.ScrollingMovementMethod;
import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.gestorflex.R;
import br.com.gestorflex.model.ComparativoVenda;

public class ComparativoVendaHolder extends RecyclerView.ViewHolder {

    private AppCompatTextView _tvNomeListagem;
    private AppCompatTextView _tvQuantidadeAntes;
    private AppCompatTextView _tvQuantidadeDepois;
    private AppCompatTextView _tvDiferenca;
    private AppCompatTextView _tvPorcentagem;

    public ComparativoVendaHolder(View itemView) {
        super(itemView);
        _tvNomeListagem = itemView.findViewById(R.id.item_comparativo_venda_nome_listagem);
        _tvQuantidadeAntes = itemView.findViewById(R.id.item_comparativo_venda_quantidade_antes);
        _tvQuantidadeDepois = itemView.findViewById(R.id.item_comparativo_venda_quantidade_depois);
        _tvDiferenca = itemView.findViewById(R.id.item_comparativo_venda_diferenca);
        _tvPorcentagem = itemView.findViewById(R.id.item_comparativo_venda_porcentagem);
    }

    public void bindRanking(ComparativoVenda comparativoVenda) {
        _tvNomeListagem.setText(comparativoVenda.getNomeListagem());
        _tvNomeListagem.setMovementMethod(new ScrollingMovementMethod());
        _tvQuantidadeAntes.setText(String.valueOf(comparativoVenda.getQuantidadeAntes()));
        _tvQuantidadeDepois.setText(String.valueOf(comparativoVenda.getQuantidadeDepois()));
        _tvDiferenca.setText(String.valueOf(comparativoVenda.getDiferenca()));
        _tvPorcentagem.setText(String.valueOf(comparativoVenda.getPorcentagem()) + "%");
    }

}
