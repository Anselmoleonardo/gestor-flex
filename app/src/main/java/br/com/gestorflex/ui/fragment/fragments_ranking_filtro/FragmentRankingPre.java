package br.com.gestorflex.ui.fragment.fragments_ranking_filtro;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.ListarRanking;
import br.com.gestorflex.model.Ranking;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.activity.RankingResultadoActivity;
import br.com.gestorflex.ui.adapter.RankingAdapter;


public class FragmentRankingPre extends Fragment {


    private RecyclerView _rvRanking;
    private TextView _tvDataRanking;
    private TextView _tvUFRanking;
    private TextView _tvDDDRanking;
    private TextView _tvListarPorRanking;
    private TextView _tvProdutoRanking;
    private TextView _tvComSemRegargaRanking;

    private RankingResultadoActivity _actRankingResultado;

    private List<Ranking> ranking;
    private ListarRanking listarRanking;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        _actRankingResultado = (RankingResultadoActivity) context;
    }

    public FragmentRankingPre() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ranking_pre, container, false);
        instanciarViews(view);
        return view;
    }

    private void instanciarViews(View view) {

        _rvRanking = view.findViewById(R.id.rv_resultado_ranking);
        _tvDataRanking = view.findViewById(R.id.tv_data_ranking);
        _tvUFRanking = view.findViewById(R.id.tv_uf_ranking);
        _tvDDDRanking = view.findViewById(R.id.tv_ddd_ranking);
        _tvListarPorRanking = view.findViewById(R.id.tv_listarpor_ranking);
        _tvProdutoRanking = view.findViewById(R.id.tv_produto_ranking);
        _tvComSemRegargaRanking = view.findViewById((R.id.tv_comsemrecarga_ranking));
        configurarViews();
    }


    private void configurarViews() {

//        listarRanking = _actRankingResultado.getListarRanking();
//        ranking = _actRankingResultado.getRanking();

        RankingAdapter adapter = new RankingAdapter(getActivity(), ranking);
        _rvRanking.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        _rvRanking.setAdapter(adapter);

        _tvUFRanking.setText("- " + listarRanking.getCdEstado());
        _tvDataRanking.setText("- " + listarRanking.getFgPeriodo());
        _tvDDDRanking.setText("- DDD:" + listarRanking.getNrDDD());
        _tvListarPorRanking.setText("- " + listarRanking.getFgListarPor());
        _tvProdutoRanking.setText("- Produto:" + listarRanking.getProduto());
        _tvComSemRegargaRanking.setText("- Tipo:" + listarRanking.getTipo());
    }
}
