package br.com.gestorflex.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Aparelho;
import br.com.gestorflex.ui.fragment.fragments_menu.FragmentMenuDashboard;
import br.com.gestorflex.ui.fragment.fragments_menu.FragmentMenuOutros;
import br.com.gestorflex.ui.fragment.fragments_menu.FragmentMenuVenda;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.EmailUtil;
import br.com.gestorflex.util.ErrorUtil;

public class MenuActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String TAG_TITLE = "title";

    private BottomNavigationView _navigation;
    private MenuItem _menuSearch;
    private Fragment _fragmentCurrent;
    private SearchView _searchView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_sair_sessao)
                .setPositiveButton(R.string.label_alert_button_sim, (dialog, which) -> MenuActivity.this.finish())
                .setNegativeButton(R.string.label_alert_button_nao, null).show();
    }


    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            _toolbar.setSubtitle(UsuarioCTRL.getUsuario(this).getMatriculaNome());
            setSupportActionBar(_toolbar);
            _navigation = findViewById(R.id.navigation);
            _navigation.setOnNavigationItemSelectedListener(this);
            _navigation.setSelectedItemId(R.id.navigation_dash);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_info:
//                new AlertDialog.Builder(this)
//                        .setTitle(R.string.title_alert_aviso)
//                        .setMessage(msgInfo)
//                        .setPositiveButton(R.string.label_alert_button_ok, null)
//                        .show();
//                return true;
            case R.id.menu_autenticar:
                irAutenticar();
                return true;
            case R.id.menu_manual:
                baixarManual();
                return true;
//            case R.id.menu_trocar_senha:
//                Intent intent = new Intent(this, TrocarSenhaActivity.class);
//                intent.putExtra("isSenhaPadrao", isSenhaPadrao);
//                startActivity(intent);
//                return true;
//            case R.id.menu_meus_dados:
//                return true;


            case R.id.menu_sair:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openFragment(Fragment fragment) {
        _fragmentCurrent = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_menu, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void irAutenticar() {
        new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso)
                .setMessage(R.string.message_alert_autenticar)
                .setPositiveButton(R.string.label_alert_button_sim, (dialogInterface, i) -> {
                    DBHelper.clearDatabase(MenuActivity.this);
                    deleteAllPreferences();
                    setPreferencesUrl(getResources().getString(R.string.url_producao));
                    startActivity(new Intent(MenuActivity.this, AutenticacaoActivity.class));
                    MenuActivity.this.finish();
                })
                .setNegativeButton(R.string.label_alert_button_nao, null).show();
    }

    private void baixarManual() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
            String nomeManual = getResources().getString(R.string.manual_name);
            String nomeVersao = Aparelho.getVersao().replace(".", "");
            Uri uri = Uri.parse(nomeManual + ".pdf");
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
            Toast.makeText(this, R.string.message_snackbar_baixando_manual, Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(this, R.string.message_snackbar_manual_sem_internet, Toast.LENGTH_SHORT).show();
    }

    public void deleteAllPreferences() {
        GenericCTRL.deleteSharedPreferences(this, OdomentroActivity.JORNADA_INICIADA);
        GenericCTRL.deleteSharedPreferences(this, "isHabiliteVenda");
        GenericCTRL.deleteSharedPreferences(this, "isLiberacao");
        GenericCTRL.deleteSharedPreferences(this, EmailUtil.SENHA_EMAIL_SUPORTE);
        GenericCTRL.deleteSharedPreferences(this, "situacao");
        GenericCTRL.deleteSharedPreferences(this, JornadaMenuActivity.PREFERENCIA_CHECKOUT);
    }

    private void setPreferencesUrl(String url) {
        SharedPreferences sharedPreferences = getSharedPreferences(AutenticacaoActivity.URL_CONEXAO, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("url", url);
        editor.apply();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_dash:
                Fragment fragmentDashboard = new FragmentMenuDashboard();
                openFragment(fragmentDashboard);
//                    msgInfo = getString(R.string.info_activity_menu_dashboard);
                if (_menuSearch != null) {
                    _menuSearch.setVisible(false);
                    _menuSearch.collapseActionView();
                }
                return true;
            case R.id.navigation_venda:
                Fragment fragmentVenda = new FragmentMenuVenda();
                openFragment(fragmentVenda);
//                    msgInfo = getString(R.string.info_activity_menu_venda);
                if (_menuSearch != null) {
                    _menuSearch.setVisible(false);
                    _menuSearch.collapseActionView();
                }
                return true;
            case R.id.navigation_outros:
                Fragment fragmentOutros = new FragmentMenuOutros();
                openFragment(fragmentOutros);
//                    msgInfo = getString(R.string.info_activity_menu_outros);
                if (_menuSearch != null) {
                    _menuSearch.setVisible(false);
                    _menuSearch.collapseActionView();
                }
                return true;
        }
        return false;
    }
}
