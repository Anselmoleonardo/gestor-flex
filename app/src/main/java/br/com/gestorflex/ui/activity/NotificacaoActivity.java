package br.com.gestorflex.ui.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;

import com.android.volley.Response;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.database.internal.controller.NotificacaoCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.Notificacao;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.notificacao.MyFirebaseMessagingService;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;
import br.com.gestorflex.util.ImageUtil;

import static br.com.gestorflex.util.GeolocalizacaoUtil.verificarInternet;

public class NotificacaoActivity extends AppCompatActivity {

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;

    private Button btnSelecionarTodos_notificacao;
    private Button btnExcluir_notificacao;
    private TextView tvQteNovas_notificacao;
    private TextView tvQteTotal_notificacao;
    private LinearLayout llMensagem_notificacao;

    private int contTotal = 0;
    private int contNovas = 0;
    private int contExcluir = 0;
    private String caminhoArquivo;
    private String titulo;

    private Typeface fonteGeral = null;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressBar;

    private Response.Listener<String> responseListenerExc;
    private Response.ErrorListener errorListenerExcl;

    private Notificacao notificacao;

    private ProgressDialog progressDialogDownload;
    private AsyncTask<String, String, String> taskDownload;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notificacao);
        instanciarObjetos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (progressDialogDownload != null && progressDialogDownload.isShowing())
            progressDialogDownload.dismiss(); // Fechando pois por algum bug ele fica aparencendo quando volta para a tela
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_info_noti, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_info:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_alert_aviso)
                        .setMessage(R.string.info_activity_notificacao)
                        .setPositiveButton(R.string.label_alert_button_ok, null)
                        .show();
                return true;
            case R.id.menu_filter:
                configurarAlertDialogFiltroNotificacao();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private void instanciarObjetos() {
        btnSelecionarTodos_notificacao = findViewById(R.id.btnSelecionarTodos_notificacao);
        btnExcluir_notificacao = findViewById(R.id.btnExcluir_notificacao);
        tvQteNovas_notificacao = findViewById(R.id.tvQteNovas_notificacao);
        tvQteTotal_notificacao = findViewById(R.id.tvQteTotal_notificacao);
        llMensagem_notificacao = findViewById(R.id.llMensagem_notificacao);
        inicializarDados();
    }

    private void inicializarDados() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(_toolbar);
            titulo = getIntent().getStringExtra(MenuActivity.TAG_TITLE);
            getSupportActionBar().setTitle(titulo);
            Usuario usuario = UsuarioCTRL.getUsuario(this);
            _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
            fonteGeral = ResourcesCompat.getFont(this, R.font.fontawesome);
            btnSelecionarTodos_notificacao.setTypeface(fonteGeral);
            btnExcluir_notificacao.setTypeface(fonteGeral);
            setupVolleyListeners();
            setupVolleyListenersExcluir();
            //   Cursor cursor = NotificacaoCTRL.getCursorNotificacao(this, -1);
            buscarNotificacao();
//            if (cursor.getCount() > 0) {
//                buscarNotificacao();
//            } else {
//                Log.e("entrou aquiii ", "" + cursor.getCount());
//               // filtrarNotificacoes(15);
//            }

        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private void configurarAlertDialogFiltroNotificacao() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View _alertDenunciaView = inflater.inflate(R.layout.layout_filtro_dialog_notificacao, null);
        AppCompatSpinner _spPeriodo = _alertDenunciaView.findViewById(R.id.sp_filtro_minhas_publicacoes_periodo);
        preencherSpinnerPeriodo(_spPeriodo);
        // Cria Alert Dialog
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Filtrar por")
                .setView(_alertDenunciaView)
                .setPositiveButton("Filtrar", (dialog, which) -> {
                    int numeroDias = -1;
                    switch (_spPeriodo.getSelectedItemPosition()) {
                        case 0:
                            numeroDias = -1;
                            break;
                        case 1:
                            numeroDias = 0;
                            break;
                        case 2:
                            numeroDias = 1;
                            break;
                        case 3:
                            numeroDias = 3;
                            break;
                        case 4:
                            numeroDias = 7;
                            break;
                        case 5:
                            numeroDias = 15;
                            break;
                        case 6:
                            numeroDias = 30;
                            break;
                        case 7:
                            numeroDias = 45;
                            break;
                    }
                    try {
                        Log.e("nr diaaas ", numeroDias + "");
                        filtrarNotificacoes(numeroDias);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .setNegativeButton(R.string.label_alert_button_cancelar, null)
                .create();
        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setAllCaps(false);
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setAllCaps(false);
    }

    public void preencherSpinnerPeriodo(AppCompatSpinner _spPeriodo) {
        String[] periodos = {getString(R.string.text_todos), getString(R.string.text_hoje), getString(R.string.text_ate_ontem), getString(R.string.text_ultimos_tres_dias), getString(R.string.text_ultimos_sete_dias),
                getString(R.string.text_ultimos_quinze_dias), getString(R.string.text_ultimos_trinta_dias), getString(R.string.text_ultimos_quarenta_cinco__dias)};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, periodos);
        _spPeriodo.setAdapter(adapter);
    }

    private void filtrarNotificacoes(int numeroDias) throws Exception {
        ErrorUtil.montarCaminhoErro();
        if (progressBar != null) {
            if (!progressBar.isShowing())
                progressBar.show();
        }
        JSONObject logParams = new JSONObject();
        logParams.put("nrDIAS", numeroDias);
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_listar_notificacao), responseListener, errorListener, null);
    }


    private void excluirPublicações(int idNotificacao) throws Exception {
        ErrorUtil.montarCaminhoErro();
        JSONObject logParams = new JSONObject();
        logParams.put("idNotificacao", idNotificacao);
        ExternalDB.addDbRequest(this, logParams, "usp_v_APPExcluirNotificacao", responseListenerExc, errorListenerExcl, null);
    }

    //VOLLEY

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressBar = new ProgressDialog(this);
        progressBar.setIndeterminate(true);
        progressBar.setMessage("Buscando notificações...");
        progressBar.setCancelable(false);
        responseListener = response -> {
            try {
                JSONObject jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
                    JSONArray arrayNotificacao = jsonRetorno.optJSONArray("notificacoes");
                    if (arrayNotificacao != null) {
                        GenericCTRL.deleteAll(this, Notificacao.TABELA);
                        for (int i = 0; i < arrayNotificacao.length(); i++) {
                            Notificacao notificacao = new Notificacao(arrayNotificacao.getJSONObject(i));
                            GenericCTRL.save(NotificacaoActivity.this, Notificacao.TABELA, notificacao.getContetValues());
                        }
//                        Intent intent = new Intent(getBaseContext(), NotificacaoActivity.class);
//                        intent.putExtra("isSenhaPadrao", false);
//                        intent.putExtra("isFromLogin", false);
//                        intent.putExtra("isNotify", true);
//                        intent.putExtra(MenuActivity.TAG_TITLE, titulo);
//                        startActivity(intent);
//                        finish();
                        buscarNotificacao();
                    }
                } else {
                    new AlertDialog.Builder(NotificacaoActivity.this)
                            .setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString("msgRetorno"))
                            .setPositiveButton(R.string.label_alert_button_ok, null)
                            .show();
                }
                progressBar.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(NotificacaoActivity.this, e);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        errorListener = error -> {
            progressBar.dismiss();
            new AlertDialog.Builder(NotificacaoActivity.this)
                    .setTitle(R.string.title_alert_aviso)
                    .setMessage("Ocorreu uma FALHA na conexão!")
                    .setPositiveButton(R.string.label_alert_button_ok, null)
                    .show();
        };
    }

    private void setupVolleyListenersExcluir() {
        ErrorUtil.montarCaminhoErro();
        responseListenerExc = response -> {
            try {
                JSONObject jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray("retorno");
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt("cdRetorno") == ExternalDB.CD_RETORNO_SUCESSO) {
                    Log.e("sucesso exc", "Sucesso ao excluir do Servidor");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(NotificacaoActivity.this, e);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
        errorListenerExcl = error -> {
            progressBar.dismiss();
            Log.e("erro exc", "Erro ao excluir do Servidor");
        };
    }

    private void buscarNotificacao() throws Exception {
        ErrorUtil.montarCaminhoErro();
        try {
            // BUSCA AS MENSAGENS SALVAS NO APP
            Cursor cursor = NotificacaoCTRL.getCursorNotificacao(this, -1);
            contTotal = cursor.getCount();
            List<Notificacao> notificacaoList = NotificacaoCTRL.getList(this);
            contNovas = 0;
            llMensagem_notificacao.removeAllViews();
            if (contTotal > 0) {
                btnSelecionarTodos_notificacao.setVisibility(View.VISIBLE);
                btnExcluir_notificacao.setVisibility(View.VISIBLE);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Notificacao notificacao = new Notificacao(cursor);
                    boolean isNova = notificacao.getFlagNova() == 1;
                    if (isNova)
                        contNovas++;
                    montarNotificacao(notificacao);
                    cursor.moveToNext();
                }

            } else {
                btnSelecionarTodos_notificacao.setVisibility(View.INVISIBLE);
                btnExcluir_notificacao.setVisibility(View.INVISIBLE);
            }
            tvQteNovas_notificacao.setText(notificacaoList.size() + "");
            tvQteTotal_notificacao.setText("/" + contTotal);
        } catch (Exception ex) {
            ErrorUtil.enviarErroPorEmail(this, ex);
            throw ex;
        }
    }



    private void montarNotificacao(Notificacao notificacao) {
        ErrorUtil.montarCaminhoErro();
        try {
            // LINEAR LAYOUT QUE CONTERÁ OS DEMAIS LINEAR LAYOUTS
            LinearLayout ll = new LinearLayout(this);
            ll.setTag(notificacao.getId() + "");
            View.inflate(this, R.layout.item_notificacao, ll);
            LinearLayout.LayoutParams lllp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lllp.bottomMargin = 10;
            ll.setLayoutParams(lllp);
            inserirMensagem(notificacao, ll);
            // SE FOR IMAGEM
            if (notificacao.getTipo() == 1)
                inserirImagem(ll, notificacao);
            // SE FOR VIDEO
            if (notificacao.getTipo() == 2)
                inserirVideo(ll, notificacao);
            llMensagem_notificacao.addView(ll);
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private void inserirMensagem(final Notificacao notificacao, final LinearLayout ll) {
        LinearLayout llMensagemImagem_mn = ll.findViewById(R.id.llMensagemImagem_mn);
        LinearLayout llCor_mn = ll.findViewById(R.id.llCor_mn);
        llCor_mn.playSoundEffect(android.view.SoundEffectConstants.CLICK);
        if (notificacao.getFlagLido() == 0 && notificacao.getFlagNova() == 1) {
            llCor_mn.setBackgroundColor(Color.rgb(0, 170, 0));
        } else
            llCor_mn.setBackgroundColor(getResources().getColor(R.color.bloqueio));
        TextView tvExcluir_mn = llCor_mn.findViewById(R.id.tvExcluir_mn);
        tvExcluir_mn.setTypeface(fonteGeral);

        LinearLayout llConteudo_mn = ll.findViewById(R.id.llConteudo_mn);
        llCor_mn.setOnClickListener(v -> {
            String tag = llCor_mn.getTag().toString();
            if (tag.equals("0"))
                tag = "1";
            else
                tag = "0";
            llCor_mn.setTag(tag);
            selecionarNotificacao(tvExcluir_mn, llConteudo_mn, tag);
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(50);
        });
        LinearLayout llIDTitulo_mn = ll.findViewById(R.id.llIDTitulo_mn);

        TextView tvID_mn = ll.findViewById(R.id.tvID_mn);
        tvID_mn.setText("#" + notificacao.getId());

        TextView tvDatetime_mn = ll.findViewById(R.id.tvDatetime_mn);
        tvDatetime_mn.setText(notificacao.getDataHora());

        TextView tvTitulo_mn = ll.findViewById(R.id.tvTitulo_mn);

        if (notificacao.getTitulo().contains("|")) {
            String titulo = String.valueOf(notificacao.getTitulo());
            String[] titNotificacao = titulo.split("\\|", 2);
            tvTitulo_mn.setText(titNotificacao[1]);
        } else
            tvTitulo_mn.setText(notificacao.getTitulo());

        TextView tvSeta_mn = ll.findViewById(R.id.tvSeta_mn);
        tvSeta_mn.setTypeface(fonteGeral);

        TextView tvMensagem_mn = ll.findViewById(R.id.tvMensagem_mn);
        tvMensagem_mn.setText(notificacao.getMensagem());

        llIDTitulo_mn.setOnClickListener(v -> exibirConteudo(llMensagemImagem_mn, tvSeta_mn, llCor_mn,
                notificacao.getId()));
    }

    private void exibirConteudo(LinearLayout llMensagemImagem_mn, TextView tvSeta_mn, LinearLayout llCor_mn, int id) {
        ErrorUtil.montarCaminhoErro();
        try {
            int isVisible = llMensagemImagem_mn.getVisibility();
            if (isVisible == View.VISIBLE) {
                llMensagemImagem_mn.setVisibility(View.GONE);
                tvSeta_mn.setText(getResources()
                        .getString(R.string.icone_baixo));
            } else {
                llMensagemImagem_mn.setVisibility(View.VISIBLE);
                tvSeta_mn.setText(getResources().getString(R.string.icone_cima));
                Notificacao notificacao = NotificacaoCTRL.getNotificacao(this, id);
                this.notificacao = notificacao;
                if (notificacao.getFlagNova() == 1) {
                    llCor_mn.setBackgroundColor(getResources().getColor(R.color.bloqueio));
                    NotificacaoCTRL.desatualizar(this, id);
                    if (verificarInternet(this, false)) {
                        if (notificacao.getTitulo().contains("|")) {
                            String titulo = notificacao.getTitulo();
                            String[] titNotificacao = titulo.split("\\|", 2);
                            informarLeituraNotificacao(id, Integer.parseInt(titNotificacao[0]));
                        } else
                            informarLeituraNotificacao(id, 0);
                    } else {
                        NotificacaoCTRL.setSync(this, id, 0);
                        NotificacaoCTRL.updateNotificacaoLida(this, 1, id);
                    }
                    List<Notificacao> notificacaoList = NotificacaoCTRL.getList(this);
                    if (MyFirebaseMessagingService.notificationManager != null)
                        MyFirebaseMessagingService.notificationManager.cancelAll();
                    if (contNovas > 0)
                        contNovas--;
                    tvQteNovas_notificacao.setText(notificacaoList.size() + "");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private void informarLeituraNotificacao(int id, int isLocalizacao) {
        ErrorUtil.montarCaminhoErro();
        try {
            if (verificarInternet(this, false)) {
                if (isLocalizacao == 1) {
                    if (!GeolocalizacaoUtil.verificarGPSHabilitado(this)) return;
                    LatLng latLngAtual = Geolocalizacao.coletarPosicaoAtual();
                    if (latLngAtual != null) {
                        String endereco = GeolocalizacaoUtil.coletarEndereco(this, latLngAtual);
                        String latitude = String.valueOf(latLngAtual.latitude);
                        String longitude = String.valueOf(latLngAtual.longitude);
                        enviarNotificacao(id, endereco, latitude, longitude);
                    }
                } else {
                    enviarNotificacao(id);
                }
                NotificacaoCTRL.setSync(this, id, 1);
            } else {
                NotificacaoCTRL.setSync(this, id, 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private void selecionarNotificacao(final TextView tvExcluir_mn, final LinearLayout llConteudo_mn, String tag) {
        if (tag.equals("0")) {
            tvExcluir_mn.setVisibility(View.GONE);
            llConteudo_mn.setBackgroundColor(Color.TRANSPARENT);
            if (contExcluir > 0)
                contExcluir--;
        } else {
            tvExcluir_mn.setVisibility(View.VISIBLE);
            llConteudo_mn.setBackgroundColor(Color.rgb(230, 230, 230));
            contExcluir++;
        }
    }

    private void inserirImagem(LinearLayout ll, final Notificacao notificacao) {
        LinearLayout llImg_mn = ll.findViewById(R.id.llImg_mn);
        llImg_mn.setVisibility(View.VISIBLE);
        llImg_mn.setOnClickListener(v -> {
            if (notificacao.getCaminhoArquivo() != null && !notificacao.getCaminhoArquivo().isEmpty())
                abrirImagem(notificacao.getCaminhoArquivo());
            else
                ImageUtil.exibirImagem(NotificacaoActivity.this, notificacao.getUrl());
        });
        AppCompatImageView iv = ll.findViewById(R.id.ivImg_mn);
        if (notificacao.getCaminhoArquivo() != null && !notificacao.getCaminhoArquivo().isEmpty()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap bmp = BitmapFactory.decodeByteArray(notificacao.getImagem(),
                    0, notificacao.getImagem().length, options);
            iv.setImageBitmap(bmp);
        } else
            ImageUtil.adicionarImagemEmImageView(NotificacaoActivity.this, iv, notificacao.getUrl());
        iv.setAdjustViewBounds(true);
    }

    protected void abrirImagem(String caminhoCompleto) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(caminhoCompleto), "image/*");
        startActivity(intent);
    }

    private void inserirVideo(LinearLayout ll, final Notificacao notificacao) {
        LinearLayout llVideo_mn = ll.findViewById(R.id.llVideo_mn);
        llVideo_mn.setVisibility(View.VISIBLE);
        final Button btnVideo_mn = ll.findViewById(R.id.btnVideo_mn);
        btnVideo_mn.setTypeface(fonteGeral);
        btnVideo_mn.setTextColor(Color.WHITE);
        final TextView tvPercentual_mn = ll.findViewById(R.id.tvPercentual_mn);
        tvPercentual_mn.setVisibility(View.VISIBLE);
        if (notificacao.getCaminhoArquivo() == null) notificacao.setCaminhoArquivo("");
        final File file = new File(notificacao.getCaminhoArquivo());
        if (file.exists())
            btnVideo_mn.setText(getResources().getString(R.string.icone_video));
        else {
            btnVideo_mn.setText(getResources().getString(R.string.icone_download));
            notificacao.setCaminhoArquivo("");
            notificacao.setFlagCompleto(0);
            GenericCTRL.update(NotificacaoActivity.this, Notificacao.TABELA, Notificacao.CAMPO_ID + " = ?",
                    new String[]{notificacao.getId() + ""}, notificacao.getContetValues());
        }
        btnVideo_mn.setOnClickListener(v -> {
            if (notificacao.getFlagCompleto() == 1)
                abrirVideo(notificacao.getCaminhoArquivo());
            else
                taskDownload = new DownloadFileAsync(btnVideo_mn, notificacao).execute();
        });
    }

    private boolean abrirVideo(String caminhoCompleto) {
        ErrorUtil.montarCaminhoErro();
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(caminhoCompleto));
            intent.setDataAndType(Uri.parse(caminhoCompleto), "video/mp4");
            startActivity(intent);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
        return false;
    }

    public void verificarSelecionados() throws Exception {
        ErrorUtil.montarCaminhoErro();
        try {
            boolean isResetar = false;
            for (int x = 0; x < llMensagem_notificacao.getChildCount(); x++) {
                LinearLayout ll = (LinearLayout) llMensagem_notificacao
                        .getChildAt(x);
                String tag = ll.getTag().toString();
                int id = Integer.parseInt(tag);
                LinearLayout ll1 = (LinearLayout) ll.getChildAt(0);
                LinearLayout ll2 = (LinearLayout) ll1.getChildAt(0);
                if (ll2.getTag().equals("1")) {
                    Log.e("excluir noti 2", "não tá entrando aquiii ");
                    isResetar = excluirNotificacao(id, ll);
                    x--;
                }
            }
            if (isResetar) {
                Intent intent = new Intent(getBaseContext(), NotificacaoActivity.class);
                intent.putExtra("isSenhaPadrao", false);
                intent.putExtra("isFromLogin", false);
                intent.putExtra("isNotify", true);
                intent.putExtra(MenuActivity.TAG_TITLE, titulo);
                startActivity(intent);
                finish();
            }
        } catch (Exception ex) {
            ErrorUtil.enviarErroPorEmail(this, ex);
            throw ex;
        }
    }

    public boolean excluirNotificacao(int id, LinearLayout ll) throws Exception {
        ErrorUtil.montarCaminhoErro();
        try {
            // SE A NOTIFICAÇÃO ERA NOVA, ATUALIZA A QUANTIDADE DE
            // NOVAS
            Notificacao notificacao = NotificacaoCTRL.getNotificacao(this, id);
            List<Notificacao> notificacaoList = NotificacaoCTRL.getList(this);
            if (notificacao.getFlagNova() == 1) {
                if (contNovas > 0)
                    contNovas--;
                tvQteNovas_notificacao.setText(notificacaoList.size() + "");
            }
            // EXCLUI ARQUIVOS
            if (notificacao.getTipo() > 0) {
                if (notificacao.getCaminhoArquivo() != null)
                    ImageUtil.excluirArquivo(notificacao.getCaminhoArquivo());
            }
            excluirPublicações(id);
            GenericCTRL.delete(this, Notificacao.TABELA, id);
            Log.e("excluir noti", "não tá entrando aquiii ");
            llMensagem_notificacao.removeView(ll);
            contTotal--;
            contExcluir--;
            // SE NÃO HOUVER MAIS NOTIFICAÇÃO, SAI DO LAÇO
            if (contTotal == 0)
                return true;
            tvQteTotal_notificacao.setText("/" + contTotal);
        } catch (Exception ex) {
            ErrorUtil.enviarErroPorEmail(this, ex);
            throw ex;
        }
        return false;
    }

    // MÉTODOS EXTERNOS
    public void btnSelecionarTodos_Click(View v) {
        ErrorUtil.montarCaminhoErro();
        try {
            contExcluir = 0;
            // VERIFICA QUANTAS NOTIFICAÇÃO NÃO ESTÃO SELECIONADAS.
            int contNaoSelecionados = 0;
            int contFilhos = llMensagem_notificacao.getChildCount();
            for (int x = 0; x < contFilhos; x++) {
                LinearLayout ll = (LinearLayout) llMensagem_notificacao.getChildAt(x);
                LinearLayout ll1 = (LinearLayout) ll.getChildAt(0);
                LinearLayout ll2 = (LinearLayout) ll1.getChildAt(0);
                String tag2 = ll2.getTag().toString();
                if (tag2.equals("0"))
                    contNaoSelecionados++;
            }
            // SELECIONA TODAS AS QUE NÃO ESTIVEREM, SENÃO, DESMARCA TODAS
            String tag = "0";
            if (contNaoSelecionados > 0)
                tag = "1";
            for (int x = 0; x < contFilhos; x++) {
                LinearLayout ll = (LinearLayout) llMensagem_notificacao
                        .getChildAt(x);
                LinearLayout ll1 = (LinearLayout) ll.getChildAt(0);
                LinearLayout ll2 = (LinearLayout) ll1.getChildAt(0);
                LinearLayout ll3 = (LinearLayout) ll1.getChildAt(1);

                TextView tv = (TextView) ll2.getChildAt(0);
                ll2.setTag(tag);
                selecionarNotificacao(tv, ll3, tag);
            }
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(50);
        } catch (Exception ex) {
            ErrorUtil.enviarErroPorEmail(this, ex);
            ex.printStackTrace();
        }
    }

    public void btnExcluir_Click(View v) {
        ErrorUtil.montarCaminhoErro();
        try {
            if (contExcluir == 0)
                Toast.makeText(this, "Não há notificações selecionadas!",
                        Toast.LENGTH_SHORT).show();
            else
                new androidx.appcompat.app.AlertDialog.Builder(NotificacaoActivity.this)
                        .setTitle("Aviso")
                        .setMessage("Deseja realmente excluir as notificações selecionadas?")
                        .setPositiveButton("OK", (dialog, which) -> {
                            try {
                                verificarSelecionados();
                                Snackbar.make(llMensagem_notificacao, "Notificações excluídas com sucesso!", Snackbar.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        })
                        .show();
        } catch (Exception ex) {
            ex.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, ex);
        }
    }

    private void enviarNotificacao(int id, String endereco, String latitude, String longitude) throws Exception {
        ErrorUtil.montarCaminhoErro();
        notificacao = new Notificacao();
        notificacao.setId(id);
        final JSONObject logParams = notificacao.getLog(endereco, latitude, longitude);
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_informar_notificacao), responseListener, errorListener, null);
    }

    private void enviarNotificacao(int id) throws Exception {
        ErrorUtil.montarCaminhoErro();
        notificacao = new Notificacao();
        notificacao.setId(id);
        final JSONObject logParams = notificacao.getLog("", "", "");
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_informar_notificacao), responseListener, errorListener, null);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                progressDialogDownload = new ProgressDialog(this);
                progressDialogDownload.setMessage("Baixando arquivo");
                progressDialogDownload.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialogDownload.setCancelable(false);
                progressDialogDownload.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", (dialog, which) -> {
                    try {
                        taskDownload.cancel(true);
                        ImageUtil.excluirArquivo(caminhoArquivo);
                        progressDialogDownload.setProgress(0);
                        Snackbar.make(llMensagem_notificacao, "Download cancelado com sucesso!", Snackbar.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                progressDialogDownload.show();
                return progressDialogDownload;
            default:
                return null;
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {

        private Button btnVideo_mn;
        private Notificacao notificacaoAsync;

        public DownloadFileAsync(Button btnVideo_mn, Notificacao notificacaoAsync) {
            this.btnVideo_mn = btnVideo_mn;
            this.notificacaoAsync = notificacaoAsync;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... aurl) {
            URL u;
            InputStream is = null;
            try {
                u = new URL(notificacaoAsync.getUrl());
                is = u.openStream();
                HttpURLConnection huc = (HttpURLConnection) u.openConnection(); //to know the size of video
                int lenghtOfFile = huc.getContentLength();
                if (huc != null) {
                    File video = null;
                    File storageDir = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        storageDir = NotificacaoActivity.this.getExternalFilesDir(Environment.DIRECTORY_MOVIES);
                        video = File.createTempFile(notificacaoAsync.getId() + "_notificacao", "_video.mp4", storageDir);
                        caminhoArquivo = video.getAbsolutePath();
                    } else {
                        storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
                        caminhoArquivo = storageDir.getAbsolutePath() + "/" + notificacaoAsync.getId() + "_notificacao_video.mp4";
                        video = new File(caminhoArquivo);
                    }
                    FileOutputStream fos = new FileOutputStream(video);
                    byte[] buffer = new byte[1024];
                    long total = 0;
                    int len1 = 0;
                    if (is != null) {
                        while ((len1 = is.read(buffer)) > 0) {
                            total += len1;
                            publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                            fos.write(buffer, 0, len1);
                        }
                    }
                    if (fos != null)
                        fos.close();
                }
            } catch (MalformedURLException mue) {
                mue.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                try {
                    if (is != null)
                        is.close();
                } catch (IOException ioe) {
                    // just going to ignore this one
                }
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            Log.d("ANDRO_ASYNC", progress[0]);
            progressDialogDownload.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String unused) {
            progressDialogDownload.setProgress(0);
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
            notificacaoAsync.setCaminhoArquivo(caminhoArquivo);
            notificacaoAsync.setFlagCompleto(1);
            GenericCTRL.update(NotificacaoActivity.this, Notificacao.TABELA, Notificacao.CAMPO_ID + " = ?",
                    new String[]{notificacaoAsync.getId() + ""}, notificacaoAsync.getContetValues());
            btnVideo_mn.setText(getResources().getString(R.string.icone_video));
            Snackbar.make(llMensagem_notificacao, "Vídeo baixado com sucesso!", Snackbar.LENGTH_SHORT).show();
        }
    }
}
