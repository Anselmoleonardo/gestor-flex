package br.com.gestorflex.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.AgenciaCTRL;
import br.com.gestorflex.database.internal.controller.DDDCTRL;
import br.com.gestorflex.database.internal.controller.EstadoCTRL;
import br.com.gestorflex.database.internal.controller.ListarPorCTRL;
import br.com.gestorflex.database.internal.controller.ProdutoCTRL;
import br.com.gestorflex.database.internal.controller.RegionalCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Agencia;
import br.com.gestorflex.model.DDD;
import br.com.gestorflex.model.Estado;
import br.com.gestorflex.model.ListarPor;
import br.com.gestorflex.model.ListarRanking;
import br.com.gestorflex.model.Produto;
import br.com.gestorflex.model.Ranking;
import br.com.gestorflex.model.Regional;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;

public class RankingFiltroActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {


    // VIEWS
    private Toolbar _tbRankingFiltro;
    private AppCompatSpinner _spRegionalRanking;
    private AppCompatSpinner _spParceiroRanking;
    private AppCompatSpinner _spDDD_ranking;
    private AppCompatSpinner _spListarPor_ranking;
    private AppCompatSpinner _spAno_ranking;
    private AppCompatSpinner _spMesRanking;
    private LinearLayoutCompat _llTipo_ranking;
    private LinearLayoutCompat _llTipoControle_ranking;
    private LinearLayoutCompat _llDDD;

    private AppCompatSpinner _spProduto_ranking;
    private AppCompatSpinner _spTipo_ranking;
    private AppCompatSpinner _spTipoControle_ranking;
    private AppCompatButton _btnPesquisar_ranking;

    private AppCompatTextView tvEstado;

    private AppCompatSpinner _spEstado;
    private AppCompatSpinner _spDiaInicial;
    private AppCompatSpinner _spDiaFinal;
    private AppCompatSpinner _spMes;
    private AppCompatSpinner _spAno;

    private FloatingActionButton _fabPesquisar;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    private Usuario usuario;
    private String dataInicial;
    private String dataFinal;

    ListarRanking listarRanking = new ListarRanking();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking_filtro);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            _tbRankingFiltro = findViewById(R.id.toolbar);

            _spRegionalRanking = findViewById(R.id.sp_filtrar_ranking_regional);
            _spParceiroRanking = findViewById(R.id.sp_filtrar_ranking_parceiro);

            _spListarPor_ranking = findViewById(R.id.sp_filtrar_ranking_listarpor);

            _spProduto_ranking = findViewById(R.id.sp_filtrar_ranking_produto);
            _spTipo_ranking = findViewById(R.id.sp_filtrar_ranking_tipo);
            _spTipoControle_ranking = findViewById(R.id.sp_filtrar_ranking_tipo_controle);
            _spDDD_ranking = findViewById(R.id.sp_filtrar_ranking_ddd);

            _llTipo_ranking = findViewById(R.id.ll_filtrar_ranking_tipo);
            _llTipoControle_ranking = findViewById(R.id.ll_filtrar_ranking_tipo_controle);
            _llDDD = findViewById(R.id.ll_filtrar_ranking_controle);

            _spEstado = findViewById(R.id.sp_filtrar_ranking_uf);
            _spDiaInicial = findViewById(R.id.sp_filtrar_ranking_dia_inicial);
            _spDiaFinal = findViewById(R.id.sp_filtrar_ranking_dia_final);
            _spMes = findViewById(R.id.sp_filtrar_ranking_mes);
            _spAno = findViewById(R.id.sp_filtrar_ranking_ano);
            _fabPesquisar = findViewById(R.id.fab_filtrar_ranking_pesquisar);
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() {
        usuario = UsuarioCTRL.getUsuario(this);
        _tbRankingFiltro.setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
        _tbRankingFiltro.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_tbRankingFiltro);
        preencherAgencia();
        preencherProduto();
        preencherRegional();
        popularSpTipo();
        popularSpTipoCTRL(_spTipoControle_ranking);
        preencherListarPor();
        preencherSpinnerUf();
        preencherSpinnerAno();
        preencherSpinnerMes();

        setupVolleyListeners();
        _fabPesquisar.setOnClickListener(this);
    }

    public void preencherAgencia() {
        List<Agencia> listAgencias = AgenciaCTRL.listAgencias(this);
        if (listAgencias.size() > 1)
            listAgencias.add(0, new Agencia(0, "TODOS"));
        ArrayAdapter<Agencia> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listAgencias);
        _spParceiroRanking.setAdapter(dataAdapter);
    }

    public void preencherRegional() {
        List<Regional> listRegional = RegionalCTRL.listRegional(this);
        if (listRegional.size() > 1)
            listRegional.add(0, new Regional(0, "TODOS", 0));
        ArrayAdapter<Regional> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listRegional);
        _spRegionalRanking.setAdapter(dataAdapter);
    }

    public void preencherSpinnerUf() {
        List<Estado> listEstados = EstadoCTRL.listEstados(this);
        if (listEstados.size() > 1)
            listEstados.add(0, new Estado(0, "TODOS"));
        ArrayAdapter<Estado> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listEstados);
        _spEstado.setAdapter(dataAdapter);
        _spEstado.setOnItemSelectedListener(this);
    }

    public void preencherListarPor() {
        List<ListarPor> listListarPor = ListarPorCTRL.listListarPor(this);
//        if(listListarPor.size() > 1)
//            listListarPor.add(0, new ListarPor(0 ,"TODOS"));
        ArrayAdapter<ListarPor> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listListarPor);
        _spListarPor_ranking.setAdapter(dataAdapter);

        _spListarPor_ranking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (listListarPor.get(i).getNrOrdem() != 1) {
                    _llDDD.setVisibility(View.VISIBLE);
                } else {
                    _llDDD.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }


    public void preencherSpinnerDDD(int idUF) {
        List<DDD> listDDDs = DDDCTRL.listDDDs(this, idUF); //
        if (listDDDs.size() > 1)
            listDDDs.add(0, new DDD(0, 0));
        ArrayAdapter<DDD> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listDDDs);
        _spDDD_ranking.setAdapter(dataAdapter);
    }

    public void preencherSpinnerDia(int ano, int mes) {
        List<String> dias = DateUtil.getTodosDiasDoMes(ano, mes);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, dias);
        _spDiaInicial.setAdapter(dataAdapter);
        _spDiaFinal.setAdapter(dataAdapter);
//        _spDiaInicial.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
//        _spDiaFinal.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
    }

    public void preencherSpinnerMes() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, DateUtil.getTodosMeses());
        _spMes.setAdapter(dataAdapter);
        Calendar calendarMes = Calendar.getInstance(TimeZone.getDefault());
        _spMes.setSelection(calendarMes.get(Calendar.MONTH));
        _spMes.setOnItemSelectedListener(this);
    }

    public void preencherSpinnerAno() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String[] gruposEcon = {(year - 1) + "", year + "", (year + 1) + ""};
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, gruposEcon);
        _spAno.setAdapter(dataAdapter);
        _spAno.setSelection(1);
        _spAno.setOnItemSelectedListener(this);
    }

    public void preencherProduto() {
        List<Produto> listProdutos = ProdutoCTRL.listProdutos(this);
        if (listProdutos.size() > 1)
            listProdutos.add(0, new Produto(0, "TODOS", 0));
        ArrayAdapter<Produto> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listProdutos);
        _spProduto_ranking.setAdapter(dataAdapter);
        _spProduto_ranking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1:
                        ajustarLayoutPreSelecionado();
                        break;
                    case 2:
                        ajustarLayoutCTRLSelecionado();
                        break;
                    default:
                        ajustarLayoutWebSelecionado();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void popularSpTipo() {
        String[] arTipo = {"TODOS", "SEM RECARGA", "COM RECARGA", "PORTABILIDADE"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, arTipo);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        _spTipo_ranking.setAdapter(arrayAdapter);
    }

    private void popularSpTipoCTRL(Spinner spTipoControle_ranking) {
        //String produto = getResources().getString(R.string.tipo_controle);
        String[] arTipo = {"TODOS", "ATIVAÇÃO", "MIGRAÇÃO", "BOLETO", "FACÍL", "PORTABILIDADE"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, arTipo);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spTipoControle_ranking.setAdapter(arrayAdapter);
    }

    private void ajustarLayoutWebSelecionado() {
        _llTipo_ranking.setVisibility(View.GONE);
        _llTipoControle_ranking.setVisibility(View.GONE);
    }

    private void ajustarLayoutCTRLSelecionado() {
        _llTipo_ranking.setVisibility(View.GONE);
        _llTipoControle_ranking.setVisibility(View.VISIBLE);
    }

    private void ajustarLayoutPreSelecionado() {
        _llTipo_ranking.setVisibility(View.VISIBLE);
        _llTipoControle_ranking.setVisibility(View.GONE);
    }

    public boolean validarDatasSelecionadas() {
        String diaInicial = (String) _spDiaInicial.getSelectedItem();
        String diaFinal = (String) _spDiaFinal.getSelectedItem();
        String mes = (String) _spMes.getSelectedItem();
        String ano = (String) _spAno.getSelectedItem();
        if (Integer.parseInt(diaInicial) > Integer.parseInt(diaFinal)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_dia_inicial_maior)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        dataInicial = diaInicial + "/" + mes + "/" + ano;
        dataFinal = diaFinal + "/" + mes + "/" + ano;
        if (!DateUtil.validateDate(dataInicial)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_dia_atual_invalido)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        if (!DateUtil.validateDate(dataFinal)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_dia_final_invalido)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        return true;
    }

    public void recuperarRanking() throws Exception {
        if (!validarDatasSelecionadas()) return;
        progressDialog.show();
        Produto spProduto = (Produto) _spProduto_ranking.getSelectedItem();
        int produtoId = spProduto.getId();

        int tipo;
        switch (produtoId) {
            case 1:
                tipo = _spTipo_ranking.getSelectedItemPosition();
                break;
            case 2:
                tipo = _spTipoControle_ranking.getSelectedItemPosition();
                break;
            default:
                tipo = 0;
                break;
        }


        listarRanking.setNrDDD(((DDD) _spDDD_ranking.getSelectedItem()).getDescricao());
        listarRanking.setFgPeriodo(DateUtil.converterDataPadraoUs(dataInicial));
        listarRanking.setFgListarPor(((ListarPor) _spListarPor_ranking.getSelectedItem()).getNrOrdem());
        listarRanking.setNomeListarPor(((ListarPor) _spListarPor_ranking.getSelectedItem()).getDescricao());
        listarRanking.setProduto(produtoId);
        listarRanking.setCdEstado(((Estado) _spEstado.getSelectedItem()).getDescricao());
        listarRanking.setCdRegional(((Regional) _spRegionalRanking.getSelectedItem()).getId());
        listarRanking.setTipo(tipo);
        listarRanking.setTxData(DateUtil.getCurrentDateUS());
        listarRanking.setDataInicio(DateUtil.converterDataPadraoUs(dataInicial));
        listarRanking.setDataFim(DateUtil.converterDataPadraoUs(dataFinal));
        listarRanking.setEmail(false);
        listarRanking.setIdParceiro(((Regional) _spRegionalRanking.getSelectedItem()).getIdAgencia());

        JSONObject logParams = listarRanking.pegarJson();
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_ranking), responseListener, errorListener, null);
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_buscando_ranking));
        progressDialog.setCancelable(false);
        responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                        JSONArray jArrayRanking = jsonRetorno.getJSONArray("ranking");
                        if (jArrayRanking.length() > 0) {
                            List<String> lista = new ArrayList<>();
                            List<Ranking> rankings = new ArrayList<>();
                            Ranking ranking = new Ranking();
                            for (int i = 0; i < jArrayRanking.length(); i++) {
                                String dados = jArrayRanking.getJSONObject(i).optString("dados");
                                int isHeader = jArrayRanking.getJSONObject(i).optInt("isHeader");
                                if (isHeader == 1) {
                                    if (lista.size() > 0) {
                                        ranking.setValores(lista);
                                        rankings.add(ranking);
                                        lista = new ArrayList<>();
                                        ranking = new Ranking();
                                    }
                                    ranking.setTitulo(dados);
                                } else
                                    lista.add(dados);
                            }
                            if (lista.size() > 0) {
                                ranking.setValores(lista);
                                rankings.add(ranking);
                            }

                            Log.e("RANKING", "Titulo" + rankings.get(0).getTitulo());
                            Log.e("RANKING", "Dados" + rankings.get(0).getValores().size() + "");
                            Intent intent = new Intent(RankingFiltroActivity.this, RankingResultadoActivity.class);
                            intent.putExtra("listarRanking", (Serializable) listarRanking );
                            intent.putExtra("rankings", (Serializable) rankings);
//                            intent.putExtra("listarRanking", (Serializable) listarRanking);
                            startActivity(intent);
                        } else {
                            new AlertDialog.Builder(RankingFiltroActivity.this)
                                    .setTitle(R.string.title_alert_aviso)
                                    .setMessage(R.string.message_alert_sem_resultados)
                                    .setPositiveButton(R.string.label_alert_button_ok, null)
                                    .show();
                        }
                    } else {
                        new AlertDialog.Builder(RankingFiltroActivity.this)
                                .setTitle(R.string.title_alert_aviso)
                                .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                                .setPositiveButton(R.string.label_alert_button_ok, null)
                                .show();
                    }
                    progressDialog.dismiss();
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(RankingFiltroActivity.this, e);
                }
            }
        };
        errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(RankingFiltroActivity.this, "Erro ao conectar ao servidor", Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.sp_filtrar_ranking_uf:
                Estado estado = (Estado) _spEstado.getSelectedItem();
                preencherSpinnerDDD(estado.getId());
                break;
            default:
                preencherSpinnerDia(Integer.parseInt((String) _spAno.getSelectedItem()),
                        Integer.parseInt((String) _spMes.getSelectedItem()));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        try {
            recuperarRanking();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
