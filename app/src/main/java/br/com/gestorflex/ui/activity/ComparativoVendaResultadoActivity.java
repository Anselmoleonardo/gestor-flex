package br.com.gestorflex.ui.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.ComparativoVenda;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.ComparativoVendaAdapter;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.ErrorUtil;

public class ComparativoVendaResultadoActivity extends AppCompatActivity {

    public static final int DIARIO = 0;
    public static final int MENSAL = 1;
    public static final int ANUAL = 2;

    private AppCompatTextView _tvCabecalhoDataAntes;
    private AppCompatTextView _tvCabecalhoDataDepois;
    private RecyclerView _lvComparativoVendas;

    private int idPeriodo;
    private String dataSelecionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparativo_venda_resultado);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        Toolbar _toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(_toolbar);
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        _tvCabecalhoDataAntes = findViewById(R.id.tv_comparativo_cabecalho_data_antes);
        _tvCabecalhoDataDepois = findViewById(R.id.tv_comparativo_cabecalho_data_depois);
        _lvComparativoVendas = findViewById(R.id.rv_comparativo_venda);
        configurarViews();
    }

    private void configurarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            idPeriodo = getIntent().getIntExtra("idPeriodo", 2);
            dataSelecionada = getIntent().getStringExtra("dataSelecionada");

            exibirDatasCabecalho(idPeriodo, getData(dataSelecionada));
            int periodoVenda = buscarPeriodoVenda(idPeriodo, getData(dataSelecionada));

            JSONArray jsonResultados = new JSONArray(getIntent().getStringExtra("jArrayResultado"));
            List<ComparativoVenda> comparativoVendas = montarComparativoVenda(jsonResultados, periodoVenda);
            exibirVendasComparadas(comparativoVendas);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void exibirDatasCabecalho(int idPeriodo, Calendar data) {
        if (idPeriodo == DIARIO) {
            _tvCabecalhoDataDepois.setText(String.format(Locale.getDefault(), "%02d/%02d", data.get(Calendar.DAY_OF_MONTH), (data.get(Calendar.MONTH) + 1)));
            data.add(Calendar.DAY_OF_MONTH, -1);
            _tvCabecalhoDataAntes.setText(String.format(Locale.getDefault(), "%02d/%02d", data.get(Calendar.DAY_OF_MONTH), (data.get(Calendar.MONTH) + 1)));
        }
        if (idPeriodo == MENSAL) {
            _tvCabecalhoDataDepois.setText(String.format(Locale.getDefault(), "%02d/%d", (data.get(Calendar.MONTH) + 1), data.get(Calendar.YEAR)));
            data.add(Calendar.MONTH, -1);
            _tvCabecalhoDataAntes.setText(String.format(Locale.getDefault(), "%02d/%d", (data.get(Calendar.MONTH) + 1), data.get(Calendar.YEAR)));
        }
        if (idPeriodo == ANUAL) {
            _tvCabecalhoDataDepois.setText(String.format(Locale.getDefault(), "%d", data.get(Calendar.YEAR)));
            data.add(Calendar.YEAR, -1);
            _tvCabecalhoDataAntes.setText(String.format(Locale.getDefault(), "%d", data.get(Calendar.YEAR)));
        }
    }

    private int buscarPeriodoVenda(int idPeriodo, Calendar data) {
        ErrorUtil.montarCaminhoErro();
        int valorPeriodo = 0;
        try {
            if (idPeriodo == DIARIO)
                valorPeriodo = data.get(Calendar.DAY_OF_MONTH);
            if (idPeriodo == MENSAL)
                valorPeriodo = data.get(Calendar.MONTH) + 1;
            if (idPeriodo == ANUAL)
                valorPeriodo = data.get(Calendar.YEAR);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
        return valorPeriodo;
    }

    private List<ComparativoVenda> montarComparativoVenda(JSONArray jsonArray, int filtroPeriodo) throws JSONException {
        List<ComparativoVenda> listaVendasComparadas = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonVendaAntes = jsonArray.getJSONObject(i);
            String nomeListagemAntes = jsonVendaAntes.getString("listagem");
            int valorPeriodoAntes = jsonVendaAntes.getInt("periodo");
            int quantidadeAntes = jsonVendaAntes.getInt("quantidade");

            String nomeListagemDepois = "";
            int valorPeriodoDepois = 0;
            int quantidadeDepois = 0;
            if (i + 1 < jsonArray.length() && jsonArray.length() != 1) {
                JSONObject jsonVendaDepois = jsonArray.getJSONObject(i + 1);
                nomeListagemDepois = jsonVendaDepois.getString("listagem");
                valorPeriodoDepois = jsonVendaDepois.getInt("periodo");
                quantidadeDepois = jsonVendaDepois.getInt("quantidade");
            }

            ComparativoVenda vendaComparada;
            if (nomeListagemAntes.equals(nomeListagemDepois) && valorPeriodoAntes != valorPeriodoDepois) {
                vendaComparada = new ComparativoVenda(nomeListagemAntes, quantidadeAntes, quantidadeDepois);
                i++;
            } else {
                if (valorPeriodoAntes < valorPeriodoDepois || valorPeriodoAntes != filtroPeriodo) {
                    vendaComparada = new ComparativoVenda(nomeListagemAntes, quantidadeAntes, 0);
                } else {
                    vendaComparada = new ComparativoVenda(nomeListagemAntes, 0, quantidadeAntes);
                }
            }
            listaVendasComparadas.add(vendaComparada);
        }
        return listaVendasComparadas;
    }

    private void exibirVendasComparadas(List<ComparativoVenda> comparativoVendas) {
        ComparativoVendaAdapter adapter = new ComparativoVendaAdapter(this, comparativoVendas);
        _lvComparativoVendas.setAdapter(adapter);
    }

    public Calendar getData(String dataSelecionada) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formato.parse(dataSelecionada);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }
}
