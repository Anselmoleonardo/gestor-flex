package br.com.gestorflex.ui.activity;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.ListarRanking;
import br.com.gestorflex.model.Ranking;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.RankingAdapter;
import br.com.gestorflex.ui.adapter.ViewPagerAdapter;
import br.com.gestorflex.ui.fragment.fragments_ranking_filtro.FragmentRankingCtrl;
import br.com.gestorflex.ui.fragment.fragments_ranking_filtro.FragmentRankingPre;
import br.com.gestorflex.ui.fragment.fragments_ranking_filtro.FragmentRankingWeb;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;

public class RankingResultadoActivity extends AppCompatActivity {

//    private static final int TAB_PRE = 0;
//    private static final int TAB_CTRL = 1;
//    private static final int TAB_WEB = 2;

//    private ViewPager _vpRanking ;
//    private TabLayout _tlRanking ;
//
//    private FragmentRankingCtrl _fgRankingCtrl;
//    private FragmentRankingPre _fgRankingPre;
//    private FragmentRankingWeb _fgRankingWeb;
//    private ViewPagerAdapter adapter;

    private TextView _tvProdutividadeTotal;
    private RecyclerView _rvRanking;
    private TextView _tvDataInicialRanking;
    private TextView _tvDataFinalRanking;
    private TextView _tvUFRanking;
    private TextView _tvDDDRanking;
    private TextView _tvListarPorRanking;
    private TextView _tvProdutoRanking;
    private TextView _tvComSemRegargaRanking;

    private List<Ranking> rankings;
    private List<Ranking> newRanking;
    private ListarRanking listarRanking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking_resultado);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        Toolbar _toolbar = findViewById(R.id.toolbar);
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_toolbar);
        _rvRanking = findViewById(R.id.rv_resultado_ranking);
//        _tvProdutividadeTotal = findViewById(R.id.tv_produtividade_total);
//        _vpRanking = findViewById(R.id.viewpager) ;
//         _tlRanking = findViewById(R.id.tabs);

//        _rvRanking = findViewById(R.id.rv_resultado_ranking);
        _tvDataInicialRanking = findViewById(R.id.tv_data_inicial_ranking);
//        _tvDataFinalRanking = findViewById(R.id.tv_data_final_ranking);
//        _tvUFRanking = findViewById(R.id.tv_uf_ranking);
//        _tvDDDRanking = findViewById(R.id.tv_ddd_ranking);
        _tvListarPorRanking = findViewById(R.id.tv_listarpor_ranking);
//        _tvProdutoRanking = findViewById(R.id.tv_produto_ranking);
//        _tvComSemRegargaRanking = findViewById((R.id.tv_comsemrecarga_ranking));
        configurarViews();
    }

    private void configurarViews() {
        rankings = (List<Ranking>) getIntent().getSerializableExtra("rankings");
        listarRanking = (ListarRanking) getIntent().getSerializableExtra("listarRanking");
//        setupViewPager();


        RankingAdapter adapter = new RankingAdapter(this, rankings);
//        _rvRanking.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        _rvRanking.setAdapter(adapter);

        _tvListarPorRanking.setText("Listar por: " + listarRanking.getNomeListarPor());
//        _tvUFRanking.setText("- " + listarRanking.getCdEstado());
        _tvDataInicialRanking.setText("Data: " + DateUtil.converterDataPadraoBr(listarRanking.getDataInicio()) + "  até  " + DateUtil.converterDataPadraoBr(listarRanking.getDataFim()));
//        _tvDataFinalRanking.setText("Até  " + DateUtil.converterDataPadraoBr(listarRanking.getDataFim()));
//        _tvDDDRanking.setText("- " + listarRanking.getNrDDD());

//        _tvProdutoRanking.setText("- " + listarRanking.getProduto());
//        _tvComSemRegargaRanking.setText("- " + listarRanking.getTipo());
    }

//    public List<Ranking> getRanking() {
//        return ranking;
//    }
//
//    public ListarRanking getListarRanking() {
//        return listarRanking;
//    }

//    private void setupViewPager() {
//        _fgRankingPre = new FragmentRankingPre();
//        _fgRankingCtrl = new FragmentRankingCtrl();
//        _fgRankingWeb = new FragmentRankingWeb();
//
//        adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(_fgRankingPre, getString(R.string.title_aba_pre));
//        adapter.addFragment(_fgRankingCtrl, getString(R.string.title_aba_ctrl));
//        adapter.addFragment(_fgRankingWeb, getString(R.string.title_aba_web));
//        _vpRanking.setAdapter(adapter);
//        _vpRanking.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tlRanking));
//        _vpRanking.setOffscreenPageLimit(3);
//        _tlRanking.setTabMode(TabLayout.MODE_FIXED);
//        _tlRanking.setupWithViewPager(_vpRanking);
//    }
}
