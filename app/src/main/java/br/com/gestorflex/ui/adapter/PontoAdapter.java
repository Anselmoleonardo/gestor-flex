package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.RetornoPonto;
import br.com.gestorflex.ui.holder.PontoHolder;


public class PontoAdapter extends RecyclerView.Adapter<PontoHolder>  {

    private List<RetornoPonto> retornoPonto;
    private Context context;
    private OnItemClickListener recyclerViewOnClickListenerHack;

    public PontoAdapter (Context context, List<RetornoPonto> listaPonto){
        this.retornoPonto = new ArrayList<>(listaPonto);
        this.context = context;


    }

    public void setRecyclerViewOnClickListenerHack(OnItemClickListener recyclerViewOnClickListenerHack) {
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
    }

    @NonNull
    @Override
    public PontoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PontoHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_ponto, parent, false), this.recyclerViewOnClickListenerHack, retornoPonto);
    }

    @Override
    public void onBindViewHolder(@NonNull PontoHolder holder, int position) {
        RetornoPonto ponto = retornoPonto.get(position);






        holder.bindPonto(ponto);
    }

    @Override
    public int getItemCount() {
        return retornoPonto != null ? retornoPonto.size() : 0;
    }
}
