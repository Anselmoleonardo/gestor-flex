package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.Cabecalho;
import br.com.gestorflex.model.Ponto;
import br.com.gestorflex.model.Produtividade;
import br.com.gestorflex.model.ProdutividadeDados;
import br.com.gestorflex.model.RetornoPonto;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.ui.adapter.PontoAdapter;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;

public class PontoResultadoActivity extends AppCompatActivity implements OnItemClickListener {

    private Toolbar _tbPontoFiltro;
    private RecyclerView _rvPonto;


    private AppCompatTextView _tvTotal;
    private AppCompatTextView _tvIsRegistro;
    private AppCompatTextView _tvPontoData;

    String data;
    String dataAlternada;
    String registro;
    boolean isRegistro;
    private Usuario usuario;
    List<RetornoPonto> retornoPonto;
    Ponto ponto;
    public PontoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ponto_resultado);
        BottomBarUtil.configurar(this);

        instanciarViews();
    }

    private void instanciarViews() {
        _tbPontoFiltro = findViewById(R.id.toolbar);
        _rvPonto = findViewById(R.id.rv_resultado_ponto);
        _tvTotal = findViewById(R.id.tv_ponto_total);
        _tvIsRegistro = findViewById(R.id.tv_register_ponto);
        _tvPontoData = findViewById(R.id.tv_data_ponto);


        configurarViews();
    }

    private void configurarViews() {

        usuario = UsuarioCTRL.getUsuario(this);
        _tbPontoFiltro.setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
        _tbPontoFiltro.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_tbPontoFiltro);


        retornoPonto = (List<RetornoPonto>) getIntent().getSerializableExtra("retornoPonto");
        ponto = (Ponto) getIntent().getSerializableExtra("ponto");
//        data = getIntent().getStringExtra("dataReal");
//        dataAlternada = getIntent().getStringExtra("dataAlternada");
        isRegistro = getIntent().getBooleanExtra("isComRegistro", true);

        _tvPontoData.setText("Data: " + ponto.getData());

        if (isRegistro) {
            registro = "- Com Registro";
        } else {
            registro = "- Sem Registro";
        }
        _tvIsRegistro.setText(registro);

        mAdapter = new PontoAdapter(this, retornoPonto);

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        _rvPonto.setAdapter(mAdapter);

        _tvTotal.setText(getString(R.string.text_total_valor, retornoPonto.size()));


    }

    @Override
    public void OnItemClickListener(View view, int position, Object item) {

        ErrorUtil.montarCaminhoErro();
        try {
            Intent intent = new Intent(PontoResultadoActivity.this, PontoPromotorActivity.class);


            intent.putExtra("isComRegistro", isRegistro);
//            intent.putExtra("dataAlternada", dataAlternada);
//            intent.putExtra("dataReal", data);
            intent.putExtra("ponto", ponto);
            intent.putExtra("retornoPonto", retornoPonto.get(position));
//            intent.putExtra("PDV", retornoPonto.get(position).getPDV());
//            intent.putExtra("PDV", retornoPonto.get(position).get);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }
}
