package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.ui.holder.SincCheckInHolder;

public class SincCheckInAdapter extends RecyclerView.Adapter<SincCheckInHolder> {

    private Context context;
    private List<RespostaPergunta> respostaPerguntas;
    private OnItemClickListener recyclerViewOnClickListenerHack;

    public SincCheckInAdapter(Context context, List<RespostaPergunta> respostaPerguntas) {
        this.context = context;
        this.respostaPerguntas = respostaPerguntas;
    }

    public void setRecyclerViewOnClickListenerHack(OnItemClickListener recyclerViewOnClickListenerHack) {
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
    }

    @NonNull
    @Override
    public SincCheckInHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SincCheckInHolder(LayoutInflater.from(context).inflate(R.layout.item_sinc_odometro,
                parent, false), this.recyclerViewOnClickListenerHack, respostaPerguntas);
    }

    @Override
    public void onBindViewHolder(@NonNull SincCheckInHolder sincCheckInHoder, int position) {
        sincCheckInHoder.bindCheckIn(context, respostaPerguntas.get(position));
    }

    @Override
    public int getItemCount() {
        return respostaPerguntas != null ? respostaPerguntas.size() : 0;
    }

    public void insertItem(RespostaPergunta odometro) {
        respostaPerguntas.add(odometro);
        notifyItemInserted(getItemCount());
    }

    public void removeItem(int position) {
        respostaPerguntas.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    public List<RespostaPergunta> getCheckIns() {
        return respostaPerguntas;
    }
}

