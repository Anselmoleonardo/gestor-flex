package br.com.gestorflex.ui.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.PdvCTRL;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.RespostaPergunta;
import br.com.gestorflex.util.DateUtil;

public class SincCheckInHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView _tvDescricao;
    private TextView _tvData;

    private OnItemClickListener recyclerViewOnClickListenerHack;
    private List<RespostaPergunta> respostaPerguntas;

    public SincCheckInHolder(@NonNull View itemView, OnItemClickListener recyclerViewOnClickListenerHack, List<RespostaPergunta> respostaPerguntas) {
        super(itemView);
        _tvDescricao = itemView.findViewById(R.id.tv_sinc_odometro_descricao);
        _tvData = itemView.findViewById(R.id.tv_sinc_odometro_data);

        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
        this.respostaPerguntas = respostaPerguntas;
        itemView.setOnClickListener(this);
    }

    public void bindCheckIn(Context context, RespostaPergunta respostaPergunta) {
        _tvDescricao.setText(PdvCTRL.getPdvById(context, respostaPergunta.getIdPdv()).getDescricao());
        _tvData.setText(DateUtil.convertDateTimeBrPattern(respostaPergunta.getDataSinc()));
    }

    @Override
    public void onClick(View v) {
        if (this.recyclerViewOnClickListenerHack != null) {
            this.recyclerViewOnClickListenerHack.OnItemClickListener(v, getLayoutPosition(), respostaPerguntas.get(getLayoutPosition()));
        }
    }
}

