package br.com.gestorflex.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Response;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.EstadoCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Estado;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;

public class ComparativoVendaFiltroActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatSpinner _spEstado;
    private AppCompatSpinner _spDia;
    private AppCompatSpinner _spMes;
    private AppCompatSpinner _spAno;
    private AppCompatSpinner _spHoraInicial;
    private AppCompatSpinner _spHoraFinal;
    private RadioButton _rbDiario;
    private RadioButton _rbMensal;
    private RadioButton _rbAnual;
    private LinearLayout _llMes;
    private LinearLayout _llDia;
    private LinearLayout _llHoraInicial;
    private LinearLayout _llHoraFinal;
    private FloatingActionButton _fabPesquisar;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    private Usuario usuario;
    private String dataSelecionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comparativo_venda_filtro);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(_toolbar);
            usuario = UsuarioCTRL.getUsuario(this);
            _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
            getSupportActionBar().setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
            _spEstado = findViewById(R.id.sp_filtrar_comparativo_venda_uf);
            _spDia = findViewById(R.id.sp_filtrar_comparativo_venda_dia);
            _spMes = findViewById(R.id.sp_filtrar_comparativo_venda_mes);
            _spAno = findViewById(R.id.sp_filtrar_comparativo_venda_ano);
            _spHoraInicial = findViewById(R.id.sp_filtrar_comparativo_venda_hora_inicial);
            _spHoraFinal = findViewById(R.id.sp_filtrar_comparativo_venda_hora_final);
            _rbDiario = findViewById(R.id.rb_filtrar_comparativo_venda_diario);
            _rbMensal = findViewById(R.id.rb_filtrar_comparativo_venda_mensal);
            _rbAnual = findViewById(R.id.rb_filtrar_comparativo_venda_anual);
            _llMes = findViewById(R.id.ll_filtrar_comparativo_venda_mes);
            _llDia = findViewById(R.id.ll_filtrar_comparativo_venda_dia);
            _llHoraInicial = findViewById(R.id.ll_filtrar_comparativo_venda_hora_inicial);
            _llHoraFinal = findViewById(R.id.ll_filtrar_comparativo_venda_hora_final);
            _fabPesquisar = findViewById(R.id.fab_filtrar_comparativo_venda_pesquisar);
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() {
        setupVolleyListeners();
        preencherSpinnerUf();
        preencherSpinnerDia();
        preencherSpinnerMes();
        preencherSpinnerAno();
        preencherHorarioInicialFinal();
        _rbDiario.setOnClickListener(this);
        _rbMensal.setOnClickListener(this);
        _rbAnual.setOnClickListener(this);
        _fabPesquisar.setOnClickListener(this);
    }

    public void preencherSpinnerUf() {
        List<Estado> listEstados = EstadoCTRL.listEstados(this);
        if (listEstados.size() > 1)
            listEstados.add(0, new Estado(0, "TODOS"));
        ArrayAdapter<Estado> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listEstados);
        _spEstado.setAdapter(dataAdapter);
    }

    public void preencherSpinnerDia() {
        List<String> dias = DateUtil.getTodosDiasDoMes(_spAno.getSelectedItemPosition(), _spMes.getSelectedItemPosition() );
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, dias);
        _spDia.setAdapter(dataAdapter);
        _spDia.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
    }

    public void preencherSpinnerMes() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, DateUtil.getTodosMeses());
        _spMes.setAdapter(dataAdapter);
        _spMes.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.MONTH));
    }

    public void preencherSpinnerAno() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String[] anos = {(year - 1) + "", year + "", (year + 1) + ""};
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, anos);
        _spAno.setAdapter(dataAdapter);
        _spAno.setSelection(1);
    }

    public void preencherHorarioInicialFinal() {
        Calendar calendarHora = Calendar.getInstance(TimeZone.getDefault());
        int hora = calendarHora.get(Calendar.HOUR_OF_DAY);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, DateUtil.getHorarioComercial());
        _spHoraInicial.setAdapter(dataAdapter);
        _spHoraFinal.setAdapter(dataAdapter);
        _spHoraFinal.setSelection(getHoraPosition(hora + 1));
        _spHoraInicial.setSelection(getHoraPosition(hora + 1));
    }

    public int getHoraPosition(int hora) {
        List<String> horas = DateUtil.getHorarioComercial();
        if (hora >= 7 && hora < 23) {
            for (int i = 0; i < horas.size(); i++) {
                if (horas.get(i).equals(hora + "")) {
                    return i;
                }
            }
        } else if (hora > 22) {
            return horas.size() - 1;
        }
        return 0;
    }

    public int getOpcaoPeriodo() {
        if (_rbDiario.isChecked()) {
            return 0;
        } else if (_rbMensal.isChecked()) {
            return 1;
        }
        return 2;
    }

    public void buscarComparativo() throws Exception {
        if (validarDataSelecionada()) {
            progressDialog.show();
            JSONObject logParams = new JSONObject();
            logParams.put("idRegional", 0 + "");
            logParams.put("estado", ((Estado) _spEstado.getSelectedItem()).getDescricao() + "");
            logParams.put("idProduto", 0 + "");
            logParams.put("idTipoProduto", 0 + "");
            logParams.put("opcListagem", 4 + "");
            logParams.put("opcPeriodo", getOpcaoPeriodo() + "");
            logParams.put("dtVenda", DateUtil.converterDataPadraoUs(dataSelecionada));
            logParams.put("nrHoraInicial", _spHoraInicial.getSelectedItem() + "");
            logParams.put("nrHoraFinal", _spHoraFinal.getSelectedItem() + "");
            ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_comparativo_vendas), responseListener, errorListener, null);
        }
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_buscando_comparativo));
        progressDialog.setCancelable(false);
        responseListener = response -> {
            try {
                JSONObject jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                    JSONArray jArrayResultado = jsonRetorno.getJSONArray("resultado");
                    if (jArrayResultado.length() > 0) {
                        Intent intent = new Intent(ComparativoVendaFiltroActivity.this, ComparativoVendaResultadoActivity.class);
                        intent.putExtra("idPeriodo", getOpcaoPeriodo());
                        intent.putExtra("dataSelecionada", DateUtil.converterDataPadraoUs(dataSelecionada));
                        intent.putExtra("jArrayResultado", jArrayResultado.toString());
                        startActivity(intent);
                    } else
                        new AlertDialog.Builder(ComparativoVendaFiltroActivity.this).setTitle(R.string.title_alert_aviso)
                                .setMessage(R.string.message_alert_sem_resultados)
                                .setPositiveButton(R.string.label_alert_button_ok, null).show();
                } else
                    new AlertDialog.Builder(ComparativoVendaFiltroActivity.this).setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
                progressDialog.dismiss();
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(ComparativoVendaFiltroActivity.this, e);
            }
        };
        errorListener = error -> {
            progressDialog.dismiss();
            Toast.makeText(ComparativoVendaFiltroActivity.this, "Erro ao conectar ao servidor", Toast.LENGTH_LONG).show();
        };
    }

    private boolean validarDataSelecionada() {
        dataSelecionada = _spDia.getSelectedItem().toString() + "/" +
                _spMes.getSelectedItem().toString() + "/" +
                _spAno.getSelectedItem().toString();
        if (!DateUtil.validateDate(dataSelecionada)) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.title_alert_aviso)
                    .setMessage(R.string.message_alert_data_invalida)
                    .setPositiveButton(R.string.label_alert_button_ok, null)
                    .show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_filtrar_comparativo_venda_pesquisar:
                try {
                    buscarComparativo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rb_filtrar_comparativo_venda_diario:
                _llDia.setVisibility(View.VISIBLE);
                _llMes.setVisibility(View.VISIBLE);
                _llHoraFinal.setVisibility(View.VISIBLE);
                _llHoraInicial.setVisibility(View.VISIBLE);
                break;
            case R.id.rb_filtrar_comparativo_venda_mensal:
                _llDia.setVisibility(View.GONE);
                _llMes.setVisibility(View.VISIBLE);
                _llHoraFinal.setVisibility(View.GONE);
                _llHoraInicial.setVisibility(View.GONE);
                break;
            case R.id.rb_filtrar_comparativo_venda_anual:
                _llDia.setVisibility(View.GONE);
                _llMes.setVisibility(View.GONE);
                _llHoraFinal.setVisibility(View.GONE);
                _llHoraInicial.setVisibility(View.GONE);
                break;
        }
    }

}
