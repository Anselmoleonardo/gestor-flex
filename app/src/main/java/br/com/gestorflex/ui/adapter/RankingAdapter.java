package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Ranking;
import br.com.gestorflex.ui.holder.RankingHolder;

public class RankingAdapter extends RecyclerView.Adapter<RankingHolder> {

    private List<Ranking> rankings;
    private Context context;

    public RankingAdapter(Context context, List<Ranking> listaRanking) {
        this.rankings = new ArrayList<>(listaRanking);
        this.context = context;
    }

    @NonNull
    @Override
    public RankingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RankingHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_ranking, parent, false), context);
    }

    @Override
    public void onBindViewHolder(RankingHolder holder, int position) {


            Ranking rankingBinding = rankings.get(position);
            holder.bindRanking(rankingBinding);

    }

//    public void insertItem(Ranking usuarioOfensor) {
//        ranking.add(usuarioOfensor);
//        notifyItemInserted(getItemCount());
//    }
//
//    public void removeItem(int position) {
//        ranking.remove(position);
//        notifyItemRemoved(position);
//        notifyItemRangeChanged(position, ranking.size());
//    }

    @Override
    public int getItemCount() {
        return rankings != null ? rankings.size() : 0;
    }

}
