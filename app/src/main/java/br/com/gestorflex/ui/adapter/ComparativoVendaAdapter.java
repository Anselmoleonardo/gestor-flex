package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.ComparativoVenda;
import br.com.gestorflex.ui.holder.ComparativoVendaHolder;

public class ComparativoVendaAdapter extends RecyclerView.Adapter<ComparativoVendaHolder> {

    private Context context;
    private List<ComparativoVenda> comparativoVendas;

    public ComparativoVendaAdapter(Context context, List<ComparativoVenda> registrosDeJornada) {
        this.context = context;
        this.comparativoVendas = registrosDeJornada;
    }

    @NonNull
    @Override
    public ComparativoVendaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ComparativoVendaHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_comparativo_venda, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ComparativoVendaHolder comparativoVendaHolder, int position) {
        ComparativoVenda comparativoVenda = comparativoVendas.get(position);
        comparativoVendaHolder.bindRanking(comparativoVenda);
    }

    @Override
    public int getItemCount() {
        return comparativoVendas == null ? 0 : comparativoVendas.size();
    }

}
