package br.com.gestorflex.ui.holder;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import br.com.gestorflex.R;
import br.com.gestorflex.model.UsuarioOfensor;

public class UsuarioOfensorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Button _btnLigar;
    private TextView _tvNome;
    private TextView _tvAdmissao;
    private TextView _tvDdd;
    private TextView _tvVendas;
    private TextView _tvMatricula;
    private TextView _tvTelefone;
    private ImageView _ivSeta;
    private LinearLayout _llInfos;
    private LinearLayout _llPrincipal;

    private Context context;
    private UsuarioOfensor usuarioOfensor;

    public UsuarioOfensorHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        _tvNome = itemView.findViewById(R.id.tv_usuario_ofensor_nome);
        _ivSeta = itemView.findViewById(R.id.iv_usuario_ofensor_seta);
        _llInfos = itemView.findViewById(R.id.ll_usuario_ofensor_infos);
        _llPrincipal = itemView.findViewById(R.id.ll_usuario_ofensor);
        _tvAdmissao = itemView.findViewById(R.id.tv_usuario_ofensor_admissao);
        _tvDdd = itemView.findViewById(R.id.tv_usuario_ofensor_ddd);
        _tvVendas = itemView.findViewById(R.id.tv_usuario_ofensor_vendas);
        _tvMatricula = itemView.findViewById(R.id.tv_usuario_ofensor_matricula);
        _tvTelefone = itemView.findViewById(R.id.tv_usuario_ofensor_telefone);
        _btnLigar = itemView.findViewById(R.id.btn_usuario_ofensor_ligar);
        _btnLigar.setOnClickListener(this);
        _llPrincipal.setOnClickListener(this);
    }

    public void bindUsuarioOfensor(UsuarioOfensor usuarioOfensor) {
        this.usuarioOfensor = usuarioOfensor;
        _tvNome.setText(usuarioOfensor.getNome());
        _tvAdmissao.setText(usuarioOfensor.getDataAdmissao());
        _tvDdd.setText(usuarioOfensor.getDdd());
        _tvVendas.setText(usuarioOfensor.getVendas());
        _tvMatricula.setText(usuarioOfensor.getMatricula());
        _tvTelefone.setText(usuarioOfensor.getTelefone());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_usuario_ofensor_ligar:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    } else {
                        new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso)
                                .setMessage(String.format(context.getString(R.string.message_alert_confirmar_ligacao), usuarioOfensor.getNome()))
                                .setPositiveButton(R.string.label_alert_button_sim, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+55" + usuarioOfensor.getTelefone()));
                                        context.startActivity(intent);
                                    }
                                })
                                .setNegativeButton(R.string.label_alert_button_nao, null)
                                .show();
                    }
                } else {
                    new AlertDialog.Builder(context).setTitle(R.string.title_alert_aviso)
                            .setMessage(String.format(context.getString(R.string.message_alert_confirmar_ligacao), usuarioOfensor.getNome()))
                            .setPositiveButton(R.string.label_alert_button_sim, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+55" + usuarioOfensor.getTelefone()));
                                    context.startActivity(intent);
                                }
                            })
                            .setNegativeButton(R.string.label_alert_button_nao, null)
                            .show();

                }

                break;
            case R.id.ll_usuario_ofensor:
                if (_llInfos.getVisibility() == View.GONE) {
                    _llInfos.setVisibility(View.VISIBLE);
                    _ivSeta.setImageResource(R.drawable.ic_seta_cima);
                } else {
                    _llInfos.setVisibility(View.GONE);
                    _ivSeta.setImageResource(R.drawable.ic_seta_baixo);
                }
                break;
        }
    }
}
