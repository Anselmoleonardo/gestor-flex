package br.com.gestorflex.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.DDDCTRL;
import br.com.gestorflex.database.internal.controller.EstadoCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.DDD;
import br.com.gestorflex.model.Estado;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.model.UsuarioOfensor;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.model.Usuario;

public class UsuarioOfensorFiltroActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private AppCompatSpinner _spEstado;
    private AppCompatSpinner _spDDD;
    private AppCompatSpinner _spDiaInicial;
    private AppCompatSpinner _spDiaFinal;
    private AppCompatSpinner _spMes;
    private AppCompatSpinner _spAno;
    private TextInputLayout _tilQuantidadeMinima;
    private FloatingActionButton _fabPesquisar;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    private Usuario usuario;
    private String dataInicial;
    private String dataFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_ofensor_filtro);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        ErrorUtil.montarCaminhoErro();
        try {
            Toolbar _toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(_toolbar);
            usuario = UsuarioCTRL.getUsuario(this);
            _toolbar.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
            getSupportActionBar().setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
            _spEstado = findViewById(R.id.sp_filtrar_usuario_ofensor_uf);
            _spDDD = findViewById(R.id.sp_filtrar__usuario_ofensor_ddd);
            _spDiaInicial = findViewById(R.id.sp_filtrar_usuario_ofensor_dia_inicial);
            _spDiaFinal = findViewById(R.id.sp_filtrar_usuario_ofensor_dia_final);
            _spMes = findViewById(R.id.sp_filtrar_usuario_ofensor_mes);
            _spAno = findViewById(R.id.sp_filtrar_usuario_ofensor_ano);
            _tilQuantidadeMinima = findViewById(R.id.til_filtrar_usuario_ofensor_quantidade);
            _fabPesquisar = findViewById(R.id.fab_filtrar_usuario_ofensor_pesquisar);
            configurarViews();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void configurarViews() {
        _fabPesquisar.setOnClickListener(this);
        preencherSpinnerUf();
        preencherSpinnerDia();
        preencherSpinnerMes();
        preencherSpinnerAno();
        setupVolleyListeners();
    }

    public void preencherSpinnerUf() {
        List<Estado> listEstados = EstadoCTRL.listEstados(this);
        if (listEstados.size() > 1)
            listEstados.add(0, new Estado(0, "TODOS"));
        ArrayAdapter<Estado> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listEstados);
        _spEstado.setAdapter(dataAdapter);
        _spEstado.setOnItemSelectedListener(this);
    }

    public void preencherSpinnerDDD(int idUf) {
        try {
            List<DDD> listDDDs = DDDCTRL.listDDDs(this, idUf);
            if (listDDDs.size() > 1 || idUf == 0)
                listDDDs.add(0, new DDD(0, 0));
            ArrayAdapter<DDD> dataAdapter = new ArrayAdapter<>(this,
                    R.layout.support_simple_spinner_dropdown_item, listDDDs);
            _spDDD.setAdapter(dataAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void preencherSpinnerDia() {
        List<String> dias = DateUtil.getTodosDiasDoMes(_spAno.getSelectedItemPosition(), _spMes.getSelectedItemPosition() );
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, dias);
        _spDiaInicial.setAdapter(dataAdapter);
        _spDiaFinal.setAdapter(dataAdapter);
        _spDiaInicial.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
        _spDiaFinal.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
    }

    public void preencherSpinnerMes() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, DateUtil.getTodosMeses());
        _spMes.setAdapter(dataAdapter);
        _spMes.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.MONTH));
    }

    public void preencherSpinnerAno() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String[] gruposEcon = {(year - 1) + "", year + "", (year + 1) + ""};
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, gruposEcon);
        _spAno.setAdapter(dataAdapter);
        _spAno.setSelection(1);
    }

    public boolean validarDatasSelecionadas() {
        String diaInicial = (String) _spDiaInicial.getSelectedItem();
        String diaFinal = (String) _spDiaFinal.getSelectedItem();
        String mes = (String) _spMes.getSelectedItem();
        String ano = (String) _spAno.getSelectedItem();
        if (Integer.parseInt(diaInicial) > Integer.parseInt(diaFinal)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_dia_inicial_maior)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        dataInicial = diaInicial + "/" + mes + "/" + ano;
        dataFinal = diaFinal + "/" + mes + "/" + ano;
        if (!DateUtil.validateDate(dataInicial)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_dia_atual_invalido)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        if (!DateUtil.validateDate(dataFinal)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_dia_final_invalido)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }
        return true;
    }

    public void recuperarUsuariosOfensores() throws Exception {
        if (!validarDatasSelecionadas()) return;
        progressDialog.show();
        JSONObject logParams = new JSONObject();
        logParams.put("estado", ((Estado) _spEstado.getSelectedItem()).getId());
        logParams.put("ddd", ((DDD) (_spDDD.getSelectedItem())).getDescricao());
        logParams.put("periodo", 0);
        logParams.put("listarPor", 4);
        logParams.put("data", DateUtil.getCurrentDateUS());
        logParams.put("dataInicial", DateUtil.converterDataPadraoUs(dataInicial));
        logParams.put("dataFinal", DateUtil.converterDataPadraoUs(dataFinal));
        logParams.put("produto", 0);
        logParams.put("tipo", 0);
        logParams.put("minimo", _tilQuantidadeMinima.getEditText().getText().toString().equals("") ? "0" : _tilQuantidadeMinima.getEditText().getText().toString());
        logParams.put("regional", 0);
        logParams.put("isEmail", 0);
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_listar_usuarios_ofensores), responseListener, errorListener, null);
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_buscando_usuarios_ofensores));
        progressDialog.setCancelable(false);
        responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                        JSONArray jArrayUsuariosOfensores = jsonRetorno.getJSONArray("usuario_ofensor");
                        if (jArrayUsuariosOfensores.length() > 0) {
                            List<UsuarioOfensor> usuariosOfensores = new ArrayList<>();
                            for (int i = 0; i < jArrayUsuariosOfensores.length(); i++) {
                                usuariosOfensores.add(new UsuarioOfensor(jArrayUsuariosOfensores.getJSONObject(i)));
                            }
                            Intent intent = new Intent(UsuarioOfensorFiltroActivity.this, UsuarioOfensorResultadoActivity.class);
                            intent.putExtra("usuariosOfensores", (Serializable) usuariosOfensores);
                            startActivity(intent);
                        } else {
                            new AlertDialog.Builder(UsuarioOfensorFiltroActivity.this).setTitle(R.string.title_alert_aviso)
                                    .setMessage(R.string.message_alert_sem_resultados)
                                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
                        }
                    } else {
                        new AlertDialog.Builder(UsuarioOfensorFiltroActivity.this).setTitle(R.string.title_alert_aviso)
                                .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                                .setPositiveButton(R.string.label_alert_button_ok, null).show();
                    }
                    progressDialog.dismiss();
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(UsuarioOfensorFiltroActivity.this, e);
                }
            }
        };
        errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(UsuarioOfensorFiltroActivity.this, "Erro ao conectar ao servidor", Toast.LENGTH_LONG).show();
            }
        };
    }


    @Override
    public void onClick(View v) {
        try {
            recuperarUsuariosOfensores();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.sp_filtrar_usuario_ofensor_uf:
                Estado estado = (Estado) _spEstado.getSelectedItem();
                preencherSpinnerDDD(estado.getId());
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
