package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Cabecalho;
import br.com.gestorflex.model.Produtividade;
import br.com.gestorflex.model.ProdutividadeDados;
import br.com.gestorflex.model.ProdutividadeDados;

import br.com.gestorflex.ui.holder.ProdutividadeHolder;


public class ProdutividadeAdapter extends RecyclerView.Adapter<ProdutividadeHolder> {

    private List<ProdutividadeDados> produtividadeDetalhes;
    private Cabecalho cabecalho;
    private Produtividade produtividadeItens;
    private Context context;

    public ProdutividadeAdapter(Context context, List<ProdutividadeDados> listaProdutividade,
                                Cabecalho cabecalho, Produtividade produtividade) {
        this.produtividadeDetalhes = new ArrayList<>(listaProdutividade);
        this.cabecalho = cabecalho;
        this.produtividadeItens = produtividade;
        this.context = context;
    }


    @NonNull
    @Override
    public ProdutividadeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProdutividadeHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_produtividade, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProdutividadeHolder holder, int position) {
        ProdutividadeDados produtividade = produtividadeDetalhes.get(position);
        holder.bindProdutividade(produtividadeItens, produtividade, cabecalho);
    }

    @Override
    public int getItemCount() {
        return produtividadeDetalhes != null ? produtividadeDetalhes.size() : 0;
    }
}
