package br.com.gestorflex.ui.activity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Geolocalizacao;
import br.com.gestorflex.model.RotaVenda;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.BottomBarUtil;

public class RotaVendaResultadoActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, View.OnClickListener {

    private GoogleMap _gmpRotaVenda;
    private Toolbar _tbRotaVenda;
    private FloatingActionButton _fabUsuario;
    private FloatingActionButton _fabRota;
    private SupportMapFragment _mapRotaVenda;

    private List<RotaVenda> rotas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rota_venda_resultado);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        _mapRotaVenda = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        _fabUsuario = findViewById(R.id.fab_rota_resultado_usuario);
        _fabRota = findViewById(R.id.fab_rota_resultado_pontos);
        _tbRotaVenda = findViewById(R.id.toolbar);
        configurarViews();
    }

    private void configurarViews() {
        _mapRotaVenda.getMapAsync(this);
        Usuario usuario = UsuarioCTRL.getUsuario(this);
        _tbRotaVenda.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_tbRotaVenda);
        getSupportActionBar().setTitle("Rotas");
        _fabUsuario.setOnClickListener(this);
        _fabRota.setOnClickListener(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        _gmpRotaVenda = googleMap;
//        _gmpRotaVenda.getUiSettings().setZoomControlsEnabled(true);
        _gmpRotaVenda.setInfoWindowAdapter(this);
        rotas = (List<RotaVenda>) getIntent().getSerializableExtra("rota");
        addMarkerUsuario();
        addMarkers();
    }

    private void addMarkers() {
        for (RotaVenda rota : rotas) {
            MarkerOptions marker = new MarkerOptions();
            marker.position(new LatLng(rota.getLatitude(), rota.getLongitude()));
            if (!rota.getNomeFantasia().equals("")) {
                marker.title("Cliente: " + rota.getNomeFantasia());
                marker.snippet(rota.getEnderecoCompleto());
            } else {
                marker.title(rota.getEnderecoCompleto());
            }
            marker.icon(rota.getMarker());
            _gmpRotaVenda.addMarker(marker);
        }
    }

    private void addMarkerUsuario() {
        _gmpRotaVenda.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Geolocalizacao.getLatitude(), Geolocalizacao.getLongitude()), 15f));
        _gmpRotaVenda.addMarker(new MarkerOptions()
                .position(new LatLng(Geolocalizacao.getLatitude(), Geolocalizacao.getLongitude()))
                .title("Eu")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
    }

    // Métodos para editar os balõeszinhos dos marcadores
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        LinearLayout info = new LinearLayout(this);
        info.setOrientation(LinearLayout.VERTICAL);

        TextView title = new TextView(this);
        title.setTextColor(Color.BLACK);
        title.setGravity(Gravity.CENTER);
        title.setTypeface(null, Typeface.BOLD);
        title.setText(marker.getTitle());

        TextView snippet = new TextView(this);
        snippet.setTextColor(Color.GRAY);
        snippet.setGravity(Gravity.CENTER);
        snippet.setText(marker.getSnippet());

        info.addView(title);
        if (marker.getSnippet() != null && !marker.getSnippet().equals(""))
            info.addView(snippet);

        return info;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_rota_resultado_usuario:
                _gmpRotaVenda.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Geolocalizacao.getLatitude(), Geolocalizacao.getLongitude()), 15f));
                break;
            case R.id.fab_rota_resultado_pontos:
                _gmpRotaVenda.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(rotas.get(0).getLatitude(), rotas.get(0).getLongitude()), 12f));
                break;
        }
    }
}
