package br.com.gestorflex.ui.holder;

import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.gridlayout.widget.GridLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.RetornoPonto;

public class PontoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    private GridLayout _glPonto;
    private AppCompatTextView _tvPontoNome;
    private AppCompatTextView _tvPontoItemNome;
    private AppCompatTextView _tvPontomatricula;
    private AppCompatTextView _tvPontoAdmissao;
    private AppCompatTextView _tvPontoFuncional;
    private AppCompatTextView _tvPontoDDD;
    private AppCompatTextView _tvPontoPDV;
    private AppCompatImageView _ivPonto;

    private OnItemClickListener recyclerViewOnClickListenerHack;
    private List<RetornoPonto> retornoPonto;

    public PontoHolder(@NonNull View itemView, OnItemClickListener recyclerViewOnClickListenerHack, List<RetornoPonto> retornoPonto) {
        super(itemView);
        _glPonto = itemView.findViewById(R.id.gl_ponto);
        _tvPontoNome = itemView.findViewById(R.id.tv_ponto_nome);
        _tvPontoItemNome = itemView.findViewById(R.id.item_ponto_nome);
        _tvPontomatricula = itemView.findViewById(R.id.item_ponto_matricula);
        _tvPontoAdmissao = itemView.findViewById(R.id.item_ponto_admissao);
        _tvPontoFuncional = itemView.findViewById(R.id.item_ponto_funcional);
        _tvPontoDDD = itemView.findViewById(R.id.item_ponto_ddd);
        _tvPontoPDV = itemView.findViewById(R.id.item_ponto_pdv);
        _ivPonto = itemView.findViewById(R.id.iv_ponto_image);

        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
        this.retornoPonto = retornoPonto;
        itemView.setOnClickListener(this);

    }

    public void bindPonto(RetornoPonto Ponto) {

        Log.e("name", "nome" + Ponto.getNome() );
        _tvPontoNome.setText(Ponto.getNome());
        _tvPontoItemNome.setText(Ponto.getNome());
        _tvPontomatricula.setText(String.valueOf(Ponto.getMatricula()));
        _tvPontoAdmissao.setText(Ponto.getData());
        _tvPontoFuncional.setText(Ponto.getFuncional());
        _tvPontoDDD.setText(String.valueOf(Ponto.getDDD()));
        _tvPontoPDV.setText(Ponto.getNomePDV());

        _tvPontoNome.setOnClickListener(this);
        _ivPonto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (this.recyclerViewOnClickListenerHack != null) {
            this.recyclerViewOnClickListenerHack.OnItemClickListener(v, getLayoutPosition(), retornoPonto.get(getLayoutPosition()));
        }
    }
}
