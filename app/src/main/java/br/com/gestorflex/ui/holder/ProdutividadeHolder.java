package br.com.gestorflex.ui.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.gridlayout.widget.GridLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Cabecalho;
import br.com.gestorflex.model.Produtividade;
import br.com.gestorflex.model.ProdutividadeDados;
import br.com.gestorflex.model.Ranking;

public class ProdutividadeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private TextView _tvListarProdutividade;
    private GridLayout _glProdutividade;
    private ImageView _ivListarProdutividade;
    private LinearLayoutCompat _llListarProdutividade;

    private TextView _tvCabecalhoMeta;
    private TextView _tvCabecalhoVendas;
    private TextView _tvCabecalhoRanking;
    private TextView _tvCabecalhoPre;
    private TextView _tvCabecalhoD;
    private TextView _tvCabecalhoCtrl;
    private TextView _tvCabecalhoCtrlVa;
    private TextView _tvCabecalhoPos;
    private TextView _tvProdMetaPre;
    private TextView _tvProdMetaD;
    private TextView _tvProdMetaCtrl;
    private TextView _tvProdMetaCtrlVa;
    private TextView _tvProdMetaPos;
    private TextView _tvProdVendasPre;
    private TextView _tvProdVendasD;
    private TextView _tvProdVendasCtrl;
    private TextView _tvProdVendasCtrlAv;
    private TextView _tvProdVendasPos;
    private TextView _tvProdRankingPre;
    private TextView _tvProdRankingD;
    private TextView _tvProdRankingCtrl;
    private TextView _tvProdRankingCtrlAv;
    private TextView _tvProdRankingPos;

    Produtividade produtividade;
    ProdutividadeDados produtividadeDetalhes;
    Cabecalho cabecalho = new Cabecalho();

    public ProdutividadeHolder(@NonNull View itemView) {
        super(itemView);
        _glProdutividade = itemView.findViewById(R.id.gl_produtividade);
        _tvListarProdutividade = itemView.findViewById(R.id.tv_listar_produtividade);
        _ivListarProdutividade = itemView.findViewById(R.id.iv_listar_produtividade);
        _llListarProdutividade = itemView.findViewById(R.id.ll_listar_produtividade);
        _ivListarProdutividade = itemView.findViewById(R.id.iv_listar_produtividade);

        _tvCabecalhoMeta = itemView.findViewById(R.id.item_cabecalho_meta);
         _tvCabecalhoVendas = itemView.findViewById(R.id.item_cabecalho_vendas);
         _tvCabecalhoRanking = itemView.findViewById(R.id.item_cabecalho_ranking);
         _tvCabecalhoPre = itemView.findViewById(R.id.item_cabecalho_pre);
         _tvCabecalhoD = itemView.findViewById(R.id.item_cabecalho_d);
         _tvCabecalhoCtrl = itemView.findViewById(R.id.item_cabecalho_ctrl);
         _tvCabecalhoCtrlVa = itemView.findViewById(R.id.item_cabecalho_ctrlav);
         _tvCabecalhoPos = itemView.findViewById(R.id.item_cabecalho_pos);
         _tvProdMetaPre = itemView.findViewById(R.id.item_produtividade_meta_pre);
         _tvProdMetaD = itemView.findViewById(R.id.item_produtividade_meta_D);
         _tvProdMetaCtrl = itemView.findViewById(R.id.item_produtividade_meta_ctrl);
         _tvProdMetaCtrlVa = itemView.findViewById(R.id.item_produtividade_meta_ctrlav);
         _tvProdMetaPos = itemView.findViewById(R.id.item_produtividade_meta_pos);
         _tvProdVendasPre = itemView.findViewById(R.id.item_produtividade_vendas_pre);
         _tvProdVendasD = itemView.findViewById(R.id.item_produtividade_vendas_D);
         _tvProdVendasCtrl = itemView.findViewById(R.id.item_produtividade_vendas_ctrl);
         _tvProdVendasCtrlAv = itemView.findViewById(R.id.item_produtividade_vendas_ctrlva);
         _tvProdVendasPos = itemView.findViewById(R.id.item_produtividade_vendas_pos);
         _tvProdRankingPre = itemView.findViewById(R.id.item_produtividade_ranking_pre);
         _tvProdRankingD = itemView.findViewById(R.id.item_produtividade_ranking_D);
         _tvProdRankingCtrl = itemView.findViewById(R.id.item_produtividade_ranking_ctrl);
         _tvProdRankingCtrlAv = itemView.findViewById(R.id.item_produtividade_ranking_ctrlav);
         _tvProdRankingPos = itemView.findViewById(R.id.item_produtividade_ranking_pos);
        //itemView.setOnClickListener(this);



    }

    public void bindProdutividade(Produtividade produtividade, ProdutividadeDados produtividadeDetalhes,
                                  Cabecalho cabecalho) {
       this.produtividade = produtividade;
        this.produtividadeDetalhes = produtividadeDetalhes;
        this.cabecalho = cabecalho;

        //Cabeçalho
        _tvListarProdutividade.setText(produtividadeDetalhes.getListar());
        _tvListarProdutividade.setOnClickListener(this);
        _ivListarProdutividade.setOnClickListener(this);

        _tvCabecalhoMeta.setText(cabecalho.getMeta());
        _tvCabecalhoVendas.setText(cabecalho.getVendas());
        _tvCabecalhoRanking.setText(cabecalho.getRanking());
        _tvCabecalhoPre.setText(cabecalho.getPreCabecalho());
        _tvCabecalhoD.setText(cabecalho.getRecarga());
        _tvCabecalhoCtrl.setText(cabecalho.getCtrl());
        _tvCabecalhoCtrlVa.setText(cabecalho.getCtrlAv());
        _tvCabecalhoPos.setText(cabecalho.getPos());
        //Produtividade String.valueof()
        _tvProdMetaPre.setText(String.valueOf(produtividadeDetalhes.getPreMeta()));
        _tvProdMetaD.setText(String.valueOf(produtividadeDetalhes.getRecargaMeta()));
        _tvProdMetaCtrl.setText(String.valueOf(produtividadeDetalhes.getCtrlMeta()));
        _tvProdMetaCtrlVa.setText(String.valueOf(produtividadeDetalhes.getCtrlAvMEta()));
        _tvProdMetaPos.setText(String.valueOf(produtividadeDetalhes.getPosMeta()));
        _tvProdVendasPre.setText(String.valueOf(produtividadeDetalhes.getPreVenda()));
        _tvProdVendasD.setText(String.valueOf(produtividadeDetalhes.getRecargaVenda()));
        _tvProdVendasCtrl.setText(String.valueOf(produtividadeDetalhes.getCtlrVenda()));
        _tvProdVendasCtrlAv.setText(String.valueOf(produtividadeDetalhes.getCtrlAvVenda()));
        _tvProdVendasPos.setText(String.valueOf(produtividadeDetalhes.getPosVenda()));
        _tvProdRankingPre.setText(String.valueOf(produtividadeDetalhes.getPreRanking()));
        _tvProdRankingD.setText(String.valueOf(produtividadeDetalhes.getRecargaRanking()));
        _tvProdRankingCtrl.setText(String.valueOf(produtividadeDetalhes.getCtrlRanking()));
        _tvProdRankingCtrlAv.setText(String.valueOf(produtividadeDetalhes.getCtrlAvRanking()));
        _tvProdRankingPos.setText(String.valueOf(produtividadeDetalhes.getPosRanking()));

    }

    @Override
    public void onClick(View v) {

        if (_glProdutividade.getVisibility() == View.GONE) {
            _glProdutividade.setVisibility(View.VISIBLE);
            _ivListarProdutividade.setImageResource(R.drawable.ic_seta_cima);
        } else {
            _glProdutividade.setVisibility(View.GONE);
            _ivListarProdutividade.setImageResource(R.drawable.ic_seta_baixo);
        }

    }
  }
