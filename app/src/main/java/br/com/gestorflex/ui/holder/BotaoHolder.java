package br.com.gestorflex.ui.holder;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Field;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.CheckInOutCTRL;
import br.com.gestorflex.database.internal.controller.NotificacaoCTRL;
import br.com.gestorflex.database.internal.controller.OdometroCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.interfaces.OnRecyclerItemClickListener;
import br.com.gestorflex.model.Botao;
import br.com.gestorflex.ui.activity.JornadaMenuActivity;
import br.com.gestorflex.ui.fragment.fragments_menu.FragmentMenuOutros;
import br.com.gestorflex.util.AnimacaoUtil;


public class BotaoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private RelativeLayout _rlMenuBotao;
    private TextView _tvIcone;
    private TextView _tvNome;
    private TextView _tvQuantidade;

    private OnRecyclerItemClickListener recyclerViewOnClickListenerHack;
    private List<Botao> botoes;

    public BotaoHolder(View itemView, OnRecyclerItemClickListener recyclerViewOnClickListenerHack, List<Botao> botoes) {
        super(itemView);
        _rlMenuBotao = itemView.findViewById(R.id.rl_menu_botao);
        _tvIcone = itemView.findViewById(R.id.item_menu_icone);
        _tvNome = itemView.findViewById(R.id.item_menu_nome);
        _tvQuantidade = itemView.findViewById(R.id.item_menu_quantidade);

        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
        this.botoes = botoes;
        itemView.setOnClickListener(this);
    }

    public void bindBotao(Context context, Botao botao) {
        _tvNome.setText(botao.getNome());
        configurarBotaoSincronizacao(context, botao);
        configurarBotaoJornada(context, botao);
        configurarBotaoBlindarPdv(context, botao);
        configuraNotificacao(context,botao);
        inserirFonte(context, _tvIcone, pegaUnicode(context, botao.getIcone()));
        _rlMenuBotao.setEnabled(botao.getBoSituacao() == 1);
        if (botao.getBoSituacao() == 1)
            _tvIcone.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.colorPrimary, null));
        else
            _tvIcone.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.colorBloqueio, null));
    }

    private void configurarBotaoSincronizacao(Context context, Botao botao) {
        try {
            if (botao.getTag().equals(JornadaMenuActivity.TAG_MENU_SINCRONIZACAO)) {
                int numSincs = OdometroCTRL.listOdometroSincronizacao(context).size();
                numSincs += CheckInOutCTRL.listCheckInOutSincronizacao(context).size();
                if (numSincs > 0) {
                    _tvQuantidade.setText(String.valueOf(numSincs));
                    _tvQuantidade.setVisibility(View.VISIBLE);
                    AnimacaoUtil.piscarView(_tvQuantidade, 250, 750);
                } else {
                    _tvQuantidade.setText("0");
                    _tvQuantidade.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void configurarBotaoJornada(Context context, Botao botao) {
        if (botao.getTag().equals(FragmentMenuOutros.TAG_JORNADA))
            botao.setBoSituacao(UsuarioCTRL.getUsuario(context).getFlagPrivilegio() == 4 ? 0 : 1);
    }

    private void configurarBotaoBlindarPdv(Context context, Botao botao) {
        if (botao.getTag().equals(FragmentMenuOutros.TAG_JORNADA))
            botao.setBoSituacao(UsuarioCTRL.getUsuario(context).getFlagPrivilegio() == 4 ? 0 : 1);
    }


    private String pegaUnicode(Context context, String nome) {
        try {
            Field campo = R.string.class.getField(nome);
            return context.getResources().getString(campo.getInt(null));
        } catch (Exception e) {
            return "";
        }
    }

    public void inserirFonte(Context context, TextView textView, String unicode) {
        Typeface font = ResourcesCompat.getFont(context, R.font.fa_solid);
        textView.setTypeface(font);
        textView.setText(unicode);
    }


    private void configuraNotificacao(Context context, Botao botao) {
        try {
            if (botao.getTag().equals(FragmentMenuOutros.TAG_NOTIFICACAO)) {
                int numNoti = NotificacaoCTRL.getList(context) != null ? NotificacaoCTRL.getList(context).size() : 0;
                if (numNoti > 0) {
                    _tvQuantidade.setText(String.valueOf(numNoti));
                    _tvQuantidade.setVisibility(View.VISIBLE);
                    AnimacaoUtil.piscarView(_tvQuantidade, 250, 750);
                } else {
                    _tvQuantidade.setText("0");
                    _tvQuantidade.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        if (this.recyclerViewOnClickListenerHack != null) {
            this.recyclerViewOnClickListenerHack.onRecyclerItemClickListener(v, getLayoutPosition(), botoes.get(getLayoutPosition()));
        }
    }

}
