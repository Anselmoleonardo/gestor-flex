package br.com.gestorflex.ui.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import br.com.gestorflex.R;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Cliente;
import br.com.gestorflex.model.Usuario;

public class ClienteHolder extends RecyclerView.ViewHolder {
    // Label's
    private TextView _tvLabelPedido;
    private TextView _tvLabelCnae;
    private TextView _tvLabelOferta;
    private TextView _tvLabelMotivo;
    private TextView _tvLabelTelefone;
    private TextView _tvLabelCelular;
    private TextView _tvLabelSituacao;
    private TextView _tvLabelQtdTecnologia;
    private TextView _tvLabelConsultor;
    // Valores
    private TextView _tvPedido;
    private TextView _tvData;
    private TextView _tvNome;
    private TextView _tvOferta;
    private TextView _tvCnae;
    private TextView _tvMotivo;
    private TextView _tvTelefone;
    private TextView _tvCelular;
    private TextView _tvSituacao;
    private TextView _tvQtdTecnologia;
    private TextView _tvConsultor;

    public ClienteHolder(View itemView) {
        super(itemView);
        // Label's
        _tvLabelPedido = itemView.findViewById(R.id.tv_cliente_pedido_label);
        _tvLabelCnae = itemView.findViewById(R.id.tv_cliente_cnae_label);
        _tvLabelOferta = itemView.findViewById(R.id.tv_cliente_oferta_label);
        _tvLabelMotivo = itemView.findViewById(R.id.tv_cliente_motivo_label);
        _tvLabelTelefone = itemView.findViewById(R.id.tv_cliente_telefone_label);
        _tvLabelCelular = itemView.findViewById(R.id.tv_cliente_celular_label);
        _tvLabelSituacao = itemView.findViewById(R.id.tv_cliente_situacao_label);
        _tvLabelQtdTecnologia = itemView.findViewById(R.id.tv_cliente_qtd_tecnologia_label);
        _tvLabelConsultor = itemView.findViewById(R.id.tv_cliente_consultor_label);
        // Valores
        _tvPedido = itemView.findViewById(R.id.tv_cliente_pedido_valor);
        _tvData = itemView.findViewById(R.id.tv_cliente_data_valor);
        _tvNome = itemView.findViewById(R.id.tv_cliente_nome_valor);
        _tvCnae = itemView.findViewById(R.id.tv_cliente_cnae_valor);
        _tvOferta = itemView.findViewById(R.id.tv_cliente_oferta_valor);
        _tvMotivo = itemView.findViewById(R.id.tv_cliente_motivo_valor);
        _tvTelefone = itemView.findViewById(R.id.tv_cliente_telefone_valor);
        _tvCelular = itemView.findViewById(R.id.tv_cliente_celular_valor);
        _tvSituacao = itemView.findViewById(R.id.tv_cliente_situacao_valor);
        _tvQtdTecnologia = itemView.findViewById(R.id.tv_cliente_qtd_tecnologia_valor);
        _tvConsultor = itemView.findViewById(R.id.tv_cliente_consultor_valor);
    }

    public void bindCliente(Context context, Cliente cliente) {
        try {
            Usuario usuario = UsuarioCTRL.getUsuario(context);
            _tvData.setText(cliente.getData());
            _tvNome.setText(cliente.getCliente());
            _tvConsultor.setText(cliente.getConsultor());
            if (cliente.getAceite() == 0) {
                _tvLabelMotivo.setVisibility(View.GONE);
                _tvMotivo.setVisibility(View.GONE);
                _tvPedido.setText(cliente.getIdPedido());
                _tvCnae.setText(cliente.getCnaeramo());
                _tvOferta.setText(cliente.getOferta());
                _tvTelefone.setText(cliente.getTelefone());
                _tvCelular.setText(cliente.getCelular());
                if (usuario.getIdAgencia() == 18)
                    _tvSituacao.setText(cliente.getSituacao());
                else if (usuario.getIdAgencia() == 34) {
                    _tvCnae.setVisibility(View.GONE);
                    _tvLabelCnae.setVisibility(View.GONE);
                    _tvSituacao.setVisibility(View.GONE);
                    _tvLabelSituacao.setVisibility(View.GONE);
                    _tvQtdTecnologia.setVisibility(View.VISIBLE);
                    _tvLabelQtdTecnologia.setVisibility(View.VISIBLE);
                    _tvQtdTecnologia.setText(cliente.getQtdTecnologias());
                } else if (usuario.getIdAgencia() == 37) {
                    _tvQtdTecnologia.setVisibility(View.GONE);
                    _tvLabelQtdTecnologia.setVisibility(View.GONE);
                    _tvOferta.setVisibility(View.GONE);
                    _tvLabelOferta.setVisibility(View.GONE);
                    _tvCnae.setVisibility(View.GONE);
                    _tvLabelCnae.setVisibility(View.GONE);
                    _tvSituacao.setVisibility(View.GONE);
                    _tvLabelSituacao.setVisibility(View.GONE);
                } else {
                    _tvSituacao.setVisibility(View.GONE);
                    _tvLabelSituacao.setVisibility(View.GONE);
                }
            } else {
                _tvLabelPedido.setText("CLIENTE");
                _tvLabelCnae.setVisibility(View.GONE);
                _tvCnae.setVisibility(View.GONE);
                _tvLabelOferta.setVisibility(View.GONE);
                _tvOferta.setVisibility(View.GONE);
                _tvLabelTelefone.setVisibility(View.GONE);
                _tvTelefone.setVisibility(View.GONE);
                _tvLabelCelular.setVisibility(View.GONE);
                _tvCelular.setVisibility(View.GONE);
                _tvPedido.setText(cliente.getIdCliente());
                _tvMotivo.setText(cliente.getMotivo());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
