package br.com.gestorflex.ui.activity;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;

import com.android.volley.Response;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.DBHelper;
import br.com.gestorflex.database.internal.controller.GenericCTRL;
import br.com.gestorflex.model.Agencia;
import br.com.gestorflex.model.Aparelho;
import br.com.gestorflex.model.Botao;
import br.com.gestorflex.model.CheckInOut;
import br.com.gestorflex.model.DDD;
import br.com.gestorflex.model.Estado;
import br.com.gestorflex.model.GrupoEconomico;
//import br.com.gestorflex.model.Jornada;
import br.com.gestorflex.model.ListarPor;
import br.com.gestorflex.model.MotivoRecusa;
import br.com.gestorflex.model.Notificacao;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.model.PDV;
import br.com.gestorflex.model.Pergunta;
import br.com.gestorflex.model.Pesquisa;
import br.com.gestorflex.model.Produto;
import br.com.gestorflex.model.Promotor;
import br.com.gestorflex.model.Regional;
import br.com.gestorflex.model.Resposta;
import br.com.gestorflex.model.SituacaoFuncional;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.model.UsuarioGerenciado;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.EmailUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;

public class AutenticacaoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PERMISSION_REQUEST_EXTERNAL_STORAGE = 6;
    public static final String URL_CONEXAO = "urlConexao";
    //views
    private Button _btnEnviarAutenticar;
    private ImageView _ivIconeApp;
    private TextInputEditText _tieCpfAutenticacao;
    private TextInputEditText _tieMatriculaAutenticacao;
    private RadioGroup _radiogroupOpcooesAutenticar;
    private AppCompatRadioButton _radioProdAutenticar;
    private AppCompatRadioButton _radioHomoAutenticar;
    private AppCompatRadioButton _radioDevAutenticar;
    private AppCompatSpinner _spEmailAutenticar;
    private AppCompatCheckBox _cb_termo_condicao_autenticar;
    private ProgressDialog _pdCarregando;
    //outras variáveis
    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private JSONObject jsonRetorno;

//    private Response.Listener<String> responseListenerDadosUsuario;
//    private Response.ErrorListener errorListenerDadosUsuario;

    private Usuario usuarioAutenticacao = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autenticacao);
        BottomBarUtil.configurar(this);
        inicializarObjetos();
        setFonts();
//        getPermissaoStorage();
        validarEmailPermissao();
        setSuporte();
        getPermissaoLocalizacao();
        setupVolleyListeners();
//        setupVolleyListenersDadosUsuario();
        String data = DateUtil.getCurrentDate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ErrorUtil.montarCaminhoErro();
        try {
            setSuporte();
            getPermissaoLocalizacao();
            GeolocalizacaoUtil geolocalizacaoUtil = new GeolocalizacaoUtil(this);
            if (!geolocalizacaoUtil.locationEnable())
                geolocalizacaoUtil.showSettingsAlert(this);
        } catch (Exception ignored) {
            ErrorUtil.enviarErroPorEmail(this, ignored);
        }
    }

    private void inicializarObjetos() {
        _btnEnviarAutenticar = findViewById(R.id.btn_enviar_autenticar);
        _ivIconeApp = findViewById(R.id.iv_icone_app_autenticar);
        _tieCpfAutenticacao = findViewById(R.id.tie_cpf_autenticacao);
        _tieMatriculaAutenticacao = findViewById(R.id.tie_matricula_autenticacao);
        _radiogroupOpcooesAutenticar = findViewById(R.id.radiogroup_opcooes_autenticar);
        _radioProdAutenticar = findViewById(R.id.radio_prod_autenticar);
        _radioHomoAutenticar = findViewById(R.id.radio_homo_autenticar);
        _radioDevAutenticar = findViewById(R.id.radio_dev_autenticar);
        _spEmailAutenticar = findViewById(R.id.sp_email_autenticar);
        _cb_termo_condicao_autenticar = findViewById(R.id.cb_termo_condicao_autenticar);
        _btnEnviarAutenticar.setOnClickListener(this);
        _cb_termo_condicao_autenticar.setOnClickListener(this);
        Notificacao.getTokenId();
    }

    private void setFonts() {
        Typeface typeface = ResourcesCompat.getFont(this, R.font.fontawesome);
        _btnEnviarAutenticar.setTypeface(typeface);
    }

    private void validarEmailPermissao() {
        ErrorUtil.montarCaminhoErro();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_CONTACTS}, 6);
                } else
                    coletarEmail();
            } else
                coletarEmail();
        } catch (Exception e) {
            e.printStackTrace();
            ErrorUtil.enviarErroPorEmail(this, e);
        }
    }

    private void coletarEmail() {
        List<String> arEmail = getEmails();
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, arEmail);
        arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        _spEmailAutenticar.setAdapter(arrayAdapter);
    }

    private List<String> getEmails() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(this).getAccounts();
        List<String> listaEmail = new ArrayList<>();
        listaEmail.add("Selecione");

        for (Account account : accounts) {
            String conta = account.name;
            if (emailPattern.matcher(conta).matches()) {
                if (conta.length() <= 100) {
                    int cont = 0;
                    for (int e = 0; e < listaEmail.size(); e++) {
                        String email = listaEmail.get(e);
                        if (email.equals(conta))
                            cont++;
                    }
                    if (cont == 0)
                        listaEmail.add(conta);
                }
            }
        }
        return listaEmail;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        ErrorUtil.montarCaminhoErro();
        if (requestCode == 6) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    coletarEmail();
                } catch (Exception e) {
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(this, e);
                }
            } else {
                try {
                    validarEmailPermissao();
                } catch (Exception e) {
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(this, e);
                }
            }
        }
    }

    public void getPermissaoLocalizacao() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 4);
                setSuporte();
            }
        }
    }

    private void setPreferencesUrl(String url) {
        SharedPreferences sharedPreferences = getSharedPreferences(URL_CONEXAO, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("url", url);
        editor.apply();
    }

    @NonNull
    private View.OnClickListener setUrlProducao() {
        return v -> {
            String url = getResources().getString(R.string.url_producao);
            setPreferencesUrl(url);
        };
    }

    @NonNull
    private View.OnClickListener setUrlDesenvolvimento() {
        return v -> {
            String url = getResources().getString(R.string.url_desenvolvimento);
            setPreferencesUrl(url);
        };
    }

    @NonNull
    private View.OnClickListener setUrlHomologacao() {
        return v -> {
            String url = getResources().getString(R.string.url_homologacao);
            setPreferencesUrl(url);
        };
    }

    public void concordarTermo() {
        boolean isChecked = _cb_termo_condicao_autenticar.isChecked();
        _btnEnviarAutenticar.setEnabled(isChecked);
        if (isChecked)
            _btnEnviarAutenticar.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        else
            _btnEnviarAutenticar.setTextColor(getResources().getColor(R.color.colorBloqueio));
    }

    public void setSuporte() {
        String iccid = "";
        String iccIds = getResources().getString(R.string.iccids);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 5);
            else
                iccid = Aparelho.getIccid(this);
        } else
            iccid = Aparelho.getIccid(this);
        if (!iccid.isEmpty()) {
            if (iccIds.contains(iccid)) {
                _radiogroupOpcooesAutenticar.setVisibility(View.VISIBLE);
                _radioProdAutenticar.setOnClickListener(setUrlProducao());
                _radioDevAutenticar.setOnClickListener(setUrlDesenvolvimento());
                _radioHomoAutenticar.setOnClickListener(setUrlHomologacao());
            }
        }
    }

    public void salvarPreferencesSenha(String senha) {
        SharedPreferences sharedPreferences = getSharedPreferences(EmailUtil.SENHA_EMAIL_SUPORTE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("senhaEmail", senha);
        editor.apply();
    }

    private void salvarBancoInterno(JSONObject jsonRetorno) throws JSONException {
        JSONArray arrayAgencia = jsonRetorno.getJSONArray("agencia");
        JSONArray arrayBotoes = jsonRetorno.getJSONArray("botoes");
        JSONArray arrayProdutos = jsonRetorno.getJSONArray("produto");
        //JSONArray arrayMotivoRecusa = jsonRetorno.getJSONArray("recusa");
        JSONArray arrayEstado = jsonRetorno.getJSONArray("uf");
        JSONArray arrayDdds = jsonRetorno.getJSONArray("ddd");
        JSONArray arrayGrupoEconomico = jsonRetorno.getJSONArray("grupo_economico");
        JSONArray arrayRegional = jsonRetorno.getJSONArray("regional");
//        JSONArray arrayUsuariosGerenciados = jsonRetorno.getJSONArray("usuarios_gerenciados");
        JSONArray arrayListarPor = jsonRetorno.getJSONArray("listarPOR");
        JSONArray arrayPdvs = jsonRetorno.getJSONArray("pdv");
        JSONArray arrayPromotor = jsonRetorno.getJSONArray("promotor");
//        JSONArray arrayCheckInOut = jsonRetorno.getJSONArray("check_in_out");

        JSONArray arraysituacaoFuncional = jsonRetorno.getJSONArray("situacaoFuncional");
        JSONArray arrayPesquisa = jsonRetorno.getJSONArray("pesquisa");
        JSONArray arrayPergunta = jsonRetorno.getJSONArray("pergunta");

        JSONArray arrayResposta = jsonRetorno.getJSONArray("resposta");
        JSONArray arrayOdometro = jsonRetorno.getJSONArray("odometro");
//        JSONArray arrayJornada = jsonRetorno.getJSONArray("jornada");

//        retorno,usuario,agencia,botoes,produto,uf,ddd,grupo_economico,regional,listarPOR,pdv,promotor,situacaoFuncional,tabs,pesquisa,pergunta,resposta,jornada


        for (int i = 0; i < arrayAgencia.length(); i++) {
            JSONObject jsonAgencia = arrayAgencia.getJSONObject(i);
            GenericCTRL.save(this, Agencia.TABELA, new Agencia(jsonAgencia).getContentValues());
        }

        for (int i = 0; i < arrayBotoes.length(); i++) {
            JSONObject jsonBotao = arrayBotoes.getJSONObject(i);
            GenericCTRL.save(this, Botao.TABELA, new Botao(jsonBotao).getContentValues());
        }

        for (int i=0; i < arrayProdutos.length(); i++){
            JSONObject jsonProduto = arrayProdutos.getJSONObject(i);
            GenericCTRL.save(this, Produto.TABELA, new Produto(jsonProduto).getContetValues());
        }

//        for (int i = 0; i < arrayMotivoRecusa.length(); i++) {
//            JSONObject jsonMotivoRecusa = arrayMotivoRecusa.getJSONObject(i);
//            GenericCTRL.save(this, MotivoRecusa.TABELA, new MotivoRecusa(jsonMotivoRecusa).getContentValues());
//        }


        for (int i = 0; i < arrayEstado.length(); i++) {
            JSONObject jsonEstado = arrayEstado.getJSONObject(i);
            GenericCTRL.save(this, Estado.TABELA, new Estado(jsonEstado).getContentValues());
        }

        for (int i = 0; i < arrayDdds.length(); i++) {
            JSONObject jsonDdd = arrayDdds.getJSONObject(i);
            GenericCTRL.save(this, DDD.TABELA, new DDD(jsonDdd).getContentValues());
        }

        for (int i = 0; i < arrayGrupoEconomico.length(); i++) {
            JSONObject jsonGrupoEconomico = arrayGrupoEconomico.getJSONObject(i);
            GenericCTRL.save(this, GrupoEconomico.TABELA, new GrupoEconomico(jsonGrupoEconomico).getContentValues());
        }

        for (int i = 0; i < arrayRegional.length(); i++) {
            JSONObject jsonRegional = arrayRegional.getJSONObject(i);
            GenericCTRL.save(this, Regional.TABELA, new Regional(jsonRegional).getContentValues());
        }

//        for (int i = 0; i < arrayUsuariosGerenciados.length(); i++) {
//            JSONObject jsonUsuariosGerenciados = arrayUsuariosGerenciados.getJSONObject(i);
//            GenericCTRL.save(this, UsuarioGerenciado.TABELA, new UsuarioGerenciado(jsonUsuariosGerenciados).getContentValues());
//        }
        for (int i = 0; i < arrayListarPor.length(); i++) {
            JSONObject jsonListarPor = arrayListarPor.getJSONObject(i);
            GenericCTRL.save(this, ListarPor.TABELA, new ListarPor(jsonListarPor).getContentValues());
        }

        for (int i = 0; i < arrayPdvs.length(); i++) {
            JSONObject jsonPdv = arrayPdvs.getJSONObject(i);
            GenericCTRL.save(this, PDV.TABELA, new PDV(jsonPdv).getContentValues());
        }


//        for (int i = 0; i < arrayCheckInOut.length(); i++) {
//            JSONObject jsonCheckInOut = arrayCheckInOut.getJSONObject(i);
//            GenericCTRL.save(this, CheckInOut.TABELA, new CheckInOut(jsonCheckInOut).getContentValues());
//        }

        for (int i = 0; i < arrayPromotor.length(); i++) {
            JSONObject jsonPromotor = arrayPromotor.getJSONObject(i);
            GenericCTRL.save(this, Promotor.TABELA, new Promotor(jsonPromotor).getContentValues());
        }


        for (int i = 0; i < arraysituacaoFuncional.length(); i++) {
            JSONObject jsonsituacaoFuncional = arraysituacaoFuncional.getJSONObject(i);
            GenericCTRL.save(this, SituacaoFuncional.TABELA, new SituacaoFuncional(jsonsituacaoFuncional).getContentValues());
        }

        for (int i = 0; i < arrayPesquisa.length(); i++) {
            JSONObject jsonPesquisa = arrayPesquisa.getJSONObject(i);
            GenericCTRL.save(this, Pesquisa.TABELA, new Pesquisa(jsonPesquisa).getContentValues());
        }

        for (int i = 0; i < arrayPergunta.length(); i++) {
            JSONObject jsonPergunta = arrayPergunta.getJSONObject(i);
            GenericCTRL.save(this, Pergunta.TABELA, new Pergunta(jsonPergunta).getContentValues());
        }
        for (int i = 0; i < arrayResposta.length(); i++) {
            JSONObject jsonResposta = arrayResposta.getJSONObject(i);
            GenericCTRL.save(this, Resposta.TABELA, new Resposta(jsonResposta).getContentValues());
        }


        for (int i = 0; i < arrayOdometro.length(); i++) {
            JSONObject jsonOdometro = arrayOdometro.getJSONObject(i);
            GenericCTRL.save(this, Odometro.TABELA, new Odometro(jsonOdometro).getContentValues());
        }

//        for (int i = 0; i < arrayJornada.length(); i++) {
//            JSONObject jsonJornada = arrayJornada.getJSONObject(i);
//            GenericCTRL.save(this, Jornada.TABELA, new Jornada(jsonJornada).getContentValues());
//        }


    }



    private void autenticar() throws Exception {
        ErrorUtil.montarCaminhoErro();
        _pdCarregando.show();
        usuarioAutenticacao = new Usuario();
        usuarioAutenticacao.setCpf(_tieCpfAutenticacao.getText().toString());
        usuarioAutenticacao.setMatricula(Integer.parseInt(_tieMatriculaAutenticacao.getText().toString()));
        usuarioAutenticacao.setEmail((String) _spEmailAutenticar.getSelectedItem());
        usuarioAutenticacao.setIdNotify(Notificacao.tokenId);
        JSONObject logParams = usuarioAutenticacao.getLogUsuario();
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_autenticar), responseListener, errorListener, null);
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        _pdCarregando = new ProgressDialog(this);
        _pdCarregando.setIndeterminate(true);
        _pdCarregando.setMessage(getString(R.string.message_progress_realizando_autenticacao));
        _pdCarregando.setCancelable(false);
        responseListener = response -> {
            try {
                jsonRetorno = new JSONObject(response);
                JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                if (jsonResposta.optInt(ExternalDB.COD_RETORNO) != ExternalDB.CD_RETORNO_SUCESSO) {
                    _pdCarregando.dismiss();
                    new AlertDialog.Builder(AutenticacaoActivity.this).setTitle(R.string.title_alert_aviso)
                            .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
                } else
                    new TaskSalvarDados().execute();
            } catch (Exception e) {
                _pdCarregando.dismiss();
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(AutenticacaoActivity.this, e);
            }
        };
        errorListener = error -> {
            _pdCarregando.dismiss();
            Snackbar.make(_btnEnviarAutenticar, R.string.message_alert_erro_servidor, Snackbar.LENGTH_LONG).show();
        };
    }

//    private void dadosUsuario() throws Exception {
//        ErrorUtil.montarCaminhoErro();
//        _pdCarregando.show();
//
//        usuarioAutenticacao.setEmail((String) _spEmailAutenticar.getSelectedItem());
//
//        JSONObject logParams = usuarioAutenticacao.getLogUsuario();
//        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_dados_usuario), responseListenerDadosUsuario, errorListenerDadosUsuario, null);
//    }



//    private void setupVolleyListenersDadosUsuario() {
//        ErrorUtil.montarCaminhoErro();
//        _pdCarregando = new ProgressDialog(this);
//        _pdCarregando.setIndeterminate(true);
//        _pdCarregando.setMessage(getString(R.string.message_progress_realizando_autenticacao));
//        _pdCarregando.setCancelable(false);
//        responseListenerDadosUsuario = response -> {
//            try {
//                jsonRetorno = new JSONObject(response);
//                JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
//                JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
//                if (jsonResposta.optInt(ExternalDB.COD_RETORNO) != ExternalDB.CD_RETORNO_SUCESSO) {
//                    _pdCarregando.dismiss();
//                    new AlertDialog.Builder(AutenticacaoActivity.this).setTitle(R.string.title_alert_aviso)
//                            .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
//                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
//                } else
//                    new TaskSalvarDados().execute();
//            } catch (Exception e) {
//                _pdCarregando.dismiss();
//                e.printStackTrace();
//                ErrorUtil.enviarErroPorEmail(AutenticacaoActivity.this, e);
//            }
//        };
//        errorListenerDadosUsuario = error -> {
//            _pdCarregando.dismiss();
//            Snackbar.make(_btnEnviarAutenticar, R.string.message_alert_erro_servidor, Snackbar.LENGTH_LONG).show();
//        };
//    }

    @Override
    public void onClick(View view) {
        ErrorUtil.montarCaminhoErro();
        if (view.getId() == R.id.btn_enviar_autenticar) {
            try {
                if (_spEmailAutenticar.getSelectedItemPosition() != 0)
                    autenticar();
//                    dadosUsuario();}
                else
                    new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_email_invalido)
                            .setPositiveButton(R.string.label_alert_button_ok, null).show();
            } catch (Exception e) {
                e.printStackTrace();
                ErrorUtil.enviarErroPorEmail(this, e);
            }
        }
        if (view.getId() == R.id.cb_termo_condicao_autenticar) {
            concordarTermo();
        }
    }

    // SALVAR DADOS EM BACKGROUND

    private class TaskSalvarDados extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            _pdCarregando.setMessage(getString(R.string.message_progress_configurando_app));
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONArray jArrayUsuario = jsonRetorno.getJSONArray("usuario");
                JSONObject jsonUsuario = jArrayUsuario.getJSONObject(0);
                usuarioAutenticacao = new Usuario(jsonUsuario);
                usuarioAutenticacao.setIccidFuncional(Aparelho.getIccid(AutenticacaoActivity.this));
                GenericCTRL.save(AutenticacaoActivity.this, Usuario.TABELA, usuarioAutenticacao.getContetValues());
                salvarPreferencesSenha(jsonUsuario.optString("senhaEmail"));
                salvarBancoInterno(jsonRetorno);
                return "sucesso";
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "erro";
        }

        @Override
        protected void onPostExecute(String s) {
            _pdCarregando.dismiss();
            if (s.equals("sucesso")) {
                Intent intent = new Intent(AutenticacaoActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            } else {
                DBHelper.clearDatabase(AutenticacaoActivity.this);
                new AlertDialog.Builder(AutenticacaoActivity.this).setTitle(R.string.title_alert_aviso)
                        .setMessage(R.string.message_alert_erro_configura_app)
                        .setPositiveButton(R.string.label_alert_button_ok, null).show();
            }
            super.onPostExecute(s);
        }
    }
}
