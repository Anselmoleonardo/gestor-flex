package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.ui.holder.SincJornadaHoder;

public class SincJornadaAdapter extends RecyclerView.Adapter<SincJornadaHoder> {

    private Context context;
    private List<Odometro> odometros;
    private OnItemClickListener recyclerViewOnClickListenerHack;

    public SincJornadaAdapter(Context context, List<Odometro> odometros) {
        this.context = context;
        this.odometros = odometros;
    }

    public void setRecyclerViewOnClickListenerHack(OnItemClickListener recyclerViewOnClickListenerHack) {
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
    }

    @NonNull
    @Override
    public SincJornadaHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SincJornadaHoder(LayoutInflater.from(context).inflate(R.layout.item_sinc_odometro,
                parent, false), this.recyclerViewOnClickListenerHack, odometros);
    }

    @Override
    public void onBindViewHolder(@NonNull SincJornadaHoder sincJornadaHoder, int position) {
        sincJornadaHoder.bindOdometro(odometros.get(position));
    }

    @Override
    public int getItemCount() {
        return odometros != null ? odometros.size() : 0;
    }

    public void insertItem(Odometro odometro) {
        odometros.add(odometro);
        notifyItemInserted(getItemCount());
    }

    public void removeItem(int position) {
        odometros.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    public List<Odometro> getVendas() {
        return odometros;
    }
}

