package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.AgenciaCTRL;
import br.com.gestorflex.database.internal.controller.RegionalCTRL;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Agencia;
import br.com.gestorflex.model.DDD;
import br.com.gestorflex.model.Estado;
import br.com.gestorflex.model.ListarPor;
import br.com.gestorflex.model.Ponto;
import br.com.gestorflex.model.Produto;
import br.com.gestorflex.model.Ranking;
import br.com.gestorflex.model.Regional;
import br.com.gestorflex.model.RetornoPonto;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;

public class PontoActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private Toolbar _tbPontoFiltro;
    private AppCompatSpinner _spRegional;
    private AppCompatSpinner _spParceiro;
    private RadioGroup _rgPonto;
    private AppCompatRadioButton _rbComRegistro;
    private AppCompatRadioButton _rbSemRegistro;
    private AppCompatSpinner _spDia;
    private AppCompatSpinner _spMes;
    private AppCompatSpinner _spAno;

    private FloatingActionButton _fabPesquisar;
    private boolean isRegistro = true;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    Ponto ponto = new Ponto();
    private Usuario usuario;
    private String dataInicial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ponto);
        BottomBarUtil.configurar(this);
        instanciarViews();
    }

    private void instanciarViews() {
        _tbPontoFiltro = findViewById(R.id.toolbar);
        _spParceiro = findViewById(R.id.sp_filtrar_ponto_parceiro);
        _spRegional = findViewById(R.id.sp_filtrar_ponto_regional);
        _rgPonto = findViewById(R.id.radiogroup_opcooes_ponto);
        _rbComRegistro = findViewById(R.id.rb_ponto_com_registro);
        _rbSemRegistro = findViewById(R.id.rb_ponto_sem_registro);
        _spDia = findViewById(R.id.sp_filtrar_ponto_dia_inicial);
        _spMes = findViewById(R.id.sp_filtrar_ponto_mes);
        _spAno = findViewById(R.id.sp_filtrar_ponto_ano);
        _fabPesquisar = findViewById(R.id.fab_filtrar_ponto_pesquisar);
        configurarViews();
    }

    private void configurarViews() {
        usuario = UsuarioCTRL.getUsuario(this);
        _tbPontoFiltro.setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
        _tbPontoFiltro.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_tbPontoFiltro);

        preencherAgencia();
        preencherRegional();
        preencherSpinnerAno();
        preencherSpinnerMes();

        setupVolleyListeners();
        _fabPesquisar.setOnClickListener(this);
    }


    public void preencherAgencia() {
        List<Agencia> listAgencias = AgenciaCTRL.listAgencias(this);
        if (listAgencias.size() > 1)
            listAgencias.add(0, new Agencia(0, "TODOS"));
        ArrayAdapter<Agencia> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listAgencias);
        _spParceiro.setAdapter(dataAdapter);
    }

    public void preencherRegional() {
        List<Regional> listRegional = RegionalCTRL.listRegional(this);
        if (listRegional.size() > 1)
            listRegional.add(0, new Regional(0, "TODOS", 0));
        ArrayAdapter<Regional> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, listRegional);
        _spRegional.setAdapter(dataAdapter);
    }

    public void preencherSpinnerDia(int ano, int mes) {
        List<String> dias = DateUtil.getTodosDiasDoMes(ano, mes);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, dias);
        _spDia.setAdapter(dataAdapter);

//        _spDiaInicial.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
//        _spDiaFinal.setSelection(Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_MONTH) - 1);
    }

    public void preencherSpinnerMes() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, DateUtil.getTodosMeses());
        _spMes.setAdapter(dataAdapter);
        Calendar calendarMes = Calendar.getInstance(TimeZone.getDefault());
        _spMes.setSelection(calendarMes.get(Calendar.MONTH));
        _spMes.setOnItemSelectedListener(this);
    }

    public void preencherSpinnerAno() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String[] gruposEcon = {(year - 1) + "", year + "", (year + 1) + ""};
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, gruposEcon);
        _spAno.setAdapter(dataAdapter);
        _spAno.setSelection(1);
        _spAno.setOnItemSelectedListener(this);
    }

    public boolean validarDatasSelecionadas() {
        String diaInicial = (String) _spDia.getSelectedItem();

        String mes = (String) _spMes.getSelectedItem();
        String ano = (String) _spAno.getSelectedItem();

        dataInicial = diaInicial + "/" + mes + "/" + ano;

        if (!DateUtil.validateDate(dataInicial)) {
            new AlertDialog.Builder(this).setTitle(R.string.title_alert_aviso).setMessage(R.string.message_alert_dia_atual_invalido)
                    .setPositiveButton(R.string.label_alert_button_ok, null).show();
            return false;
        }

        return true;
    }

//    public int getOpcaoPeriodo() {
//        if (_rbDiario.isChecked()) {
//            return 0;
//        } else if (_rbMensal.isChecked()) {
//            return 1;
//        }
//        return 2;
//    }

    public void recuperarPonto() throws Exception {
        if (!validarDatasSelecionadas()) return;
        progressDialog.show();

//        Ponto ponto = new Ponto();

        ponto.setIdParceiro(((Regional) _spRegional.getSelectedItem()).getIdAgencia());
        ponto.setCdRegional(((Regional) _spRegional.getSelectedItem()).getId());
        ponto.setDataAlternada(DateUtil.converterDataPadraoUs(dataInicial));
        ponto.setData(dataInicial);
        ponto.setComRegistro(isRegistro);


        JSONObject logParams = ponto.pegarJson();
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_ponto), responseListener, errorListener, null);
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_buscando_ponto));
        progressDialog.setCancelable(false);
        responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                        JSONArray jArrayPonto = jsonRetorno.getJSONArray("ponto");
                        if (jArrayPonto.length() > 0) {

                            List<RetornoPonto> retornoPonto = new ArrayList<>();
                            for (int i = 0; i < jArrayPonto.length(); i++) {
                                retornoPonto.add(new RetornoPonto(jArrayPonto.getJSONObject(i)));
                            }

                            Intent intent = new Intent(PontoActivity.this, PontoResultadoActivity.class);

                            intent.putExtra("retornoPonto", (Serializable)retornoPonto );
                            intent.putExtra("ponto",(Serializable) ponto );
                            intent.putExtra("isComRegistro", isRegistro);
//                            intent.putExtra("dataAlternada", DateUtil.converterDataPadraoUs(dataInicial));
//                            intent.putExtra("dataReal", dataInicial);
                            startActivity(intent);
                        } else {
                            new AlertDialog.Builder(PontoActivity.this)
                                    .setTitle(R.string.title_alert_aviso)
                                    .setMessage(R.string.message_alert_sem_resultados)
                                    .setPositiveButton(R.string.label_alert_button_ok, null)
                                    .show();
                        }
                    } else {
                        new AlertDialog.Builder(PontoActivity.this)
                                .setTitle(R.string.title_alert_aviso)
                                .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                                .setPositiveButton(R.string.label_alert_button_ok, null)
                                .show();
                    }
                    progressDialog.dismiss();
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(PontoActivity.this, e);
                }
            }
        };
        errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(PontoActivity.this, "Erro ao conectar ao servidor", Toast.LENGTH_LONG).show();
            }
        };
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.rb_ponto_com_registro:
                if (checked)
                    isRegistro = true;

                    break;
            case R.id.rb_ponto_sem_registro:
                if (checked)
                    isRegistro = false;

                    break;
        }
    }

    @Override
    public void onClick(View v) {
        try {
            recuperarPonto();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {

            default:
                preencherSpinnerDia(Integer.parseInt((String) _spAno.getSelectedItem()),
                        Integer.parseInt((String) _spMes.getSelectedItem()));
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
