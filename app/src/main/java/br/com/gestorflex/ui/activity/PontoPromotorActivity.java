package br.com.gestorflex.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.database.external.ExternalDB;
import br.com.gestorflex.database.internal.controller.UsuarioCTRL;
import br.com.gestorflex.model.Ponto;
import br.com.gestorflex.model.Regional;
import br.com.gestorflex.model.RetornoPonto;
import br.com.gestorflex.model.Usuario;
import br.com.gestorflex.util.BottomBarUtil;
import br.com.gestorflex.util.DateUtil;
import br.com.gestorflex.util.ErrorUtil;
import br.com.gestorflex.util.GeolocalizacaoUtil;

public class PontoPromotorActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, View.OnClickListener, AdapterView.OnItemSelectedListener {

    private GoogleMap mapa_ponto;
    private SupportMapFragment mapFragment;

    private AppCompatTextView _tvData;
    private AppCompatTextView _tvNome;
    private AppCompatTextView _tvPDV;
    private AppCompatTextView _tvMatricula;
    private FloatingActionButton _fabPosicaoAtual;
    private FloatingActionButton _fabTelefone;

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private ProgressDialog progressDialog;

    private GeolocalizacaoUtil geolocalizacaoUtil;

    private boolean isRegistro;
    private String data;
    private String dataAlternada;
    private int idPromotor;
    private int idPDV;

    private String nome;
    private String telefone;
    private String nomePDV;
    private int isBlindado;
    private LatLng latLngPDV = null;
    double latitudePDV;
    double longitudePDV;

    private Circle circle;
    private Marker markerPDV;
    private Marker Entrada;
    private Marker Saida;
    private Marker entrada;
    private Marker saida;

    RetornoPonto retornoPonto;
    Ponto ponto;
    private Toolbar _tbPonto;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ponto_promotor);
        BottomBarUtil.configurar(this);

        instanciarViews();
    }

    private void instanciarViews() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        _tbPonto = findViewById(R.id.toolbar);
        _fabPosicaoAtual = findViewById(R.id.fab_ponto_posicao_atual);
        _fabTelefone = findViewById(R.id.fab_ponto_ligar);
        _tvData = findViewById(R.id.tv_ponto_data);
        _tvNome = findViewById(R.id.tv_ponto_nome);
        _tvPDV = findViewById(R.id.tv_ponto_pdv);
        _tvMatricula = findViewById(R.id.tv_ponto_matricula);
        configurarViews();
    }

    private void configurarViews() {

        usuario = UsuarioCTRL.getUsuario(this);
        _tbPonto.setTitle(getIntent().getStringExtra(MenuActivity.TAG_TITLE));
        _tbPonto.setSubtitle(usuario.getMatricula() + " - " + usuario.getNome());
        setSupportActionBar(_tbPonto);
        _fabPosicaoAtual.setOnClickListener(this);

        isRegistro = getIntent().getBooleanExtra("isComRegistro", true);
//        data = getIntent().getStringExtra("dataReal");
//        dataAlternada = getIntent().getStringExtra("dataAlternada");
        idPromotor = getIntent().getIntExtra("promotor", 0);
        idPDV = getIntent().getIntExtra("PDV", 0);
        ponto = (Ponto) getIntent().getSerializableExtra("ponto");
        retornoPonto = (RetornoPonto) getIntent().getSerializableExtra("retornoPonto");

        _tvNome.setText(retornoPonto.getNome());
        _tvMatricula.setText(String.valueOf(retornoPonto.getMatricula()));
        _tvPDV.setText(retornoPonto.getNomePDV());
        _tvData.setText(ponto.getData());

        ponto.setPromotor(retornoPonto.getIdPromotor());
        ponto.setPdv(retornoPonto.getPDV());


    }

    public void criarCirculo() {
        if (circle != null)
            circle.remove();
        circle = mapa_ponto.addCircle(new CircleOptions().center(markerPDV.getPosition()).radius(80)
                .fillColor(Color.argb(50, 255, 0, 0)).strokeWidth(0));
    }


    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa_ponto = googleMap;
        mapa_ponto.setInfoWindowAdapter(this);
        addMarkerPDV();
    }

    private void addMarkerPDV() {
        MapsInitializer.initialize(getApplicationContext());

    }


    public void recuperarPonto() throws Exception {


        JSONObject logParams = ponto.pegarJson();
        ExternalDB.addDbRequest(this, logParams, getString(R.string.procedure_ponto_promotor), responseListener, errorListener, null);
    }

    private void setupVolleyListeners() {
        ErrorUtil.montarCaminhoErro();
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.message_progress_buscando_ponto));
        progressDialog.setCancelable(false);
        responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonRetorno = new JSONObject(response);
                    JSONArray jArrayResposta = jsonRetorno.getJSONArray(ExternalDB.RETORNO);
                    JSONObject jsonResposta = jArrayResposta.getJSONObject(0);
                    if (jsonResposta.optInt(ExternalDB.COD_RETORNO) == ExternalDB.CD_RETORNO_SUCESSO) {
                        JSONArray jArrayPonto = jsonRetorno.getJSONArray("ponto");
                        if (jArrayPonto.length() > 0) {

                            JSONObject jsonPonto = jArrayPonto.getJSONObject(0);

                            nome = jsonPonto.getString("NOME");
                            telefone = jsonPonto.getString("TELEFONE");

                            int matricula = jsonPonto.getInt("MATRICULA");
                            nomePDV = jsonPonto.getString("NOME_PDV");
                            isBlindado = jsonPonto.getInt("ISBLINDADO");
                            latitudePDV = Double.parseDouble(jsonPonto.getString("LATITUDE_PDV"));
                            longitudePDV = Double.parseDouble(jsonPonto.getString("LONGITUDE_PDV"));


                        } else {
                            new AlertDialog.Builder(PontoPromotorActivity.this)
                                    .setTitle(R.string.title_alert_aviso)
                                    .setMessage(R.string.message_alert_sem_resultados)
                                    .setPositiveButton(R.string.label_alert_button_ok, null)
                                    .show();
                        }
                        JSONArray jArrayRegistro = jsonRetorno.getJSONArray("registro");


                    } else {
                        new AlertDialog.Builder(PontoPromotorActivity.this)
                                .setTitle(R.string.title_alert_aviso)
                                .setMessage(jsonResposta.optString(ExternalDB.MSG_RETORNO))
                                .setPositiveButton(R.string.label_alert_button_ok, null)
                                .show();
                    }
                    progressDialog.dismiss();
                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    ErrorUtil.enviarErroPorEmail(PontoPromotorActivity.this, e);
                }
            }
        };
        errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(PontoPromotorActivity.this, "Erro ao conectar ao servidor", Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

    }
}
