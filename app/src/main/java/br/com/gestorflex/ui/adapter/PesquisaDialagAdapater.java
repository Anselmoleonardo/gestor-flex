package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.interfaces.OnItemClickListener;
import br.com.gestorflex.ui.holder.PesquisaDialagHolder;

public class PesquisaDialagAdapater  extends   RecyclerView.Adapter<PesquisaDialagHolder>{

    private Context context;
    private int qtdVendas;
    private int validacao;
    private OnItemClickListener recyclerViewOnClickListenerHack;
    private List<TextInputLayout> list;


    public PesquisaDialagAdapater(Context context, int qtdVendas, int validacao) {
        this.context = context;
        this.qtdVendas = qtdVendas;
        this.validacao = validacao;
        list = new ArrayList<>();
    }
    public void setRecyclerViewOnClickListenerHack (OnItemClickListener recyclerViewOnClickListenerHack) {
        this.recyclerViewOnClickListenerHack = recyclerViewOnClickListenerHack;
    }

    @NonNull
    @Override
    public PesquisaDialagHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PesquisaDialagHolder(LayoutInflater.from(context).inflate(R.layout.item_pesquisa_dialag,
                parent, false), this.recyclerViewOnClickListenerHack, qtdVendas);
    }

    @Override
    public void onBindViewHolder(@NonNull PesquisaDialagHolder pesquisaDialagHolder, int i) {
        pesquisaDialagHolder.bindPesquisaDialag();
        list = pesquisaDialagHolder.getList();

    }

    public List<TextInputLayout> getList() {
        return list;
    }

    public void setList(List<TextInputLayout> list) {
        this.list = list;
    }

    public int getValidacao() {
        return validacao;
    }

    @Override
    public int getItemCount() {
        return qtdVendas ;
    }
}
