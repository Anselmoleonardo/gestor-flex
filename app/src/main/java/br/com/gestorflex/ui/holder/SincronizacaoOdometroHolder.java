package br.com.gestorflex.ui.holder;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.gestorflex.R;
import br.com.gestorflex.model.Odometro;
import br.com.gestorflex.util.DateUtil;


public class SincronizacaoOdometroHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    // Valores
    private AppCompatTextView _tvDescricao;
    private AppCompatTextView _tvTipo;

    public SincronizacaoOdometroHolder(@NonNull View itemView) {
        super(itemView);
        _tvDescricao = itemView.findViewById(R.id.tv_item_rota_sincronizacao_descricao);
        _tvTipo = itemView.findViewById(R.id.tv_item_rota_sincronizacao_tipo);
        itemView.setOnClickListener(this);
    }

    public void bindOdometro(Context context, Odometro odometro) {
        String tipo = odometro.getAtividade() == Odometro.INICIAR_JORNADA ?
                context.getString(R.string.text_iniciar_jornada) : context.getString(R.string.text_finalizar_jornada);
        _tvDescricao.setText(tipo);
        _tvTipo.setText(DateUtil.converterDataPadraoUs(odometro.getDataEnvio()));
    }

    @Override
    public void onClick(View v) {
    }

}

