package br.com.gestorflex.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.gestorflex.R;
import br.com.gestorflex.model.UsuarioOfensor;
import br.com.gestorflex.ui.holder.UsuarioOfensorHolder;

public class UsuarioOfensorAdapter extends RecyclerView.Adapter<UsuarioOfensorHolder> {

    private List<UsuarioOfensor> usuarioOfensors;
    private Context context;

    public UsuarioOfensorAdapter(Context context, List<UsuarioOfensor> listaVendas) {
        this.usuarioOfensors = new ArrayList<>(listaVendas);
        this.context = context;
    }

    @NonNull
    @Override
    public UsuarioOfensorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UsuarioOfensorHolder(context, LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_usuario_ofensor, parent, false));
    }

    @Override
    public void onBindViewHolder(UsuarioOfensorHolder holder, int position) {
        UsuarioOfensor usuarioOfensor = usuarioOfensors.get(position);
        holder.bindUsuarioOfensor(usuarioOfensor);
    }

    public void insertItem(UsuarioOfensor usuarioOfensor) {
        usuarioOfensors.add(usuarioOfensor);
        notifyItemInserted(getItemCount());
    }

    public void removeItem(int position) {
        usuarioOfensors.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, usuarioOfensors.size());
    }

    @Override
    public int getItemCount() {
        return usuarioOfensors != null ? usuarioOfensors.size() : 0;
    }

}
